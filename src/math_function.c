/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: math_function.c
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>

double arithmetic_mean(double x1, double x2, double p) {
  double x_mean;
  x_mean = p * x1 + (1 - p) * x2;
  return x_mean;
}

double geometric_mean(double x1, double x2, double p) {
  double x_mean;
  x_mean = pow(x1, p) * pow(x2, 1 - p);
  return x_mean;
}

double harmonic_mean(double x1, double x2, double p) {
  double x_mean;
  x_mean = 1 / (p / x1 + (1 - p) / x2);
  return x_mean;
}

// double quadratic_mean(double x1,double x2,double p)
//{
//   double x_mean;
//   x_mean=sqrt(p*pow(x1,2)+(1-p)*pow(x2,2));
//   return x_mean;
// }

double max(double a, double b) {
  if (a < b)
    a = b;
  return a;
}
