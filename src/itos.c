/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: itos.c
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include "libprint.h"
#include "time_series.h"
#include "GC.h"
#include "CHR.h"
#include "IO.h"

#include "TTC.h"

char *TTC_name_equa_type(int iname) {

  char *name;

  switch (iname) {
  case HEAT_TTC:
    name = strdup("HEAT");
    break;
  case SOLUTE_TTC:
    name = strdup("SOLUTE");
    break;
  }
  return name;
}

char *TTC_name_process(int iname) {

  char *name;

  switch (iname) {
  case CONVECTION_TTC:
    name = strdup("CONVECTION");
    break;
  case DIFFUSION_TTC:
    name = strdup("DIFFUSION");
    break;
  }
  return name;
}

char *TTC_name_regime(int iname) {

  char *name;

  switch (iname) {
  case STEADY_TTC:
    name = strdup("STEADY");
    break;
  case TRANSIENT_TTC:
    name = strdup("TRANSIENT");
    break;
  }
  return name;
}

char *TTC_mean_type(int iname) {

  char *name;

  switch (iname) {
  case ARITHMETIC_TTC:
    name = strdup("ARITHMETIC");
    break;
  case HARMONIC_TTC:
    name = strdup("HARMONIC");
    break;
  case GEOMETRIC_TTC:
    name = strdup("GEOMETRIC");
    break;
  case QUADRATIC_TTC:
    name = strdup("QUADRATIC");
    break;
  }
  return name;
}

char *TTC_name_freq(int iname) {

  char *name;

  switch (iname) {
  case CONSTANT_TTC:
    name = strdup("CONSTANT");
    break;
  case VARIABLE_TTC:
    name = strdup("VARIABLE");
    break;
  }
  return name;
}

char *TTC_coeff(int iname) {

  char *name;

  switch (iname) {
  case DISP_TTC:
    name = strdup("D");
    break;
  case S_TTC:
    name = strdup("S");
    break;
  }
  return name;
}

char *TTC_name_bound(int iname) {

  char *name;

  switch (iname) {
  case DIRI_CENTER_TTC:
    name = strdup("DIRI_CENTER");
    break;
  case DIRI_FACE_TTC:
    name = strdup("DIRI_FACE");
    break;
  case NEU_FACE_TTC:
    name = strdup("NEU_FACE");
    break;
  case NEU_CENTER_TTC:
    name = strdup("NEU_CENTER");
    break;
  case NO_BOUND_TTC:
    name = strdup("NO_BOUND");
    break;
  case CONF_DIFF_TTC:
    name = strdup("CONF_DIFF");
    break;
  case BARRAGE_TTC:
    name = strdup("BARRAGE");
    break;
  }
  return name;
}

char *TTC_count(int iname) {

  char *name;

  switch (iname) {
  case NELE_TTC:
    name = strdup("NELE");
    break;
  case NVOIS_ELE_TTC:
    name = strdup("NVOIS_ELE");
    break;
  case NBC_TTC:
    name = strdup("NBC");
    break;
  case NSPECIES_TTC:
    name = strdup("NSPECIES");
    break;
  case NAQUITARD_TTC:
    name = strdup("NAQUITARD");
    break;
  case NRIVERBED_TTC:
    name = strdup("NRIVERBED");
    break;
  }
  return name;
}

char *TTC_general_param(int iname) {

  char *name;

  switch (iname) {
  case THETA_TTC:
    name = strdup("THETA");
    break;
  case EPS_Q_TTC:
    name = strdup("EPS_Q");
    break;
  }
  return name;
}

char *TTC_output_type(int iname) {

  char *name;

  switch (iname) {
  case VAR_TTC:
    name = strdup("VAR");
    break;
  case FLUX_TTC:
    name = strdup("FLUX");
    break;
  }
  return name;
}

char *TTC_thermic_param(int iname) {

  char *name;

  switch (iname) {
  case LAMBDA_TTC:
    name = strdup("LAMBDA");
    break;
  case HEAT_CAP_TTC:
    name = strdup("HEAT_CAP");
    break;
  case RHO_TTC:
    name = strdup("RHO");
    break;
  }
  return name;
}

char *TTC_param_card(int iname) {

  char *name;

  switch (iname) {
  case EAST_TTC:
    name = strdup("EAST");
    break;
  case WEST_TTC:
    name = strdup("WEST");
    break;
  case NORTH_TTC:
    name = strdup("NORTH");
    break;
  case SOUTH_TTC:
    name = strdup("SOUTH");
    break;
  case TOP_TTC:
    name = strdup("UPPER");
    break;
  case BOTTOM_TTC:
    name = strdup("LOWER");
    break;
  }
  return name;
}

char *TTC_param_card_river(int iname) {

  char *name;

  switch (iname) {
  case DOWNSTREAM_TTC:
    name = strdup("DOWNSTREAM");
    break;
  case UPSTREAM_TTC:
    name = strdup("UPSTREAM");
    break;
  case BANKS_TTC:
    name = strdup("BANKS");
    break;
  case ATMOS_TTC:
    name = strdup("ATMOSPHERE");
    break;
  case RIVAQ_TTC:
    name = strdup("RIVERBED");
    break;
  }
  return name;
}

char *TTC_environnement(int iname) {
  char *name;
  switch (iname) {
  case WATER_TTC:
    name = strdup("FREE WATER");
    break;
  case SOLID_TTC:
    name = strdup("SOLID");
    break;
  case POROUS_TTC:
    name = strdup("POROUS");
    break;
  }
  return name;
}

char *TTC_param_syst(int iname) {

  char *name;

  switch (iname) {
  case DISPERSIVITY_TTC:
    name = strdup("DISPERSIVITY");
    break;
  case POROSITY_TTC:
    name = strdup("POROSITY");
    break;
  case SIZE_TTC:
    name = strdup("SIZE");
    break;
  }
  return name;
}

char *TTC_kindof_bound(int iname) {

  char *name;

  switch (iname) {
  case HOMOGENOUS_TTC:
    name = strdup("HOMOGENOUS");
    break;
  case HETEROGENOUS_TTC:
    name = strdup("HETEROGENOUS");
    break;
  }
  return name;
}

char *TTC_name_source(int iname) {

  char *name;

  switch (iname) {
  case SOURCE_TEMPERATURE_TTC:
    name = strdup("Temperature");
    break;
  case SOURCE_HEAT_TTC:
    name = strdup("Heat");
    break;
  case SOURCE_CONCENTRATION_TTC:
    name = strdup("Concentration");
    break;
  case SOURCE_INJECTION_TTC:
    name = strdup("Injection");
    break;
  case SOURCE_GEOTHERMAL_TTC:
    name = strdup("Geothermal");
    break;
  default: {
    name = strdup("Not a source");
    break;
  }
  }
  return name;
}

char *TTC_param_aquitard(int iname) {

  char *name;

  switch (iname) {
  case LAMBDA_AQ_TTC:
    name = strdup("Aquitard Lambda");
    break;
  case THICKNESS_AQUITARD:
    name = strdup("Aquitard Thickness");
    break;
  default: {
    name = strdup("Not an aquitard parameter");
    break;
  }
  }
  return name;
}

/* NG : 21/06/2023 : Beware, YES and NO TTC param enumeration order is not consistent with LP param enum.
Hence the addition of the function below. */
char *TTC_answer(int iname) {

  char *answ;

  switch (iname) {
  case NO_TTC:
    answ = strdup("NO");
    break;
  case YES_TTC:
    answ = strdup("YES");
    break;
  }
  return answ;
}
