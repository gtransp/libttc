/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: manage_param_aquitard.c
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "CHR.h"
#include "GC.h"
#include "IO.h"

#include "TTC.h"
// DK AR 23 08 2021 new structure to allocate should be inserted here
// allocation de la structure param_base_ttc

/**\fn s_aquitard_ttc **TTC_init_aquitard(int nele)
 *\brief Initialize the aquitard structure by allocating it in each element inside p_species
 */
s_aquitard_ttc **TTC_init_aquitard(int naquitard) {

  s_aquitard_ttc **p_aquitard_ttc;

  p_aquitard_ttc = ((s_aquitard_ttc **)malloc(naquitard * sizeof(s_aquitard_ttc *)));
  for (int i = 0; i < naquitard; i++) {
    p_aquitard_ttc[i] = new_aquitard_ttc();
  }

  return p_aquitard_ttc;
}

/**\fn void Calc_upsilon( s_species_ttc *pspecies, int i, FILE *fpout)
 *\brief Calculate upsilon term of the (Lambda/rhoc_water) of Kurylyk et al. 2017 - Eqn 2
 */
void Calc_upsilon(s_species_ttc *pspecies, int i, int type_solution, FILE *fpout) {
  double rhow, cw, lambda;

  rhow = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][RHO_TTC];    // density of water
  cw = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][HEAT_CAP_TTC]; // heat capacity

  if (type_solution == AQUITARD_TTC) {
    for (int j = 0; j < NVAR_AQUITARD_TTC; j++) {
      lambda = pspecies->p_aquitard_ttc[i]->lambda[j];
      pspecies->p_aquitard_ttc[i]->upsilon[j] = lambda / (rhow * cw);
      //        LP_printf(fpout,"In func %s, in aquitard: i is %d, upsilon: %e,\n",__func__ , i,
      //                  pspecies->p_aquitard_ttc[i]->upsilon[j]);
    }
  } else if (type_solution == RIVERBED_TTC) {
    for (int j = 1; j < NVAR_AQUITARD_TTC; j++) { // We start from 1 because we solve 2 layer solution (riverbed+aquifer)
      lambda = pspecies->p_riverbed_ttc[i]->lambda[j];
      pspecies->p_riverbed_ttc[i]->upsilon[j] = lambda / (rhow * cw);
      //        LP_printf(fpout,"In func %s, in rivbed: i is %d, upsilon: %e, lambda: %lf \n",__func__ , i,
      //                  pspecies->p_riverbed_ttc[i]->upsilon[j], lambda);
    }
  }
}

/**\fn void  Calc_aquitard_peclet( s_species_ttc *pspecies, int i, int type_solution, FILE *fpout)
 *\brief Calculate the Peclet number to calculate the heat fluxes based on Kurylyk et al. 2017 - Eqn 12
 */
void Calc_aquitard_peclet(s_species_ttc *pspecies, int i, int type_solution, FILE *fpout) {
  double rhow, cw, lambda; // Thermal properties of the studied domain
  double b;                // Physical properties of the studied domain (b = Thickness of layer);
  double q;                // Darcy velocity of the vater passing through the medium (Vertically) in m/s

  double coeff_1;

  rhow = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][RHO_TTC];    // density of water
  cw = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][HEAT_CAP_TTC]; // heat capacity

  if (type_solution == AQUITARD_TTC) {
    for (int j = 0; j < NVAR_AQUITARD_TTC; j++) {
      lambda = pspecies->p_aquitard_ttc[i]->lambda[j];
      b = pspecies->p_aquitard_ttc[i]->thick[j];
      coeff_1 += b / lambda;
    }
    q = pspecies->p_aquitard_ttc[i]->q;
    pspecies->p_aquitard_ttc[i]->nb_peclet = q * rhow * cw * coeff_1;
    //        LP_printf(fpout,"In func %s, in aquitard: i is %d, nb_peclet: %lf,\n",__func__ , i, pspecies->p_aquitard_ttc[i]->nb_peclet);

  } else if (type_solution == RIVERBED_TTC) {
    for (int j = 1; j < NVAR_AQUITARD_TTC; j++) { // We start from 1 because we solve 2 layer solution (riverbed+aquifer) and river temperature is known already
      lambda = pspecies->p_riverbed_ttc[i]->lambda[j];
      b = pspecies->p_riverbed_ttc[i]->thick[j];
      coeff_1 += b / lambda;
    }
    q = pspecies->p_riverbed_ttc[i]->q;
    pspecies->p_riverbed_ttc[i]->nb_peclet = q * rhow * cw * coeff_1;
    //        LP_printf(fpout,"In func %s, in riverbed: i is %d, nb_peclet: %lf, q: %lf, rhow: %e, cw: %e, coeff_1: %e\n",__func__ , i, pspecies->p_riverbed_ttc[i]->nb_peclet, q,rhow,cw,coeff_1);
  }
}

/**\fn void Calc_q(s_carac_ttc *pcarac, s_species_ttc *pspecies, int i, FILE *fpout)
 *\brief Pass the values of the darcy veloctity from uface into the aquitard structure
 */
void Calc_q(s_carac_ttc *pcarac, s_species_ttc *pspecies, int i, int type_solution, FILE *fpout) {
  if (type_solution == AQUITARD_TTC) {
    pspecies->p_aquitard_ttc[i]->q = pcarac->uface[i][BOTTOM_TTC][0];
    if (pspecies->p_aquitard_ttc[i]->q == 0) {
      pspecies->p_aquitard_ttc[i]->q = 1e-15;
    }
    //    LP_printf(fpout,"Passing the velocity at the faces at id %d of the aquitard: %e, %e, %e\n",i, pspecies->p_aquitard_ttc[i]->q,  pcarac->uface[i][BOTTOM_TTC][0],  pcarac->uface[i][NORTH_TTC][1]);
  } else {
    LP_error(fpout, "In func %s, riverbed is activated whereas it should be aquitard", __func__);
  }
}

/**\fn void Calc_alpha( s_species_ttc *pspecies, int i, FILE *fpout )
 *\brief Calculate the alpha term of Kurylyk et al. 2017 - Eqn: 11
 */
void Calc_alpha(s_species_ttc *pspecies, int i, int type_solution, FILE *fpout) {
  double q, b, upsilon;
  double alpha;
  double term1;

  if (type_solution == AQUITARD_TTC) {
    if (pspecies->p_aquitard_ttc[i]->q == 0) {
      pspecies->p_aquitard_ttc[i]->q = 1e-20;
    }
    q = pspecies->p_aquitard_ttc[i]->q;

    for (int j = 0; j < NVAR_AQUITARD_TTC; j++) {
      b = pspecies->p_aquitard_ttc[i]->thick[j];
      if (j == VAR_ABOVE_TTC || j == VAR_BELOW_TTC) { // here we divide the b (thickness, m) by two because we take temperatures from the center of the cell
        b = b / 2;
      }
      upsilon = pspecies->p_aquitard_ttc[i]->upsilon[j];
      term1 += b / upsilon;
      //        LP_printf(fpout,"In func %s, i is %d, b is %f, upsilon: %e, term1: %e\n",__func__ , i,
      //                  b,  upsilon, term1);
    }
    alpha = exp(q * term1);
    if (isinf(alpha) == 1 || alpha > 1000) {
      //            LP_printf(fpout,"In func %s, i is %d, alpha is infinite, %d \n",__func__ , i,   isinf(alpha));
      pspecies->p_aquitard_ttc[i]->alpha = 1000;
    } else {
      //            LP_printf(fpout,"In func %s, i is %d, alpha is finite, %d \n",__func__ , i,   isinf(alpha));
      pspecies->p_aquitard_ttc[i]->alpha = alpha;
    }

    /*    LP_printf(fpout,"In func %s, i is %d, alpha: %f, q is %e\n", __func__ ,
                  i, alpha, q);*/ // DK check

  } else if (type_solution == RIVERBED_TTC) {
    //        if ( pspecies->p_riverbed_ttc[i]->q == 0) {
    //            pspecies->p_riverbed_ttc[i]->q = 1e-20;
    //        }
    q = pspecies->p_riverbed_ttc[i]->q;

    for (int j = 1; j < NVAR_AQUITARD_TTC; j++) {
      b = pspecies->p_riverbed_ttc[i]->thick[j];
      if (j == VAR_ABOVE_TTC || j == VAR_BELOW_TTC) { // here we divide the b (thickness, m) by two because we take temperatures from the center of the cell
        b = b / 2;
      }
      upsilon = pspecies->p_riverbed_ttc[i]->upsilon[j];
      term1 += b / upsilon;
      //        LP_printf(fpout,"In func %s, rivbed i is %d, b is %f, upsilon: %e, term1: %e, q is %lf\n",__func__ , i,
      //                  b,  upsilon, term1, q);
    }
    alpha = exp(q * term1);
    if (isinf(alpha) == 1 || alpha > 1000) {
      //            LP_printf(fpout,"In func %s, i is %d, alpha is infinite, %d \n",__func__ , i,   isinf(alpha));
      pspecies->p_riverbed_ttc[i]->alpha = 1000;
    } else {
      //            LP_printf(fpout,"In func %s, i is %d, alpha is finite, %d \n",__func__ , i,   isinf(alpha));
      pspecies->p_riverbed_ttc[i]->alpha = alpha;
    }
    //
    //        LP_printf(fpout,"In func %s, i is %d, alpha: %f, q is %e\n", __func__ ,
    //              i, alpha, q); // DK check
  }
}

/**\fn void Calc_aquitard_parameters( s_species_ttc *pspecies, int i, FILE *fpout)
 *\brief Calculate the coefficients applied on the aquitard
 */
void Calc_aquitard_parameters(s_species_ttc *pspecies, int i, int type_solution, FILE *fpout) {
  Calc_upsilon(pspecies, i, type_solution, fpout);
  Calc_alpha(pspecies, i, type_solution, fpout);
}

/**\fn void Calc_coeff( s_species_ttc *pspecies, int i, FILE *fpout )
 *\brief Coefficients of the Kurlyk analytical solution
 */
void Calc_coeff(s_species_ttc *pspecies, int i, int type_solution, FILE *fpout) {
  double c_oneone, c_onetwo;     // Coefficients of the 1st layer (degC)
  double c_twoone, c_twotwo;     // Coefficients of the 2nd layer (degC)
  double c_threeone, c_threetwo; // Coefficients of the 3rd layer (degC)
  double ttop, tbottom;          // Temperatures of above and below layer (degC)
  double alpha;                  // Term alpha Kurylyk et al. 2017 - Eqn 11
  double q;                      // Darcy velocity (m/s)
  double z;                      // Depth (m)
  double upsilon_lone, upsilon_ltwo, upsilon_lthree;
  double b_one, b_two, b_three; // Thickness of the layer 1, 2, 3 (m)
  double coeff_top, coeff_bot;  // coefficients to be used in the libgc to solve the analytical solution
  double term1, term2;

  if (type_solution == AQUITARD_TTC) {
    q = pspecies->p_aquitard_ttc[i]->q;
    alpha = pspecies->p_aquitard_ttc[i]->alpha;
    upsilon_lone = pspecies->p_aquitard_ttc[i]->upsilon[VAR_ABOVE_TTC];
    upsilon_ltwo = pspecies->p_aquitard_ttc[i]->upsilon[VAR_AQUITARD_TTC];
    upsilon_lthree = pspecies->p_aquitard_ttc[i]->upsilon[VAR_BELOW_TTC];

    ttop = pspecies->p_aquitard_ttc[i]->temperature[VAR_ABOVE_TTC][0];
    tbottom = pspecies->p_aquitard_ttc[i]->temperature[VAR_BELOW_TTC][0];

    b_one = pspecies->p_aquitard_ttc[i]->thick[VAR_ABOVE_TTC] / 2;
    b_two = pspecies->p_aquitard_ttc[i]->thick[VAR_AQUITARD_TTC];
    z = b_one + b_two / 2; // depth of the aquitard center from the center of the above layer

    c_oneone = (tbottom - ttop) / (alpha - 1);
    c_onetwo = ((alpha * ttop) - tbottom) / (alpha - 1);
    if (alpha == 1000) {
      c_twoone = c_oneone * 1000;
    } else {
      c_twoone = c_oneone * exp(q * b_one * ((1 / upsilon_lone) - (1 / upsilon_ltwo)));
    }
    c_twotwo = c_onetwo;
    if (alpha == 1000) {
      c_threeone = c_twoone * 1000;
    } else {
      c_threeone = c_twoone * exp(q * (b_one + b_two) * ((1 / upsilon_ltwo) - (1 / upsilon_lthree)));
    }
    c_threetwo = c_onetwo;

    if (alpha == 1000) {
      term1 = 1.;
      term2 = 0.;
      alpha = 0.;
    } else {
      if (q * b_one > -1e-4) { // Limit the exponential term for very high velocities wheree the solution is not valid

        term1 = exp((q * b_one * (1 / upsilon_lone - 1 / upsilon_ltwo)));
        term2 = exp((q * z / upsilon_ltwo));
      } else {
        term1 = exp((-1e-4 * (1 / upsilon_lone - 1 / upsilon_ltwo)));
        term2 = exp((-1e-4 * z / upsilon_ltwo));
        //	    LP_warning(fpout,"In func %s, i is %d, q too high term1: %lf; term2=%lf alpha %e q: %e, z %lf b_one %lf upsilon_lone %e upsilon ltwo %e \n", __func__ , i, term1, term2, alpha, q, z, b_one, upsilon_lone, upsilon_ltwo);
      }
    }

    //  LP_printf(fpout,"In func %s, i is %d, term1: %lf; term2=%lf alpha %e q: %e, z %lf b_one %lf upsilon_lone %e upsilon ltwo %e \n", __func__ , i, term1, term2, alpha, q, z, b_one, upsilon_lone, upsilon_ltwo);

    // Coefficients of the aquitard transport to be put in mat6 of the transport coefficient matrix
    coeff_top = (alpha - term1 * term2); ///(alpha-1);
    coeff_bot = (term1 * term2 - 1);     ///(alpha-1);

    //    LP_printf(fpout,"In func %s, i is %d, coeff_top: %lf; coeff_bot=%lf\n", __func__ , i, coeff_top, coeff_bot);

    pspecies->p_aquitard_ttc[i]->coeff_top = coeff_top;
    pspecies->p_aquitard_ttc[i]->coeff_bot = coeff_bot;

    //    LP_printf(fpout,"In func %s, i is %d, alpha: %f, q is %e, b_one: %f, b_two: %f\n", __func__ ,
    //              i, alpha, q, b_one, b_two);
    //    LP_printf(fpout,"In func %s, coneone: %f, c_onetwo: %f, c_twoone: %f, c_twotwo: %f, c_threeone: %f, c_threetwo: %f\n", __func__ ,
    //              c_oneone, c_onetwo, c_twoone, c_twotwo, c_threeone, c_threetwo);

    /*        double temp_aquitard = c_twoone*exp((q*z/upsilon_ltwo)) + c_twotwo;
            pspecies->p_aquitard_ttc[i]->temperature[VAR_AQUITARD_TTC][0] = temp_aquitard;*/

    /*    LP_printf(fpout,"In func %s, upsilon_ltwo: %e ttop: %f, tbot= %f temperature aquitard is: %f \n", __func__ ,
                  upsilon_ltwo, ttop, tbottom, temp_aquitard);*/
  } else if (type_solution == RIVERBED_TTC) {
    q = pspecies->p_riverbed_ttc[i]->q;
    alpha = pspecies->p_riverbed_ttc[i]->alpha;
    upsilon_lone = pspecies->p_riverbed_ttc[i]->upsilon[VAR_AQUITARD_TTC]; // pspecies->p_riverbed_ttc[i]->upsilon[VAR_ABOVE_TTC];
    upsilon_ltwo = pspecies->p_riverbed_ttc[i]->upsilon[VAR_BELOW_TTC];
    // upsilon_lthree =

    ttop = pspecies->p_riverbed_ttc[i]->temperature[VAR_ABOVE_TTC][0];
    tbottom = pspecies->p_riverbed_ttc[i]->temperature[VAR_BELOW_TTC][0];

    b_one = pspecies->p_riverbed_ttc[i]->thick[VAR_AQUITARD_TTC]; // pspecies->p_riverbed_ttc[i]->thick[VAR_ABOVE_TTC]/2;
    b_two = pspecies->p_riverbed_ttc[i]->thick[VAR_BELOW_TTC];
    z = b_one / 2; // depth of the aquitard center from the center of the above layer

    c_oneone = (tbottom - ttop) / (alpha - 1);
    c_onetwo = ((alpha * ttop) - tbottom) / (alpha - 1);
    if (alpha == 1000) {
      c_twoone = c_oneone * 1000;
    } else {
      c_twoone = c_oneone * exp(q * b_one * ((1 / upsilon_lone) - (1 / upsilon_ltwo)));
    }

    c_twotwo = c_onetwo;
    // c_threeone = c_twoone*exp(q*(b_one+b_two)*((1/upsilon_ltwo)-(1/upsilon_lthree)));
    // c_threetwo = c_onetwo;

    if (alpha == 1000) {
      term1 = 1.; // In riverbed solution, we use 2 layers hence term1 reduces to 1 because only top layer coefficient is sufficient.
      term2 = 1000;
    } else {
      term1 = 1.; // In riverbed solution, we use 2 layers hence term1 reduces to 1 because only top layer coefficient is sufficient.
      term2 = exp((q * z / upsilon_lone));
    }

    //        LP_printf(fpout,"In func %s, i is %d, term1: %lf; term2=%lf, q = %lf z = %lf alpha = %lf \n", __func__ , i, term1, term2, q, z, alpha);

    // Coefficients of the aquitard transport to be put in mat6 of the transport coefficient matrix
    coeff_top = (alpha - term1 * term2); ///(alpha-1)
    coeff_bot = (term1 * term2 - 1);     ///(alpha-1)
    //        coeff_top = 0; ///(alpha-1)
    //        coeff_bot = 999; ///(alpha-1)

    //    LP_printf(fpout,"In func %s, i is %d, coeff_top: %lf; coeff_bot=%lf\n", __func__ , i, coeff_top, coeff_bot);

    if (q == 0.) {
      pspecies->p_riverbed_ttc[i]->coeff_top = 0.;
      pspecies->p_riverbed_ttc[i]->coeff_bot = 100;

    } else {
      pspecies->p_riverbed_ttc[i]->coeff_top = coeff_top;
      pspecies->p_riverbed_ttc[i]->coeff_bot = coeff_bot;
    }
    //        LP_printf(fpout,"In func %s, riverbed i is %d, coeff_top: %lf; coeff_bot=%lf\n", __func__ , i, coeff_top, coeff_bot);
    //
    //    LP_printf(fpout,"In func %s, i is %d, alpha: %f, q is %e, b_one: %f, b_two: %f\n", __func__ ,
    //              i, alpha, q, b_one, b_two);
    //    LP_printf(fpout,"In func %s, coneone: %f, c_onetwo: %f, c_twoone: %f, c_twotwo: %f,\n", __func__ ,
    //              c_oneone, c_onetwo, c_twoone, c_twotwo );

    //        double temp_aquitard = c_oneone*exp((q*z/upsilon_lone)) + c_onetwo;
    // pspecies->p_aquitard_ttc[i]->temperature[VAR_AQUITARD_TTC][0] = temp_aquitard;

    //    LP_printf(fpout,"In func %s, at depth: %f: ttop: %f, tbot= %f \n", __func__ ,
    //              z, ttop, tbottom);
  }
}

/**\fn s_aquitard_ttc **TTC_init_aquitard(int nele)
 *\brief Initialize the aquitard structure by allocating it in each element inside p_species
 */
s_aquitard_ttc **TTC_init_riverbed(int nriverbed) {

  s_aquitard_ttc **p_aquitard_ttc;

  p_aquitard_ttc = ((s_aquitard_ttc **)malloc(nriverbed * sizeof(s_aquitard_ttc *)));
  for (int i = 0; i < nriverbed; i++) {
    p_aquitard_ttc[i] = new_aquitard_ttc();
  }

  return p_aquitard_ttc;
}

/**\fn void Calc_aquitard_fluxes( s_species_ttc *pspecies, int i, FILE *fpout )
 *\brief Coefficients of the Kurlyk analytical solution
 */
void Calc_aquitard_fluxes(s_species_ttc *pspecies, int i, int type_solution, FILE *fpout) {
  double ttop, tbottom, taquitard; // Temperatures of above and below layer (degC)
  double lambda_top, lambda_bottom;
  double lambda_bulk;
  double q = 1e-20;                                                             // Darcy velocity (m/s)
  double z_above, z_below;                                                      // Depth (m)
  double b_one, b_two, b_three;                                                 // Thickness of the layer 1, 2, 3 (m)
  double rhow = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][RHO_TTC];    // Water density kg/m3
  double cw = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][HEAT_CAP_TTC]; // Water specific heat capacity J/kg/K
  double rhowcw = rhow * cw;                                                    // Water volumetric heat capacity J/m3/K
  double flux_advective = 0;                                                    // Initialization advective flux 0 W/m2
  double flux_conduction = 0;                                                   // Initialization conductive flux 0 W/m2

  if (type_solution == AQUITARD_TTC) {
    q = pspecies->p_aquitard_ttc[i]->q;

    ttop = pspecies->p_aquitard_ttc[i]->temperature[VAR_ABOVE_TTC][0];
    taquitard = pspecies->p_aquitard_ttc[i]->temperature[VAR_AQUITARD_TTC][0];
    tbottom = pspecies->p_aquitard_ttc[i]->temperature[VAR_BELOW_TTC][0];

    b_one = pspecies->p_aquitard_ttc[i]->thick[VAR_ABOVE_TTC] / 2;
    b_two = pspecies->p_aquitard_ttc[i]->thick[VAR_AQUITARD_TTC];
    b_three = pspecies->p_aquitard_ttc[i]->thick[VAR_BELOW_TTC] / 2;
    z_above = b_one + b_two / 2;   // depth of the aquitard center from the center of the above layer
    z_below = b_three + b_two / 2; // depth of the aquitard center from the center of the above layer
    lambda_top = geometric_mean(pspecies->p_aquitard_ttc[i]->lambda[VAR_ABOVE_TTC], pspecies->p_aquitard_ttc[i]->lambda[VAR_AQUITARD_TTC], b_one / (b_one + b_two));
    lambda_bottom = geometric_mean(pspecies->p_aquitard_ttc[i]->lambda[VAR_AQUITARD_TTC], pspecies->p_aquitard_ttc[i]->lambda[VAR_BELOW_TTC], b_two / (b_two + b_three));
    lambda_bulk = geometric_mean(lambda_top, pspecies->p_aquitard_ttc[i]->lambda[VAR_BELOW_TTC], (b_one + b_two) / (b_one + b_two + b_three));

    pspecies->p_aquitard_ttc[i]->flux_adv[VAR_ABOVE_TTC] = q * rhowcw * (ttop - tbottom);
    pspecies->p_aquitard_ttc[i]->flux_adv[VAR_BELOW_TTC] = q * rhowcw * (ttop - tbottom);

    pspecies->p_aquitard_ttc[i]->flux_cond[VAR_ABOVE_TTC] = -lambda_bulk * (tbottom - ttop) / (z_above + z_below);
    pspecies->p_aquitard_ttc[i]->flux_cond[VAR_BELOW_TTC] = -lambda_bulk * (tbottom - ttop) / (z_above + z_below);

    pspecies->p_aquitard_ttc[i]->flux_tot[VAR_ABOVE_TTC] = pspecies->p_aquitard_ttc[i]->flux_cond[VAR_ABOVE_TTC] + pspecies->p_aquitard_ttc[i]->flux_adv[VAR_ABOVE_TTC];
    pspecies->p_aquitard_ttc[i]->flux_tot[VAR_BELOW_TTC] = pspecies->p_aquitard_ttc[i]->flux_cond[VAR_BELOW_TTC] + pspecies->p_aquitard_ttc[i]->flux_adv[VAR_BELOW_TTC];
    //        LP_printf(fpout,"In func %s, aquitard above z: %f, Fluxes Advection: %f, Conduction= %f Total: %f, lambda: %lf, q: %e, ttop: %lf \n", __func__ ,
    //                  z_above, pspecies->p_aquitard_ttc[i]->flux_adv[VAR_ABOVE_TTC],pspecies->p_aquitard_ttc[i]->flux_cond[VAR_ABOVE_TTC],
    //                  pspecies->p_aquitard_ttc[i]->flux_tot[VAR_ABOVE_TTC],lambda_top, q, ttop);
    //        LP_printf(fpout,"In func %s, aquitard below z: %f, Fluxes Advection: %f, Conduction= %f Total: %f, lambda: %lf, q: %e, ttop: %lf \n", __func__ ,
    //                  z_below, pspecies->p_aquitard_ttc[i]->flux_adv[VAR_BELOW_TTC],pspecies->p_aquitard_ttc[i]->flux_cond[VAR_BELOW_TTC],
    //                  pspecies->p_aquitard_ttc[i]->flux_tot[VAR_BELOW_TTC],lambda_bottom, q, tbottom);
  } else if (type_solution == RIVERBED_TTC) {
    q = pspecies->p_riverbed_ttc[i]->q;

    //        lambda_top = geometric_mean(pspecies->p_riverbed_ttc[i]->lambda[VAR_ABOVE_TTC],pspecies->p_riverbed_ttc[i]->lambda[VAR_AQUITARD_TTC],b_one/(b_one+b_two));

    ttop = pspecies->p_riverbed_ttc[i]->temperature[VAR_ABOVE_TTC][0];
    taquitard = pspecies->p_riverbed_ttc[i]->temperature[VAR_AQUITARD_TTC][0];
    tbottom = pspecies->p_riverbed_ttc[i]->temperature[VAR_BELOW_TTC][0];

    b_one = pspecies->p_riverbed_ttc[i]->thick[VAR_AQUITARD_TTC]; // pspecies->p_riverbed_ttc[i]->thick[VAR_ABOVE_TTC]/2;
    b_two = pspecies->p_riverbed_ttc[i]->thick[VAR_BELOW_TTC];

    z_above = b_one / 2; // depth of the aquitard center from the center of the above layer
    z_below = b_one + b_two / 2;
    // Bulk thermal conductivity calculated by geometric mean
    lambda_bottom = arithmetic_mean(pspecies->p_riverbed_ttc[i]->lambda[VAR_AQUITARD_TTC], pspecies->p_riverbed_ttc[i]->lambda[VAR_BELOW_TTC], b_one / z_below);

    if (q < 0) { // T_DATUM to reorient T to 0C

      flux_advective = q * rhowcw * (tbottom - T_DATUM);

    } else {

      flux_advective = q * rhowcw * (ttop - T_DATUM);
    }

    pspecies->p_riverbed_ttc[i]->flux_adv[VAR_ABOVE_TTC] = flux_advective;
    pspecies->p_riverbed_ttc[i]->flux_adv[VAR_BELOW_TTC] = -q * rhowcw * (ttop - tbottom);

    pspecies->p_riverbed_ttc[i]->flux_cond[VAR_ABOVE_TTC] = lambda_bottom * (taquitard - T_DATUM) / z_below;
    pspecies->p_riverbed_ttc[i]->flux_cond[VAR_BELOW_TTC] = lambda_bottom * (tbottom - ttop) / z_below;

    pspecies->p_riverbed_ttc[i]->flux_tot[VAR_ABOVE_TTC] = pspecies->p_riverbed_ttc[i]->flux_cond[VAR_ABOVE_TTC] + pspecies->p_riverbed_ttc[i]->flux_adv[VAR_ABOVE_TTC];
    pspecies->p_riverbed_ttc[i]->flux_tot[VAR_BELOW_TTC] = pspecies->p_riverbed_ttc[i]->flux_cond[VAR_BELOW_TTC] + pspecies->p_riverbed_ttc[i]->flux_adv[VAR_BELOW_TTC];

    //        LP_printf(fpout,"In func %s, riverbed above z: %f, Fluxes Advection: %f, Conduction= %f Total: %f, lambda: %lf, q: %e, ttop: %lf, taquitard: %lf tbottom %lf \n", __func__ ,
    //              z_above, pspecies->p_riverbed_ttc[i]->flux_adv[VAR_ABOVE_TTC],pspecies->p_riverbed_ttc[i]->flux_cond[VAR_ABOVE_TTC],
    //              pspecies->p_riverbed_ttc[i]->flux_tot[VAR_ABOVE_TTC],lambda_bottom, q, ttop, taquitard, tbottom);
    //        LP_printf(fpout,"In func %s, riverbed below z: %f, Fluxes Advection: %f, Conduction= %f Total: %f, lambda: %lf, q: %e, tbot: %lf, ttop: %lf \n", __func__ ,
    //                  z_below, pspecies->p_riverbed_ttc[i]->flux_adv[VAR_BELOW_TTC],pspecies->p_riverbed_ttc[i]->flux_cond[VAR_BELOW_TTC],
    //                  pspecies->p_riverbed_ttc[i]->flux_tot[VAR_BELOW_TTC],lambda_bottom, q, tbottom, ttop);
  }
}
