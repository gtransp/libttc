/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: manage_link_ttc.c
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "CHR.h"
#include "GC.h"
#include "IO.h"

#include "TTC.h"

s_link_ttc *TTC_create_link() {
  int nparam;
  s_link_ttc *plink;
  plink = new_link_ttc();
  bzero((char *)plink, sizeof(s_link_ttc *));
  return plink;
}

/*
 * DK 18 08 2021:
 * Changes in the plink
 * Removed pu_ttc init
 *
 */

s_link_ttc *TTC_init_link(s_carac_ttc *pcarac, s_species_ttc *pspecies, int nele) // NG : 30/09/2021 s_link_ttc* argument removed.
{

  s_link_ttc *plink; // NG : 30/09/2021 : plink was filled without being created.
  plink = TTC_create_link();

  plink->p_param_calc_ttc = TTC_init_param_calc(nele);
  plink->p_neigh_ttc = TTC_init_neigh(nele);
  plink->preoxy_ttc = TTC_init_reoxy(nele);
  plink->nsub = TTC_allocate_nsub(nele);
  plink->pspecies = pspecies;
  // plink->uface = pcarac->uface; // Add allocation uface DK AR 27 08 2021

  //  plink->pbase_ttc=TTC_init_param_base(nele); DK AR neigh goes to p_carac 23 08 2021

  return plink;
}

char *TTC_link_get_name(s_link_ttc *plink) { return plink->pspecies->name; }

// DK AR 27 08 2021 change in the p_aq_link[i] to plink because there will be only 1 link for each species
s_link_ttc *TTC_get_link_by_name(s_carac_ttc *pcarac, char *name, FILE *flog) {
  int i;
  int nspecies;
  s_link_ttc *plink = NULL;

  nspecies = pcarac->count[NSPECIES_TTC];
  for (i = 0; i < nspecies; i++) {
    if (strcmp(name, TTC_link_get_name(pcarac->p_link[i])) == 0)
      plink = pcarac->p_link[i];
  }
  if (plink == NULL)
    LP_error(flog, "In libttc%4.2f %s l%d: Impossible to find species %s\n", VERSION_TTC, __FILE__, __LINE__, name);
  return plink;
}

// Allocation mémoire de la matrice de stockage des nsub par élément et par face
// Ne gère pas la dimension Z
int **TTC_allocate_nsub(int nele) {
  int **nsub;
  int i, j;

  nsub = (int **)malloc(nele * sizeof(int *));
  for (i = 0; i < nele; i++) {
    nsub[i] = (int *)malloc(NB_CARD_TTC * sizeof(int));
    for (j = 0; j < NB_CARD_TTC; j++)
      nsub[i][j] = 0;
  }
  return nsub;
}

// Allocation mémoire de la matrice de stockage des uface par élément et par face
// Ne gère pas la dimension Z
double ***TTC_allocate_uface(int nele) {
  double ***uface;
  int i, j, k;

  uface = (double ***)malloc(nele * sizeof(double **));
  for (i = 0; i < nele; i++) {
    uface[i] = (double **)malloc(NB_CARD_TTC * sizeof(double *));
    for (j = 0; j < NB_CARD_TTC; j++) {
      uface[i][j] = (double *)malloc(SUB_CARD_TTC * sizeof(double));
    }
  }

  for (i = 0; i < nele; i++) {
    for (j = 0; j < NB_CARD_TTC; j++) {
      for (k = 0; k < SUB_CARD_TTC; ++k) {
        uface[i][j][k] = 0.; // initialize uface to 0
      }
    }
  }
  return uface;
}

double *TTC_allocate_dvdt(int nele) {
  double *dvdt;
  int i;

  dvdt = (double *)malloc(nele * sizeof(double));

  for (i = 0; i < nele; i++) {
    dvdt[i] = 0.; // initialize uface to 0
  }
  return dvdt;
}

double *TTC_allocate_SinkSource(int nele) {
  double *SinkSource;
  int i;

  SinkSource = (double *)malloc(nele * sizeof(double));

  for (i = 0; i < nele; i++) {
    SinkSource[i] = 0.; // initialize uface to 0
  }
  return SinkSource;
}