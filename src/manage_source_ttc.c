/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: manage_source_ttc.c
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "CHR.h"
#include "GC.h"
#include "IO.h"

#include "TTC.h"

/*s_boundary_ttc *TTC_create_source()
{
  s_boundary_ttc *pboundary_ttc;
  pboundary_ttc=new_boundary_ttc();
  bzero((char *)pboundary_ttc,sizeof(s_boundary_ttc));
  return pboundary_ttc;
}

s_boundary_ttc **TTC_init_source(int nele, int tstep)
{
  s_boundary_ttc **p_boundary_ttc;

  p_boundary_ttc=(s_boundary_ttc **) malloc(nele * sizeof(s_boundary_ttc * ));
    for (int i = 0; i < nele; i++) {
        p_boundary_ttc[i] = (s_boundary_ttc *) malloc(tstep * sizeof(s_boundary_ttc));
    }

  return p_boundary_ttc;
}
*/

// NG : 05/03/2020
/**\fn s_bound_aq ***AQ_create_source(FILE *fpout)
 *\brief Create pointer towards source structure for one element. Manage memory allocation.
 *\return psource pointer
 */
s_source_ttc ***TTC_create_source(FILE *fpout) {

  s_source_ttc ***psource = NULL;
  int i;

  psource = (s_source_ttc ***)malloc(NB_SOURCE_TTC * sizeof(s_source_ttc **));
  if (psource == NULL)
    LP_error(fpout, "libttc%4.2f in %s of %s at line %d: Memory allocation failure for psource pointer.\n", VERSION_TTC, __func__, __FILE__, __LINE__);
  bzero((char *)psource, NB_SOURCE_TTC * sizeof(s_source_ttc **));

  for (i = 0; i < NB_SOURCE_TTC; i++) {
    psource[i] = (s_source_ttc **)malloc(sizeof(s_source_ttc *));
    if (psource[i] == NULL)
      LP_error(fpout, "libttc%4.2f in %s of %s at line %d: Memory allocation failure for psource[kindof] pointer.\n", VERSION_TTC, __func__, __FILE__, __LINE__);
    bzero((char *)psource[i], sizeof(s_source_ttc *)); // Initialisation avec un tableau d'un seul element. Chaque tableau de kindof sera realloc a la declaration d'une nouvelle source via AQ_add_source_to_element()
  }

  return psource;
}

/**\fn s_source_ttc *TTC_create_sboundary(int kindof)
 *\brief create BC structur
 *\return pbound
 */
s_source_ttc *TTC_create_sboundary(int kindof) {
  s_source_ttc *psource;
  psource = new_source_ttc();
  bzero((char *)psource, sizeof(s_source_ttc)); // qlim est bien mis a 0 ici
  psource->type = kindof;

  return psource;
}

/**\fn s_source_ttc *TTC_define_source_val(int kindof,s_ft *pft,double stot,FILE *fpout)
 *\brief define sources values (tjs pareil quasiment la meme fonction que  AQ_define_bound_val vraiment pertinent?)
 *\return *s_bound_aq
 */
s_source_ttc *TTC_define_source_val(int kindof, s_ft *pft, double stot, FILE *fpout) {
  s_source_ttc *psource;
  s_ft *ptmp;

  int sign = 1;

  psource = TTC_create_sboundary(kindof);

  stot = 1;
  ptmp = TS_division_ft_double(pft, stot, fpout);

  pft = TS_free_ts(pft, fpout);
  psource->pft = ptmp;
  if (psource->pft->next != NULL)
    psource->freq = VARIABLE_TTC;

  //    LP_printf(fpout,"Fonction %s : Value of input geothermal is %e, time %e. and stot is %e m2\n",__func__,psource->pft->ft, psource->pft->t, stot);  // NG check
  //    LP_printf(fpout,"Fonction %s : Assigning frequency %s.\n",__func__,TTC_name_freq(psource->freq ));  // NG check

  psource->origin = SOURCE_GEOTHERMAL_TTC; // NG 07/03/2020

  return psource;
}
