/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: manage_carac_ttc.c
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "GC.h"
#include "CHR.h"
#include "IO.h"

#include "TTC.h"

// allocation de la structure carac_ttc et initialisation par defaut des parametres numeriques
s_carac_ttc *TTC_create_carac() {
  s_carac_ttc *pcarac_ttc;
  pcarac_ttc = new_carac_ttc();
  bzero((char *)pcarac_ttc, sizeof(s_carac_ttc));
  return pcarac_ttc;
}

// allocation des parametres numeriques de la structure carac_ttc
s_carac_ttc *TTC_init_carac() {
  s_carac_ttc *pcarac_ttc;
  s_chronos_CHR *pchronos;

  pcarac_ttc = TTC_create_carac();
  pcarac_ttc->regime = STEADY_TTC;
  pcarac_ttc->theta = THET_TTC;
  //  pcarac_ttc->pchronos;
  pchronos = CHR_init_chronos();
  for (int i = 0; i < NEXTREMA_CHR; i++) { // DK Weird initialization here, it causes memory leak so it has to be deallocated properly 31 01 2022
    free(pchronos->pd[i]);
  }
  pcarac_ttc->pchronos = pchronos;

  free(pchronos);
  return pcarac_ttc;
}

/* Affichage des propriétés des carac et species
   Fonction de vérification pour s'assurer que les propriétés ont bien été attribuées */
void TTC_print_carac_properties(s_carac_ttc *pcarac_ttc, FILE *flog) // NG : 02/10/2021 : Updated version for libttc0.19 (DK branch)
{
  int i, j;
  int nb_species;
  s_species_ttc *pspecies;

  if (pcarac_ttc == NULL)
    LP_error(flog, "libttc%4.2f : Error in file %s, function %s at line %d : TTC-carac pointer has not been allocated yet.\n", VERSION_TTC, __FILE__, __func__, __LINE__);

  LP_printf(flog, "\nTTC general parametrization : \n");
  LP_printf(flog, "Calculation regime : %s\n", TTC_name_regime(pcarac_ttc->regime));
  LP_printf(flog, "Implicity parameter : %7.2f\n", pcarac_ttc->theta);

  LP_printf(flog, "\nGeometry counters : \n");
  for (i = 0; i < N_COUNT_TTC; i++)
    LP_printf(flog, "%s value = %d\n", TTC_count(i), pcarac_ttc->count[i]);

  if (pcarac_ttc->count[NSPECIES_TTC] > 0) {
    for (i = 0; i < pcarac_ttc->count[NSPECIES_TTC]; i++) {
      pspecies = pcarac_ttc->p_species[i];
      LP_printf(flog, "\nProperties of species %d named %s. TTC Intern ID : %d\n", i, pspecies->name, pspecies->id);
      LP_printf(flog, "Specie type : %s\n", TTC_name_equa_type(pspecies->pset->type));
      LP_printf(flog, "Max number of iteration : %d\n", pspecies->pset->iteration);
      LP_printf(flog, "Convergence criteria value : %15.6e\n", pspecies->pset->crconv);
      LP_printf(flog, "Media type : %s\n", TTC_environnement(pspecies->pset->media_type));
      for (j = 0; j < NPROCESSES_TTC; j++)
        LP_printf(flog, "Accounting for %s process = %s\n", TTC_name_process(j), LP_answer(pspecies->pset->calc_process[j], flog));

      LP_printf(flog, "Calculation time-step : %15.1f sec\n", pcarac_ttc->pchronos->dt);
      LP_printf(flog, "libgc application number : %d\n\n", pspecies->pset->iappli_gc);
    }
  }
}
