/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: print_mat_ttc.c
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "CHR.h"
#include "GC.h"
#include "IO.h"

#include "TTC.h"

// print_mat // 15/03/2021 AR
/////////////////////////////////////////////////////////////////////////////////////////

//
void TTC_print_mat(s_species_ttc *pspecies, FILE *fpout) {
  s_gc *pgc;
  int i, nele, ndl;
  int nvois; // LHS and RHS id
  // int    *nsub;
  int *mat4; // LHS and RHS id
  int lmat5;
  int *mat5;
  double *mat6;
  double *b;
  double *val;
  double *delta_L;
  double *x;
  char *filename_mat4 = NULL;
  char *filename_mat5 = NULL;
  FILE *fpmat = NULL;
  FILE *fpmat4 = NULL;
  // pgc=pspecies->pgc;
  lmat5 = pspecies->pgc->lmat5;
  ndl = pspecies->pgc->ndl;

  val = pspecies->pval_ttc->val;
  mat4 = pspecies->pgc->mat4;
  mat5 = pspecies->pgc->mat5;
  mat6 = pspecies->pgc->mat6;
  b = pspecies->pgc->b;
  x = pspecies->pgc->x;
  // nsub = pspecies->plink->nsub;
  //    nvois = pspecies->plink->p_neigh_ttc[i]->nvois;
  // printf("coucou from print_mat \n"); // AR check
  char *speciename = pspecies->name;

  filename_mat5 = (char *)malloc(STRING_LENGTH_LP * sizeof(char));
  sprintf(filename_mat5, "../Result/Output_mat_%s.txt", speciename);

  fpmat = fopen(filename_mat5, "w+");
  if (fpmat == NULL)
    LP_error(fpout, "In %s : Impossible to open file for steady output, mat5 & 6 !\n", __func__);

  fprintf(fpmat, "outputs are: \n");
  fprintf(fpmat, "ID \t mat5 \t mat6 \n");
  for (int j = 0; j < lmat5; ++j) {
    fprintf(fpmat, "%i \t %d \t %e \n", j, mat5[j], mat6[j]); // var[j]
  }
  fclose(fpmat);

  filename_mat4 = (char *)malloc(STRING_LENGTH_LP * sizeof(char));
  sprintf(filename_mat4, "../Result/Output_mat4_%s.txt", speciename);
  fpmat4 = fopen(filename_mat4, "w+");
  if (fpmat4 == NULL)
    LP_error(fpout, "In %s : Impossible to open file for steady output, mat4 !\n", __func__);

  fprintf(fpmat4, "outputs are: \n");
  fprintf(fpmat4, "ID \t mat4 \t b \t x \n");
  for (int j = 0; j < ndl + 1; ++j) {
    fprintf(fpmat4, "%i \t %d \t %e \t %e \t %e \n", j, mat4[j], b[j], val[j], x[j]);
  }
  fclose(fpmat4);

  LP_printf(fpout, "\n matrices mat4, mat5, mat6 are written at location : %s.\n", filename_mat4);
}

// SW 18/10/2021
void TTC_print_LHS_RHS(s_gc *pgc, FILE *flog) {
  int nrow = 0, ncol; // row and column number
  int i, j;
  int ind_mat6 = 0;
  int ndl;
  double **mat_tab;
  ndl = pgc->ndl;

  mat_tab = (double **)malloc(sizeof(double *) * ndl);

  for (i = 0; i < ndl; i++) {
    mat_tab[i] = (double *)malloc(sizeof(double) * ndl);
    memset(mat_tab[i], 0, sizeof(double) * ndl);
  }
  for (i = 1; i < pgc->ndl + 1; i++) // mat4 dimension
  {

    for (j = pgc->mat4[i - 1]; j < pgc->mat4[i]; j++) {
      ncol = pgc->mat5[j] - 1;
      mat_tab[nrow][ncol] = pgc->mat6[ind_mat6++];
    }
    nrow++;
  }
  LP_printf(flog, "ind_mat6 = %d\n", ind_mat6);
  for (nrow = 0; nrow < ndl; nrow++) {
    for (ncol = 0; ncol < ndl; ncol++)
      LP_printf(flog, "%10.6f\t", mat_tab[nrow][ncol]); // print LHS
    LP_printf(flog, "%10.6f\n", pgc->b[nrow]);          // print RHS
  }

  for (i = 0; i < ndl; i++) // free memery
  {
    free(*(mat_tab + i));
  }
  mat_tab = NULL;
}
