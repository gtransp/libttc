/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: struct_TTC.h
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <time.h>

typedef struct carac_ttc s_carac_ttc;
typedef struct link_ttc s_link_ttc;
typedef struct species_ttc s_species_ttc;
typedef struct settings_ttc s_settings_ttc;

// NG 03/10/2021 : Started to add proper comments for each type of attributes. Need to be verified by AR

/**\Struct carac_ttc (s_carac_ttc)
 *\brief
 */
struct carac_ttc {
  int regime;             /*!< Type of regime in N_REGI_TTC. */
  int count[N_COUNT_TTC]; /* Geometry counters in N_COUNT_TTC. Counts the total element number, the sum (elements+associated neighbors), number of boundaries, number of species. */
  double theta;           /*!< Implicity coefficient. (0 = explicit; 1 = implicit)*/
  double ***uface;        /*!< Velocites or discharges at element's faces, characterizing hydro flows. Table therefore sized over nele, card and sub_icard. */
  double *dVdt;           /* Change in the volume of water in the cell */
  double *qSink;          /* Collect the sink terms (UPTAKE etc) */
  double *qSource;        /* Collect the SOURCE terms (RECHARGE etc) */

  s_species_ttc **p_species; /*!< Transport species attributes structure. Array sized over the total number of species. */
  s_link_ttc **p_link;       /*!< Transport links attributes strucutre. Array sized over the total number of species. */
  s_chronos_CHR *pchronos;   /*!< Temporal characteristics of the simulation required with this mesh in libttc tini, tend, dt. */
};

/**\Struct link_ttc (s_link_ttc)
 *\brief Link structure to store simulation related variables. It stores certain variables of the species its linked to.
 */
struct link_ttc {
  int **nsub;                                                        /*!< Array of number of subfaces for all four directions. Sized over the number of elements and icard. */
  s_species_ttc *pspecies;                                           /* Utilitary shortcut : Pointer towards the current specie. */
  s_coef_roxy_ttc *preoxy_ttc; /* ?????????  (SW) link with PROSE */ // COMMENT STILL TO BE ADDED
  s_param_calc_ttc **p_param_calc_ttc;                               /* structure contenant les parametres de calcul, temporal coefficients & diffusion disp */
  s_neigh_ttc **p_neigh_ttc;                                         /* structure voisins: # of neighbours, distance between cells, identification of cells (faces & subfaces ) */
  // s_aquitard_ttc **p_aquitard_ttc; // Aquitard structure DK AR 10 12 2021

  //  double *val; /*!< table of double of size nele, containig the concentration or temperature value for each element of the mesh*///NF 18/8/2021 i prefer to have val instead of var
};

/**\Struct species_ttc (s_species_ttc)
 *\brief
 */
struct species_ttc {
  int id;                          /*!< Specie intern ID */
  char *name;                      /*!< Specie name */
  s_param_ttc **p_param_ttc;       /*!< structure contenant les parametres de system (size, poros, dispersivity), thermal parameters (conductivity, capacity, rho), diff_mol_solute */
  s_settings_ttc *pset;            /*!< Pointer towards the setting structure */
  s_val_ttc *pval_ttc;             // DK AR 25 08 2021 pvar ttc > pval ttc contains the initial values and solution
  s_flux_ttc **p_flux_ttc;         // structure contenant les types de flux (advectif, dispersif)
  s_aquitard_ttc **p_aquitard_ttc; // structure of the aquitard: parameters and required variables DK AR 14 12 2021;
  s_aquitard_ttc **p_riverbed_ttc; // structure of the aquitard for riverbed: parameters and required variables DK AR 14 12 2021;
  s_boundary_ttc **p_boundary_ttc; // structure contenant les vecteurs des BC//NF 18/8/2021 the number of element has to be allocated. It is an array of pointers towards s_boundary_ttc structures
  s_boundary_ttc **p_source_ttc;
  s_species_ttc *next; // Link to next species in the same compartment  (river & aquifer)
  s_species_ttc *prev; // Link to previous species in the same simulated compartment (river & aquifer)
  s_gc *pgc;           /*!< Pointer to the structure containing the libgc linear algebra attributes to the solve conjugate gradient system */
};

/**\Struct settings_ttc (s_settings_ttc)
 *\brief Settings of the simulation (type of simulation, number of iteration, convergence criteria, environment (river & aquifer),
 * calc process includes dispersion or advection activation status, iappli_gc is the application number for the gc system; oxygen comes from PROSE.
 */
struct settings_ttc {
  int type;                                                                                                                   /*!< Specie type (HEAT,SOLUTE). In N_EQUA_TYPE_TTC. */
  int iteration;                                                                                                              /*!< Maximum iteration number for steady calculations */
  double crconv;                                                                                                              /*!< Convergence criteria value for steady calculations */
  int media_type;                                                                                                             /*!< Environment media type (Free water, porous media). In NENV_TTC. */
  int calc_process[NPROCESSES_TTC];                                                                                           /*!< Type of transport process which are accounted for (convection, diffusion). Array of boolean activation statuses (YES_TS,NO_TS) */
  int iappli_gc;                                                                                                              /*!< Libgc application identifier. Cannot go above 12 currently */
  int oxygen;                                                                                                                 /*!< Dissolved oxygen. Boolean activation status.*/
  int aquitard;                                                                                                               // If aquitard exists in the simulation or not // DK AR 17 12
  int riverbed;                                                                                                               // If riverbed analytical solution exists in the simulation or not // DK AR 17 12 // 2021
  int source;                                                                                                                 // if a source term exists or not
  int boundaries; /*!< YES/NO flag to detect if boundaries are explicitly set from COMM file or not. Set to NO by default. */ // NG : 21/06/2023
};

#define new_carac_ttc() ((s_carac_ttc *)malloc(sizeof(s_carac_ttc)))
#define new_species_ttc() ((s_species_ttc *)malloc(sizeof(s_species_ttc)))
#define new_link_ttc() ((s_link_ttc *)malloc(sizeof(s_link_ttc)))
#define new_settings_ttc() ((s_settings_ttc *)malloc(sizeof(s_settings_ttc)))
