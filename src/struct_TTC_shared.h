/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: struct_TTC_shared.h
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

/*! \file structure link with a mesh
Here are the strucuturs used when libttc and a mesh are coupled
*/
typedef struct param_ttc s_param_ttc;
typedef struct param_calc_ttc s_param_calc_ttc; // parametres de l'equation calcule a partir de param_base qui contient param_thermic et param_solute et des champs de vitesse
typedef struct reoxy_ttc s_coef_roxy_ttc;       // SW 24/10/2018
typedef struct val_ttc s_val_ttc;               // DK AR 24 08 2021 Structure containing the initial value and values stored after each iteration
typedef struct flux_ttc s_flux_ttc;
typedef struct boundary_ttc s_boundary_ttc;
typedef struct neigh_ttc s_neigh_ttc; // NF 18/8/2021 this is a geometry structure, that contains the mesh information
typedef struct aquitard_ttc s_aquitard_ttc;
typedef struct source_ttc s_source_ttc;
typedef struct counter_ttc s_count_ttc;

/*
 * Structure:
 * Parameters of thermal or solute
 * Parameters of cells (porosity, dispersivity, size of the cell
 *
 */
struct param_ttc {
  double param_syst[NPARAM_SYST_TTC]; // vecteurs de taille maille, dispersivite et omega (dimension nele) [SIZE_TTC, DISPERSIVITY_TTC,POROSITY_TTC,NPARAM_SYST_TTC]
  double thickness;
  double param_thermic[NENV_TTC][NPARAM_THERM_TTC]; /*!< parameter in column of nele. The parameters are ordered as defined in param_TTC_shared.h LAMBDA_TTC,HEAT_CAP_TTC,RHO_TTC,NPARAM_THERM_TTC*/
  double diff_mol_solute;                           /*!< Molecular diffusivity, for solute transport */
  double surf;                                      // Wet surface area of the river
  double width;                                     // Width of the river column
};

// NF 18/8/2021 please add a comment for each structure
/* DK 18/07/2021
 * Calculated thermal and solute parameters
 */
struct param_calc_ttc {
  double disp_face[NB_CARD_TTC][SUB_CARD_TTC];
  double lambda;
  double rhoc;
  double coeff[NCOEFF_TTC]; /*!< list of coefficient (terme dispersion and s)*/
};

/// Reoxygenation parameters DK 18/07/2021
struct reoxy_ttc /// AR MERCI DE METTRE DES COMMENTAIRES POUR EXPLIQUER A QUOI CA ERT
{
  double *debit_surverse; //(dim nele)*/
  double *frac_debit_oxy; /*! (dim nele)*/
  double *rd;
  double *rn;
  double osat;
};

// DK AR 24 08 2021 Structure containing the initial value and values stored after each iteration
// Update variable name from var to val, remove var_nk
struct val_ttc {
  double *val;     /*!< table of double of size nele, containing the concentration or temperature value for each element of the mesh*/
  double *val_old; /*!< table of var [Temperature,Solute] (dim nele)*/
};

/// Calculated fluxes are stored here DK 18/07/2021
/**\Struct flux_ttc
 *\brief Includes the heat fluxes of the species at each time step for the faces & subfaces.
 * Advection, Conduction, and total head fluxes are stored (Advection + Conduction).
 *
 */
struct flux_ttc {
  double totface[NB_CARD_TTC][SUB_CARD_TTC];  // Total flux
  double condface[NB_CARD_TTC][SUB_CARD_TTC]; // Conduction
  double advface[NB_CARD_TTC][SUB_CARD_TTC];  // Advection
  double adv_dVdt;                            // Advection due to rate of change of the transient groundwater storage
  double adv_Sink;                            // Advection due to flow rate of the sink term
  double adv_Source;                          // Advection due to flow rate of the Source term
  double dTdt;                                // Change of temperature per time step
  double qtot;
  double qface;
  double mb_transport; // The balance of the cell

}; // Une structure flux par espece (solute, ou heat)

/*!\Struct boundary_ttc
 * Structure: Boundary types, boundary values, number of boundaries by element DK 18/07/2021
 */
struct boundary_ttc {
  int nbound;  // vecteur donnant le nbre de face avec CL par element (dimension nele) DK change from pointer to single value 6/08/2021
  int name_bc; // vecteur donnant le nom CL par element (dimension nele) DK change from pointer to single value 6/08/2021
  int icl[NB_CARD_TTC][SUB_CARD_TTC];
  double valcl[NB_CARD_TTC][SUB_CARD_TTC];
  int type;  /*!< Type of boundary in [dirichlet,neumann,cauchy]*/
  int freq;  /*!< boundary frequency in [constant,variable]*/
  s_ft *pft; /*!< pointer to the values attributed to boundaries conditions. */
};

/*!\Struct aquitard_ttc
 * Structure: Boundary types, boundary values, number of boundaries by element DK 18/07/2021
 */
struct aquitard_ttc {
  int active; // Check if the aquitard is activated in the cell or not;
  // Aquitard
  int id;       // abs id of cell of which aquitard is below
  int nlayer;   // number of aquifer layer at which the aquitard is at the bottom of
  int id_neigh; // the neighbouring cell of the aquitard for the aquitard
  int id_intern;

  int id_ttc_above; // libttc id of the aquifer element above to do necessary modification in mat6
  int id_ttc_below; // libttc id of the aquifer element below to do necessary modification in mat6

  // Riverbed
  int abs_ele_id; // absolute id to fill up mat5 columns for the riverbed
  // Parameters filled in this variable from the input.y inside the pele of libMESH.
  double param[NPARAM_AQUITARD_TTC]; // Parameters of the aquitard that are read from input.y

  // pspecies
  double lambda[NVAR_AQUITARD_TTC]; // Lambda of the cell above, the aquitard and the cell below
  double thick[NVAR_AQUITARD_TTC];  // Thicknesses of above cell, aquitard, and the cell below

  double q;     // q Darcy flux is same in each location (m/s);
  double alpha; // alpha term in Kurylyk et al 2017 - Equation 11;

  double upsilon[NVAR_AQUITARD_TTC]; // Kurlyk et al 2017 - effective thermal conductivity

  double nb_peclet;                    // The calculation of the Peclet number according to Kurylyk et al 2017 - Equation 12;
  double flux_cond[NVAR_AQUITARD_TTC]; // The calculation of conductive heat flux in the aquitard using the Peclet number;
  double flux_adv[NVAR_AQUITARD_TTC];  // The calculation of advective heat flux in the aquitard using the Peclet number;
  double flux_tot[NVAR_AQUITARD_TTC];  // The calculation of total heat flux in the aquitard;

  double coeff_top;
  double coeff_bot;

  double temperature[NVAR_AQUITARD_TTC][SUB_CARD_TTC]; // Temperature of the aquitard & it could be also extended into solute transport
  double temperature_old;

  double flux_advection;
  double flux_conduction;
};

/*!\Struct source_ttc
 * Structure: Source terms
 */
struct source_ttc {
  int type;  /*!< Type of boundary in [dirichlet,neumann,cauchy]*/
  int freq;  /*!< boundary frequency in [constant,variable]*/
  s_ft *pft; /*!< pointer to the values attributed to bondaries conditions. This value is either the head(dirichlet) or the discharge(neumann) **/
  /*!< Parameter tracking the origin of the source flux imposed in case of multiple sources inputs on an aquifer element.
       Values in NORIGIN_SOURCE_AQ. LOCAL_SOURCE_AQ refers to isolated recharges or uptakes. */
  int origin;
};

// NF 18/8/2021 this is actually a geometry, mesh related structure
//  Geometry structure that does not change by species but by grid same in each simulation
struct neigh_ttc {
  int ivois[NB_CARD_TTC][SUB_CARD_TTC];      // identifiant voisins
  double delta_L[NB_CARD_TTC][SUB_CARD_TTC]; // Delta_L = distance centre maille et centre voisin
  int nvois;                                 // nbre de faces avec voisin
  int ncell_layer;                           // nbre de cell dedans la couche DK 26 01 2023 // Filled at CAW_fill_neighbor_one_species_aq in CaWaQS
                                             // int **ivois; //identifiant voisins
  // double **delta_L; //Delta_L = distance centre maille et centre voisin
};

/**
 * \struct counter_hydro of type s_count_hydro_aq
 * \brief This structure groups the counters of hydraulics properties
 */
struct counter_ttc {
  int *nele_type;      /*!< Number of boundaries*/
  s_id_io **elebound;  /*!< Layer and Elements id which are boundaries*/
  s_id_io **elesource; /*!< Layer and Elements id which get imposed */
  s_id_io *eleactive;  /*!< Layer and Elements id which are active */
  int nborder;         /*!< Number of BORDER elements. It is only used for the OPEN_BOUNDARY option */
  s_id_io ***p_eletype;
};

#define new_param_ttc() ((s_param_ttc *)malloc(sizeof(s_param_ttc)))
#define new_param_calc_ttc() ((s_param_calc_ttc *)malloc(sizeof(s_param_calc_ttc)))
#define new_reoxy_ttc() ((s_coef_roxy_ttc *)malloc(sizeof(s_coef_roxy_ttc)))
#define new_val_ttc() ((s_val_ttc *)malloc(sizeof(s_val_ttc)))
#define new_flux_ttc() ((s_flux_ttc *)malloc(sizeof(s_flux_ttc)))
#define new_boundary_ttc() ((s_boundary_ttc *)malloc(sizeof(s_boundary_ttc)))
#define new_neigh_ttc() ((s_neigh_ttc *)malloc(sizeof(s_neigh_ttc)))
#define new_aquitard_ttc() ((s_aquitard_ttc *)malloc(sizeof(s_aquitard_ttc)))
#define new_source_ttc() ((s_source_ttc *)malloc(sizeof(s_source_ttc)))
#define new_count_ttc() ((s_count_ttc *)malloc(sizeof(s_count_ttc)))
