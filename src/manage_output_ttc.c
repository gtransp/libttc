/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: manage_output_ttc.c
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "CHR.h"
#include "GC.h"
#include "IO.h"

#include "TTC.h"

void TTC_header_output(FILE *fpvar, int type) {
  fpvar = fopen("../Result/Output", "w+");
  switch (type) {
  case HEAT_TTC:
    fprintf(fpvar, "Time \t id \t Temperature [°C] \n");
    break;
  case SOLUTE_TTC:
    fprintf(fpvar, "Time \t id  \t Solute\n");
    break;
  }
  fclose(fpvar);
}

void TTC_header_output_steady(FILE *fpvar, int type) {
  fpvar = fopen("../Result/Output", "w+");
  switch (type) {
  case HEAT_TTC:
    fprintf(fpvar, "id \t Temperature [°C] \n");
    break;
  case SOLUTE_TTC:
    fprintf(fpvar, "id  \t Solute\n");
    break;
  }
  fclose(fpvar);
}

void TTC_header_output_MB(FILE *fpmb, int type) {
  fpmb = fopen("../Result/Output_MB", "w+");
  switch (type) {
  case HEAT_TTC:
    fprintf(fpmb, "Time \t id \t Tot_EST \t Tot_WEST \t Tot_NORTH \t Tot_SOUTH \t Cond_EST \t Cond_WEST \t Cond_NORTH \t Cond_SOUTH \t Adv_EST \t Adv_WEST \t Adv_NORTH \t Adv_SOUTH  \n");
    break;
  case SOLUTE_TTC:
    fprintf(fpmb, "Time \t id \t Tot_EST \t Tot_WEST \t Tot_NORTH \t Tot_SOUTH \t Cond_EST \t Cond_WEST \t Cond_NORTH \t Cond_SOUTH \t Adv_EST \t Adv_WEST \t Adv_NORTH \t Adv_SOUTH  \n");
    break;
  }
  fclose(fpmb);
}

void TTC_header_output_MB_steady(FILE *fpmb, int type) {
  fpmb = fopen("../Result/Output_MB", "w+");
  switch (type) {
  case HEAT_TTC:
    fprintf(fpmb, "id \t Tot_EST \t Tot_WEST \t Tot_NORTH \t Tot_SOUTH \t Cond_EST \t Cond_WEST \t Cond_NORTH \t Cond_SOUTH \t Adv_EST \t Adv_WEST \t Adv_NORTH \t Adv_SOUTH  \n");
    break;
  case SOLUTE_TTC:
    fprintf(fpmb, "id \t Tot_EST \t Tot_WEST \t Tot_NORTH \t Tot_SOUTH \t Cond_EST \t Cond_WEST \t Cond_NORTH \t Cond_SOUTH \t Adv_EST \t Adv_WEST \t Adv_NORTH \t Adv_SOUTH  \n");
    break;
  }
  fclose(fpmb);
}
// DK changes in s_var_ttc will bereflected here 17 08 2021
// p_var in the second argument is changed to plink because var > val & its moved into plink structure
void TTC_print_output_var(s_carac_ttc *pcarac_ttc, s_link_ttc *plink_ttc, double t, FILE *fpvar) {
  int i, nele;
  nele = pcarac_ttc->count[NELE_TTC];
  fpvar = fopen("../Result/Output", "a");
  for (i = 0; i < nele; i++) {
    fprintf(fpvar, "%lf \t %i \t %lf \n", t, i, plink_ttc->pspecies->pval_ttc->val[i]);
  }
  fclose(fpvar);
}

void TTC_print_output_var_steady(s_carac_ttc *pcarac_ttc, s_link_ttc *plink_ttc, FILE *fpvar) {
  int i, nele;
  nele = pcarac_ttc->count[NELE_TTC];
  fpvar = fopen("../Result/Output", "a");
  for (i = 0; i < nele; i++) {
    fprintf(fpvar, "%i \t %lf \n", i, plink_ttc->pspecies->pval_ttc->val[i]);
  }
  fclose(fpvar);
}

// DK 18 08 2021 removed param_calc_ttc as it is not used here
void TTC_print_output_MB(s_carac_ttc *pcarac_ttc, s_species_ttc *pspecies, double t, FILE *fpmb) {
  int i, nele, icard;
  int sub_icard;
  sub_icard = 0;
  double qtot_E, qtot_W, qtot_N, qtot_S;
  double qcondu_E, qcondu_W, qcondu_N, qcondu_S;
  double qadv_E, qadv_W, qadv_N, qadv_S;
  nele = pcarac_ttc->count[NELE_TTC];
  fpmb = fopen("../Result/Output_MB", "a");

  for (i = 0; i < nele; i++) {
    for (icard = 0; icard < NB_CARD_TTC; icard++) {
      if (icard == EAST_TTC) {
        qcondu_E = pspecies->p_flux_ttc[i]->condface[icard][sub_icard];
        qadv_E = pspecies->p_flux_ttc[i]->advface[icard][sub_icard];
        qtot_E = pspecies->p_flux_ttc[i]->totface[icard][sub_icard];
      }
      if (icard == WEST_TTC) {
        qcondu_W = pspecies->p_flux_ttc[i]->condface[icard][sub_icard];
        qadv_W = pspecies->p_flux_ttc[i]->advface[icard][sub_icard];
        qtot_W = pspecies->p_flux_ttc[i]->totface[icard][sub_icard];
      }
      if (icard == NORTH_TTC) {
        qcondu_N = pspecies->p_flux_ttc[i]->condface[icard][sub_icard];
        qadv_N = pspecies->p_flux_ttc[i]->advface[icard][sub_icard];
        qtot_N = pspecies->p_flux_ttc[i]->totface[icard][sub_icard];
      }
      if (icard == SOUTH_TTC) {
        qcondu_S = pspecies->p_flux_ttc[i]->condface[icard][sub_icard];
        qadv_S = pspecies->p_flux_ttc[i]->advface[icard][sub_icard];
        qtot_S = pspecies->p_flux_ttc[i]->totface[icard][sub_icard];
      }
    }

    fprintf(fpmb, "%lf \t %i \t %e \t %e \t %e \t %e \t %e \t %e \t %e \t %e \t %e \t %e \t %e \t %e\n", t, i, qtot_E, qtot_W, qtot_N, qtot_S, qcondu_E, qcondu_W, qcondu_N, qcondu_S, qadv_E, qadv_W, qadv_N, qadv_S);
  }
  fclose(fpmb);
}

void TTC_print_output_MB_steady(s_carac_ttc *pcarac_ttc, s_species_ttc *pspecies, FILE *fpmb) {
  int i, nele, icard, sub_icard;
  sub_icard = 0;
  double qtot_E, qtot_W, qtot_N, qtot_S;
  double qcondu_E, qcondu_W, qcondu_N, qcondu_S;
  double qadv_E, qadv_W, qadv_N, qadv_S;
  nele = pcarac_ttc->count[NELE_TTC];
  fpmb = fopen("../Result/Output_MB", "a");

  for (i = 0; i < nele; i++) {
    for (icard = 0; icard < NB_CARD_TTC; icard++) {
      if (icard == EAST_TTC) {
        qcondu_E = pspecies->p_flux_ttc[i]->condface[icard][sub_icard];
        qadv_E = pspecies->p_flux_ttc[i]->advface[icard][sub_icard];
        qtot_E = pspecies->p_flux_ttc[i]->totface[icard][sub_icard];
      }
      if (icard == WEST_TTC) {
        qcondu_W = pspecies->p_flux_ttc[i]->condface[icard][sub_icard];
        qadv_W = pspecies->p_flux_ttc[i]->advface[icard][sub_icard];
        qtot_W = pspecies->p_flux_ttc[i]->totface[icard][sub_icard];
      }
      if (icard == NORTH_TTC) {
        qcondu_N = pspecies->p_flux_ttc[i]->condface[icard][sub_icard];
        qadv_N = pspecies->p_flux_ttc[i]->advface[icard][sub_icard];
        qtot_N = pspecies->p_flux_ttc[i]->totface[icard][sub_icard];
      }
      if (icard == SOUTH_TTC) {
        qcondu_S = pspecies->p_flux_ttc[i]->condface[icard][sub_icard];
        qadv_S = pspecies->p_flux_ttc[i]->advface[icard][sub_icard];
        qtot_S = pspecies->p_flux_ttc[i]->totface[icard][sub_icard];
      }
    }
    fprintf(fpmb, "%i \t %e \t %e \t %e \t %e \t %e \t %e \t %e \t %e \t %e \t %e \t %e \t %e\n", i, qtot_E, qtot_W, qtot_N, qtot_S, qcondu_E, qcondu_W, qcondu_N, qcondu_S, qadv_E, qadv_W, qadv_N, qadv_S);
  }
  fclose(fpmb);
}
