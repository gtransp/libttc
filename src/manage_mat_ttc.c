/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: manage_mat_ttc.c
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "CHR.h"
#include "GC.h"
#include "IO.h"

#include "TTC.h"

int TTC_calc_lmat5(s_link_ttc *plink, int nele, FILE *fpout) {

  s_gc *pgc;
  int *mat5;
  int i, icard, sub_icard, id_neigh;
  int lmat5;
  pgc = plink->pspecies->pgc;
  mat5 = pgc->mat5; // methode CRS col_ind a l envers

  lmat5 = 0;

  for (i = 0; i < nele; i++) {
    for (icard = 0; icard < NB_CARD_TTC; icard++) {
      for (sub_icard = 0; sub_icard < SUB_CARD_TTC; sub_icard++) {
        id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];
        if (id_neigh != NONE_TTC) {
          lmat5 += 1;
        }
      }
    }
  }

  lmat5 += nele;
  // LP_printf(fpout, "In %s : length of mat5 = %d\n", __func__, lmat5);
  return lmat5;
}

int TTC_calc_lmat5_test(s_link_ttc *plink, int nele, int naquitard, int nriverbed, FILE *fpout) {
  s_gc *pgc;
  int *mat5;
  int i, icard, sub_icard, id_neigh;
  int lmat5;
  pgc = plink->pspecies->pgc;
  mat5 = pgc->mat5; // methode CRS col_ind a l envers
  lmat5 = 0;

  for (i = 0; i < nele; i++) {
    //        LP_printf(fpout,"inside %s, at iteration i: %d\n",__func__ , i);
    for (icard = 0; icard < NB_CARD_TTC; icard++)
      for (sub_icard = 0; sub_icard < SUB_CARD_TTC; sub_icard++) // SW 28/05/2018 pour confluence
      {
        //		   id_neigh=plink->pspecies->p_neigh_ttc[i]->ivois[icard];
        id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard]; // SW 28/05/2018 pour confluence //AR: MERCI DE NE PAS MODIFIER LE CODE SANS RESPECTER LE TRAVAIL REALISER EN AMONT. CETTE MODIFICATION N'EST PAS GENERIQUE ET EMPECHE LE BON FONCTIONNEMENT DE LA LIBRARIE SI LE CODE N'EST PAS COUPLE AVEC PROSE. merci d'ajouter un test en cas de couplage avec pose pour modifier cette structure,
                                                                   // ajouter la prise en compte de subface devrait être fait
        if (id_neigh != NONE_TTC) {
          lmat5 += 1;
        }
      }
  }
  // LP_printf(fpout," lenght of mat5 is %i, nele  %d,naquitard %d, nriv: %d \n",lmat5, nele, naquitard, nriverbed);

  if (plink->pspecies->pset->aquitard == AQUITARD_ON_TTC || plink->pspecies->pset->riverbed == AQUITARD_ON_TTC) {

    lmat5 += nele + naquitard * 3 + nriverbed * 2; // We include nriverbed 2 times (1x nriverbed, 1xnvoisriverbed (aquifer) cells
  } else {
    lmat5 += nele;
  }

  // LP_printf(fpout," lenght of mat5 is %i, nele  %d,naquitard %d, nriv: %d \n",lmat5, nele, naquitard, nriverbed);
  return lmat5;
}

void TTC_init_mat5_mat4(s_link_ttc *plink, int nele, FILE *fpout) {

  s_gc *pgc;
  int i, icard, loc_mat5, lmat5, ndl, neletot;
  int one = 1;
  int *mat4, *mat5;
  int id_neigh;
  int sub_icard;
  pgc = plink->pspecies->pgc;
  pgc->ndl = nele;
  pgc->lmat5 = TTC_calc_lmat5(plink, nele, fpout);
  pgc->mat5 = GC_create_mat_int(pgc->lmat5);
  pgc->mat4 = GC_create_mat_int(pgc->ndl + one);
  lmat5 = pgc->lmat5;
  ndl = pgc->ndl;
  loc_mat5 = 0;
  mat4 = pgc->mat4;
  mat5 = pgc->mat5;
  neletot = 0;

  for (i = 0; i < nele; i++) {

    for (icard = 0; icard < NB_CARD_TTC; icard++) {
      for (sub_icard = 0; sub_icard < SUB_CARD_TTC; sub_icard++) // SW 28/05/2018 pour confluence
      {
        // id_neigh=plink->pspecies->p_neigh_ttc[i]->ivois[icard];
        id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard]; // SW 28/05/2018 pour confluence //AR: MERCI DE NE PAS MODIFIER LE CODE SANS RESPECTER LE TRAVAIL REALISER EN AMONT. CETTE MODIFICATION N'EST PAS GENERIQUE ET EMPECHE LE BON FONCTIONNEMENT DE LA LIBRARIE SI LE CODE N'EST PAS COUPLE AVEC PROSE. merci d'ajouter un test en cas de couplage avec pose pour modifier cette structure,
                                                                   // ajouter la prise en compte de subface devrait être fait

        if (id_neigh != NONE_TTC) {
          if (loc_mat5 < pgc->lmat5) {
            pgc->mat5[loc_mat5] = id_neigh + 1;
            loc_mat5++;
          } else {
            LP_error(fpout, "depassement de tableau dans mat5\n");
          }
        }
      }
    }

    if (loc_mat5 < pgc->lmat5) {
      mat5[loc_mat5] = i + 1;
      loc_mat5++;
    } else {
      //	      LP_printf(fpout,"loc_mat5 = %d dim mat5 = %d\n",loc_mat5,pgc->lmat5);
      LP_error(fpout, "depassement de tableau dans mat5\n");
    }
    if (neletot < pgc->ndl) {
      mat4[++neletot] = loc_mat5;
    } else {
      LP_error(fpout, "depassement de tableau dans mat4\n");
    }
  }
}

void TTC_init_mat5_mat4_libaq(s_link_ttc *plink, int nele, FILE *fpout) { // NG : 22/06/2023 Revised function

  s_gc *pgc;
  int i, icard, loc_mat5, lmat5, ndl, neletot, nb_sub, id_neigh, sub_icard;
  int one = 1;
  int *mat4, *mat5, *nsub;
  pgc = plink->pspecies->pgc;
  pgc->ndl = nele;
  pgc->lmat5 = TTC_calc_lmat5(plink, nele, fpout);
  pgc->mat5 = GC_create_mat_int(pgc->lmat5);
  pgc->mat4 = GC_create_mat_int(pgc->ndl + one);
  lmat5 = pgc->lmat5;
  ndl = pgc->ndl;
  loc_mat5 = 0;
  mat4 = pgc->mat4;
  mat5 = pgc->mat5;
  neletot = 0;

  // LP_printf(fpout,"lmat5 = %d\n",pgc->lmat5);

  for (i = 0; i < nele; i++) {

    for (icard = 0; icard < NB_CARD_TTC; icard++) {

      nb_sub = plink->nsub[i][icard];

      switch (nb_sub) {
      case 0: {
        sub_icard = 0;
        id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];
        // LP_printf(fpout,"element %d card %s sub_card %d --> neigh_id %d\n",i+1,TTC_param_card(icard),sub_icard,id_neigh); // NG check
        if (id_neigh != NONE_TTC) { // DK AR 24 11 2021 test  for neighbour mismatch after estimating id_neigh with ABS_MSH
          id_neigh = id_neigh - 1;
          if (loc_mat5 < pgc->lmat5) {
            pgc->mat5[loc_mat5] = id_neigh + 1;
            loc_mat5++;
          } else {
            LP_error(fpout, "In %s, function %s at line %d : Out-of-bounds when filling mat5 array.\n", __FILE__, __func__, __LINE__);
          }
        }
        break;
      }
      default: {
        for (sub_icard = 0; sub_icard < nb_sub; sub_icard++) {
          id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];
          // LP_printf(fpout,"element %d card %s sub_card %d --> neigh_id %d\n",i+1,TTC_param_card(icard),sub_icard,id_neigh); // NG check

          if (id_neigh != NONE_TTC) {
            id_neigh = id_neigh - 1;

            if (loc_mat5 < pgc->lmat5) {
              pgc->mat5[loc_mat5] = id_neigh + 1;
              loc_mat5++;
            } else {
              LP_error(fpout, "In %s, function %s at line %d : Out-of-bounds when filling mat5 array.\n", __FILE__, __func__, __LINE__);
            }
          }
        }
        break;
      }
      }
    } // fin icard

    if (loc_mat5 < pgc->lmat5) {
      mat5[loc_mat5] = i + 1;
      loc_mat5++;
    } else {
      LP_error(fpout, "depassement de tableau dans mat5 : loc_mat5 = %d dim mat5 = %d\n", loc_mat5, pgc->lmat5);
    }

    if (neletot < pgc->ndl) {
      mat4[++neletot] = loc_mat5;
    } else {
      LP_error(fpout, "depassement de tableau dans mat4\n");
    }
  }
  // LP_printf(fpout,"loc_mat5 = %d\n",loc_mat5);
}

// mat5 mat4 fill with the aquitard calculations
void TTC_init_mat5_mat4_libaq_test(s_link_ttc *plink, int nele, int naquitard, int nriverbed, FILE *fpout) {
  s_gc *pgc;
  int i, icard, loc_mat5, lmat5, ndl, neletot, nb_sub, id_neigh, sub_icard;
  int one = 1;
  int aquitard_id_above, aquitard_id_below, aquitard_id_lay_neigh;
  int ncell_layer = 0;
  int *mat4, *mat5, *nsub;
  pgc = plink->pspecies->pgc;

  ndl = pgc->ndl;
  lmat5 = pgc->lmat5;

  mat4 = pgc->mat4;
  mat5 = pgc->mat5;

  loc_mat5 = 0;
  neletot = 0;

  LP_printf(fpout, "lmat5 = %d\n", pgc->lmat5);

  for (i = 0; i < nele; i++) {
    ncell_layer = plink->p_neigh_ttc[i]->ncell_layer; // Find the number of cells in mesh grid DK 26 01 2023;
    for (icard = 0; icard < NB_CARD_TTC; icard++) {
      nb_sub = plink->nsub[i][icard];

      switch (nb_sub) {
      case 0: {
        sub_icard = 0;
        id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];
        if (id_neigh != -99) { // DK AR 24 11 2021 test  for neighbour mismatch after estimating id_neigh with ABS_MSH
          id_neigh = id_neigh - 1;
        }
        //                    LP_printf(fpout,"Inside %s, i = %d, ivois = %d ncell_layer = %d\n",__func__, i, plink->p_neigh_ttc[i]->ivois[icard][sub_icard],ncell_layer);
        if (id_neigh != NONE_TTC) {
          if (loc_mat5 < pgc->lmat5) {

            if (icard == TOP_TTC && i < nele) {
              pgc->mat5[loc_mat5] = nele + i - ncell_layer + 1;
              //                                    LP_printf(fpout,"Filling mat5, loc_mat5: %d, id_neigh = %d FACE %s \n", loc_mat5, pgc->mat5[loc_mat5],
              //                                              TTC_param_card(icard)); // NG check
            } else if (icard == BOTTOM_TTC && i < naquitard) {
              pgc->mat5[loc_mat5] = nele + i + 1;
              //                                    LP_printf(fpout,"Filling mat5, loc_mat5: %d, id_neigh = %d FACE %s \n", loc_mat5, pgc->mat5[loc_mat5],
              //                                              TTC_param_card(icard)); // NG check

            } else {
              pgc->mat5[loc_mat5] = id_neigh + 1;
              //                                    LP_printf(fpout,"Filling mat5, loc_mat5: %d, id_neigh = %d \n", loc_mat5, pgc->mat5[loc_mat5]);
            }

            loc_mat5++;

          } else
            LP_error(fpout, "depassement de tableau dans mat5\n");
        }

        //                        LP_printf(fpout,"loc_mat5 = %d\n",loc_mat5);
        break;
      } // fin case 0
      default: {
        for (sub_icard = 0; sub_icard < nb_sub; sub_icard++) // AR 20/11/2020 nombre de subface par face
        {
          id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];
          if (id_neigh != -99) { // DK AR 24 11 2021 test  for neighbour mismatch after estimating id_neigh with ABS_MSH
            id_neigh = id_neigh - 1;
          }
          if (id_neigh != NONE_TTC) {
            if (loc_mat5 < pgc->lmat5) {
              if (icard == TOP_TTC && i < nele) {
                pgc->mat5[loc_mat5] = nele - ncell_layer + i + 1;
                //                                    LP_printf(fpout,"Filling mat5, loc_mat5: %d, id_neigh = %d FACE %s \n", loc_mat5, pgc->mat5[loc_mat5],
                //                                              TTC_param_card(icard));
              } else if (icard == BOTTOM_TTC && i < naquitard) {
                pgc->mat5[loc_mat5] = nele + i + 1;
                //                                    LP_printf(fpout,"Filling mat5, loc_mat5: %d, id_neigh = %d FACE %s \n", loc_mat5, pgc->mat5[loc_mat5],
                //                                              TTC_param_card(icard));
              } else {
                pgc->mat5[loc_mat5] = id_neigh + 1;
                //                                    LP_printf(fpout,"Filling mat5, loc_mat5: %d, id_neigh = %d \n", loc_mat5, pgc->mat5[loc_mat5]);
              }
              loc_mat5++;
            } else
              LP_error(fpout, "depassement de tableau dans mat5\n");
          }
        }

        break;
      } // fin case 2
      } // fin switch
    }   // fin icard

    if (loc_mat5 < pgc->lmat5) {
      mat5[loc_mat5] = i + 1;
      //                LP_printf(fpout,"Filling mat5 Diago  loc_mat5: %d, id_cell: %d \n", loc_mat5, mat5[loc_mat5]); // NG check
      loc_mat5++;
    } else {
      LP_error(fpout, "depassement de tableau dans mat5 : loc_mat5 = %d dim mat5 = %d\n", loc_mat5, pgc->lmat5);
    }

    // Fill mat4
    if (neletot < pgc->ndl)
      mat4[++neletot] = loc_mat5;
    else
      LP_error(fpout, "depassement de tableau dans mat4\n");

    //           LP_printf(fpout,"loc_mat5 = %d, neletot= %d\n",loc_mat5, neletot ); // DK check

  } // fin loop nele

  if (plink->pspecies->pset->aquitard == AQUITARD_ON_TTC) {
    for (int j = 0; j < naquitard; j++) {
      if (plink->pspecies->p_aquitard_ttc[j] != NULL) { // if the aquitard is not null we incrase

        aquitard_id_above = plink->pspecies->p_aquitard_ttc[j]->id;
        aquitard_id_below = plink->pspecies->p_aquitard_ttc[j]->id_neigh;
        //                LP_printf(fpout,"aquitard_id_above: %d, aquitard_id_below: %d\n",aquitard_id_above, aquitard_id_below ); // DK check

        // mat4 part
        if (loc_mat5 < pgc->lmat5) {
          mat5[loc_mat5] = aquitard_id_above;
          //                    LP_printf(fpout,"Filling mat5 AQUITARD ABOVE, loc_mat5: %d, id_neigh = %d \n", loc_mat5, mat5[loc_mat5]); // NG check
          loc_mat5++;
        } else
          LP_error(fpout, "depassement de tableau dans mat5 : loc_mat5 = %d dim mat5 = %d\n", loc_mat5, pgc->lmat5);

        // mat4 part
        if (loc_mat5 < pgc->lmat5) {
          mat5[loc_mat5] = aquitard_id_below;
          //                    LP_printf(fpout,"Filling mat5  AQUITARD BELOW, loc_mat5: %d, id_neigh = %d \n", loc_mat5, mat5[loc_mat5]); // NG check
          loc_mat5++;
        } else
          LP_error(fpout, "depassement de tableau dans mat5 : loc_mat5 = %d dim mat5 = %d\n", loc_mat5, pgc->lmat5);

        // mat4 part
        if (loc_mat5 < pgc->lmat5) {
          mat5[loc_mat5] = nele + j + 1;
          //                    LP_printf(fpout,"Filling mat5  AQUITARD DIAGONL, loc_mat5: %d, id_cell = %d \n", loc_mat5, mat5[loc_mat5]); // NG check

          loc_mat5++;
        } else {
          LP_error(fpout, "depassement de tableau dans mat5 : loc_mat5 = %d dim mat5 = %d\n", loc_mat5, pgc->lmat5);
        }

        // Fill mat4
        if (neletot < pgc->ndl)
          mat4[++neletot] = loc_mat5;
        else
          LP_error(fpout, "depassement de tableau dans mat4\n");
      }
    } // fin loop naquitard

  } // fin aquitard
    // Loc_mat5 is at nele+nvois+naquitard+nvois_aquitard//
  if (plink->pspecies->pset->riverbed == AQUITARD_ON_TTC) {

    for (int j = 0; j < nriverbed; j++) {
      if (plink->pspecies->p_riverbed_ttc[j] != NULL) { // if the aquitard is not null we incrase
                                                        //                LP_printf(fpout,"Filling mat5 riverbed, j is %d \n", j); // NG check

        // aquitard_id_above = plink->pspecies->p_riverbed_ttc[j]->id;
        aquitard_id_below = plink->pspecies->p_riverbed_ttc[j]->abs_ele_id;

        //                LP_printf(fpout,"aquitard_id_above: %d, aquitard_id_below: %d\n",aquitard_id_above, aquitard_id_below ); // DK check

        // mat4 part
        //                if(loc_mat5<pgc->lmat5)
        //                {
        //                    mat5[loc_mat5]= nele+naquitard+nriverbed+j; // The river cells above
        //                    LP_printf(fpout,"Filling mat5 riverbed, loc_mat5: %d, id_neigh = %d \n", loc_mat5, mat5[loc_mat5]); // NG check
        //                    loc_mat5++;
        //                }
        //                else
        //                    LP_error(fpout,"depassement de tableau dans mat5 : loc_mat5 = %d dim mat5 = %d\n",loc_mat5,pgc->lmat5);

        // mat4 part
        if (loc_mat5 < pgc->lmat5) {
          mat5[loc_mat5] = aquitard_id_below;
          //                    LP_printf(fpout,"Filling mat5 riverbed, loc_mat5: %d, id_neigh = %d \n", loc_mat5, aquitard_id_below); // NG check
          loc_mat5++;
        } else
          LP_error(fpout, "depassement de tableau dans mat5 : loc_mat5 = %d dim mat5 = %d\n", loc_mat5, pgc->lmat5);

        // mat4 part
        if (loc_mat5 < pgc->lmat5) {
          mat5[loc_mat5] = nele + naquitard + j + 1;
          //                    LP_printf(fpout,"Filling mat5 riverbed, loc_mat5: %d, id_neigh = %d \n", loc_mat5, mat5[loc_mat5]); // NG check

          loc_mat5++;
        } else {
          LP_error(fpout, "depassement de tableau dans mat5 : loc_mat5 = %d dim mat5 = %d\n", loc_mat5, pgc->lmat5);
        }

        // Fill mat4
        if (neletot < pgc->ndl) {
          mat4[++neletot] = loc_mat5;
          //                    LP_printf(fpout,"Filling mat4 riverbed, neletot: %d, loc_mat5 = %d \n", neletot, loc_mat5); // NG check
        } else {
          LP_error(fpout, "depassement de tableau dans mat4\n");
        }
      }
    } // fin loop nriverbed
  }   // fin if riverbed active

  // LP_printf(fpout,"loc_mat5 = %d\n",loc_mat5); // DK check
} // fin TTC_init

/**\fn void TTC_init_sol_vec(s_gc *pgc)
 *\brief function called in solve_aq to initialize the solution vector
 *\return s_ft *
 *
 */
void TTC_init_sol_vec(s_gc *pgc) {

  if (pgc->x != NULL) {
    free(pgc->x);
    pgc->x = NULL;
  }

  pgc->x = GC_create_mat_double(pgc->ndl);
}

/**\fn void TTC_init_sol_vec(s_gc *pgc)
 *\brief function called in solve_ttc to initialize the solution vector
 *\return s_ft *
 *
 */
void TTC_init_RHS(s_gc *pgc) {

  if (pgc->b != NULL) {
    free(pgc->b);
    pgc->b = NULL;
  }

  pgc->b = GC_create_mat_double(pgc->ndl);
}

void TTC_free_mat6(s_gc *pgc) {

  if (pgc->mat6 != NULL) {
    free(pgc->mat6);
    pgc->mat6 = NULL;
  }
}

void TTC_free_mat4(s_gc *pgc) {

  if (pgc->mat4 != NULL) {
    free(pgc->mat4);
    pgc->mat4 = NULL;
  }
}

////// Modified version to fix the nested, DK AR, the correct version is att line 969 TEST VERSION DK 18 10 2021
//////Fonction qui construit mat6 et b
// void TTC_no_cap_libaq(s_carac_ttc *pcarac_ttc,s_link_ttc *plink, s_param_calc_ttc **p_param_calc_ttc,s_species_ttc *pspecies,int i,int tstep,FILE *fpout) {
//     s_gc *pgc;
//     double *mat6, *b, dist, delta_L, *var, u;
//     int tcl, type, nb_sub;
//     int *nsub;
//     int icard, id_neigh;
//     int *mat4, *mat5, ra, rd, nvois;
//     double theta, dlip, rhow, cw, rhowcw, cl_val;
//     int nvois_ele_i = 0 ;
//     int sub_icard; // SW 28/05/2018
//     int id_neigh1, id_neigh2;
//     double u_east, u_west;
//     double u_id_neigh1, u_id_neigh2;
//     tstep=tstep-1;
//     LP_printf(fpout, "Entering %s, tstep is %d\n", __func__, tstep); // DK check
//
//
//     pgc = pspecies->pgc;
//     type = pspecies->pset->type;
//     var = pspecies->pval_ttc->val; // tableau contenant la variable etudiee (temperature ou solute)
//     dist = pspecies->p_param_ttc[i]->param_syst[SIZE_TTC]; // taille de la maille
//     //dist = 1;
//     mat6 = pgc->mat6; //donne val
//     mat5 = pgc->mat5; //donne l'identifiant de chaque maille et de leur voisine
//     mat4 = pgc->mat4; //permet de connaitre le nombre de voisin pour chaque maille
//     b = pgc->b; //donne b
//
////     LP_printf(fpout,"In %s : Var[%d] = %f\n",__func__,i,b[i]); // Check NG pour la première itération --> OK
//
//    nvois = plink->p_neigh_ttc[i]->nvois;
//    theta = pcarac_ttc->theta; // parametre de semi implicite
//    if (type == HEAT_TTC) {
//        rhow = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][RHO_TTC];
//        cw = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][HEAT_CAP_TTC];
//        rhowcw = rhow * cw;
//    } else if (type == SOLUTE_TTC) {
//        rhowcw = 1;
//    }
//    ra = pspecies->pset->calc_process[ADVECTION_TTC]; // terme advectif (YES or NO)
//    rd = pspecies->pset->calc_process[DIFFUSION_TTC]; // terme dispersif (YES or NO)
////    LP_printf(fpout, "nele = %d, number_neighbour: %d, nvois_ele_i: %d \n", i, nvois, nvois_ele_i); // DK check
//    LP_printf(fpout, "advection = %d, diffusion: %d , yes : %d \n", ra, rd,YES_TS); // DK check
////    LP_printf(fpout, "nele = %d, binit: %e \n", i, b[i]); // DK check
//    for (icard = 0; icard < NB_CARD_TTC; icard++) {
//        nb_sub = plink->nsub[i][icard];
//        LP_printf(fpout, "nele = %d, icard: %d, nb_sub: %d \n", i, icard, nb_sub); // DK check
//
//        switch (nb_sub) { // line 276  should start from
//            default: {
//                //LP_printf(fpout, "enter case: 0\n" );
//                sub_icard = 0;
//                cl_val = 0;
//                //dist = plink->pspecies->p_neigh_ttc[i]->delta_L[icard][sub_icard];
//                //dist = plink->pspecies->p_neigh_ttc->pdelta_dist[i].delta_dist[icard][sub_icard];
//
//                tcl      =  pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];
//                id_neigh =  plink->p_neigh_ttc[i]->ivois[icard][sub_icard];
//                delta_L  =  plink->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // distance entre le centre de la maille et de son voisin
//                dlip     =  p_param_calc_ttc[i]->disp_face[icard][sub_icard]; //
////                LP_printf(fpout, "inside func: %s, iteration is %d, dlip is %e \n",__func__, i, dlip);
//
//
//                LP_printf(fpout, "id_neigh for ele: %d , icard: %d , id_neigh: %d, nvois= %d \n", i, icard, id_neigh, nvois);
//
//                /*LP_printf(fpout, "condition_limit: %d, id_neigh = %d, delta_L: %e, dlip = %e, cl_val = %e \n", tcl,
//                          id_neigh, delta_L, dlip, cl_val); // DK check*/
//
//                //Advectif term si on considere la partie advectif de lequation
//                if (ra==YES_TS){
//
//                    LP_printf(fpout, "Entering advection in func: %s\n",__func__ );
//                    u=plink->uface[i][icard][sub_icard]; //// vitesse de Darcy
//                    LP_printf(fpout, "velocity in m mat is %e\n", u);
//                    switch(icard){
//                        case EAST_TTC:{
//                            if(u < 0.0) { //fill the diagonal value
//
//                                u_west = plink->uface[i][WEST_TTC][sub_icard];
//                                LP_printf(fpout, "i = %d, id_neigh = %d,  u is %e, u_west is %e",i, id_neigh, u, u_west);
//                                //mat6[mat4[i+1]-1] += theta*u/dist;
//                                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta*u/dist;
//
//
//                                //                                mat6[mat4[i+1] - 1] += theta*u/dist + theta*(u - u_west)/dist;
//                                //b[i] += -((1-theta)*u/dist)*var[i];
////                                b[i] += -((1-theta)*u/dist)*var[i] + (1-theta)*var[i]*(u - u_west)/dist;
//                                LP_printf(fpout,
//                                          "EAST_TTC bound: %d, b: %e, mat6_maille: %e, dlip: %e, dist: %e, nmaille: %d, var = %e, velo East %f, velo West %f \n",
//                                          tcl, b[i], mat6[mat4[i + 1] - 1], dlip, dist, mat4[i + 1] - 1, var[i], u, u_west); // DK check
////                                nvois_ele_i++;
//                                LP_printf(fpout,
//                                          "NOF boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
//                            } else {
//                                switch (tcl) {
//                                    case NO_BOUND_TTC:{
//                                        //face active //fill the value for neighbor
////                                        if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard][0]!=DIRI_CENTER_TTC) {
//                                        LP_printf(fpout, "ADV FACE active no bound i = %d, id_neigh = %d,  u is %e, u_west is %e",i, id_neigh, u, u_west);
//
//                                            mat6[mat4[i+1]-1] += -theta*u/dist;
////                                            mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta*u/dist;
////                                            LP_printf(fpout,
////                                                      "NOF bound: %d, b: %e, mat6_voisin: %e, dlip: %e, dist: %e, nvois: %d, var[idneigh]: %e \n",
////                                                      tcl, b[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], dlip, dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh]); // DK check
//                                            b[i] += -(1-theta)*u/dist*var[id_neigh];
////                                        }
////                                        nvois_ele_i++;
//                                        LP_printf(fpout,
//                                                  "NOF boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
//                                        break;
//                                    }
//                                    case DIRI_FACE_TTC:{
//                                        //face active //bound adv
//                                        if (pcarac_ttc->regime == TRANSIENT_TTC) {
//                                            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                        } else {
//                                            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                        }
//                                        mat6[mat4[i+1]-1] += -theta*u/dist;
//                                        b[i] += -theta*cl_val*u/dist;
//                                        b[i] += -((1-theta)*cl_val*u/dist);
//
////                                        nvois_ele_i++;
//                                        LP_printf(fpout,
//                                                  "Dirif boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
//                                        break;
//                                    }
//                                }
//                            }
//                            break;
//                        }
//
//
//                        case WEST_TTC:{
//                            if(u < 0.0) { //fill the diagonal value
//
//                                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] +=  theta*u/dist;
//
////                                mat6[mat4[i+1]-1] += theta*u/dist;
////                                b[i] += -((1-theta)*u/dist)*var[i];
//                                LP_printf(fpout,
//                                          "WEST_TTC bound: %d, b: %e, mat6_maille: %e, dlip: %e, dist: %e, nmaille: %d, var = %e \n",
//                                          tcl, b[i], mat6[mat4[i + 1] - 1], dlip, dist, mat4[i + 1] - 1, var[i]); // DK check
//
////                                nvois_ele_i++;
//                                LP_printf(fpout,
//                                          "NOF boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
//                            }
//                            else
//                            {
//                                switch (tcl) {
//                                    case NO_BOUND_TTC:{
//                                        //face active
////                                        if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard][0]!=DIRI_CENTER_TTC) {
//                                            //mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - theta*u/dist;
//                                            u_east = plink->uface[i][EAST_TTC][sub_icard];
//                                            mat6[mat4[i+1]-1] += -theta*u/dist;
//                                            mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += - theta*u_east/dist;
//                                            //b[i]+= (1-theta)*u/dist*var[id_neigh];
//                                            b[i]+= (1-theta)*u/dist*var[id_neigh];
////                                            LP_printf(fpout,
////                                                      "NOF bound: %d, b: %e, mat6_voisin: %e, dlip: %e, dist: %e, nvois: %d, var[idneigh]: %e \n",
////                                                      tcl, b[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], dlip, dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh]); // DK check
////                                        }
////                                        nvois_ele_i++;
//                                        LP_printf(fpout,
//                                                  "NOF boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
//                                        break;
//                                    }
//                                    case DIRI_FACE_TTC:{
//                                        if (pcarac_ttc->regime == TRANSIENT_TTC) {
//                                            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                        } else {
//                                            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                        }
//                                        LP_printf(fpout, "ADV FACE active DIRI i = %d, id_neigh = %d,  u is %e, u_west is %e",i, id_neigh, u, u_west);
//
//                                        mat6[mat4[i+1]-1] += -theta*u/dist;
//                                        b[i] += theta*cl_val*u/dist;
//                                        b[i] += ((1-theta)*cl_val*u/dist);
//
////                                        nvois_ele_i++;
//                                        LP_printf(fpout,
//                                                  "Dirif boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
//
//                                        break;
//                                    }
//                                }
//                            }
//                            break; //case tcl West
//                        }
//
//                        case NORTH_TTC:{
//                            if(u < 0.0) { //fill the diagonal value
////                                LP_printf(fpout, "case NORTH_TTC u > 0\n");
//////                                mat6[mat4[i+1]-1] += -theta*u/dist;
////                                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta * u / dist;
////
////                                LP_printf(fpout, "ADV FACE active NO bound i = %d, id_neigh = %d,  u is %e",i, id_neigh, u );
////
//////                                b[i] += -((1-theta)*u/dist)*var[i];
////                                LP_printf(fpout,
////                                          "NOF bound: %d, b: %e, mat6_maille: %e, dlip: %e, dist: %e, nmaille: %d, var = %e , V NORTH_TTC %f\n",
////                                          tcl, b[i], mat6[mat4[i + 1] - 1], dlip, dist, mat4[i + 1] - 1, var[i], u ); // DK check
////
//////                                nvois_ele_i++;
////                                LP_printf(fpout,
////                                          "NOF boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
//                                switch (tcl) {
//                                    case NO_BOUND_TTC:{
//                                        //face active //fill the value for neighbor
////                                        if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard][0]!=DIRI_CENTER_TTC) {
//                                        LP_printf(fpout, "ADV FACE active NO bound i = %d, id_neigh = %d,  u is %e",i, id_neigh, u );
//
////                                        mat6[mat4[i+1]-1] += theta*u/dist;
//                                            mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u / dist;
////                                        b[i] += -(1 - theta) * u / dist * var[id_neigh];
//                                        LP_printf(fpout,
//                                                  "NOF bound: %d, b: %e, mat6_voisin: %e, dlip: %e, dist: %e, nvois: %d, var[idneigh]: %e, V N NO_BOUND : %f \n",
//                                                  tcl, b[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], dlip,
//                                                  dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh],
//                                                  u); // DK check
////                                        }
////                                        nvois_ele_i++;
//                                        LP_printf(fpout,
//                                                  "NOF boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
//                                        break;
//                                    }
//                                    case DIRI_FACE_TTC:{
//                                        //face active //bound adv
//                                        if (pcarac_ttc->regime == TRANSIENT_TTC) {
//                                            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                        } else {
//                                            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                        }
//
//                                        mat6[mat4[i+1]-1] += theta*u/dist;
//                                        b[i] +=  theta*cl_val*u/dist;
//
//                                        b[i] += - ((1-theta)*cl_val*u/dist);
//                                        LP_printf(fpout,
//                                                  "DIRIF boundary adv: %e, mat5: %d, dist= %e,  mat6_voisin: %e, velocity: %e, nmaille: %d, b : %e, val %e\n",
//                                                  cl_val, mat5[i], dist, mat6[mat4[i + 1] - 1], u, mat4[i + 1] - 1 , b[i], cl_val); // DK check
//                                        LP_printf(fpout, "BC NORTH_TTC %e \n", cl_val);
////                                        nvois_ele_i++;
//                                        LP_printf(fpout,
//                                                  "Dirif boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
//                                        break;
//                                    }
//                                }
//
//                            }
//                            else
//                            {  LP_printf(fpout, "case NORTH_TTC u < 0\n");
//                                switch (tcl) {
//                                    case NO_BOUND_TTC:{
//                                        //face active //fill the value for neighbor
////                                        if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard][0]!=DIRI_CENTER_TTC) {
//                                        LP_printf(fpout, "ADV FACE active NO bound i = %d, id_neigh = %d,  u is %e",i, id_neigh, u );
//
//                                            mat6[mat4[i+1]-1] += theta*u/dist;
////                                            mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u / dist;
//                                            b[i] += -(1 - theta) * u / dist * var[id_neigh];
//                                            LP_printf(fpout,
//                                                      "NOF bound: %d, b: %e, mat6_voisin: %e, dlip: %e, dist: %e, nvois: %d, var[idneigh]: %e, V N NO_BOUND : %f \n",
//                                                      tcl, b[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], dlip,
//                                                      dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh],
//                                                      u); // DK check
////                                        }
////                                        nvois_ele_i++;
//                                        LP_printf(fpout,
//                                                  "NOF boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
//                                        break;
//                                    }
//                                    case DIRI_FACE_TTC:{
//                                        //face active //bound adv
//                                        if (pcarac_ttc->regime == TRANSIENT_TTC) {
//                                            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                        } else {
//                                            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                        }
//
//                                        mat6[mat4[i+1]-1] += theta*u/dist;
//                                        b[i] +=  theta*cl_val*u/dist;
//
//                                        b[i] += - ((1-theta)*cl_val*u/dist);
//                                        LP_printf(fpout,
//                                                  "DIRIF boundary adv: %e, mat5: %d, dist= %e,  mat6_voisin: %e, velocity: %e, nmaille: %d, b : %e, val %e\n",
//                                                  cl_val, mat5[i], dist, mat6[mat4[i + 1] - 1], u, mat4[i + 1] - 1 , b[i], cl_val); // DK check
//                                        LP_printf(fpout, "BC NORTH_TTC %e \n", cl_val);
////                                        nvois_ele_i++;
//                                        LP_printf(fpout,
//                                                  "Dirif boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
//                                        break;
//                                    }
//                                }
//                            }
//                            break; //case tcl North
//                        }
//
//
//                        case SOUTH_TTC:{
//                            if(u < 0.0) { //fill the diagonal value
////                                LP_printf(fpout, "case SOUTH  u < 0\n");
////
////                                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta * u / dist;
////
//////                                mat6[mat4[i+1]-1] += -theta*u/dist;
//////                                b[i] += ((1-theta)*u/dist)*var[i];
////                                LP_printf(fpout,
////                                          "NOF bound: in advective %d, b: %e, mat6_maille: %e, dlip: %e, dist: %e, nmaille: %d, var = %e , V south TTC %f\n",
////                                          tcl, b[i], mat6[mat4[i + 1] - 1], dlip, dist, mat4[i + 1] - 1, var[i], u ); // DK check
////
//////                                nvois_ele_i++;
////                                LP_printf(fpout,
////                                          "NOF boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
//                                switch (tcl) {
//                                    case NO_BOUND_TTC:  //face active
////                                        if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard][0]!=DIRI_CENTER_TTC) {
////                                        mat6[mat4[i+1]-1] += theta*u/dist;
//                                            mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta * u / dist;
////                                        b[i] += (1 - theta) * u / dist * var[id_neigh];
//                                        LP_printf(fpout,
//                                                  "NOF bound in advective: %d, b: %e, mat6_voisin: %e, velocity: %e, dist: %e, nvois: %d, var[idneigh]: %e , mat6 = %e\n",
//                                                  tcl, b[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], u,
//                                                  dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh],
//                                                  -theta * u / dist); // DK check
////                                        }
////                                        nvois_ele_i++;
//                                        LP_printf(fpout,
//                                                  "NOF boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
//                                        break;
//                                    case DIRI_FACE_TTC:{
//                                        if (pcarac_ttc->regime == TRANSIENT_TTC) {
//                                            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                        } else {
//                                            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                        }
//                                        LP_printf(fpout, "BC SOUTH_TTC %e \n", cl_val);
//                                        mat6[mat4[i+1]-1] += theta*u/dist;
//                                        b[i] += theta*cl_val*u/dist;
//
//                                        b[i] += ((1-theta)*cl_val*u/dist);
////                                        nvois_ele_i++;
//
//                                        LP_printf(fpout,
//                                                  "DIRIF boundary: %e, mat5: %d, dist= %e,  mat6_voisin: %e, velocity: %e, nmaille: %d, b : %e, val %e\n",
//                                                  cl_val, mat5[i], dist, mat6[mat4[i + 1] - 1], u, mat4[i + 1] - 1 , b[i], cl_val); // DK check
//                                        LP_printf(fpout,
//                                                  "Dirif boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
//                                        break;
//                                    }
//
//                                }
//                            }
//                            else
//                            {   LP_printf(fpout, "case SOUTH  u > 0\n");
//                                switch (tcl) {
//                                    case NO_BOUND_TTC:  //face active
////                                        if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard][0]!=DIRI_CENTER_TTC) {
//                                            mat6[mat4[i+1]-1] += theta*u/dist;
////                                            mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u / dist;
//                                            b[i] += (1 - theta) * u / dist * var[id_neigh];
//                                            LP_printf(fpout,
//                                                      "NOF bound in advective: %d, b: %e, mat6_voisin: %e, velocity: %e, dist: %e, nvois: %d, var[idneigh]: %e , mat6 = %e\n",
//                                                      tcl, b[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], u,
//                                                      dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh],
//                                                      -theta * u / dist); // DK check
////                                        }
////                                        nvois_ele_i++;
//                                        LP_printf(fpout,
//                                                  "NOF boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
//                                        break;
//                                    case DIRI_FACE_TTC:{
//                                        if (pcarac_ttc->regime == TRANSIENT_TTC) {
//                                            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                        } else {
//                                            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                        }
//                                        LP_printf(fpout, "BC SOUTH_TTC %e \n", cl_val);
//                                        mat6[mat4[i+1]-1] += theta*u/dist;
//                                        b[i] += theta*cl_val*u/dist;
//
//                                        b[i] += ((1-theta)*cl_val*u/dist);
////                                        nvois_ele_i++;
//
//                                        LP_printf(fpout,
//                                                  "DIRIF boundary: %e, mat5: %d, dist= %e,  mat6_voisin: %e, velocity: %e, nmaille: %d, b : %e, val %e\n",
//                                                  cl_val, mat5[i], dist, mat6[mat4[i + 1] - 1], u, mat4[i + 1] - 1 , b[i], cl_val); // DK check
//                                        LP_printf(fpout,
//                                                  "Dirif boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
//                                        break;
//                                    }
//
//                                }
//                            }
//                            break; //case tcl South
//                        }
//                        break;
//                    }
//                }
//
////Dispersif term
//                if (rd == YES_TS) {
//                    switch (tcl) {
//                        case NO_BOUND_TTC: {
//                            //fill the diagonal value
//                            if (id_neigh != NONE_TTC) { // SW 28/05/2018 pour confluence
//
//                                mat6[mat4[i + 1] - 1] +=  theta * dlip / (delta_L*dist); ;  //dist = dx ou dy; delta_L = distance centre maille * delta_L
//                                b[i] += -((1 - theta) * dlip / (dist * delta_L)) * var[i];
//                                //fill the value for each neighbor
//                                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * dlip / (delta_L*dist);//* delta_L
//                                b[i] += (1 - theta) * dlip / (dist * delta_L) * var[id_neigh];
//
//                                LP_printf(fpout,
//                                          "NOF bound: %d, mat5: %d, mat6_maille: %e, theta: %e, dlip: %e, delta_L: %e , dist: %e, nmaille: %d, var = %e \n",
//                                          tcl, mat5[i], mat6[mat4[i + 1] - 1], theta, dlip, delta_L, dist, mat4[i + 1] - 1, var[i]); // DK check
//                                LP_printf(fpout,
//                                          "NOF bound: %d, mat5: %d, mat6_voisin: %e, theta: %e, dlip: %e, delta_L: %e, dist: %e, nvois: %d, var[idneigh]: %e, b = %e \n",
//                                          tcl, mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], theta, dlip, delta_L, dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh],b[i]); // DK check
//                            }
//                            nvois_ele_i++;
////                            LP_printf(fpout,
////                                      "NOF boundary, Dispersion, nvois ele: %d\n", nvois_ele_i); // DK check
//                            break;            // fin face active
//                        }
//                        case DIRI_FACE_TTC: {
//                            if (pcarac_ttc->regime == TRANSIENT_TTC) {
//                                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                            } else {
//                                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                            }
//
//                            mat6[mat4[i + 1] - 1] += 2 * theta * dlip / (dist * dist);
//                            b[i] += -2 * (1 - theta) * var[i] * dlip / (dist * dist);
//                            b[i] += 2 * theta * cl_val * dlip / (dist * dist);
//                            b[i] += 2 * (1 - theta) * cl_val * dlip / (dist * dist);
//
//                            LP_printf(fpout,
//                                      "DIRIF boundary: %e, mat5: %d, dist= %e,  mat6_voisin: %e, dlip: %e, nmaille: %d, b : %e, val %e\n",
//                                      cl_val, mat5[i], dist, mat6[mat4[i + 1] - 1], dlip, mat4[i + 1] - 1 , b[i], cl_val); // DK check
////                            LP_printf(fpout,
////                                      "Dirif boundary, Dispersion, nvois ele: %d\n", nvois_ele_i); // DK check
////                            nvois_ele_i++;
//                            break;            // fin face diri
//                        }
//                    }
//                }
//
//                /* if(tcl==DIRI_CENTER_TTC) // variable impose au centre de la maille
//                {
//                    cl_val=plink->pspecies->p_boundary_ttc[i]->valcl[1][1];
//                    b[i]=cl_val;
//                    mat6[mat4[i+1]-1]=1;//reste d'Arthur à verifier le bilan semble faux
//                }
//                if (tcl==NEU_FACE_TTC) //faceNEU_FACE_TTC) // flux impose a la face de la maille
//                {
//                    cl_val=plink->pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                    //b[i] += -cl_val/(dist*rhowcw);
//                    if(cl_val > 0){ // SW 08/06/2018 apport, si un prelevement, cela change pas la concentration
//                        b[i] += cl_val/(dist*rhowcw); // SW 17/05/2018 flux entrant ou sortie depend du signe de cl_val soit QC ou UM
//                    }
//                    else if (cl_val < 0){ // prelevement cl_val = Q/volume; /s pour solute
//                        mat6[mat4[i+1]-1] += (-cl_val*theta/dist);
//                        b[i] += ((1-theta)*cl_val/dist)*var[i]; //(1-theta)*cl_val*var[i];//
//
//                    }
//                } */
//                break;
//
//
//            }
//            case 2: {
//                //LP_printf(fpout, "enter case: 2\n" );
//                sub_icard = 0;
//                for (sub_icard = 0; sub_icard < nb_sub; sub_icard++) {
//                    cl_val = 0;
//
//                    tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];
//                    id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];
//                    delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // distance entre le centre de la maille et de son voisin
//                    dlip = p_param_calc_ttc[i]->disp_face[icard][sub_icard]; //
//                    dlip = dlip*2/3;
////                    LP_printf(fpout, "id_neigh for ele: %d , icard: %d , id_neigh: %d\n", i, icard, id_neigh);
//                    //nvois_ele_i++;
////Dispersif term
//                    //if (rd==YES_TS)
//                    if (rd == YES_TS) {
//                        switch (tcl) {
//                            case NO_BOUND_TTC: {
//                                //fill the diagonal value
//                                if (id_neigh != NONE_TTC) { // SW 28/05/2018 pour confluence
//
//                                    mat6[mat4[i + 1] - 1] += theta * dlip /(dist*dist); //(dist *2);    //dist = dx ou dy; delta_L = distance centre maille  * delta_L
//                                    b[i] += -((1 - theta) * dlip / (dist * delta_L)) * var[i];
//                                    //fill the value for each neighbor
//                                    mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * dlip /(dist*dist); //(dist * 2); //* delta_L
//                                    b[i] += (1 - theta) * dlip / (dist * delta_L) * var[id_neigh];
//
//
////                                    LP_printf(fpout,
////                                              "NOF bound: %d, mat5: %d, mat6_maille: %e, theta: %e, dlip: %e, delta_L: %e , dist: %e, nmaille: %d\n",
////                                              tcl, mat5[i], mat6[mat4[i + 1] - 1], theta, dlip, delta_L, dist, mat4[i + 1] - 1); // DK check
////                                    LP_printf(fpout,
////                                              "NOF bound: %d, mat5: %d, mat6_voisin: %e, theta: %e, dlip: %e, delta_L: %e, dist: %e, nvois: %d\n",
////                                              tcl, mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], theta, dlip, delta_L, dist, mat4[i + 1] - 1 - nvois + nvois_ele_i); // DK check
//                                }
//                                nvois_ele_i++;
//                                break;            // fin face active
//                            }
//
//                        }
//                    }
////Advectif term
//                    if (ra==YES_TS) // si on considere la partie advectif de lequation
//                    { // TODO  debug advectif term: print the velocities to check if they are read correctly
//
//                        LP_printf(fpout, "entering_advection\n");
//                        u=plink->uface[i][icard][sub_icard]; // SW 29/05/2018 pour confluence
//                        LP_printf(fpout, "velocity in m mat is %e\n", u);
//                        u_west = plink->uface[i][WEST_TTC][sub_icard];
//                        switch(icard){
//                            case EAST_TTC:
//                                if(u >0.0) { //fill the diagonal value
//                                    //mat6[mat4[i+1]-1] += theta*u/dist;
//                                    mat6[mat4[i+1]-1] += theta*u/dist + theta*(u - u_west)/dist;
//                                    //b[i] += -((1-theta)*u/dist)*var[i];
//                                    b[i] += -((1-theta)*u/dist)*var[i] + (1-theta)*var[i]*(u - u_west)/dist;
//                                } else {
//                                    switch (tcl) {
//                                        case NO_BOUND_TTC:{
//                                            //face active //fill the value for neighbor
//                                            if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard][0]!=DIRI_CENTER_TTC) {
//
//                                                mat6[mat4[i+1]-1-nvois+nvois_ele_i] += theta*u/dist;
//                                                b[i] += -(1-theta)*u/dist*var[id_neigh];
//                                            }
//                                            break;
//                                        }
//                                        case DIRI_FACE_TTC:{
//                                            //face active //bound adv
//                                            cl_val=pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                            b[i] += -theta*cl_val*u/dist;
//                                            b[i] += -((1-theta)*cl_val*u/dist);
//                                            break;
//                                        }
//                                    }
//                                }
//                                break;
//                            case WEST_TTC:
//                                if(u < 0.0) { //fill the diagonal value
//                                    mat6[mat4[i+1]-1] += theta*u/dist;
//                                    b[i] += -((1-theta)*u/dist)*var[i];
//                                }
//                                else
//                                {
//                                    switch (tcl) {
//                                        case NO_BOUND_TTC:{//face active
//                                            if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard][0]!=DIRI_CENTER_TTC) {
//                                                //mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - theta*u/dist;
//                                                u_east = plink->uface[i][EAST_TTC][sub_icard];
//                                                mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - theta*u_east/dist;
//                                                //b[i]+= (1-theta)*u/dist*var[id_neigh];
//                                                b[i]+= (1-theta)*u/dist*var[id_neigh];
//                                            }
//                                            break;
//                                            }
//                                        case DIRI_FACE_TTC:{
//                                            cl_val=pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                            b[i] += theta*cl_val*u/dist;
//                                            b[i] += ((1-theta)*cl_val*u/dist);
//                                            break;
//                                        }
//                                    }
//                                }
//                                break;
//
//                            case NORTH_TTC:
//                                if(u >0.0) { //fill the diagonal value
//                                    mat6[mat4[i+1]-1] += theta*u/dist;
//                                    b[i] += -((1-theta)*u/dist)*var[i];
//                                }
//                                else
//                                {
//                                    switch (tcl) {
//                                        case NO_BOUND_TTC:  //face active //fill the value for neighbor
//                                            //if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
//                                            if(id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard]!=DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
//                                                mat6[mat4[i+1]-1-nvois+nvois_ele_i] += theta*u/dist;
//                                                b[i] += -(1-theta)*u/dist*var[id_neigh];
//                                            }
//                                            break;
//                                        case DIRI_FACE_TTC: //face active //bound adv
//                                            cl_val=pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                            b[i] += - theta*cl_val*u/dist;
//                                            b[i] += - ((1-theta)*cl_val*u/dist);
//                                            break;
//                                    }
//                                }
//                                break;
//
//                            case SOUTH_TTC:
//                                if(u < 0.0) { //fill the diagonal value
//                                    mat6[mat4[i+1]-1] += -theta*u/dist;
//                                    b[i] += ((1-theta)*u/dist)*var[i];
//                                }
//                                else
//                                {
//                                    switch (tcl) {
//                                        case NO_BOUND_TTC:  //face active
//                                            //if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
//                                            if(id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard]!=DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
//                                                mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - theta*u/dist;
//                                                b[i]+= (1-theta)*u/dist*var[id_neigh];
//                                            }
//                                            break;
//                                        case DIRI_FACE_TTC:
//                                            cl_val=pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                            b[i] += theta*cl_val*u/dist;
//                                            b[i] += ((1-theta)*cl_val*u/dist);
//                                            break;
//                                    }
//                                }
//                                break;
//                        }
//                    }
//                    /*
//                     if(tcl==NO_BOUND_TTC && id_neigh != NONE_TTC)
//                    {
//                        nvois_ele_i++;
//                    } // fin face active nb neig
//                    */
//
//                    if(tcl==DIRI_CENTER_TTC) // variable impose au centre de la maille
//                    {
//                        cl_val=pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                        b[i]=cl_val;
//                        mat6[mat4[i+1]-1]=1;//reste d'Arthur à verifier le bilan semble faux
//                    }
//                    if (tcl==NEU_FACE_TTC) //faceNEU_FACE_TTC) // flux impose a la face de la maille
//                    {
//                        cl_val=pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                        //b[i] += -cl_val/(dist*rhowcw);
//                        if(cl_val > 0){ // SW 08/06/2018 apport, si un prelevement, cela change pas la concentration
//                            b[i] += cl_val/(dist*rhowcw); // SW 17/05/2018 flux entrant ou sortie depend du signe de cl_val soit QC ou UM
//                        }
//                        else if (cl_val < 0){ // prelevement cl_val = Q/volume; /s pour solute
//                            mat6[mat4[i+1]-1] += (-cl_val*theta/dist);
//                            b[i] += ((1-theta)*cl_val/dist)*var[i]; //(1-theta)*cl_val*var[i];//
//                            //if(i == 3392) //debug
//                            // printf("id == 3392\n");
//                        }
//                    }
//
//                } // fin sub_icard
//
//
//                break;
//            } // case 2
//                //LP_printf(fpout,"icard: %d, number_subface: %d \n",icard,  nb_sub ); // DK check
//                //if (nb_sub == 0) nb_sub = 1; // Parade pour rentrer dans le for si pas de subfaces
//                //LP_printf(fpout,"condition_limit: %d, id_neigh = %d, delta_L: %e, dlip = %e, cl_val = %e \n", tcl, id_neigh, delta_L, dlip , cl_val); // DK check
//        } //fin case nb_sub
//
//
//    } // fin icard
////    LP_printf(fpout, "nele = %d, bfinal: %e \n", i, b[i]); // DK check
//  //  LP_printf(fpout, "id: %d, b: %e, mat6: %e \n", i, b[i], mat6[mat4[i + 1] - 1]); // DK check
////    LP_printf(fpout,"In %s : b[%d] = %f\n",__func__,i,b[i]); // Check NG pour la première itération --> OK
////    LP_printf(fpout, "Exiting %s \n", __func__); // DK check
//} // fin TTC_no_lib_aq

// DK BACKUP advection
// if (ra==YES_TS){
//
// LP_printf(fpout, "Entering advection in func: %s\n",__func__ );
// u=plink->uface[i][icard][sub_icard]; //// vitesse de Darcy
//
// LP_printf(fpout, "velocity in m mat is %e\n", u);
// switch(icard){
// case EAST_TTC:{
// if(u < 0.0) { //fill the diagonal value
//
// mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta*u/dist;
//
// LP_printf(fpout,
//"EAST_TTC bound: %d, b: %e, mat6_maille: %e, dlip: %e, dist: %e, nmaille: %d, var = %e, velo East %f, velo West %f \n",
// tcl, b[i], mat6[mat4[i + 1] - 1], dlip, dist, mat4[i + 1] - 1, var[i], u, u_west); // DK check
//
// LP_printf(fpout,
//"NOF boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
// if (rd == 1) { nvois_ele_i++; };
//} else {
// switch (tcl) {
// case NO_BOUND_TTC:{
////face active //fill the value for neighbor
////                                        if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard][0]!=DIRI_CENTER_TTC) {
// LP_printf(fpout, "ADV FACE active no bound i = %d, id_neigh = %d,  u is %e, u_west is %e",i, id_neigh, u, u_west);
//
// mat6[mat4[i+1]-1] += theta*u/dist;
////                                            LP_printf(fpout,
////                                                      "NOF bound: %d, b: %e, mat6_voisin: %e, dlip: %e, dist: %e, nvois: %d, var[idneigh]: %e \n",
////                                                      tcl, b[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], dlip, dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh]); // DK check
// b[i] += -(1-theta)*u/dist*var[id_neigh];
////                                        }
// LP_printf(fpout,
//"NOF boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
// if (rd == 1) { nvois_ele_i++; };
// break;
// }
// case DIRI_FACE_TTC:{
////face active //bound adv
// if (pcarac_ttc->regime == TRANSIENT_TTC) {
// cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
// } else {
// cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
// }
// mat6[mat4[i+1]-1] += theta*u/dist;
// b[i] += theta*cl_val*u/dist;
// b[i] += -((1-theta)*cl_val*u/dist);
//
////                                        nvois_ele_i++;
// LP_printf(fpout,
//"Dirif boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
// break;
// }
// }
// }
// break;
// }
//
//
// case WEST_TTC:{
// if(u < 0.0) { //fill the diagonal value
// mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta*u/dist;
//
// LP_printf(fpout,
//"WEST_TTC bound: %d, b: %e, mat6_maille: %e, dlip: %e, dist: %e, nmaille: %d, var = %e \n",
// tcl, b[i], mat6[mat4[i + 1] - 1], dlip, dist, mat4[i + 1] - 1, var[i]); // DK check
//
// if (rd == 1) { nvois_ele_i++; };
// LP_printf(fpout,
//"NOF boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
// }
// else
//{
// switch (tcl) {
// case NO_BOUND_TTC:{
////face active
////                                        if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard][0]!=DIRI_CENTER_TTC) {
////mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - theta*u/dist;
// mat6[mat4[i+1]-1] += theta*u/dist;
////b[i]+= (1-theta)*u/dist*var[id_neigh];
// b[i]+= -(1-theta)*u/dist*var[id_neigh];
//
// LP_printf(fpout,
//"NOF boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
// if (rd == 1) { nvois_ele_i++; };
//
// break;
// }
// case DIRI_FACE_TTC:{
// if (pcarac_ttc->regime == TRANSIENT_TTC) {
// cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
// } else {
// cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
// }
// LP_printf(fpout, "ADV FACE active DIRI i = %d, id_neigh = %d,  u is %e, u_west is %e",i, id_neigh, u, u_west);
//
// mat6[mat4[i+1]-1] += theta*u/dist;
// b[i] += theta*cl_val*u/dist;
// b[i] += ((1-theta)*cl_val*u/dist);
//
// LP_printf(fpout,
//"Dirif boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
//
// break;
// }
// }
// }
// break; //case tcl West
// }
//
// case NORTH_TTC:{
// if(u < 0.0) { //fill the diagonal value
// mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta*u/dist;
//
// LP_printf(fpout,
//"case NORTH_TTC u < 0, NOF bound: Velcity %e, dist = %e, b: %e, mat6_voisin: %e \n",
// u , dist, b[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i]    ); // DK check
// if (rd == 1) { nvois_ele_i++; };
//
// }
// else
//{  LP_printf(fpout, "case NORTH_TTC u > 0\n");
// switch (tcl) {
// case NO_BOUND_TTC:{
////face active //fill the value for neighbor
// LP_printf(fpout, "ADV FACE active NO bound i = %d, id_neigh = %d,  u is %e",i, id_neigh, u );
//
// mat6[mat4[i+1]-1] += theta*u/dist;
// b[i] += -(1 - theta) * u / dist * var[id_neigh];
// LP_printf(fpout,
//"NOF bound: %d, b: %e, mat6_voisin: %e, dlip: %e, dist: %e, nvois: %d, var[idneigh]: %e, V N NO_BOUND : %f \n",
// tcl, b[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], dlip,
// dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh],
// u); // DK check
// LP_printf(fpout,
//"NOF boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
// if (rd == 1) { nvois_ele_i++; };
//
// break;
// }
// case DIRI_FACE_TTC:{
////face active //bound adv
// if (pcarac_ttc->regime == TRANSIENT_TTC) {
// cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
// } else {
// cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
// }
//
// mat6[mat4[i+1]-1] += theta*u/dist;
// b[i] +=  theta*cl_val*u/dist;
//
// b[i] += - ((1-theta)*cl_val*u/dist);
// LP_printf(fpout,
//"DIRIF boundary cl_val: %lf, dist= %e,  mat6_diag: %e, velocity: %e, b : %e, val %e\n",
// cl_val, dist, mat6[mat4[i + 1] - 1], u, b[i], cl_val); // DK check
// break;
// }
// }
// }
// break; //case tcl North
// }
//
//
// case SOUTH_TTC:{
// if(u < 0.0) { //fill the diagonal value
// mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta* u / dist;
//
// LP_printf(fpout,
//"NOF bound: in advective %d, b: %e, mat6_maille: %e, dlip: %e, dist: %e, nmaille: %d, var = %e , V south TTC %f\n",
// tcl, b[i], mat6[mat4[i + 1] - 1], dlip, dist, mat4[i + 1] - 1, var[i], u ); // DK check
//
// LP_printf(fpout,
//"NOF boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
// if (rd == 1) { nvois_ele_i++; };
//
// }
// else
//{   LP_printf(fpout, "case SOUTH  u > 0\n");
// switch (tcl) {
// case NO_BOUND_TTC:  //face active
// mat6[mat4[i+1]-1] += theta*u/dist;
// b[i] += (1 - theta) * u / dist * var[id_neigh];
// LP_printf(fpout,
//"NOF bound in advective: %d, b: %e, mat6_voisin: %e, velocity: %e, dist: %e, nvois: %d, var[idneigh]: %e , mat6 = %e\n",
// tcl, b[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], u,
// dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh],
//-theta * u / dist); // DK check
// LP_printf(fpout,
//"NOF boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
// if (rd == 1) { nvois_ele_i++; };
//
// break;
// case DIRI_FACE_TTC:{
// if (pcarac_ttc->regime == TRANSIENT_TTC) {
// cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
// } else {
// cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
// }
// LP_printf(fpout, "BC SOUTH_TTC %e \n", cl_val);
// mat6[mat4[i+1]-1] += theta*u/dist;
// b[i] += theta*cl_val*u/dist;
//
// b[i] += ((1-theta)*cl_val*u/dist);
//
// LP_printf(fpout,
//"DIRIF boundary: %e, mat5: %d, dist= %e,  mat6_voisin: %e, velocity: %e, nmaille: %d, b : %e, val %e\n",
// cl_val, mat5[i], dist, mat6[mat4[i + 1] - 1], u, mat4[i + 1] - 1 , b[i], cl_val); // DK check
// LP_printf(fpout,
//"Dirif boundary, Advection, nvois ele: %d\n", nvois_ele_i); // DK check
// break;
// }
//
// }
// }
// break; //case tcl South
// }
// break;
// }
// }

// DK backup to test the advective fluxes 18 10 2021
////// Modified version to fix the nested, DK AR, the correct version is att line 969
//////Fonction qui construit mat6 et b
// void TTC_no_cap_libaq(s_carac_ttc *pcarac_ttc,s_link_ttc *plink, s_param_calc_ttc **p_param_calc_ttc,s_species_ttc *pspecies,int i,int tstep,FILE *fpout) {
void TTC_no_cap_libaq(s_carac_ttc *pcarac_ttc, s_link_ttc *plink, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, int i, FILE *fpout) {

  s_gc *pgc;
  double *mat6, *b, dist, delta_L, *var, u;
  int tcl, type, nb_sub;
  int *nsub;
  int icard, id_neigh;
  int *mat4, *mat5, ra, rd, nvois;
  double theta, dlip, rhow, cw, rhowcw, cl_val;
  int nvois_ele_i = 0;
  int sub_icard; // SW 28/05/2018
  int id_neigh1, id_neigh2;
  double u_east, u_west;
  double u_id_neigh1, u_id_neigh2;
  // tstep=tstep-1;
  //    LP_printf(fpout, "Entering %s, tstep is %d\n", __func__, tstep); // DK check

  pgc = pspecies->pgc;
  type = pspecies->pset->type;
  var = pspecies->pval_ttc->val;                         // tableau contenant la variable etudiee (temperature ou solute)
  dist = pspecies->p_param_ttc[i]->param_syst[SIZE_TTC]; // taille de la maille
  // dist = 1;
  mat6 = pgc->mat6; // donne val
  mat5 = pgc->mat5; // donne l'identifiant de chaque maille et de leur voisine
  mat4 = pgc->mat4; // permet de connaitre le nombre de voisin pour chaque maille
  b = pgc->b;       // donne b

  //     LP_printf(fpout,"In %s : Var[%d] = %f\n",__func__,i,b[i]); // Check NG pour la première itération --> OK

  nvois = plink->p_neigh_ttc[i]->nvois;
  theta = pcarac_ttc->theta; // parametre de semi implicite
  if (type == HEAT_TTC) {
    rhow = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][RHO_TTC];
    cw = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][HEAT_CAP_TTC];
    rhowcw = rhow * cw;
  } else if (type == SOLUTE_TTC) {
    rhowcw = 1;
  }
  ra = pspecies->pset->calc_process[ADVECTION_TTC]; // terme advectif (YES or NO)
  rd = pspecies->pset->calc_process[DIFFUSION_TTC]; // terme dispersif (YES or NO)
                                                    //    LP_printf(fpout, "nele = %d, number_neighbour: %d, nvois_ele_i: %d \n", i, nvois, nvois_ele_i); // DK check
                                                    //    LP_printf(fpout, "advection = %d, diffusion: %d , yes : %d \n", ra, rd,YES_TS); // DK check
                                                    //    LP_printf(fpout, "nele = %d, binit: %e \n", i, b[i]); // DK check

  for (icard = 0; icard < NB_CARD_TTC; icard++) {
    nb_sub = plink->nsub[i][icard];
    LP_printf(fpout, "nele = %d, icard: %d, nb_sub: %d \n", i, icard, nb_sub); // DK check

    switch (nb_sub) { // line 276  should start from
    default: {
      // LP_printf(fpout, "enter case: 0\n" );
      sub_icard = 0;
      cl_val = 0;
      // dist = plink->pspecies->p_neigh_ttc[i]->delta_L[icard][sub_icard];
      // dist = plink->pspecies->p_neigh_ttc->pdelta_dist[i].delta_dist[icard][sub_icard];

      tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];
      id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];
      if (id_neigh != -99) { // DK AR 24 11 2021 test  for neighbour mismatch after estimating id_neigh with ABS_MSH
        id_neigh = id_neigh - 1;
      }
      delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // distance entre le centre de la maille et de son voisin
      dlip = p_param_calc_ttc[i]->disp_face[icard][sub_icard];    //
      LP_printf(fpout, "inside func: %s, iteration is %d, dlip is %e , delta_L: %f, id_neigh: %d, tcl: %d\n", __func__, i, dlip, delta_L, id_neigh, tcl);

      //                LP_printf(fpout, "id_neigh for ele: %d , icard: %d , id_neigh: %d, nvois= %d \n", i, icard, id_neigh, nvois);

      /*LP_printf(fpout, "condition_limit: %d, id_neigh = %d, delta_L: %e, dlip = %e, cl_val = %e \n", tcl,
                id_neigh, delta_L, dlip, cl_val); // DK check*/

      // Advectif term si on considere la partie advectif de lequation

      if (ra == YES_TS) // si on considere la partie advectif de lequation
      {                 // TODO  debug advectif term: print the velocities to check if they are read correctly

        //                    LP_printf(fpout, "entering_advection\n");
        u = pcarac_ttc->uface[i][icard][sub_icard]; // SW 29/05/2018 pour confluence
                                                    //                    LP_printf(fpout, "velocity in m mat is %e\n", u);
        u_west = pcarac_ttc->uface[i][WEST_TTC][sub_icard];
        switch (icard) {
        case EAST_TTC:
          if (u > 0.0) { // fill the diagonal value
            // mat6[mat4[i+1]-1] += theta*u/dist;
            mat6[mat4[i + 1] - 1] += theta * u / dist + theta * (u - u_west) / dist;
            // b[i] += -((1-theta)*u/dist)*var[i];
            b[i] += -((1 - theta) * u / dist) * var[i] + (1 - theta) * var[i] * (u - u_west) / dist;

            //                                LP_printf(fpout,
            //                                          "ADV NOF u: %lf, mat5: %d, mat6_voisin: %e, delta_L: %e, dist: %e, nvois_ele_i: %d, var[idneigh]: %e, b = %e \n",
            //                                          u, mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],delta_L, dist, nvois_ele_i, var[id_neigh],b[i]); // DK check

          } else {
            switch (tcl) {
            case NO_BOUND_TTC: {
              // face active //fill the value for neighbor
              if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][0] != DIRI_CENTER_TTC) {

                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta * u / dist;
                b[i] += -(1 - theta) * u / dist * var[id_neigh];

                //
                //                                            LP_printf(fpout,
                //                                                      "ADV NOF u: %lf, mat5: %d, mat6_voisin: %e, delta_L: %e, dist: %e, nvois_ele_i: %d, var[idneigh]: %e, b = %e \n",
                //                                                      u, mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],delta_L, dist, nvois_ele_i, var[id_neigh],b[i]); // DK check
              }
              break;
            }
            case DIRI_FACE_TTC: {
              // face active //bound adv
              if (pcarac_ttc->regime == TRANSIENT_TTC) {
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
              } else {
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
              }
              b[i] += -theta * cl_val * u / dist;
              b[i] += -((1 - theta) * cl_val * u / dist);
              //
              //                                        LP_printf(fpout,
              //                                                  "DIRIF boundary: %lf, nvois_ele_i = %d, dist= %lf,  mat6_voisin: %e, dlip: %e, nmaille: %d, b : %e, velocity: %e\n",
              //                                                  cl_val, nvois_ele_i, dist, mat6[mat4[i + 1] - 1], dlip, mat4[i + 1] - 1 , b[i], u); // DK check

              break;
            }
            }
          }
          break;
        case WEST_TTC:
          if (u < 0.0) { // fill the diagonal value
            mat6[mat4[i + 1] - 1] += theta * u / dist;
            b[i] += -((1 - theta) * u / dist) * var[i];

            //                                LP_printf(fpout,
            //                                          "ADV NOF u: %lf, mat5: %d, mat6_voisin: %e, delta_L: %e, dist: %e, nvois_ele_i: %d, var[idneigh]: %e, b = %e \n",
            //                                          u, mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],delta_L, dist, nvois_ele_i, var[id_neigh],b[i]); // DK check

          } else {
            switch (tcl) {
            case NO_BOUND_TTC: { // face active
              if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][0] != DIRI_CENTER_TTC) {
                // mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - theta*u/dist;
                u_east = pcarac_ttc->uface[i][EAST_TTC][sub_icard];
                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u_east / dist;
                // b[i]+= (1-theta)*u/dist*var[id_neigh];
                b[i] += (1 - theta) * u / dist * var[id_neigh];

                //                                            LP_printf(fpout,
                //                                                      "ADV NOF u: %lf, mat5: %d, mat6_voisin: %e, delta_L: %e, dist: %e, nvois_ele_i: %d, var[idneigh]: %e, b = %e \n",
                //                                                      u, mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],delta_L, dist, nvois_ele_i, var[id_neigh],b[i]); // DK check
              }
              break;
            }
            case DIRI_FACE_TTC: {
              if (pcarac_ttc->regime == TRANSIENT_TTC) {
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
              } else {
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
              }
              b[i] += theta * cl_val * u / dist;
              b[i] += ((1 - theta) * cl_val * u / dist);
              //
              //                                        LP_printf(fpout,
              //                                                  "DIRIF boundary: %lf, nvois_ele_i = %d, dist= %lf,  mat6_voisin: %e, dlip: %e, nmaille: %d, b : %e, velocity: %e\n",
              //                                                  cl_val, nvois_ele_i, dist, mat6[mat4[i + 1] - 1], dlip, mat4[i + 1] - 1 , b[i], u); // DK check

              break;
            }
            }
          }
          break;

        case NORTH_TTC:
          if (u > 0.0) { // fill the diagonal value

            mat6[mat4[i + 1] - 1] += theta * u / dist;
            b[i] += -((1 - theta) * u / dist) * var[i];

            //                                    LP_printf(fpout,
            //                                              "ADV NOF u: %lf, mat5: %d, mat6_voisin: %e, delta_L: %e, dist: %e, nvois_ele_i: %d, var[idneigh]: %e, b = %e \n",
            //                                              u, mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],delta_L, dist, nvois_ele_i, var[id_neigh],b[i]); // DK check

          } else {
            switch (tcl) {
            case NO_BOUND_TTC: // face active //fill the value for neighbor
              // if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
              if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta * u / dist;
                b[i] += -(1 - theta) * u / dist * var[id_neigh];

                //                                            LP_printf(fpout,
                //                                                      "ADV NOF u: %lf, mat5: %d, mat6_voisin: %e, delta_L: %e, dist: %e, nvois_ele_i: %d, var[idneigh]: %e, b = %e \n",
                //                                                      u, mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],delta_L, dist, nvois_ele_i, var[id_neigh],b[i]); // DK check                                   u, mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],delta_L, dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh],b[i]); // DK check
              }
              break;
            case DIRI_FACE_TTC: // face active //bound adv
              if (pcarac_ttc->regime == TRANSIENT_TTC) {
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
              } else {
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
              }
              b[i] += -theta * cl_val * u / dist;
              b[i] += -((1 - theta) * cl_val * u / dist);

              LP_printf(fpout, "ADV DIRIF boundary cl_val: %lf, nvois_ele_i = %d, dist= %lf,  mat6_voisin: %e, dlip: %e, nmaille: %d, b : %e, velocity: %e\n", cl_val, nvois_ele_i, dist, mat6[mat4[i + 1] - 1], dlip, mat4[i + 1] - 1, b[i], u); // DK check

              break;
            }
          }
          break;

        case SOUTH_TTC:
          if (u < 0.0) { // fill the diagonal value
            mat6[mat4[i + 1] - 1] += -theta * u / dist;
            b[i] += ((1 - theta) * u / dist) * var[i];
            //                                LP_printf(fpout,
            //                                          "ADV NOF u: %lf, mat5: %d, mat6_voisin: %e, delta_L: %e, dist: %e, nvois_ele_i: %d, var[idneigh]: %e, b = %e \n",
            //                                          u, mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],delta_L, dist, nvois_ele_i, var[id_neigh],b[i]); // DK check     u, mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],delta_L, dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh],b[i]); // DK check

          } else {
            switch (tcl) {
            case NO_BOUND_TTC: // face active
              // if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
              if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u / dist;
                b[i] += (1 - theta) * u / dist * var[id_neigh];

                //                                            LP_printf(fpout,
                //                                                      "ADV NOF u: %lf, mat5: %d, mat6_voisin: %e, delta_L: %e, dist: %e, nvois_ele_i: %d, var[idneigh]: %e, b = %e \n",
                //                                                      u, mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],delta_L, dist, nvois_ele_i, var[id_neigh],b[i]); // DK check nvois + nvois_ele_i],delta_L, dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh],b[i]); // DK check
              }
              break;
            case DIRI_FACE_TTC:
              if (pcarac_ttc->regime == TRANSIENT_TTC) {
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
              } else {
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
              }
              b[i] += theta * cl_val * u / dist;
              b[i] += ((1 - theta) * cl_val * u / dist);
              LP_printf(fpout, "ADV DIRIF boundary cl val: %lf, nvois_ele_i = %d, dist= %lf,  mat6_voisin: %e,  nmaille: %d, b : %e, velocity: %e\n", cl_val, nvois_ele_i, dist, mat6[mat4[i + 1] - 1], mat4[i + 1] - 1, b[i], u); // DK check

              break;
            }
          }
          break;
        }
        if (rd == 1 && tcl == NO_BOUND_TTC && id_neigh != NONE_TTC) { // Modify the nvois_ele_i when there is an active boundary for pure advection case
          nvois_ele_i++;
        }
      }
      // Dispersif term
      if (rd == YES_TS) {
        switch (tcl) {
        case NO_BOUND_TTC: {
          // fill the diagonal value
          if (id_neigh != NONE_TTC) { // SW 28/05/2018 pour confluence

            mat6[mat4[i + 1] - 1] += theta * dlip / (delta_L * dist);
            ; // dist = dx ou dy; delta_L = distance centre maille * delta_L
            b[i] += -((1 - theta) * dlip / (dist * delta_L)) * var[i];
            // fill the value for each neighbor
            mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * dlip / (delta_L * dist); //* delta_L
            b[i] += (1 - theta) * dlip / (dist * delta_L) * var[id_neigh];

            LP_printf(fpout, "NOF bound: %d, mat5: %d, mat6_maille: %e, theta: %e, dlip: %e, delta_L: %e , dist: %e, nmaille: %d, var = %e \n", tcl, mat5[i], mat6[mat4[i + 1] - 1], theta, dlip, delta_L, dist, mat4[i + 1] - 1, var[i]);                                                                       // DK check
            LP_printf(fpout, "NOF bound: %d, mat5: %d, mat6_voisin: %e, theta: %e, dlip: %e, delta_L: %e, dist: %e, nvois: %d, var[idneigh]: %e, b = %e \n", tcl, mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], theta, dlip, delta_L, dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh], b[i]); // DK check
          }
          //                            nvois_ele_i++;
          //                            LP_printf(fpout,
          //                                      "NOF boundary, Dispersion, nvois ele: %d\n", nvois_ele_i); // DK check
          break; // fin face active
        }
        case DIRI_FACE_TTC: {
          if (pcarac_ttc->regime == TRANSIENT_TTC) {
            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
          } else {
            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
          }

          mat6[mat4[i + 1] - 1] += 2 * theta * dlip / (dist * dist);
          b[i] += -2 * (1 - theta) * var[i] * dlip / (dist * dist);
          b[i] += 2 * theta * cl_val * dlip / (dist * dist); // *100000 for test
          b[i] += 2 * (1 - theta) * cl_val * dlip / (dist * dist);

          LP_printf(fpout, "DIRIF boundary cl_val: %e, mat5: %d, dist= %e,  mat6_voisin: %e, dlip: %e, nmaille: %d, b[%d] : %e\n", cl_val, mat5[i], dist, mat6[mat4[i + 1] - 1], dlip, mat4[i + 1] - 1, i, b[i]); // DK check
                                                                                                                                                                                                                  //                            LP_printf(fpout,
                                                                                                                                                                                                                  //                                      "Dirif boundary, Dispersion, nvois ele: %d\n", nvois_ele_i); // DK check
                                                                                                                                                                                                                  //                            nvois_ele_i++;
          break;                                                                                                                                                                                                  // fin face diri
        }
        }
        if (tcl == NO_BOUND_TTC && id_neigh != NONE_TTC) { // Modify the nvois_ele_i when there is an active boundary for pure advection case
          nvois_ele_i++;
        }
      }

      //                 if(tcl==DIRI_CENTER_TTC) // variable impose au centre de la maille
      //                {
      //                    cl_val=plink->pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
      //                    b[i]=cl_val;
      //                    mat6[mat4[i+1]-1]=1;//reste d'Arthur à verifier le bilan semble faux
      //                }
      if (tcl == NEU_FACE_TTC) // face NEU_FACE_TTC) // flux impose a la face de la maille // DK AR Here is the flux at the face imposed
      {
        cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
        // b[i] += -cl_val/(dist*rhowcw);
        if (cl_val > 0) {                   // SW 08/06/2018 apport, si un prelevement, cela change pas la concentration
          b[i] += cl_val / (dist * rhowcw); // SW 17/05/2018 flux entrant ou sortie depend du signe de cl_val soit QC ou UM
        } else if (cl_val < 0) {            // prelevement cl_val = Q/volume; /s pour solute
          mat6[mat4[i + 1] - 1] += (-cl_val * theta / dist);
          b[i] += ((1 - theta) * cl_val / dist); //(1-theta)*cl_val;// Check this term it will be removed, check the fluxes if they are calculated well
        }
      }
      break;
    }
    case 2: {
      // LP_printf(fpout, "enter case: 2\n" );
      sub_icard = 0;
      for (sub_icard = 0; sub_icard < nb_sub; sub_icard++) {
        cl_val = 0;

        tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];
        id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];
        delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // distance entre le centre de la maille et de son voisin
        dlip = p_param_calc_ttc[i]->disp_face[icard][sub_icard];    //
        dlip = dlip * 2 / 3;
        //                    LP_printf(fpout, "id_neigh for ele: %d , icard: %d , id_neigh: %d\n", i, icard, id_neigh);
        // nvois_ele_i++;
        // Dispersif term
        // if (rd==YES_TS)
        if (rd == YES_TS) {
          switch (tcl) {
          case NO_BOUND_TTC: {
            // fill the diagonal value
            if (id_neigh != NONE_TTC) { // SW 28/05/2018 pour confluence

              mat6[mat4[i + 1] - 1] += theta * dlip / (dist * dist); //(dist *2);    //dist = dx ou dy; delta_L = distance centre maille  * delta_L
              b[i] += -((1 - theta) * dlip / (dist * delta_L)) * var[i];
              // fill the value for each neighbor
              mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * dlip / (dist * dist); //(dist * 2); //* delta_L
              b[i] += (1 - theta) * dlip / (dist * delta_L) * var[id_neigh];

              //                                    LP_printf(fpout,
              //                                              "NOF bound: %d, mat5: %d, mat6_maille: %e, theta: %e, dlip: %e, delta_L: %e , dist: %e, nmaille: %d\n",
              //                                              tcl, mat5[i], mat6[mat4[i + 1] - 1], theta, dlip, delta_L, dist, mat4[i + 1] - 1); // DK check
              //                                    LP_printf(fpout,
              //                                              "NOF bound: %d, mat5: %d, mat6_voisin: %e, theta: %e, dlip: %e, delta_L: %e, dist: %e, nvois: %d\n",
              //                                              tcl, mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], theta, dlip, delta_L, dist, mat4[i + 1] - 1 - nvois + nvois_ele_i); // DK check
            }
            nvois_ele_i++;
            break; // fin face active
          }
          }
        }
        // Advectif term
        if (ra == YES_TS) // si on considere la partie advectif de lequation
        {                 // TODO  debug advectif term: print the velocities to check if they are read correctly

          LP_printf(fpout, "entering_advection\n");
          u = pcarac_ttc->uface[i][icard][sub_icard]; // SW 29/05/2018 pour confluence
          LP_printf(fpout, "velocity in m mat is %e\n", u);
          u_west = pcarac_ttc->uface[i][WEST_TTC][sub_icard];
          switch (icard) {
          case EAST_TTC:
            if (u > 0.0) { // fill the diagonal value
              // mat6[mat4[i+1]-1] += theta*u/dist;
              mat6[mat4[i + 1] - 1] += theta * u / dist + theta * (u - u_west) / dist;
              // b[i] += -((1-theta)*u/dist)*var[i];
              b[i] += -((1 - theta) * u / dist) * var[i] + (1 - theta) * var[i] * (u - u_west) / dist;
            } else {
              switch (tcl) {
              case NO_BOUND_TTC: {
                // face active //fill the value for neighbor
                if (pspecies->p_boundary_ttc[id_neigh]->icl[icard][0] != DIRI_CENTER_TTC) {

                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta * u / dist;
                  b[i] += -(1 - theta) * u / dist * var[id_neigh];
                }
                break;
              }
              case DIRI_FACE_TTC: {
                // face active //bound adv
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
                b[i] += -theta * cl_val * u / dist;
                b[i] += -((1 - theta) * cl_val * u / dist);
                break;
              }
              }
            }
            break;
          case WEST_TTC:
            if (u < 0.0) { // fill the diagonal value
              mat6[mat4[i + 1] - 1] += theta * u / dist;
              b[i] += -((1 - theta) * u / dist) * var[i];
            } else {
              switch (tcl) {
              case NO_BOUND_TTC: { // face active
                if (pspecies->p_boundary_ttc[id_neigh]->icl[icard][0] != DIRI_CENTER_TTC) {
                  // mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - theta*u/dist;
                  u_east = pcarac_ttc->uface[i][EAST_TTC][sub_icard];
                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u_east / dist;
                  // b[i]+= (1-theta)*u/dist*var[id_neigh];
                  b[i] += (1 - theta) * u / dist * var[id_neigh];
                }
                break;
              }
              case DIRI_FACE_TTC: {
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
                b[i] += theta * cl_val * u / dist;
                b[i] += ((1 - theta) * cl_val * u / dist);
                break;
              }
              }
            }
            break;

          case NORTH_TTC:
            if (u > 0.0) { // fill the diagonal value
              mat6[mat4[i + 1] - 1] += theta * u / dist;
              b[i] += -((1 - theta) * u / dist) * var[i];
            } else {
              switch (tcl) {
              case NO_BOUND_TTC: // face active //fill the value for neighbor
                // if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
                if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta * u / dist;
                  b[i] += -(1 - theta) * u / dist * var[id_neigh];
                }
                break;
              case DIRI_FACE_TTC: // face active //bound adv
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
                b[i] += -theta * cl_val * u / dist;
                b[i] += -((1 - theta) * cl_val * u / dist);
                break;
              }
            }
            break;

          case SOUTH_TTC:
            if (u < 0.0) { // fill the diagonal value
              mat6[mat4[i + 1] - 1] += -theta * u / dist;
              b[i] += ((1 - theta) * u / dist) * var[i];
            } else {
              switch (tcl) {
              case NO_BOUND_TTC: // face active
                // if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
                if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u / dist;
                  b[i] += (1 - theta) * u / dist * var[id_neigh];
                }
                break;
              case DIRI_FACE_TTC:
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
                b[i] += theta * cl_val * u / dist;
                b[i] += ((1 - theta) * cl_val * u / dist);
                break;
              }
            }
            break;
          }
        }
        /*
         if(tcl==NO_BOUND_TTC && id_neigh != NONE_TTC)
        {
            nvois_ele_i++;
        } // fin face active nb neig
        */

        if (tcl == DIRI_CENTER_TTC) // variable impose au centre de la maille
        {
          cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
          b[i] = cl_val;
          mat6[mat4[i + 1] - 1] = 1; // reste d'Arthur à verifier le bilan semble faux
        }
        if (tcl == NEU_FACE_TTC) // faceNEU_FACE_TTC) // flux impose a la face de la maille
        {
          cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
          // b[i] += -cl_val/(dist*rhowcw);
          if (cl_val > 0) {                   // SW 08/06/2018 apport, si un prelevement, cela change pas la concentration
            b[i] += cl_val / (dist * rhowcw); // SW 17/05/2018 flux entrant ou sortie depend du signe de cl_val soit QC ou UM
          } else if (cl_val < 0) {            // prelevement cl_val = Q/volume; /s pour solute
            mat6[mat4[i + 1] - 1] += (-cl_val * theta / dist);
            b[i] += ((1 - theta) * cl_val / dist) * var[i]; //(1-theta)*cl_val*var[i];//
                                                            // if(i == 3392) //debug
            //  printf("id == 3392\n");
          }
        }

      } // fin sub_icard

      break;
    } // case 2
      // LP_printf(fpout,"icard: %d, number_subface: %d \n",icard,  nb_sub ); // DK check
      // if (nb_sub == 0) nb_sub = 1; // Parade pour rentrer dans le for si pas de subfaces
      // LP_printf(fpout,"condition_limit: %d, id_neigh = %d, delta_L: %e, dlip = %e, cl_val = %e \n", tcl, id_neigh, delta_L, dlip , cl_val); // DK check
    } // fin case nb_sub

  } // fin icard

  LP_printf(fpout, "In %s : Var[%d] = %f\n", __func__, i, b[i]); // Check NG pour la première itération --> OK

  //    LP_printf(fpout, "nele = %d, bfinal: %e \n", i, b[i]); // DK check
  //  LP_printf(fpout, "id: %d, b: %e, mat6: %e \n", i, b[i], mat6[mat4[i + 1] - 1]); // DK check
  //    LP_printf(fpout,"In %s : b[%d] = %f\n",__func__,i,b[i]); // Check NG pour la première itération --> OK
  //    LP_printf(fpout, "Exiting %s \n", __func__); // DK check
} // fin TTC_no_lib_aq

// DK backup to test the advective fluxes 18 10 2021
////// Modified version to fix the nested, DK AR, the correct version is att line 969
//////Fonction qui construit mat6 et b
// void TTC_no_cap_libaq_test (s_carac_ttc *pcarac_ttc, s_link_ttc *plink, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, int i, int tstep, FILE *fpout) {
void TTC_no_cap_libaq_test(s_carac_ttc *pcarac_ttc, s_link_ttc *plink, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, int i, FILE *fpout) {

  s_gc *pgc = pspecies->pgc;

  int *mat4 = pgc->mat4;                 // permet de connaitre le nombre de voisin pour chaque maille
  int *mat5 = pgc->mat5;                 // donne l'identifiant de chaque maille et de leur voisine
  double *mat6 = pgc->mat6;              // donne val
  double *b = pgc->b;                    // donne b
  double *var = pspecies->pval_ttc->val; // tableau contenant la variable etudiee (temperature ou solute)

  int type = pspecies->pset->type;
  double theta = pcarac_ttc->theta;                     // parametre de semi implicite
  int ra = pspecies->pset->calc_process[ADVECTION_TTC]; // terme advectif (YES or NO)
  int rd = pspecies->pset->calc_process[DIFFUSION_TTC]; // terme dispersif (YES or NO)

  int nele = pcarac_ttc->count[NELE_TTC];   // Number of total elements
  int nvois = plink->p_neigh_ttc[i]->nvois; // Number of neighbours of a given cell

  double dist = pspecies->p_param_ttc[i]->param_syst[SIZE_TTC]; // taille de la maille
  double thickness = pspecies->p_param_ttc[i]->thickness;       // taille de la maille
  double volume = dist * dist * thickness;

  // Debug ID neigh
  // int id_neigh_debug = -99;

  double delta_L, u;

  int *nsub = plink->nsub[i]; // Array of subfaces of each element
  int nb_sub;                 // number of subfaces at a given cell and face

  int tcl; // Type of the boundary condition
  double cl_val;
  int id_neigh;

  double dlip, rhow, cw, rhowcw; // Thermal transport parameters

  // int testval = 97; //  DK DEBUG

  int nvois_ele_i = 0;
  int sub_icard; // subfaces at a given face [i][icard]
  int id_neigh1, id_neigh2;
  double u_east, u_west;
  double u_id_neigh1, u_id_neigh2;

  // Aquitard variables
  int naquitard = 0;
  int nriverbed = 0;
  double coeff_top, coeff_bot;
  double taquitard; // The temperature of the aquitard

  int iaq = 0; // the variable to check the aquitard table
  int it_aquitard = i;
  int aquitard_cell_bot = 0;
  int aquitard_cell_top = 0;
  double temp_aquitard_top = 0;
  double temp_aquitard_bottom = 0;
  double aq_thickness_top = 0;
  double aq_thickness_bottom = 0;

  // tstep = tstep - 1;
  //    LP_printf(fpout, "Entering %s, tstep is %d\n", __func__, tstep); // DK check
  if (pspecies->pset->aquitard == AQUITARD_ON_TTC) {
    naquitard = pcarac_ttc->count[NAQUITARD_TTC];
  }
  if (pspecies->pset->riverbed == AQUITARD_ON_TTC) {
    nriverbed = pcarac_ttc->count[NRIVERBED_TTC];
  }
  //     LP_printf(fpout,"In %s : id_species : %d \n",__func__,pspecies->id); // Check NG pour la première itération --> OK

  //     LP_printf(fpout,"In %s : Var[%d] = %f\n",__func__,i,b[i]); // Check NG pour la première itération --> OK

  if (type == HEAT_TTC) {
    rhow = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][RHO_TTC];
    cw = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][HEAT_CAP_TTC];
    rhowcw = rhow * cw;
  } else if (type == SOLUTE_TTC) {
    rhowcw = 1;
  }

  //    LP_printf(fpout, "nele = %d, number_neighbour: %d, nvois_ele_i: %d \n", i, nvois, nvois_ele_i); // DK check
  //    LP_printf(fpout, "advection = %d, diffusion: %d \n", ra, rd); // DK check
  //    LP_printf(fpout, "nele = %d, binit: %e \n", i, b[i]); // DK check

  //    if ( i < naquitard ) {
  //        LP_printf(fpout, "The aquitard is active in this cell %d , %d, %d, %d, \n", i,
  //                  pspecies->p_aquitard_ttc[i]->id, pspecies->p_aquitard_ttc[i]->nlayer,
  //                  pspecies->p_aquitard_ttc[i]->id_neigh);
  //
  //    }
  for (int icard = 0; icard < NB_CARD_TTC; icard++) {
    nb_sub = nsub[icard];
    //        LP_printf(fpout, "ele = %d, icard: %d, nb_sub: %d \n", i, icard, nb_sub); // DK check
    // TODO This lookup causes too much computational burden, it should be fixed
    /*        if ( icard == 0 &&  pspecies->pset->aquitard == AQUITARD_ON_TTC ) {
                while ( iaq < naquitard) {

                    if ( it_aquitard == pspecies->p_aquitard_ttc[iaq]->id-1 ) {
                        aquitard_cell_bot = 1;  // Activate the aquitard link indicator
                        temp_aquitard_bottom = pspecies->p_aquitard_ttc[iaq]->temperature_old;
                        aq_thickness_bottom =  pspecies->p_aquitard_ttc[iaq]->thick[VAR_AQUITARD_TTC];
    //                    LP_printf(fpout, "The aquitard is active at the face %s, iaq: %d in this cell %d , %d, %d, %d, %lf, %d, thickness: %lf \n",
    //                              TTC_param_card(icard), iaq, it_aquitard,
    //                              pspecies->p_aquitard_ttc[iaq]->id, pspecies->p_aquitard_ttc[iaq]->nlayer,
    //                              pspecies->p_aquitard_ttc[iaq]->id_neigh, temp_aquitard_bottom , i  , pspecies->p_aquitard_ttc[iaq]->thick[VAR_AQUITARD_TTC]);
                    }
                    if ( it_aquitard ==  pspecies->p_aquitard_ttc[iaq]->id_neigh-1   ) {
                        aquitard_cell_top = 1; // Activate the aquitard link indicator
                        temp_aquitard_top = pspecies->p_aquitard_ttc[iaq]->temperature_old;
                        aq_thickness_top =  pspecies->p_aquitard_ttc[iaq]->thick[VAR_AQUITARD_TTC];
    //                    LP_printf(fpout, "The aquitard is active at the face %s, iaq: %d in this cell %d , %d, %d, %d, %lf thickness: %lf \n",TTC_param_card(icard),
    //                              it_aquitard, iaq,
    //                              pspecies->p_aquitard_ttc[iaq]->id,
    //                              pspecies->p_aquitard_ttc[iaq]->nlayer,
    //                              pspecies->p_aquitard_ttc[iaq]->id_neigh,
    //                              temp_aquitard_top    , pspecies->p_aquitard_ttc[iaq]->thick[VAR_AQUITARD_TTC]);

                    }
                    iaq++;
                }
            }*/

    switch (nb_sub) {
    case 0: {
      //                LP_printf(fpout, "Entering case nosubface at face %s\n", TTC_param_card(icard) );
      // LP_printf(fpout, "enter case: 0\n" );
      sub_icard = 0;
      cl_val = 0;

      delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // distance entre le centre de la maille et de son voisin
      dlip = p_param_calc_ttc[i]->disp_face[icard][sub_icard];    //
      tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];
      id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];
      //    id_neigh_debug = plink->p_neigh_ttc[testval]->ivois[icard][sub_icard];
      //                LP_printf(fpout, "In function %s the BC type of ele %d at face %s is %s and %s \n", __func__, i ,
      //                          TTC_param_card(icard) , TTC_name_freq(pspecies->p_boundary_ttc[i]->freq),  TTC_name_freq(pspecies->p_boundary_ttc[i]->freq));

      if (id_neigh != NONE_TTC) { // DK AR 24 11 2021 test  for neighbour mismatch after estimating id_neigh with ABS_MSH
        id_neigh = id_neigh - 1;
        //    id_neigh_debug = id_neigh_debug - 1;
      }
      if (id_neigh == i)
        LP_error(fpout, "%s LINE %d id_neigh is same as i Check your input!", __func__, __LINE__);

      //                LP_printf(fpout, "inside func: %s, id is %d, dlip is %e , delta_L: %f, id_neigh: %d, tcl: %d\n",__func__, i, dlip, delta_L, id_neigh, tcl);

      //                LP_printf(fpout, "id_neigh for ele: %d , icard: %d , id_neigh: %d, nvois= %d \n", i, icard, id_neigh, nvois);

      //                LP_printf(fpout, "Cell i: %d , face: %s condition_limit: %s, id_neigh = %d \n", i,
      //                          TTC_param_card(icard), TTC_name_bound(tcl),
      //                          id_neigh); // DK check

      // Advectif term si on considere la partie advectif de lequation

      if (ra == YES_TS) // si on considere la partie advectif de lequation
      {
        //                    LP_printf(fpout, "Calculating advection \n"); // DK check
        u = pcarac_ttc->uface[i][icard][sub_icard]; // vitesse de Darcy

        //                    if ( i == 63588 || i == 63578 || i == 57214 || i == 63589 || i == 63879  ) {
        //                        LP_printf(fpout, "nosub face %s BC %s delta_L %lf "
        //                                         "var[%d] = %lf var[%d] = %lf u %e dlip %e rhowcw %e \n", TTC_param_card(icard), TTC_name_bound(tcl),
        //                                  delta_L, i, var[i], id_neigh, var[id_neigh], u, dlip ,rhowcw);
        //                    }

        //                    if ( i == 30 || i == 31 || i == 32 || i == 33 || i == testval  || i == 29 || i == 59 || i == 44 || i == 109 || i == 46  || i == 20 || i == 35 || i == 45 || i == id_neigh_debug ) {
        //                        LP_printf(fpout, "subface %d face %s BC %s delta_L %lf "
        //                                         "var[%d] = %lf var[%d] = %lf u %e rhowcw %e \n", sub_icard, TTC_param_card(icard), TTC_name_bound(tcl),
        //                                  delta_L, i, var[i], id_neigh, var[id_neigh], u, rhowcw);
        //                    }

        //                    if ( var[i] < 273.15 ||  var[i] > 300 || var[id_neigh] > 300  ) {
        //                        LP_printf(fpout, "nosubface %d face %s BC %s delta_L %lf "
        //                                         "var[%d] = %lf var[%d] = %lf u %e dlip %e rhowcw %e \n", sub_icard, TTC_param_card(icard), TTC_name_bound(tcl),
        //                                  delta_L, i, var[i], id_neigh, var[id_neigh], u, dlip ,rhowcw);
        //                    }
        //                    LP_printf(fpout, "Function: %s U[%d][%s] = %e \n", __func__, i, TTC_param_card(icard), u);
        switch (icard) {
        case EAST_TTC:
          if (u >= 0.0) { // fill the diagonal value
            // mat6[mat4[i+1]-1] += theta*u/dist;
            //                                mat6[mat4[i+1]-1]   += theta*u/dist; // + theta*u/delta_L; // Diagonal term
            //                                b[i]                += -((1-theta)*u/dist)*var[i];// + (1-theta)*var[i]*(u)/delta_L;
            mat6[mat4[i + 1] - 1] += theta * u / volume;  // + theta*u/delta_L; // Diagonal term
            b[i] += -((1 - theta) * u / volume) * var[i]; // + (1-theta)*var[i]*(u)/delta_L;

            //                                if ( i == testval ) {
            //                                    LP_printf(fpout,
            //                                              "ADV DIAG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6 %e b = %e \n",
            //                                              TTC_param_card(icard), theta, u,
            //                                              delta_L, dist, thickness,
            //                                              nvois, nvois_ele_i,
            //                                              i, var[i],
            //                                              id_neigh, var[id_neigh],
            //                                              mat5[i], mat6[mat4[i+1]-1],  b[i]); // DK check
            //                                    }
          } else {
            switch (tcl) {
            case NO_BOUND_TTC: {
              // face active //fill the value for neighbor
              if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][0] != DIRI_CENTER_TTC) {

                //                                            mat6[mat4[i+1]-1-nvois+nvois_ele_i] += theta*u/dist;
                //                                            b[i]                                += (1-theta)*u/dist*var[id_neigh];

                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta * u / volume;
                b[i] += (1 - theta) * u / volume * var[id_neigh];
                //                                            if ( i == testval ) {
                //                                                LP_printf(fpout,
                //                                                          "ADV NEIG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6_voisin %e b = %e \n",
                //                                                          TTC_param_card(icard), theta, u,
                //                                                          delta_L, dist, thickness,
                //                                                          nvois, nvois_ele_i,
                //                                                          i, var[i],
                //                                                          id_neigh, var[id_neigh],
                //                                                          mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],  b[i]); // DK check
                //
                //                                            }
              }
              break;
            }
            case DIRI_FACE_TTC: {
              // face active //bound adv
              cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

              //                                        b[i] += -theta*cl_val*u/dist;
              //                                        b[i] += -(1-theta)*cl_val*u/dist;
              b[i] += -theta * cl_val * u / volume;
              b[i] += -(1 - theta) * cl_val * u / volume;
              //                                        if ( i == testval ) {
              //                                        LP_printf(fpout,
              //                                                  "ADV DIRI face: %s, DIRIF boundary cl val: %e, nvois_ele_i = %d, dist= %lf, delta_L: %lf,  b : %e, velocity: %e\n",
              //                                                  TTC_param_card(icard),cl_val, nvois_ele_i, dist, delta_L,  b[i], u); // DK check                                        cl_val, nvois_ele_i, dist, mat6[mat4[i + 1] - 1], dlip, mat4[i + 1] - 1 , b[i], u); // DK check
              //                                        }
              break;
            }
            }
          }
          break;
        case WEST_TTC:
          if (u <= 0.0) { // fill the diagonal value
            //                                mat6[mat4[i+1]-1]   += -theta*u/dist;
            //                                b[i]                += -(1-theta)*u/dist*var[i];
            mat6[mat4[i + 1] - 1] += -theta * u / volume;
            b[i] += -(1 - theta) * u / volume * var[i];
            //                                if ( i == testval ) {
            //                                    LP_printf(fpout,
            //                                              "ADV DIAG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6 %e b = %e \n",
            //                                              TTC_param_card(icard), theta, u,
            //                                              delta_L, dist, thickness,
            //                                              nvois, nvois_ele_i,
            //                                              i, var[i],
            //                                              id_neigh, var[id_neigh],
            //                                              mat5[i], mat6[mat4[i + 1] - 1],  b[i]); // DK check
            //                                }
          } else {
            switch (tcl) {
            case NO_BOUND_TTC: { // face active
              if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][0] != DIRI_CENTER_TTC) {
                // mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - theta*u/dist;
                //                                            mat6[mat4[i+1]-1-nvois+nvois_ele_i] +=  -theta*u/dist;
                //                                            b[i]                                += (1-theta)*u/dist*var[id_neigh];
                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u / volume;
                b[i] += (1 - theta) * u / volume * var[id_neigh];
                //                                            if ( i == testval ) {
                //                                                LP_printf(fpout,
                //                                                          "ADV NEIG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6_voisin %e b = %e \n",
                //                                                          TTC_param_card(icard), theta, u,
                //                                                          delta_L, dist, thickness,
                //                                                          nvois, nvois_ele_i,
                //                                                          i, var[i],
                //                                                          id_neigh, var[id_neigh],
                //                                                          mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],  b[i]); // DK check
                //                                            }
              }
              break;
            }
            case DIRI_FACE_TTC: {
              cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

              //                                        b[i] += theta * cl_val * u / dist;
              //                                        b[i] += -(1 - theta) * cl_val * u / dist;
              b[i] += theta * cl_val * u / volume;
              b[i] += -(1 - theta) * cl_val * u / volume;
              //                                    if ( i == testval ) {
              //                                        LP_printf(fpout,
              //                                                  "ADV DIRI: %s, DIRIF boundary cl val: %e, nvois_ele_i = %d, dist= %lf, delta_L: %lf,  b : %e, velocity: %e\n",
              //                                                  TTC_param_card(icard),cl_val, nvois_ele_i, dist, delta_L,  b[i], u); // DK check
              //                                    }
              break;
            }
            }
          }
          break;

        case NORTH_TTC:
          if (u >= 0.0) { // fill the diagonal value

            //                                mat6[mat4[i+1]-1]   += theta*u/dist; // Diagonal term
            //                                b[i]                += -(1-theta)*u/dist*var[i];

            mat6[mat4[i + 1] - 1] += theta * u / volume; // Diagonal term
            b[i] += -(1 - theta) * u / volume * var[i];
            //                                if ( i == testval ) {
            //                                    LP_printf(fpout,
            //                                              "ADV DIAG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6 %e b = %e \n",
            //                                              TTC_param_card(icard), theta, u,
            //                                              delta_L, dist, thickness,
            //                                              nvois, nvois_ele_i,
            //                                              i, var[i],
            //                                              id_neigh, var[id_neigh],
            //                                              mat5[i], mat6[mat4[i + 1] - 1],  b[i]); // DK check
            //                                }
          } else {
            switch (tcl) {
            case NO_BOUND_TTC: // face active //fill the value for neighbor
              // if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
              if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence

                //                                            mat6[mat4[i+1]-1-nvois+nvois_ele_i] += theta*u/dist;
                //                                            b[i]                                += (1-theta)*u/dist*var[id_neigh];
                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta * u / volume;
                b[i] += (1 - theta) * u / volume * var[id_neigh];
                //                                            if ( i == testval ) {
                //                                                LP_printf(fpout,
                //                                                          "ADV NEIG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6_voisin %e b = %e \n",
                //                                                          TTC_param_card(icard), theta, u,
                //                                                          delta_L, dist, thickness,
                //                                                          nvois, nvois_ele_i,
                //                                                          i, var[i],
                //                                                          id_neigh, var[id_neigh],
                //                                                          mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],  b[i]); // DK check
                //                                            }
              }
              break;
            case DIRI_FACE_TTC: // face active //bound adv
              cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

              //                                        b[i] +=  -theta*cl_val*u/dist;
              //                                        b[i] += - (1-theta)*cl_val*u/dist;
              b[i] += -theta * cl_val * u / volume;
              b[i] += -(1 - theta) * cl_val * u / volume;
              //                                        if ( i == testval ) {
              //                                        LP_printf(fpout,
              //                                                  "ADV DIRI: %s, DIRIF boundary cl val: %e, nvois_ele_i = %d, dist= %lf, delta_L: %lf,  b : %e, velocity: %e\n",
              //                                                  TTC_param_card(icard),cl_val, nvois_ele_i, dist, delta_L,  b[i], u); // DK check
              //                                        }
              break;
            }
          }
          break;

        case SOUTH_TTC:
          if (u <= 0.0) { // fill the diagonal value // if u < 0, the flow is
            //                                mat6[mat4[i+1]-1]   += -theta*u/dist;
            //                                b[i]                += -(1-theta)*u/dist*var[i];
            mat6[mat4[i + 1] - 1] += -theta * u / volume;
            b[i] += -(1 - theta) * u / volume * var[i];
            //                                if ( i == testval ) {
            //                                    LP_printf(fpout,
            //                                              "ADV DIAG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6 %e b = %e \n",
            //                                              TTC_param_card(icard), theta, u,
            //                                              delta_L, dist, thickness,
            //                                              nvois, nvois_ele_i,
            //                                              i, var[i],
            //                                              id_neigh, var[id_neigh],
            //                                              mat5[i], mat6[mat4[i + 1] - 1 ],  b[i]); // DK check
            //                                }
          } else {
            switch (tcl) {
            case NO_BOUND_TTC: // face active
              // if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
              if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
                                                                                                                          //                                            mat6[mat4[i+1]-1-nvois+nvois_ele_i] +=  -theta*u/dist;
                                                                                                                          //                                            b[i]+= (1-theta)*u/dist*var[id_neigh];
                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u / volume;
                b[i] += (1 - theta) * u / volume * var[id_neigh];
                //                                            if ( i == testval ) {
                //                                                LP_printf(fpout,
                //                                                          "ADV NEIG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6_voisin %e b = %e \n",
                //                                                          TTC_param_card(icard), theta, u,
                //                                                          delta_L, dist, thickness,
                //                                                          nvois, nvois_ele_i,
                //                                                          i, var[i],
                //                                                          id_neigh, var[id_neigh],
                //                                                          mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],  b[i]); // DK check
                //                                            }
              }
              break;
            case DIRI_FACE_TTC:
              cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

              //                                        b[i] += theta*cl_val*u/dist;
              //                                        b[i] += - (1-theta)*cl_val*u/dist;
              b[i] += theta * cl_val * u / volume;
              b[i] += -(1 - theta) * cl_val * u / volume;
              //                                        if ( i == testval ) {
              //                                            LP_printf(fpout,
              //                                                      "ADV DIRI: %s, DIRIF boundary cl val: %e, nvois_ele_i = %d, dist= %lf, delta_L: %lf,  b : %e, velocity: %e\n",
              //                                                      TTC_param_card(icard), cl_val, nvois_ele_i, dist, delta_L, b[i],
              //                                                      u); // DK check                                        break;
              //                                        }
              break;
            }
          }
          break;
        case TOP_TTC:
          if (u >= 0.0) { // fill the diagonal value

            //                                mat6[mat4[i+1]-1]   += theta*u/thickness; // Instead of dist, we divide by thickness of cell in Z direction
            //                                b[i]                += -(1-theta)*u/thickness*var[i];
            mat6[mat4[i + 1] - 1] += theta * u / volume; // Instead of dist, we divide by thickness of cell in Z direction
            b[i] += -(1 - theta) * u / volume * var[i];
            //                                if ( i == testval ) {
            //                                    LP_printf(fpout,
            //                                              "ADV DIAG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6 %e b = %e \n",
            //                                              TTC_param_card(icard), theta, u,
            //                                              delta_L, dist, thickness,
            //                                              nvois, nvois_ele_i,
            //                                              i, var[i],
            //                                              id_neigh, var[id_neigh],
            //                                              mat5[i], mat6[mat4[i + 1] - 1 ],  b[i]); // DK check
            //                                }
          } else {
            switch (tcl) {
            case NO_BOUND_TTC: // face active //fill the value for neighbor
              // if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
              if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) {
                /*mat6[mat4[i+1]-1-nvois+nvois_ele_i] += theta*u/dist;
                b[i] += -(1-theta)*u/dist*var[id_neigh];*/
                //                                            mat6[mat4[i+1]-1-nvois+nvois_ele_i] +=  theta*u/thickness;
                //                                            b[i]                                += (1-theta)*u/thickness*var[id_neigh];
                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta * u / volume;
                b[i] += (1 - theta) * u / volume * var[id_neigh];
                //                                            if ( i == testval ) {
                //                                                LP_printf(fpout,
                //                                                          "ADV NEIG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6_voisin %e b = %e \n",
                //                                                          TTC_param_card(icard), theta, u,
                //                                                          delta_L, dist, thickness,
                //                                                          nvois, nvois_ele_i,
                //                                                          i, var[i],
                //                                                          id_neigh, var[id_neigh],
                //                                                          mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],  b[i]); // DK check
                //                                            }
              }
              break;
            case DIRI_FACE_TTC: // face active //bound adv
              cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

              //                                        b[i] += - theta*cl_val*u/thickness;
              //                                        b[i] += - (1-theta)*cl_val*u/thickness;
              b[i] += -theta * cl_val * u / volume;
              b[i] += -(1 - theta) * cl_val * u / volume;
              //                                        if ( i == testval || i == 48 ) {
              //                                            LP_printf(fpout,
              //                                                      "ADV[%d] DIRI %s cell %d  cl_val: %e, nvois_ele_i = %d, dist= %lf,  mat6_voisin: %e, dlip: %e, delta_L: %lf, b : %e, velocity: %e thick %lf\n",
              //                                                      i, TTC_param_card(icard), i, cl_val, nvois_ele_i, dist,
              //                                                      mat6[mat4[i + 1] - 1], dlip, delta_L, b[i], u, thickness); // DK check
              //                                        }
              break;
            }
          }
          break;

        case BOTTOM_TTC:
          if (u <= 0.0) { // fill the diagonal value
            //                                mat6[mat4[i+1]-1]   += - theta*u/thickness;
            //                                b[i]                += -(1-theta)*u/thickness*var[i];
            mat6[mat4[i + 1] - 1] += -theta * u / volume;
            b[i] += -(1 - theta) * u / volume * var[i];

            //                                if ( i == testval ) {
            //                                    LP_printf(fpout,
            //                                              "ADV DIAG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6 %e b = %e \n",
            //                                              TTC_param_card(icard), theta, u,
            //                                              delta_L, dist, thickness,
            //                                              nvois, nvois_ele_i,
            //                                              i, var[i],
            //                                              id_neigh, var[id_neigh],
            //                                              mat5[i], mat6[mat4[i + 1] - 1 ],  b[i]); // DK check
            //                                }
          } else {
            switch (tcl) {
            case NO_BOUND_TTC: // face active
              // if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
              if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
                /*mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - theta*u/dist;
                b[i]+= (1-theta)*u/dist*var[id_neigh];*/

                //                                            mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - theta*u/thickness;
                //                                            b[i]                                += (1-theta)*u/thickness*var[id_neigh];
                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u / volume;
                b[i] += (1 - theta) * u / volume * var[id_neigh];
                //                                            if ( i == testval ) {
                //                                                LP_printf(fpout,
                //                                                          "ADV NEIG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6_voisin %e b = %e \n",
                //                                                          TTC_param_card(icard), theta, u,
                //                                                          delta_L, dist, thickness,
                //                                                          nvois, nvois_ele_i,
                //                                                          i, var[i],
                //                                                          id_neigh, var[id_neigh],
                //                                                          mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],  b[i]); // DK check
                //                                            }
              }
              break;
            case DIRI_FACE_TTC:
              cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

              //                                        b[i] +=  theta*cl_val*u/thickness;
              //                                        b[i] += -(1-theta)*cl_val*u/thickness;
              b[i] += theta * cl_val * u / volume;
              b[i] += -(1 - theta) * cl_val * u / volume;
              //                                        if ( i == testval ) {
              //                                            LP_printf(fpout,
              //                                                      "ADV DIRI: %s, cl val: %e, nvois_ele_i = %d, dist= %lf, delta_L: %lf thickness %lf  b : %e, velocity: %e\n",
              //                                                      TTC_param_card(icard), cl_val, nvois_ele_i, dist, delta_L, thickness, b[i],
              //                                                      u); // DK check
              //                                        }
              break;
            }
          }
          break;
        }
        if (rd == 1 && tcl == NO_BOUND_TTC && id_neigh != NONE_TTC) { // Modify the nvois_ele_i when there is an active boundary for pure advection case
          nvois_ele_i++;
        }
      }
      // Dispersif term
      if (rd == YES_TS) {
        //                    LP_printf(fpout, "Calculating Diffusion \n"); // DK check
        switch (tcl) {
        case NO_BOUND_TTC: {
          // fill the diagonal value
          if (id_neigh != NONE_TTC) {

            if ((icard == TOP_TTC || icard == BOTTOM_TTC) && pspecies->pset->aquitard == AQUITARD_OFF_TTC) {
              //
              mat6[mat4[i + 1] - 1] += theta * dlip / (delta_L * thickness);
              ; // dist = dx ou dy; delta_L = distance centre maille * delta_L
              b[i] += -((1 - theta) * dlip / (delta_L * thickness)) * var[i];
              // fill the value for each neighbor
              mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * dlip / (delta_L * thickness); //* delta_L
              b[i] += (1 - theta) * dlip / (delta_L * thickness) * var[id_neigh];
              //                                    LP_printf(fpout,"In %s : cell: %d, loc_mat6 = %d\n",__func__,i,mat4[i + 1] - 1); // Check NG pour la première itération --> OK
              //                                    LP_printf(fpout,"In %s : cell: %d, loc_mat6 = %d\n",__func__,i,mat4[i + 1] - 1 - nvois + nvois_ele_i); // Check NG pour la première itération --> OK
              //                                    LP_printf(fpout,
              //                                              "NOF bound: i: %d, face: %s delta_L: %e , dist: %e, id_neigh: %d, var = %e, var[idneigh]: %lf\n",
              //                                              i, TTC_param_card(icard), delta_L, dist, id_neigh, var[i], var[id_neigh]); // DK check
            } else if ((icard == TOP_TTC || icard == BOTTOM_TTC) && pspecies->pset->aquitard == AQUITARD_ON_TTC) {
              if (icard == TOP_TTC) {
                //                                        delta_L = delta_L+aq_thickness_top;
                mat6[mat4[i + 1] - 1] += theta * dlip / (delta_L * thickness);
                ; // dist = dx ou dy; delta_L = distance centre maille * delta_L
                b[i] += -((1 - theta) * dlip / (delta_L * thickness)) * var[i];
                // fill the value for each neighbor
                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * dlip / (delta_L * thickness); //* delta_L
                                                                                                      //                                        b[i] += (1 - theta) * dlip / (delta_L * thickness) * temp_aquitard_top;
                b[i] += (1 - theta) * dlip / (delta_L * thickness) * var[id_neigh];
                //                                        LP_printf(fpout,
                //                                                  "NOF bound: i: %d, face: %s delta_L: %e , dist: %e, id_neigh: %d, var = %e, aquitard_cell: %d, t_aquitard_top: %lf\n",
                //                                                  i, TTC_param_card(icard), delta_L, dist, id_neigh, var[i], aquitard_cell_top, temp_aquitard_top); // DK check
              } else if (icard == BOTTOM_TTC) {
                //                                        delta_L = delta_L+aq_thickness_bottom;
                mat6[mat4[i + 1] - 1] += theta * dlip / (delta_L * thickness);
                ; // dist = dx ou dy; delta_L = distance centre maille * delta_L
                b[i] += -(1 - theta) * dlip / (delta_L * thickness) * var[i];
                // fill the value for each neighbor
                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * dlip / (delta_L * thickness); //* delta_L
                                                                                                      //                                        b[i] += (1 - theta) * dlip / (delta_L * thickness) * temp_aquitard_bottom;
                b[i] += (1 - theta) * dlip / (delta_L * thickness) * var[id_neigh];
                //                                        LP_printf(fpout,
                //                                                  "NOF bound: i: %d, face: %s delta_L: %e , dist: %e, id_neigh: %d, var = %e, aquitard_cell: %d, t_aquitard_bot: %lf, b[i]: %lf\n",
                //                                                  i, TTC_param_card(icard), delta_L, dist, id_neigh, var[i], aquitard_cell_bot, temp_aquitard_bottom, b[i]); // DK check
              } else {
                mat6[mat4[i + 1] - 1] += theta * dlip / (delta_L * dist);
                ; // dist = dx ou dy; delta_L = distance centre maille * delta_L
                b[i] += -(1 - theta) * dlip / (delta_L * dist) * var[i];
                // fill the value for each neighbor
                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * dlip / (delta_L * dist); //* delta_L
                b[i] += (1 - theta) * dlip / (delta_L * dist) * var[id_neigh];
                //                                        LP_printf(fpout,
                //                                                  "NOF bound: i: %d, face: %s delta_L: %e , dist: %e, id_neigh: %d, var = %e, var[idneigh]: %lf\n",
                //                                                  i, TTC_param_card(icard), delta_L, dist, id_neigh, var[i], var[id_neigh]); // DK check
              }
            }
            //                                LP_printf(fpout,
            //                                          "NOF bound: %d, mat5: %d, mat6_maille: %e, theta: %e, dlip: %e, delta_L: %e , dist: %e, nmaille: %d, var = %e \n",
            //                                          tcl, mat5[i], mat6[mat4[i + 1] - 1], theta, dlip, delta_L, dist, mat4[i + 1] - 1, var[i]); // DK check
            //                                LP_printf(fpout,
            //                                          "NOF bound: %d, mat5: %d, mat6_voisin: %e, theta: %e, dlip: %e, delta_L: %e, dist: %e, nvois: %d, var[idneigh]: %e, b = %e \n",
            //                                          tcl, mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], theta, dlip, delta_L, dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh],b[i]); // DK check
          }
          //                            nvois_ele_i++;
          //                            LP_printf(fpout,
          //                                      "NOF boundary, Dispersion, nvois ele: %d\n", nvois_ele_i); // DK check
          break; // fin face active
        }
        case DIRI_FACE_TTC: {
          cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

          if (icard == TOP_TTC || icard == BOTTOM_TTC) { // In the vertical direction the size of the cell is thickness
            mat6[mat4[i + 1] - 1] += 2 * theta * dlip / (delta_L * thickness);
            b[i] += -2 * (1 - theta) * var[i] * dlip / (delta_L * thickness);
            b[i] += 2 * theta * cl_val * dlip / (delta_L * thickness); //
            b[i] += 2 * (1 - theta) * cl_val * dlip / (delta_L * thickness);
          } else {
            mat6[mat4[i + 1] - 1] += 2 * theta * dlip / (delta_L * dist);
            b[i] += -2 * (1 - theta) * var[i] * dlip / (delta_L * dist);
            b[i] += 2 * theta * cl_val * dlip / (delta_L * dist); //
            b[i] += 2 * (1 - theta) * cl_val * dlip / (delta_L * dist);
          }

          //                            LP_printf(fpout,
          //                                      "DIRIF boundary cl_val: %e, delta_L: %f, dist= %e, i %d  , dlip: %e, nmaille: %d, b[%d] : %e, at face %s\n",
          //                                      cl_val, delta_L, dist, i, dlip, mat4[i + 1] - 1,i , b[i],
          //                                      TTC_param_card(icard)); // DK check

          //                            LP_printf(fpout,
          //                                      "Dirif boundary, Dispersion, nvois ele: %d\n", nvois_ele_i); // DK check
          //                            nvois_ele_i++;
          break; // fin face diri
        }
        }
        if (tcl == NO_BOUND_TTC && id_neigh != NONE_TTC) { // Modify the nvois_ele_i when there is an active boundary for pure advection case
          nvois_ele_i++;
        }
      }
      //                 if(tcl==DIRI_CENTER_TTC) // variable impose au centre de la maille
      //                {
      //                    cl_val=plink->pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
      //                    b[i]=cl_val;
      //                    mat6[mat4[i+1]-1]=1;//reste d'Arthur à verifier le bilan semble faux
      //                }
      if (tcl == NEU_FACE_TTC) // face NEU_FACE_TTC) // flux impose a la face de la maille // DK AR Here is the flux at the face imposed
      {
        cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
        // b[i] += -cl_val/(dist*rhowcw);
        if (cl_val >= 0) {                     // SW 08/06/2018 apport, si un prelevement, cela change pas la concentration
          b[i] += cl_val / (delta_L * rhowcw); // SW 17/05/2018 flux entrant ou sortie depend du signe de cl_val soit QC ou UM
        } else if (cl_val < 0) {               // prelevement cl_val = Q/volume; /s pour solute
          mat6[mat4[i + 1] - 1] += (-cl_val * theta / delta_L);
          b[i] += ((1 - theta) * cl_val / delta_L); //(1-theta)*cl_val;
        }
        //                        if (cl_val >= 0) { // SW 08/06/2018 apport, si un prelevement, cela change pas la concentration
        //                            b[i] += cl_val / (delta_L *
        //                                              rhowcw); // SW 17/05/2018 flux entrant ou sortie depend du signe de cl_val soit QC ou UM
        //                        } else if (cl_val < 0) { // prelevement cl_val = Q/volume; /s pour solute
        //                            mat6[mat4[i + 1] - 1] += (-cl_val * theta / delta_L);
        //                            b[i] += ((1 - theta) * cl_val /
        //                                    delta_L); //(1-theta)*cl_val;// Check this term it will be removed, check the fluxes if they are calculated well
        //
        //                        }
        //                    LP_printf(fpout,
        //                              "NEU_FACE_TTC cl_val: %e, mat5: %d, dist= %e, delta_L = %e,   mat6_voisin: %e, dlip: %e, nmaille: %d, b[%d] : %e, of the card %s\n",
        //                              cl_val, mat5[i], dist, delta_L, mat6[mat4[i + 1] - 1], dlip, mat4[i + 1] - 1,i , b[i], TTC_param_card(icard)); // DK check
      }
      break;
    }
    default: {
      //                    LP_printf(fpout, "Enterirng case subface at face %s\n", TTC_param_card(icard) );
      for (sub_icard = 0; sub_icard < nb_sub; sub_icard++) {

        cl_val = 0;

        tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];
        id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];
        delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // distance entre le centre de la maille et de son voisin
        dlip = p_param_calc_ttc[i]->disp_face[icard][sub_icard];    //

        if (id_neigh != NONE_TTC) { // DK AR 24 11 2021 test  for neighbour mismatch after estimating id_neigh with ABS_MSH
          id_neigh = id_neigh - 1;
          // id_neigh_debug = id_neigh_debug - 1;
        }
        if (id_neigh == i)
          LP_error(fpout, "%s LINE %d id_neigh is same as i Check your input!", __func__, __LINE__);
        //
        if (nb_sub == 2) { // For 2 subfaces the factor is 2/3
          dlip = dlip * 2 / 3;
        } else if (nb_sub == 4) { // For 4 subfaces the factor is 1/4
          dlip = dlip * 1 / 4;
        }
        //                        dlip = dlip*2/3;
        //                    LP_printf(fpout, "id_neigh for ele: %d , icard: %d , id_neigh: %d\n", i, icard, id_neigh);
        // Dispersif term

        if (ra == YES_TS) // si on considere la partie advectif de lequation
        {

          //                    LP_printf(fpout, "entering_advection\n");
          u = pcarac_ttc->uface[i][icard][sub_icard];

          //                        if ( i == 63588 || i == 63578 || i == 57214 || i == 63589 || i == 63879  ) {
          //                            LP_printf(fpout, "subface %d face %s BC %s delta_L %lf "
          //                                             "var[%d] = %lf var[%d] = %lf u %e dlip %e rhowcw %e \n", sub_icard, TTC_param_card(icard), TTC_name_bound(tcl),
          //                                      delta_L, i, var[i], id_neigh, var[id_neigh], u, dlip ,rhowcw);
          //                        }

          //                        if ( i == 30 || i == 31 || i == 32 || i == 33 || i == testval  || i == 29 || i == 59 || i == 44 || i == 109 || i == 46 || i == 20 || i == 35 || i == 45 || i == id_neigh_debug  ) {
          //                            LP_printf(fpout, "subface %d face %s BC %s delta_L %lf "
          //                                             "var[%d] = %lf var[%d] = %lf u %e dlip %e rhowcw %e \n", sub_icard, TTC_param_card(icard), TTC_name_bound(tcl),
          //                                      delta_L, i, var[i], id_neigh, var[id_neigh], u, dlip ,rhowcw);
          //                        }
          //                        if ( var[i] < 273.15  || var[i] > 300 || var[id_neigh] > 300  ) {
          //                            LP_printf(fpout, "subface %d face %s BC %s delta_L %lf "
          //                                             "var[%d] = %lf var[%d] = %lf u %e dlip %e rhowcw %e \n", sub_icard, TTC_param_card(icard), TTC_name_bound(tcl),
          //                                      delta_L, i, var[i], id_neigh, var[id_neigh], u, dlip ,rhowcw);
          //                        }

          //                    LP_printf(fpout, "velocity in m mat is %e\n", u);
          switch (icard) {
          case EAST_TTC:
            if (u >= 0.0) { // fill the diagonal value
              // mat6[mat4[i+1]-1] += theta*u/dist;
              //                                    mat6[mat4[i+1]-1]   += theta*u/dist; //+ theta*u/delta_L;
              //                                    b[i]                += -(1-theta)*u/dist*var[i]; // + (1-theta)*var[i]*(u )/dist;
              mat6[mat4[i + 1] - 1] += theta * u / volume; //+ theta*u/delta_L;
              b[i] += -(1 - theta) * u / volume * var[i];  // + (1-theta)*var[i]*(u )/dist;
              // b[i] += -((1-theta)*u/dist)*var[i];
              //                                    if ( i == testval ) {
              //                                        LP_printf(fpout,
              //                                                  "ADV DIAG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6 %e b = %e \n",
              //                                                  TTC_param_card(icard), theta, u,
              //                                                  delta_L, dist, thickness,
              //                                                  nvois, nvois_ele_i,
              //                                                  i, var[i],
              //                                                  id_neigh, var[id_neigh],
              //                                                  mat5[i], mat6[mat4[i + 1] - 1],  b[i]); // DK check
              //                                    }
            } else {
              switch (tcl) {
              case NO_BOUND_TTC: {
                // face active //fill the value for neighbor
                if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][0] != DIRI_CENTER_TTC) {

                  //                                                mat6[mat4[i+1]-1-nvois+nvois_ele_i] += theta*u/dist;
                  //                                                b[i]                                += (1-theta)*u/dist*var[id_neigh];
                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta * u / volume;
                  b[i] += (1 - theta) * u / volume * var[id_neigh];
                  //                                                if ( i == testval ) {
                  //                                                    LP_printf(fpout,
                  //                                                              "ADV NEIG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6_voisin %e b = %e \n",
                  //                                                              TTC_param_card(icard), theta, u,
                  //                                                              delta_L, dist, thickness,
                  //                                                              nvois, nvois_ele_i,
                  //                                                              i, var[i],
                  //                                                              id_neigh, var[id_neigh],
                  //                                                              mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],  b[i]); // DK check
                  //                                                }
                }
                break;
              }
              case DIRI_FACE_TTC: {
                // face active //bound adv
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

                //                                            b[i] += -theta*cl_val*u/dist;
                //                                            b[i] += -(1-theta)*cl_val*u/dist;
                b[i] += -theta * cl_val * u / volume;
                b[i] += -(1 - theta) * cl_val * u / volume;
                //                                            if ( i == testval ) {
                //                                                LP_printf(fpout,
                //                                                          "DIRI boundary %s : %lf, nvois_ele_i = %d, dist= %lf,  mat6_voisin: %e, dlip: %e, nmaille: %d, b : %e, velocity: %e\n",
                //                                                          TTC_param_card(icard), cl_val, nvois_ele_i, dist,
                //                                                          mat6[mat4[i + 1] - 1], dlip, mat4[i + 1] - 1, b[i],
                //                                                          u); // DK check
                //                                                }
                break;
              }
              }
            }
            break;
          case WEST_TTC:
            if (u <= 0.0) { // fill the diagonal value
              //                                    mat6[mat4[i+1]-1]   += -theta*u/dist;
              //                                    b[i]                += -(1-theta)*u/dist*var[i];
              mat6[mat4[i + 1] - 1] += -theta * u / volume;
              b[i] += -(1 - theta) * u / volume * var[i];
              //                                    if ( i == testval ) {
              //                                        LP_printf(fpout,
              //                                                  "ADV DIAG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6 %e b = %e \n",
              //                                                  TTC_param_card(icard), theta, u,
              //                                                  delta_L, dist, thickness,
              //                                                  nvois, nvois_ele_i,
              //                                                  i, var[i],
              //                                                  id_neigh, var[id_neigh],
              //                                                  mat5[i], mat6[mat4[i + 1] - 1 ],  b[i]); // DK check
              //                                    }
            } else {
              switch (tcl) {
              case NO_BOUND_TTC: { // face active
                if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][0] != DIRI_CENTER_TTC) {
                  // mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - theta*u/dist;

                  //                                                mat6[mat4[i+1]-1-nvois+nvois_ele_i] +=  -theta*u/dist;
                  //                                                b[i]                                += (1-theta)*u/dist*var[id_neigh];
                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u / volume;
                  b[i] += (1 - theta) * u / volume * var[id_neigh];
                  // b[i]+= (1-theta)*u/dist*var[id_neigh];
                  //                                                if ( i == testval ) {
                  //                                                    LP_printf(fpout,
                  //                                                              "ADV NEIG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6_voisin %e b = %e \n",
                  //                                                              TTC_param_card(icard), theta, u,
                  //                                                              delta_L, dist, thickness,
                  //                                                              nvois, nvois_ele_i,
                  //                                                              i, var[i],
                  //                                                              id_neigh, var[id_neigh],
                  //                                                              mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],  b[i]); // DK check
                  //                                                }
                }
                break;
              }
              case DIRI_FACE_TTC: {
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

                //                                            b[i] += theta*cl_val*u/dist;
                //                                            b[i] += -(1-theta)*cl_val*u/dist;
                b[i] += theta * cl_val * u / volume;
                b[i] += -(1 - theta) * cl_val * u / volume;
                //                                            if ( i == testval ) {
                //                                                LP_printf(fpout,
                //                                                          "DIRIF boundary %s : %lf, nvois_ele_i = %d, dist= %lf,  mat6_voisin: %e, dlip: %e, nmaille: %d, b : %e, velocity: %e\n",
                //                                                          TTC_param_card(icard), cl_val, nvois_ele_i, dist,
                //                                                          mat6[mat4[i + 1] - 1], dlip, mat4[i + 1] - 1, b[i],
                //                                                          u); // DK check
                //                                            }
                break;
              }
              }
            }
            break;
          case NORTH_TTC:
            if (u >= 0.0) { // fill the diagonal value
              //                                    mat6[mat4[i+1]-1]   += theta*u/dist;
              //                                    b[i]                += -((1-theta)*u/dist)*var[i];
              mat6[mat4[i + 1] - 1] += theta * u / volume;
              b[i] += -((1 - theta) * u / volume) * var[i];
              //                                    if ( i == testval ) {
              //                                        LP_printf(fpout,
              //                                                  "ADV DIAG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6 %e b = %e \n",
              //                                                  TTC_param_card(icard), theta, u,
              //                                                  delta_L, dist, thickness,
              //                                                  nvois, nvois_ele_i,
              //                                                  i, var[i],
              //                                                  id_neigh, var[id_neigh],
              //                                                  mat5[i], mat6[mat4[i + 1] - 1 ],  b[i]); // DK check
              //                                    }
            } else {
              switch (tcl) {
              case NO_BOUND_TTC: // face active //fill the value for neighbor
                // if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
                if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
                                                                                                                            //                                                mat6[mat4[i+1]-1-nvois+nvois_ele_i] += theta*u/dist;
                                                                                                                            //                                                b[i]                                += (1-theta)*u/dist*var[id_neigh];
                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta * u / volume;
                  b[i] += (1 - theta) * u / volume * var[id_neigh];
                  //                                                if ( i == testval ) {
                  //                                                    LP_printf(fpout,
                  //                                                              "ADV NEIG %s u: %e, mat5: %d, mat6_voisin: %e, delta_L: %e, dist: %e, nvois_ele_i: %d, var[idneigh]: %e, b = %e \n",
                  //                                                              TTC_param_card(icard), u, mat5[i],
                  //                                                              mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], delta_L,
                  //                                                              dist, nvois_ele_i, var[id_neigh],
                  //                                                              b[i]); // DK check                                   u, mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],delta_L, dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh],b[i]); // DK check
                  //                                                }
                }
                break;
              case DIRI_FACE_TTC: // face active //bound adv
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

                //                                            b[i] +=  -theta*cl_val*u/dist;
                //                                            b[i] += - (1-theta)*cl_val*u/dist;
                b[i] += -theta * cl_val * u / volume;
                b[i] += -(1 - theta) * cl_val * u / volume;
                //                                            if ( i == testval ) {
                //                                                LP_printf(fpout,
                //                                                          "ADV DIRIF %s boundary cl_val: %lf, nvois_ele_i = %d, dist= %lf,  mat6_voisin: %e, dlip: %e, nmaille: %d, b : %e, velocity: %e\n",
                //                                                          TTC_param_card(icard), cl_val, nvois_ele_i, dist,
                //                                                          mat6[mat4[i + 1] - 1], dlip, mat4[i + 1] - 1, b[i],
                //                                                          u); // DK check
                //                                            }
                break;
              }
            }
            break;
          case SOUTH_TTC:
            if (u <= 0.0) { // fill the diagonal value
              //                                    mat6[mat4[i+1]-1]   += -theta*u/dist;
              //                                    b[i]                += (1-theta)*u/dist*var[i];
              mat6[mat4[i + 1] - 1] += -theta * u / volume;
              b[i] += (1 - theta) * u / volume * var[i];
              //                                    if ( i == testval ) {
              //                                        LP_printf(fpout,
              //                                                  "ADV DIAG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6 %e b = %e \n",
              //                                                  TTC_param_card(icard), theta, u,
              //                                                  delta_L, dist, thickness,
              //                                                  nvois, nvois_ele_i,
              //                                                  i, var[i],
              //                                                  id_neigh, var[id_neigh],
              //                                                  mat5[i], mat6[mat4[i + 1] - 1 ],  b[i]); // DK check
              //                                    }
            } else {
              switch (tcl) {
              case NO_BOUND_TTC: // face active
                // if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
                if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
                                                                                                                            //                                                mat6[mat4[i+1]-1-nvois+nvois_ele_i] += -theta*u/dist;
                                                                                                                            //                                                b[i]                                += (1-theta)*u/dist*var[id_neigh];
                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u / volume;
                  b[i] += (1 - theta) * u / volume * var[id_neigh];
                  //                                                if ( i == testval ) {
                  //                                                    LP_printf(fpout,
                  //                                                              "ADV NEIG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6_voisin %e b = %e \n",
                  //                                                              TTC_param_card(icard), theta, u,
                  //                                                              delta_L, dist, thickness,
                  //                                                              nvois, nvois_ele_i,
                  //                                                              i, var[i],
                  //                                                              id_neigh, var[id_neigh],
                  //                                                              mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],  b[i]); // DK check
                  //                                                }
                }
                break;
              case DIRI_FACE_TTC:
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

                //                                            b[i] += theta*cl_val*u/dist;
                //                                            b[i] += -(1-theta)*cl_val*u/dist;
                b[i] += theta * cl_val * u / volume;
                b[i] += -(1 - theta) * cl_val * u / volume;
                //                                            if ( i == testval ) {
                //                                                LP_printf(fpout,
                //                                                          "ADV DIRIF boundary %s cl val: %lf, nvois_ele_i = %d, dist= %lf,  mat6_voisin: %e,  nmaille: %d, b : %e, velocity: %e\n",
                //                                                          TTC_param_card(icard), cl_val, nvois_ele_i, dist,
                //                                                          mat6[mat4[i + 1] - 1], mat4[i + 1] - 1, b[i], u); // DK check
                //                                            }
                break;
              }
            }
            break;
          case TOP_TTC:
            if (u >= 0.0) { // fill the diagonal value

              //                                    mat6[mat4[i+1]-1]   +=  theta*u/thickness;
              //                                    b[i]                += -(1-theta)*u/thickness*var[i];
              mat6[mat4[i + 1] - 1] += theta * u / volume;
              b[i] += -(1 - theta) * u / volume * var[i];
              //                                    if ( i == testval ) {
              //                                        LP_printf(fpout,
              //                                                  "ADV DIAG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6 %e b = %e \n",
              //                                                  TTC_param_card(icard), theta, u,
              //                                                  delta_L, dist, thickness,
              //                                                  nvois, nvois_ele_i,
              //                                                  i, var[i],
              //                                                  id_neigh, var[id_neigh],
              //                                                  mat5[i], mat6[mat4[i + 1] - 1 ],  b[i]); // DK check
              //                                    }
            } else {
              switch (tcl) {
              case NO_BOUND_TTC: // face active //fill the value for neighbor
                // if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
                if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
                  /*mat6[mat4[i+1]-1-nvois+nvois_ele_i] += theta*u/dist;
                  b[i] += -(1-theta)*u/dist*var[id_neigh];*/
                  //                                                mat6[mat4[i+1]-1-nvois+nvois_ele_i] +=  theta*u/thickness;
                  //                                                b[i]                                += (1-theta)*u/thickness*var[id_neigh];
                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta * u / volume;
                  b[i] += (1 - theta) * u / volume * var[id_neigh];
                  //                                                if ( i == testval ) {
                  //                                                    LP_printf(fpout,
                  //                                                              "ADV NEIG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6_voisin %e b = %e \n",
                  //                                                              TTC_param_card(icard), theta, u,
                  //                                                              delta_L, dist, thickness,
                  //                                                              nvois, nvois_ele_i,
                  //                                                              i, var[i],
                  //                                                              id_neigh, var[id_neigh],
                  //                                                              mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],  b[i]); // DK check
                  //                                                }
                }
                break;
              case DIRI_FACE_TTC: // face active //bound adv
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

                //                                            b[i] += - theta*cl_val*u/thickness;
                //                                            b[i] += - (1-theta)*cl_val*u/thickness;
                b[i] += -theta * cl_val * u / volume;
                b[i] += -(1 - theta) * cl_val * u / volume;
                //                                            if ( i == testval ) {
                //                                                LP_printf(fpout,
                //                                                          "ADV[%d] DIRI %s  cl_val: %e, nvois_ele_i = %d, dist= %lf,  mat6_voisin: %e, dlip: %e, delta_L: %lf, b : %e, velocity: %e, theta %d\n",
                //                                                          i, TTC_param_card(icard), cl_val, nvois_ele_i, dist,
                //                                                          mat6[mat4[i + 1] - 1], dlip, delta_L, b[i], u,
                //                                                          theta); // DK check
                //                                            }
                break;
              }
            }
            break;

          case BOTTOM_TTC:
            if (u <= 0.0) { // fill the diagonal value
              //                                    mat6[mat4[i+1]-1]   += - theta*u/thickness;
              //                                    b[i]                += -(1-theta)*u/thickness*var[i];
              mat6[mat4[i + 1] - 1] += -theta * u / volume;
              b[i] += -(1 - theta) * u / volume * var[i];
              //                                    if ( i == testval ) {
              //                                        LP_printf(fpout,
              //                                                  "ADV DIAG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6 %e b = %e \n",
              //                                                  TTC_param_card(icard), theta, u,
              //                                                  delta_L, dist, thickness,
              //                                                  nvois, nvois_ele_i,
              //                                                  i, var[i],
              //                                                  id_neigh, var[id_neigh],
              //                                                  mat5[i], mat6[mat4[i + 1] - 1 ],  b[i]); // DK check
              //                                    }
            } else {
              switch (tcl) {
              case NO_BOUND_TTC: // face active
                // if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
                if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
                  /*mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - theta*u/dist;
                  b[i]+= (1-theta)*u/dist*var[id_neigh];*/
                  //                                                mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - theta*u/thickness;
                  //                                                b[i]                                += (1-theta)*u/thickness*var[id_neigh];
                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u / volume;
                  b[i] += (1 - theta) * u / volume * var[id_neigh];
                  //                                                if ( i == testval ) {
                  //                                                    LP_printf(fpout,
                  //                                                              "ADV NEIG %s | theta %lf u %e | delta_L %lf  dist: %lf thickness %lf | nvois %d nvois_ele_i %d | var[%d] %e var[%d] %e | mat5 %d mat6_voisin %e b = %e \n",
                  //                                                              TTC_param_card(icard), theta, u,
                  //                                                              delta_L, dist, thickness,
                  //                                                              nvois, nvois_ele_i,
                  //                                                              i, var[i],
                  //                                                              id_neigh, var[id_neigh],
                  //                                                              mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i],  b[i]); // DK check
                  //                                                }
                }
                break;
              case DIRI_FACE_TTC:
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

                //                                            b[i] +=  theta*cl_val*u/thickness;
                //                                            b[i] += -(1-theta)*cl_val*u/thickness;
                b[i] += theta * cl_val * u / volume;
                b[i] += -(1 - theta) * cl_val * u / volume;
                //                                            if ( i == testval ) {
                //                                                LP_printf(fpout,
                //                                                          "ADV DIRI: %s, cl val: %e, nvois_ele_i = %d, dist= %lf,  mat6_voisin: %e,  nmaille: %d, b : %e, velocity: %e\n",
                //                                                          TTC_param_card(icard), cl_val, nvois_ele_i, dist,
                //                                                          mat6[mat4[i + 1] - 1], mat4[i + 1] - 1, b[i], u); // DK check
                //                                            }
                break;
              }
            }
            break;
          }
          if (rd == 1 && tcl == NO_BOUND_TTC && id_neigh != NONE_TTC) { // Modify the nvois_ele_i when there is an active boundary for pure advection case
            nvois_ele_i++;
          }
        }
        // Dispersif term
        if (rd == YES_TS) {
          switch (tcl) {
          case NO_BOUND_TTC: {
            // fill the diagonal value
            if (id_neigh != NONE_TTC) { // SW 28/05/2018 pour confluence

              //                                    mat6[mat4[i + 1] - 1]   +=  theta * dlip / (delta_L*delta_L); ;  //dist = dx ou dy; delta_L = distance centre maille * delta_L
              //                                    b[i]                    += -(1 - theta) * dlip / (delta_L * delta_L) * var[i];
              //                                    //fill the value for each neighbor
              //                                    mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * dlip / (delta_L*delta_L);//* delta_L
              //                                    b[i]                                        += (1 - theta) * dlip / (delta_L * delta_L) * var[id_neigh];

              if ((icard == TOP_TTC || icard == BOTTOM_TTC) && pspecies->pset->aquitard == AQUITARD_OFF_TTC) {
                //

                mat6[mat4[i + 1] - 1] += theta * dlip / (delta_L * thickness);
                ; // dist = dx ou dy; delta_L = distance centre maille * delta_L
                b[i] += -((1 - theta) * dlip / (delta_L * thickness)) * var[i];
                // fill the value for each neighbor
                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * dlip / (delta_L * thickness); //* delta_L
                b[i] += (1 - theta) * dlip / (delta_L * thickness) * var[id_neigh];
                //                                    LP_printf(fpout,"In %s : cell: %d, loc_mat6 = %d\n",__func__,i,mat4[i + 1] - 1); // Check NG pour la première itération --> OK
                //                                    LP_printf(fpout,"In %s : cell: %d, loc_mat6 = %d\n",__func__,i,mat4[i + 1] - 1 - nvois + nvois_ele_i); // Check NG pour la première itération --> OK

              } else if ((icard == TOP_TTC || icard == BOTTOM_TTC) && pspecies->pset->aquitard == AQUITARD_ON_TTC) {

                if (icard == TOP_TTC) {
                  //                                            delta_L = thickness/2+aq_thickness_top;
                  mat6[mat4[i + 1] - 1] += theta * dlip / (delta_L * thickness);
                  ; // dist = dx ou dy; delta_L = distance centre maille * delta_L
                  b[i] += -((1 - theta) * dlip / (delta_L * thickness)) * var[i];
                  // fill the value for each neighbor
                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * dlip / (delta_L * thickness); //* delta_L
                                                                                                        //                                            b[i] += (1 - theta) * dlip / (delta_L * thickness) * temp_aquitard_top;
                  b[i] += (1 - theta) * dlip / (delta_L * thickness) * var[id_neigh];
                  //                                        LP_printf(fpout,
                  //                                                  "NOF bound: i: %d, face: %s delta_L: %e , dist: %e, id_neigh: %d, var = %e, aquitard_cell: %d, t_aquitard_top: %lf\n",
                  //                                                  i, TTC_param_card(icard), delta_L, dist, id_neigh, var[i], aquitard_cell_top, temp_aquitard_top); // DK check
                } else if (icard == BOTTOM_TTC) {
                  //                                            delta_L = thickness/2+aq_thickness_bottom;
                  mat6[mat4[i + 1] - 1] += theta * dlip / (delta_L * thickness);
                  ; // dist = dx ou dy; delta_L = distance centre maille * delta_L
                  b[i] += -((1 - theta) * dlip / (delta_L * thickness)) * var[i];
                  // fill the value for each neighbor
                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * dlip / (delta_L * thickness); //* delta_L
                                                                                                        //                                            b[i] += (1 - theta) * dlip / (delta_L * thickness) * temp_aquitard_bottom;
                  b[i] += (1 - theta) * dlip / (delta_L * thickness) * var[id_neigh];
                  //                                        LP_printf(fpout,
                  //                                                  "NOF bound: i: %d, face: %s delta_L: %e , dist: %e, id_neigh: %d, var = %e, aquitard_cell: %d, t_aquitard_bot: %lf, b[i]: %lf\n",
                  //                                                  i, TTC_param_card(icard), delta_L, dist, id_neigh, var[i], aquitard_cell_bot, temp_aquitard_bottom, b[i]); // DK check
                } else {
                  mat6[mat4[i + 1] - 1] += theta * dlip / (delta_L * dist);
                  ; // dist = dx ou dy; delta_L = distance centre maille * delta_L
                  b[i] += -(1 - theta) * dlip / (delta_L * dist) * var[i];
                  // fill the value for each neighbor
                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * dlip / (delta_L * dist); //* delta_L
                  b[i] += (1 - theta) * dlip / (delta_L * dist) * var[id_neigh];
                  //                                        LP_printf(fpout,
                  //                                                  "NOF bound: i: %d, face: %s delta_L: %e , dist: %e, id_neigh: %d, var = %e, var[idneigh]: %lf\n",
                  //                                                  i, TTC_param_card(icard), delta_L, dist, id_neigh, var[i], var[id_neigh]); // DK check
                }
              }
              //                                    else if ( icard == TOP_TTC || icard == BOTTOM_TTC && pspecies->pset->aquitard == AQUITARD_ON_TTC ) {
              //
              //                                        mat6[mat4[i + 1] - 1] +=  theta * dlip / (dist*delta_L); ;  //dist = dx ou dy; delta_L = distance centre maille * delta_L
              //                                        b[i] += -((1 - theta) * dlip / (dist * delta_L)) * var[i];
              //                                        //fill the value for each neighbor
              //                                        mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * dlip / (dist*delta_L);//* delta_L
              //                                        b[i] += (1 - theta) * dlip / (dist * delta_L) * var[id_neigh];
              //
              ////                                    LP_printf(fpout,"In %s : cell: %d, loc_mat6 = %d\n",__func__,i,mat4[i + 1] - 1); // Check NG pour la première itération --> OK
              ////                                    LP_printf(fpout,"In %s : cell: %d, loc_mat6 = %d\n",__func__,i,mat4[i + 1] - 1 - nvois + nvois_ele_i); // Check NG pour la première itération --> OK
              //
              //
              //
              //                                    }
              //                                LP_printf(fpout,
              //                                          "NOF bound: %d, mat5: %d, mat6_maille: %e, theta: %e, dlip: %e, delta_L: %e , dist: %e, nmaille: %d, var = %e \n",
              //                                          tcl, mat5[i], mat6[mat4[i + 1] - 1], theta, dlip, delta_L, dist, mat4[i + 1] - 1, var[i]); // DK check
              //                                LP_printf(fpout,
              //                                          "NOF bound: %d, mat5: %d, mat6_voisin: %e, theta: %e, dlip: %e, delta_L: %e, dist: %e, nvois: %d, var[idneigh]: %e, b = %e \n",
              //                                          tcl, mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], theta, dlip, delta_L, dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh],b[i]); // DK check
            }
            //                            nvois_ele_i++;
            //                            LP_printf(fpout,
            //                                      "NOF boundary, Dispersion, nvois ele: %d\n", nvois_ele_i); // DK check
            break; // fin face active
          }
          case DIRI_FACE_TTC: {
            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

            if (icard == TOP_TTC || icard == BOTTOM_TTC) { // in the vertical direction we divide by the thickness i.e. size of cell in vertical
              mat6[mat4[i + 1] - 1] += 2 * theta * dlip / (delta_L * thickness);
              b[i] += -2 * (1 - theta) * var[i] * dlip / (delta_L * thickness);
              b[i] += 2 * theta * cl_val * dlip / (delta_L * thickness);
              b[i] += 2 * (1 - theta) * cl_val * dlip / (delta_L * thickness);
            } else {
              mat6[mat4[i + 1] - 1] += 2 * theta * dlip / (delta_L * dist);
              b[i] += -2 * (1 - theta) * var[i] * dlip / (delta_L * dist);
              b[i] += 2 * theta * cl_val * dlip / (delta_L * dist);
              b[i] += 2 * (1 - theta) * cl_val * dlip / (delta_L * dist);
            }

            //                               LP_printf(fpout,
            //                                         "DIRIF boundary cl_val: %e, mat5: %d, dist= %e,  mat6_voisin: %e, dlip: %e, nmaille: %d, b[%d] : %e\n",
            //                                         cl_val, mat5[i], dist, mat6[mat4[i + 1] - 1], dlip, mat4[i + 1] - 1,i , b[i]); // DK check

            //                            LP_printf(fpout,
            //                                      "Dirif boundary, Dispersion, nvois ele: %d\n", nvois_ele_i); // DK check
            //                            nvois_ele_i++;
            break; // fin face diri
          }
          }
          if (tcl == NO_BOUND_TTC && id_neigh != NONE_TTC) { // Modify the nvois_ele_i when there is an active boundary for pure advection case
            nvois_ele_i++;
          }
        }
        /*
         if(tcl==NO_BOUND_TTC && id_neigh != NONE_TTC)
        {
            nvois_ele_i++;
        } // fin face active nb neig
        */

        if (tcl == DIRI_CENTER_TTC) // variable impose au centre de la maille
        {
          cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
          b[i] = cl_val;
          mat6[mat4[i + 1] - 1] = 1; // reste d'Arthur à verifier le bilan semble faux
        }
        if (tcl == NEU_FACE_TTC) // faceNEU_FACE_TTC) // flux impose a la face de la maille
        {
          cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
          // b[i] += -cl_val/(dist*rhowcw);
          if (cl_val >= 0) {                     // SW 08/06/2018 apport, si un prelevement, cela change pas la concentration
            b[i] += cl_val / (delta_L * rhowcw); // SW 17/05/2018 flux entrant ou sortie depend du signe de cl_val soit QC ou UM
          } else if (cl_val < 0) {               // prelevement cl_val = Q/volume; /s pour solute
            mat6[mat4[i + 1] - 1] += (-cl_val * theta / delta_L);
            b[i] += ((1 - theta) * cl_val / delta_L) * var[i]; //(1-theta)*cl_val*var[i];//
            // if(i == 3392) //debug
            //  printf("id == 3392\n");
          }
        }

      } // fin sub_icard
      break;
    } // fin case sub_card
    } // fin case nb_sub
  }   // fin icard

  // Aquitard solution: 3 layers 2 unknowns (aquifer temperature above & below)
  if (pspecies->pset->aquitard == AQUITARD_ON_TTC) { // if the aquitard calculations are active

    if (i < naquitard) {
      coeff_top = pspecies->p_aquitard_ttc[i]->coeff_top;
      coeff_bot = pspecies->p_aquitard_ttc[i]->coeff_bot;

      //            LP_printf(fpout,"In %s : TEST cell: %d, loc_mat6 = %d\n",__func__,i,mat4[i+1]-2);
      //            LP_printf(fpout,"In %s : TEST cell: %d, loc_mat6 = %d\n",__func__,i,mat4[i+1]-1);
      //            LP_printf(fpout,"In %s : TEST cell: %d, loc_mat6 = %d\n",__func__,i,mat4[i+64+1]-2);
      //            LP_printf(fpout,"In %s : TEST cell: %d, loc_mat6 = %d\n",__func__,i,mat4[i+64+1]-1);
      //            mat6[mat4[i+64+1]-2] += -dlip*(coeff_bot); // Coefficient towards the cell above
      //            mat6[mat4[i+64+1]-1] += dlip*(coeff_bot); // Coefficient towards the cell above
      //
      //            mat6[mat4[i+1]-2] += -dlip*(coeff_top); // Coefficient towards the cell above
      //            mat6[mat4[i+1]-1] += dlip*(coeff_top); // Coefficient towards the cell above

      mat6[mat4[nele + i + 1] - 3] += -dlip * (coeff_top); // Coefficient towards the cell above
                                                           //            LP_printf(fpout,"In %s : cell: %d, loc_mat6 = %d\n",__func__,i,mat4[nele+i+1]-3); // Check NG pour la première itération --> OK
      b[nele + i] += 0;
      mat6[mat4[nele + i + 1] - 2] += -dlip * (coeff_bot); // Coefficient towards the cell below
                                                           //            LP_printf(fpout,"In %s : cell: %d, loc_mat6 = %d\n",__func__,i,mat4[nele+i+1]-2); // Check NG pour la première itération --> OK

      mat6[mat4[nele + i + 1] - 1] += dlip * (coeff_top + coeff_bot); // diagonal term of the mat6
                                                                      //            LP_printf(fpout,"In %s : cell: %d, loc_mat6 = %d\n",__func__,i,mat4[nele+i+1]-1); // Check NG pour la première itération --> OK
                                                                      //            LP_printf(fpout,"In %s AQUITARD: cell: %d, dlip: %e coeff_bot %lf coeff_top %lf  \n",__func__, i, dlip, coeff_bot, coeff_top ); // Check NG pour la première itération --> OK
    }
  }

  // Riverbed solution: 2 layers 1 unknowns (Aquifer temperature below), 1 known (Riverbed temperature)
  if (pspecies->pset->riverbed == AQUITARD_ON_TTC) { // if the riverbed calculations are active
    if (i < nriverbed) {
      double temperature_river = pspecies->p_riverbed_ttc[i]->temperature[VAR_ABOVE_TTC][0]; // Known variable from the river solution

      coeff_top = pspecies->p_riverbed_ttc[i]->coeff_top;
      coeff_bot = pspecies->p_riverbed_ttc[i]->coeff_bot;

      //            mat6[mat4[nele+naquitard+i+1]-3] += ; // Coefficient towards the cell above
      b[nele + naquitard + i] += temperature_river * dlip * (coeff_top);
      //    LP_printf(fpout,"In %s : cell: %d, loc_mat6 = %d\n",__func__,i,mat4[nele+naquitard+i+1]-3); // Check NG pour la première itération --> OK
      mat6[mat4[nele + naquitard + i + 1] - 2] += -dlip * (coeff_bot); // Coefficient towards the cell below
                                                                       //    LP_printf(fpout,"In %s : cell: %d, loc_mat6 = %d\n",__func__,i,mat4[nele+naquitard+i+1]-2); // Check NG pour la première itération --> OK

      mat6[mat4[nele + naquitard + i + 1] - 1] += dlip * (coeff_top + coeff_bot); // diagonal term of the mat6
                                                                                  //    LP_printf(fpout,"In %s : cell: %d, loc_mat6 = %d\n",__func__,i,mat4[nele+naquitard+i+1]-1); // Check NG pour la première itération --> OK
    }
  }

  //    LP_printf(fpout,"In %s : Var[%d] = %f\n",__func__,i,b[i]); // Check NG pour la première itération --> OK

} // fin TTC_no_lib_aq_test

// Backup DK 13 10 2021
//// Modified version to fix the nested, DK AR, the correct version is att line 969
////Fonction qui construit mat6 et b
// void TTC_no_cap_libaq(s_carac_ttc *pcarac_ttc,s_link_ttc *plink, s_param_calc_ttc **p_param_calc_ttc,s_species_ttc *pspecies,int i,int tstep,FILE *fpout) {
//     s_gc *pgc;
//     double *mat6, *b, dist, delta_L, *var, u;
//     int tcl, type, nb_sub;
//     int *nsub;
//     int icard, id_neigh;
//     int *mat4, *mat5, ra, rd, nvois;
//     double theta, dlip, rhow, cw, rhowcw, cl_val;
//     int nvois_ele_i = 0 ;
//     int sub_icard; // SW 28/05/2018
//     int id_neigh1, id_neigh2;
//     double u_east, u_west;
//     double u_id_neigh1, u_id_neigh2;
//     tstep=tstep-1;
//     LP_printf(fpout, "Entering %s, tstep is %d\n", __func__, tstep); // DK check
//
//
//     pgc = pspecies->pgc;
//     type = pspecies->pset->type;
//     var = pspecies->pval_ttc->val; // tableau contenant la variable etudiee (temperature ou solute)
//     dist = pspecies->p_param_ttc[i]->param_syst[SIZE_TTC]; // taille de la maille
//     //dist = 1;
//     mat6 = pgc->mat6; //donne val
//     mat5 = pgc->mat5; //donne l'identifiant de chaque maille et de leur voisine
//     mat4 = pgc->mat4; //permet de connaitre le nombre de voisin pour chaque maille
//     b = pgc->b; //donne b
//
////     LP_printf(fpout,"In %s : Var[%d] = %f\n",__func__,i,b[i]); // Check NG pour la première itération --> OK
//
//    nvois = plink->p_neigh_ttc[i]->nvois;
//    theta = pcarac_ttc->theta; // parametre de semi implicite
//    if (type == HEAT_TTC) {
//        rhow = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][RHO_TTC];
//        cw = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][HEAT_CAP_TTC];
//        rhowcw = rhow * cw;
//    } else if (type == SOLUTE_TTC) {
//        rhowcw = 1;
//    }
//    ra = pspecies->pset->calc_process[ADVECTION_TTC]; // terme advectif (YES or NO)
//    rd = pspecies->pset->calc_process[DIFFUSION_TTC]; // terme dispersif (YES or NO)
////    LP_printf(fpout, "nele = %d, number_neighbour: %d, nvois_ele_i: %d \n", i, nvois, nvois_ele_i); // DK check
////    LP_printf(fpout, "advection = %d, diffusion: %d , yes : %d \n", ra, rd,YES_TS); // DK check
////    LP_printf(fpout, "nele = %d, binit: %e \n", i, b[i]); // DK check
//    for (icard = 0; icard < NB_CARD_TTC; icard++) {
//        nb_sub = plink->nsub[i][icard];
//        LP_printf(fpout, "nele = %d, icard: %d, nb_sub: %d \n", i, icard, nb_sub); // DK check
//
//        switch (nb_sub) { // line 276  should start from
//            default: {
//                //LP_printf(fpout, "enter case: 0\n" );
//                sub_icard = 0;
//                cl_val = 0;
//                //dist = plink->pspecies->p_neigh_ttc[i]->delta_L[icard][sub_icard];
//                //dist = plink->pspecies->p_neigh_ttc->pdelta_dist[i].delta_dist[icard][sub_icard];
//
//                tcl      =  pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];
//                id_neigh =  plink->p_neigh_ttc[i]->ivois[icard][sub_icard];
//                delta_L  =  plink->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // distance entre le centre de la maille et de son voisin
//                dlip     =  p_param_calc_ttc[i]->disp_face[icard][sub_icard]; //
////                LP_printf(fpout, "inside func: %s, iteration is %d, dlip is %e \n",__func__, i, dlip);
//
//
//                LP_printf(fpout, "id_neigh for ele: %d , icard: %d , id_neigh: %d, nvois= %d \n", i, icard, id_neigh, nvois);
//
////Dispersif term
//                if (rd == YES_TS) {
//                    switch (tcl) {
//                        case NO_BOUND_TTC: {
//                            //fill the diagonal value
//                            if (id_neigh != NONE_TTC) { // SW 28/05/2018 pour confluence
//
//                                mat6[mat4[i + 1] - 1] +=  theta * dlip / (delta_L*dist); ;  //dist = dx ou dy; delta_L = distance centre maille * delta_L
//                                b[i] += -((1 - theta) * dlip / (dist * delta_L)) * var[i];
//                                //fill the value for each neighbor
//                                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * dlip / (delta_L*dist);//* delta_L
//                                b[i] += (1 - theta) * dlip / (dist * delta_L) * var[id_neigh];
//
////                                LP_printf(fpout,
////                                          "NOF bound: %d, mat5: %d, mat6_maille: %e, theta: %e, dlip: %e, delta_L: %e , dist: %e, nmaille: %d, var = %e \n",
////                                          tcl, mat5[i], mat6[mat4[i + 1] - 1], theta, dlip, delta_L, dist, mat4[i + 1] - 1, var[i]); // DK check
////                                LP_printf(fpout,
////                                          "NOF bound: %d, mat5: %d, mat6_voisin: %e, theta: %e, dlip: %e, delta_L: %e, dist: %e, nvois: %d, var[idneigh]: %e, b = %e \n",
////                                          tcl, mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], theta, dlip, delta_L, dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh],b[i]); // DK check
//                            }
//                            nvois_ele_i++;
//                            break;            // fin face active
//                        }
//                        case DIRI_FACE_TTC: {
//                            if (pcarac_ttc->regime == TRANSIENT_TTC) {
//                                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                            } else {
//                                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                            }
//
//                            mat6[mat4[i + 1] - 1] += 2 * theta * dlip / (dist * dist);
//                            b[i] += -2 * (1 - theta) * var[i] * dlip / (dist * dist);
//                            b[i] += 2 * theta * cl_val * dlip / (dist * dist);
//                            b[i] += 2 * (1 - theta) * cl_val * dlip / (dist * dist);
//
////                            LP_printf(fpout,
////                                      "DIRIF boundary: %e, mat5: %d, dist= %e,  mat6_voisin: %e, dlip: %e, nmaille: %d, b : %e, val %e\n",
////                                      cl_val, mat5[i], dist, mat6[mat4[i + 1] - 1], dlip, mat4[i + 1] - 1 , b[i], cl_val); // DK check
//                            break;            // fin face diri
//                        }
//                    }
//                }
//                /*LP_printf(fpout, "condition_limit: %d, id_neigh = %d, delta_L: %e, dlip = %e, cl_val = %e \n", tcl,
//                          id_neigh, delta_L, dlip, cl_val); // DK check*/
//
//                //Advectif term si on considere la partie advectif de lequation
//                if (ra==YES_TS){
//
//                    LP_printf(fpout, "Entering advection in func: %s\n",__func__ );
//
//                    u=plink->uface[i][icard][sub_icard]; // SW 29/05/2018 pour confluence
//                    LP_printf(fpout, "velocity in m mat is %e\n", u);
//                    switch(icard){
//                        case EAST_TTC:{
//                            if(u >0.0) { //fill the diagonal value
//                                u_west = plink->uface[i][WEST_TTC][sub_icard];
//                                //mat6[mat4[i+1]-1] += theta*u/dist;
//                                mat6[mat4[i+1] - 1] += theta*u/dist + theta*(u - u_west)/dist;
//                                //b[i] += -((1-theta)*u/dist)*var[i];
//                                b[i] += -((1-theta)*u/dist)*var[i] + (1-theta)*var[i]*(u - u_west)/dist;
//                                LP_printf(fpout,
//                                          "EAST_TTC bound: %d, b: %e, mat6_maille: %e, dlip: %e, dist: %e, nmaille: %d, var = %e, velo East %f, velo West %f \n",
//                                          tcl, b[i], mat6[mat4[i + 1] - 1], dlip, dist, mat4[i + 1] - 1, var[i], u, u_west); // DK check
//                                ;
//                            } else {
//                                switch (tcl) {
//                                    case NO_BOUND_TTC:{
//                                        //face active //fill the value for neighbor
//                                        if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard][0]!=DIRI_CENTER_TTC) {
//                                            mat6[mat4[i+1]-1] += -theta*u/dist;
//                                            mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta*u/dist;
////                                            LP_printf(fpout,
////                                                      "NOF bound: %d, b: %e, mat6_voisin: %e, dlip: %e, dist: %e, nvois: %d, var[idneigh]: %e \n",
////                                                      tcl, b[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], dlip, dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh]); // DK check
//                                            b[i] += -(1-theta)*u/dist*var[id_neigh];
//                                        }
//                                        nvois_ele_i++;
//                                        break;
//                                    }
//                                    case DIRI_FACE_TTC:{
//                                        //face active //bound adv
//                                        if (pcarac_ttc->regime == TRANSIENT_TTC) {
//                                            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                        } else {
//                                            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                        }
//                                        mat6[mat4[i+1]-1] += -theta*u/dist;
//                                        b[i] += -theta*cl_val*u/dist;
//                                        b[i] += -((1-theta)*cl_val*u/dist);
//
//                                        nvois_ele_i++;
//                                        break;
//                                    }
//                                }
//                            }
//                            break;
//                        }
//
//
//                        case WEST_TTC:{
//                            if(u < 0.0) { //fill the diagonal value
//                                mat6[mat4[i+1]-1] += theta*u/dist;
//                                b[i] += -((1-theta)*u/dist)*var[i];
//                                LP_printf(fpout,
//                                          "WEST_TTC bound: %d, b: %e, mat6_maille: %e, dlip: %e, dist: %e, nmaille: %d, var = %e \n",
//                                          tcl, b[i], mat6[mat4[i + 1] - 1], dlip, dist, mat4[i + 1] - 1, var[i]); // DK check
//
//                                nvois_ele_i++;
//                            }
//                            else
//                            {
//                                switch (tcl) {
//                                    case NO_BOUND_TTC:{
//                                        //face active
//                                        if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard][0]!=DIRI_CENTER_TTC) {
//                                            //mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - theta*u/dist;
//                                            u_east = plink->uface[i][EAST_TTC][sub_icard];
//                                            mat6[mat4[i+1]-1] += -theta*u/dist;
//                                            mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += - theta*u_east/dist;
//                                            //b[i]+= (1-theta)*u/dist*var[id_neigh];
//                                            b[i]+= (1-theta)*u/dist*var[id_neigh];
////                                            LP_printf(fpout,
////                                                      "NOF bound: %d, b: %e, mat6_voisin: %e, dlip: %e, dist: %e, nvois: %d, var[idneigh]: %e \n",
////                                                      tcl, b[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], dlip, dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh]); // DK check
//                                        }
//                                        nvois_ele_i++;
//                                        break;
//                                    }
//                                    case DIRI_FACE_TTC:{
//                                        if (pcarac_ttc->regime == TRANSIENT_TTC) {
//                                            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                        } else {
//                                            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                        }
//                                        mat6[mat4[i+1]-1] += -theta*u/dist;
//                                        b[i] += theta*cl_val*u/dist;
//                                        b[i] += ((1-theta)*cl_val*u/dist);
//
//                                        nvois_ele_i++;
//                                        break;
//                                    }
//                                }
//                            }
//                            break; //case tcl West
//                        }
//
//                        case NORTH_TTC:{
//                            if(u >0.0) { //fill the diagonal value
//                                LP_printf(fpout, "case NORTH_TTC u > 0\n");
//                                mat6[mat4[i+1]-1] += theta*u/dist;
//                                b[i] += -((1-theta)*u/dist)*var[i];
//                                LP_printf(fpout,
//                                          "NOF bound: %d, b: %e, mat6_maille: %e, dlip: %e, dist: %e, nmaille: %d, var = %e , V NORTH_TTC %f\n",
//                                          tcl, b[i], mat6[mat4[i + 1] - 1], dlip, dist, mat4[i + 1] - 1, var[i], u ); // DK check
//
//                                nvois_ele_i++;
//                            }
//                            else
//                            {  LP_printf(fpout, "case NORTH_TTC u < 0\n");
//                                switch (tcl) {
//                                    case NO_BOUND_TTC:{
//                                        //face active //fill the value for neighbor
//                                        if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard][0]!=DIRI_CENTER_TTC) {
//
//                                            mat6[mat4[i+1]-1] += theta*u/dist;
//                                            mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u / dist;
//                                            b[i] += -(1 - theta) * u / dist * var[id_neigh];
//                                            LP_printf(fpout,
//                                                      "NOF bound: %d, b: %e, mat6_voisin: %e, dlip: %e, dist: %e, nvois: %d, var[idneigh]: %e, V N NO_BOUND : %f \n",
//                                                      tcl, b[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], dlip,
//                                                      dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh],
//                                                      u); // DK check
//                                        }
//                                        nvois_ele_i++;
//                                        break;
//                                    }
//                                    case DIRI_FACE_TTC:{
//                                        //face active //bound adv
//                                        if (pcarac_ttc->regime == TRANSIENT_TTC) {
//                                            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                        } else {
//                                            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                        }
//
//                                        mat6[mat4[i+1]-1] += theta*u/dist;
//                                        b[i] +=  theta*cl_val*u/dist;
//
//                                        b[i] += - ((1-theta)*cl_val*u/dist);
//                                        LP_printf(fpout,
//                                                  "DIRIF boundary: %e, mat5: %d, dist= %e,  mat6_voisin: %e, velocity: %e, nmaille: %d, b : %e, val %e\n",
//                                                  cl_val, mat5[i], dist, mat6[mat4[i + 1] - 1], u, mat4[i + 1] - 1 , b[i], cl_val); // DK check
//                                        LP_printf(fpout, "BC NORTH_TTC %e \n", cl_val);
//                                        nvois_ele_i++;
//                                        break;
//                                    }
//                                }
//                            }
//                            break; //case tcl North
//                        }
//
//
//                        case SOUTH_TTC:{
//                            if(u < 0.0) { //fill the diagonal value
//                                LP_printf(fpout, "case SOUTH  u < 0\n");
//                                mat6[mat4[i+1]-1] += -theta*u/dist;
//                                b[i] += ((1-theta)*u/dist)*var[i];
//                                LP_printf(fpout,
//                                          "NOF bound: in advective %d, b: %e, mat6_maille: %e, dlip: %e, dist: %e, nmaille: %d, var = %e , V south TTC %f\n",
//                                          tcl, b[i], mat6[mat4[i + 1] - 1], dlip, dist, mat4[i + 1] - 1, var[i], u ); // DK check
//
//                                nvois_ele_i++;
//                            }
//                            else
//                            {   LP_printf(fpout, "case SOUTH  u > 0\n");
//                                switch (tcl) {
//                                    case NO_BOUND_TTC:  //face active
//                                        if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard][0]!=DIRI_CENTER_TTC) {
//                                            mat6[mat4[i+1]-1] += theta*u/dist;
//                                            mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u / dist;
//                                            b[i] += (1 - theta) * u / dist * var[id_neigh];
//                                            LP_printf(fpout,
//                                                      "NOF bound in advective: %d, b: %e, mat6_voisin: %e, velocity: %e, dist: %e, nvois: %d, var[idneigh]: %e , mat6 = %e\n",
//                                                      tcl, b[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], u,
//                                                      dist, mat4[i + 1] - 1 - nvois + nvois_ele_i, var[id_neigh],
//                                                      -theta * u / dist); // DK check
//                                        }
//                                        nvois_ele_i++;
//                                        break;
//                                    case DIRI_FACE_TTC:{
//                                        if (pcarac_ttc->regime == TRANSIENT_TTC) {
//                                            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                        } else {
//                                            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                        }
//                                        LP_printf(fpout, "BC SOUTH_TTC %e \n", cl_val);
//                                        mat6[mat4[i+1]-1] += theta*u/dist;
//                                        b[i] += theta*cl_val*u/dist;
//
//                                        b[i] += ((1-theta)*cl_val*u/dist);
//                                        nvois_ele_i++;
//
//                                        LP_printf(fpout,
//                                                  "DIRIF boundary: %e, mat5: %d, dist= %e,  mat6_voisin: %e, velocity: %e, nmaille: %d, b : %e, val %e\n",
//                                                  cl_val, mat5[i], dist, mat6[mat4[i + 1] - 1], u, mat4[i + 1] - 1 , b[i], cl_val); // DK check
//                                        break;
//                                    }
//
//                                }
//                            }
//                            break; //case tcl South
//                        }
//                            break;
//                    }
//                }
//
//                /* if(tcl==DIRI_CENTER_TTC) // variable impose au centre de la maille
//                {
//                    cl_val=plink->pspecies->p_boundary_ttc[i]->valcl[1][1];
//                    b[i]=cl_val;
//                    mat6[mat4[i+1]-1]=1;//reste d'Arthur à verifier le bilan semble faux
//                }
//                if (tcl==NEU_FACE_TTC) //faceNEU_FACE_TTC) // flux impose a la face de la maille
//                {
//                    cl_val=plink->pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                    //b[i] += -cl_val/(dist*rhowcw);
//                    if(cl_val > 0){ // SW 08/06/2018 apport, si un prelevement, cela change pas la concentration
//                        b[i] += cl_val/(dist*rhowcw); // SW 17/05/2018 flux entrant ou sortie depend du signe de cl_val soit QC ou UM
//                    }
//                    else if (cl_val < 0){ // prelevement cl_val = Q/volume; /s pour solute
//                        mat6[mat4[i+1]-1] += (-cl_val*theta/dist);
//                        b[i] += ((1-theta)*cl_val/dist)*var[i]; //(1-theta)*cl_val*var[i];//
//
//                    }
//                } */
//                break;
//            }
//            case 2: {
//                //LP_printf(fpout, "enter case: 2\n" );
//                sub_icard = 0;
//                for (sub_icard = 0; sub_icard < nb_sub; sub_icard++) {
//                    cl_val = 0;
//
//                    tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];
//                    id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];
//                    delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // distance entre le centre de la maille et de son voisin
//                    dlip = p_param_calc_ttc[i]->disp_face[icard][sub_icard]; //
//                    dlip = dlip*2/3;
////                    LP_printf(fpout, "id_neigh for ele: %d , icard: %d , id_neigh: %d\n", i, icard, id_neigh);
//                    //nvois_ele_i++;
////Dispersif term
//                    //if (rd==YES_TS)
//                    if (rd == YES_TS) {
//                        switch (tcl) {
//                            case NO_BOUND_TTC: {
//                                //fill the diagonal value
//                                if (id_neigh != NONE_TTC) { // SW 28/05/2018 pour confluence
//
//                                    mat6[mat4[i + 1] - 1] += theta * dlip /(dist*dist); //(dist *2);    //dist = dx ou dy; delta_L = distance centre maille  * delta_L
//                                    b[i] += -((1 - theta) * dlip / (dist * delta_L)) * var[i];
//                                    //fill the value for each neighbor
//                                    mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * dlip /(dist*dist); //(dist * 2); //* delta_L
//                                    b[i] += (1 - theta) * dlip / (dist * delta_L) * var[id_neigh];
//
//
////                                    LP_printf(fpout,
////                                              "NOF bound: %d, mat5: %d, mat6_maille: %e, theta: %e, dlip: %e, delta_L: %e , dist: %e, nmaille: %d\n",
////                                              tcl, mat5[i], mat6[mat4[i + 1] - 1], theta, dlip, delta_L, dist, mat4[i + 1] - 1); // DK check
////                                    LP_printf(fpout,
////                                              "NOF bound: %d, mat5: %d, mat6_voisin: %e, theta: %e, dlip: %e, delta_L: %e, dist: %e, nvois: %d\n",
////                                              tcl, mat5[i], mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i], theta, dlip, delta_L, dist, mat4[i + 1] - 1 - nvois + nvois_ele_i); // DK check
//                                }
//                                nvois_ele_i++;
//                                break;            // fin face active
//                            }
//
//                        }
//                    }
////Advectif term
//                    if (ra==YES_TS) // si on considere la partie advectif de lequation
//                    { // TODO  debug advectif term: print the velocities to check if they are read correctly
//
//                        LP_printf(fpout, "entering_advection\n");
//
//                        u=plink->uface[i][icard][sub_icard]; // SW 29/05/2018 pour confluence
//                        LP_printf(fpout, "velocity in m mat is %e\n", u);
//                        u_west = plink->uface[i][WEST_TTC][sub_icard];
//                        switch(icard){
//                            case EAST_TTC:
//                                if(u >0.0) { //fill the diagonal value
//                                    //mat6[mat4[i+1]-1] += theta*u/dist;
//                                    mat6[mat4[i+1]-1] += theta*u/dist + theta*(u - u_west)/dist;
//                                    //b[i] += -((1-theta)*u/dist)*var[i];
//                                    b[i] += -((1-theta)*u/dist)*var[i] + (1-theta)*var[i]*(u - u_west)/dist;
//                                } else {
//                                    switch (tcl) {
//                                        case NO_BOUND_TTC:{
//                                            //face active //fill the value for neighbor
//                                            if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard][0]!=DIRI_CENTER_TTC) {
//
//                                                mat6[mat4[i+1]-1-nvois+nvois_ele_i] += theta*u/dist;
//                                                b[i] += -(1-theta)*u/dist*var[id_neigh];
//                                            }
//                                            break;
//                                        }
//                                        case DIRI_FACE_TTC:{
//                                            //face active //bound adv
//                                            cl_val=pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                            b[i] += -theta*cl_val*u/dist;
//                                            b[i] += -((1-theta)*cl_val*u/dist);
//                                            break;
//                                        }
//                                    }
//                                }
//                                break;
//                            case WEST_TTC:
//                                if(u < 0.0) { //fill the diagonal value
//                                    mat6[mat4[i+1]-1] += theta*u/dist;
//                                    b[i] += -((1-theta)*u/dist)*var[i];
//                                }
//                                else
//                                {
//                                    switch (tcl) {
//                                        case NO_BOUND_TTC:{//face active
//                                            if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard][0]!=DIRI_CENTER_TTC) {
//                                                //mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - theta*u/dist;
//                                                u_east = plink->uface[i][EAST_TTC][sub_icard];
//                                                mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - theta*u_east/dist;
//                                                //b[i]+= (1-theta)*u/dist*var[id_neigh];
//                                                b[i]+= (1-theta)*u/dist*var[id_neigh];
//                                            }
//                                            break;
//                                        }
//                                        case DIRI_FACE_TTC:{
//                                            cl_val=pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                            b[i] += theta*cl_val*u/dist;
//                                            b[i] += ((1-theta)*cl_val*u/dist);
//                                            break;
//                                        }
//                                    }
//                                }
//                                break;
//
//                            case NORTH_TTC:
//                                if(u >0.0) { //fill the diagonal value
//                                    mat6[mat4[i+1]-1] += theta*u/dist;
//                                    b[i] += -((1-theta)*u/dist)*var[i];
//                                }
//                                else
//                                {
//                                    switch (tcl) {
//                                        case NO_BOUND_TTC:  //face active //fill the value for neighbor
//                                            //if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
//                                            if(id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard]!=DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
//                                                mat6[mat4[i+1]-1-nvois+nvois_ele_i] += theta*u/dist;
//                                                b[i] += -(1-theta)*u/dist*var[id_neigh];
//                                            }
//                                            break;
//                                        case DIRI_FACE_TTC: //face active //bound adv
//                                            cl_val=pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                            b[i] += - theta*cl_val*u/dist;
//                                            b[i] += - ((1-theta)*cl_val*u/dist);
//                                            break;
//                                    }
//                                }
//                                break;
//
//                            case SOUTH_TTC:
//                                if(u < 0.0) { //fill the diagonal value
//                                    mat6[mat4[i+1]-1] += -theta*u/dist;
//                                    b[i] += ((1-theta)*u/dist)*var[i];
//                                }
//                                else
//                                {
//                                    switch (tcl) {
//                                        case NO_BOUND_TTC:  //face active
//                                            //if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
//                                            if(id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard]!=DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
//                                                mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - theta*u/dist;
//                                                b[i]+= (1-theta)*u/dist*var[id_neigh];
//                                            }
//                                            break;
//                                        case DIRI_FACE_TTC:
//                                            cl_val=pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                                            b[i] += theta*cl_val*u/dist;
//                                            b[i] += ((1-theta)*cl_val*u/dist);
//                                            break;
//                                    }
//                                }
//                                break;
//                        }
//                    }
//                    /*
//                     if(tcl==NO_BOUND_TTC && id_neigh != NONE_TTC)
//                    {
//                        nvois_ele_i++;
//                    } // fin face active nb neig
//                    */
//
//                    if(tcl==DIRI_CENTER_TTC) // variable impose au centre de la maille
//                    {
//                        cl_val=pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                        b[i]=cl_val;
//                        mat6[mat4[i+1]-1]=1;//reste d'Arthur à verifier le bilan semble faux
//                    }
//                    if (tcl==NEU_FACE_TTC) //faceNEU_FACE_TTC) // flux impose a la face de la maille
//                    {
//                        cl_val=pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
//                        //b[i] += -cl_val/(dist*rhowcw);
//                        if(cl_val > 0){ // SW 08/06/2018 apport, si un prelevement, cela change pas la concentration
//                            b[i] += cl_val/(dist*rhowcw); // SW 17/05/2018 flux entrant ou sortie depend du signe de cl_val soit QC ou UM
//                        }
//                        else if (cl_val < 0){ // prelevement cl_val = Q/volume; /s pour solute
//                            mat6[mat4[i+1]-1] += (-cl_val*theta/dist);
//                            b[i] += ((1-theta)*cl_val/dist)*var[i]; //(1-theta)*cl_val*var[i];//
//                            //if(i == 3392) //debug
//                            // printf("id == 3392\n");
//                        }
//                    }
//
//                } // fin sub_icard
//
//
//                break;
//            } // case 2
//                //LP_printf(fpout,"icard: %d, number_subface: %d \n",icard,  nb_sub ); // DK check
//                //if (nb_sub == 0) nb_sub = 1; // Parade pour rentrer dans le for si pas de subfaces
//                //LP_printf(fpout,"condition_limit: %d, id_neigh = %d, delta_L: %e, dlip = %e, cl_val = %e \n", tcl, id_neigh, delta_L, dlip , cl_val); // DK check
//        } //fin case nb_sub
//
//
//    } // fin icard
////    LP_printf(fpout, "nele = %d, bfinal: %e \n", i, b[i]); // DK check
//    //  LP_printf(fpout, "id: %d, b: %e, mat6: %e \n", i, b[i], mat6[mat4[i + 1] - 1]); // DK check
////    LP_printf(fpout,"In %s : b[%d] = %f\n",__func__,i,b[i]); // Check NG pour la première itération --> OK
////    LP_printf(fpout, "Exiting %s \n", __func__); // DK check
//} // fin TTC_no_lib_aq
//

// schema centre // SW 27/04/2018
void TTC_schema_centre(s_carac_ttc *pcarac_ttc, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, int i, double dt, FILE *fpout) {
  s_gc *pgc;
  double *mat6, *b, dist, delta_L, *var, u, rhow, cw, rhowcw;
  int tcl, type;
  int icard, id_neigh, sub_icard;
  int *mat4, *mat5, ra, rd, nvois;
  double theta, dlip, cl_val;
  double terme_convection;
  int nvois_ele_i = 0;

  pgc = pspecies->pgc;
  type = pspecies->pset->type;
  var = pspecies->pval_ttc->val;                         // tableau contenant la variable etudiee (temperature ou solute)
  dist = pspecies->p_param_ttc[i]->param_syst[SIZE_TTC]; // taille de la maille
  mat6 = pgc->mat6;                                      // donne val
  mat5 = pgc->mat5;                                      // donne l'identifiant de chaque maille et de leur voisine
  mat4 = pgc->mat4;                                      // permet de connaitre le nombre de voisin pour chaque maille
  b = pgc->b;                                            // donne b
  nvois = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->nvois;
  theta = pcarac_ttc->theta;                        // parametre de semi implicite
  ra = pspecies->pset->calc_process[ADVECTION_TTC]; // terme advectif (YES or NO)
  rd = pspecies->pset->calc_process[DIFFUSION_TTC]; // terme dispersif (YES or NO)

  if (type == HEAT_TTC) {
    rhow = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][RHO_TTC];
    cw = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][HEAT_CAP_TTC];
    rhowcw = rhow * cw;
  } else if (type == SOLUTE_TTC) {
    rhowcw = 1;
  }

  for (icard = 0; icard < NB_CARD_TTC; icard++) {
    tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];
    if (tcl == NO_BOUND_TTC) {
      for (sub_icard = 0; sub_icard < SUB_CARD_TTC; sub_icard++) { // SW 28/05/2018 pour confluence
                                                                   // id_neigh=plink->pspecies->p_neigh_ttc[i]->ivois[icard];
        // delta_L=plink->pspecies->p_neigh_ttc[i]->delta_L[icard]; // distance entre le centre de la maille et de son voisin
        dlip = p_param_calc_ttc[i]->disp_face[icard][sub_icard];                               // dispersion + conduction
        id_neigh = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->ivois[icard][sub_icard];  // SW 28/05/2018 pour confluence
        delta_L = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // SW 28/05/2018 pour confluence
        // if (rd==YES_TS) // si on considere la partie dispersif de lequation
        if (rd == YES_TS && id_neigh != NONE_TTC) // SW 28/05/2018 pour confluence
        {
          // fill the diagonal value
          mat6[mat4[i + 1] - 1] += theta * dlip / (dist * delta_L);
          b[i] += -((1 - theta) * dlip / (dist * delta_L)) * var[i];
          // fill the value for each neighbor
          mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] = -theta * dlip / (dist * delta_L);
          b[i] += (1 - theta) * dlip / (dist * delta_L) * var[id_neigh];
        }

        // if(ra==YES_TS)
        if (ra == YES_TS && id_neigh != NONE_TTC) // SW 28/05/2018 pour confluence
        {
          u = pcarac_ttc->uface[i][icard][sub_icard]; // vitesse de Darcy
          // no diagonal coefficient for schema centre
          // par defaut transport de WEST a EAST, de NORTH a SOUTH V > 0, sinon V <0

          switch (icard) {
          case EAST_TTC: {
            // mat6[mat4[i+1]-1-nvois+nvois_ele_i] += theta*u/(2*dist) - theta*dt*u*u/(2*dist*delta_L);
            // b[i] += -(1 - theta)*u/(2*dist)*var[id_neigh] + (1-theta)*u*u*dt/(2*dist*delta_L)*var[id_neigh];
            // mat6[mat4[i+1]-1] += theta*dt*u*u/(2*dist*delta_L);
            // b[i] += -(1-theta)*u*u*dt/(2*dist*delta_L)*var[i]; // SW 27/04/2018 schema Lax-Wendroff
            mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta * u / (2 * dist);
            b[i] += -(1 - theta) * u / (2 * dist) * var[id_neigh];
            break;
          }
          case WEST_TTC: {
            // mat6[mat4[i+1]-1-nvois+nvois_ele_i] += -theta*u/(2*dist) - theta*dt*u*u/(2*dist*delta_L);
            // b[i] += (1 - theta)*u/(2*dist)*var[id_neigh] + (1-theta)*u*u*dt/(2*dist*delta_L)*var[id_neigh];
            // mat6[mat4[i+1]-1] += theta*dt*u*u/(2*dist*delta_L);
            // b[i] += -(1-theta)*u*u*dt/(2*dist*delta_L)*var[i];
            mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u / (2 * dist);
            b[i] += (1 - theta) * u / (2 * dist) * var[id_neigh];
            break;
          }
          case NORTH_TTC: {
            mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta * u / (2 * dist);
            b[i] += -(1 - theta) * u / (2 * dist) * var[id_neigh];
            break;
          }
          case SOUTH_TTC: {
            mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u / (2 * dist);
            b[i] += (1 - theta) * u / (2 * dist) * var[id_neigh];
            break;
          }
          }
        }
        nvois_ele_i++;
      }
    } else if (tcl == DIRI_FACE_TTC) {
      delta_L = dist / 2;
      dlip = p_param_calc_ttc[i]->disp_face[icard][sub_icard]; // dispersion + conduction
      cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
      if (rd == YES_TS) // si on considere la partie dispersif de lequation
      {
        // fill the diagonal value
        mat6[mat4[i + 1] - 1] += theta * dlip / (dist * dist);
        b[i] += -((1 - theta) * dlip / (dist * dist)) * var[i];
        // fill the value for each neighbor
        b[i] += theta * cl_val * dlip / (dist * delta_L);
      }
      if (ra == YES_TS) {
        // TODO correct the hard value of the subfaces for the advective term
        u = pcarac_ttc->uface[i][icard][0]; // SW 28/05/2018 pour cette condition, n'accepte qu'ne valeur
        switch (icard) {
        case EAST_TTC: {
          if (u > 0.0) {
            mat6[mat4[i + 1] - 1] += theta * u / dist;
            b[i] += -(1 - theta) * u / dist * var[i];
          } else
            b[i] += u * cl_val / dist;
          break;
        }
        case WEST_TTC: {
          if (u > 0.0)
            b[i] += u * cl_val / dist;
          else {
            mat6[mat4[i + 1] - 1] += -theta * u / dist;
          }

          break;
        }
        case NORTH_TTC: {
          if (u > 0.0)
            b[i] += u * cl_val / dist;
          else {
            mat6[mat4[i + 1] - 1] += -theta * u / dist;
          }

          break;
        }
        case SOUTH_TTC: {
          if (u > 0.0) {
            mat6[mat4[i + 1] - 1] += theta * u / dist;
            b[i] += -(1 - theta) * u / dist * var[i];
          } else
            b[i] += u * cl_val / dist;
          break;
        }
        }
      }
    } else if (tcl == NEU_FACE_TTC) {
      cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
      b[i] += cl_val / (dist * rhowcw);
    }
  }
}

// Fonction qui construit mat6 et b
/*void TTC_no_cap(s_carac_ttc *pcarac_ttc,s_param_calc_ttc **p_param_calc_ttc,s_species_ttc *pspecies,int i,FILE *fpout)
{
  s_gc *pgc;
  double *mat6,*b,dist,delta_L,*var,u;
  int tcl,type;
  int icard,id_neigh;
  int *mat4,*mat5,ra,rd,nvois;
  double theta,dlip,rhow,cw,rhowcw,cl_val;
  int nvois_ele_i=0;


  pgc=pspecies->pgc;
  type=pspecies->pset->type;
  var=plink->pspecies->pval_ttc->val; // tableau contenant la variable etudiee (temperature ou solute)
  dist=pspecies->p_param_ttc[i]->param_syst[SIZE_TTC]; // taille de la maille
  mat6=pgc->mat6; //donne val
  mat5=pgc->mat5; //donne l'identifiant de chaque maille et de leur voisine
  mat4=pgc->mat4; //permet de connaitre le nombre de voisin pour chaque maille
  b=pgc->b; //donne b
  nvois=plink->pspecies->p_neigh_ttc[i]->nvois;
  theta=pspecies->pset->theta; // parametre de semi implicite
  if (type==HEAT_TTC)
  {
  rhow=pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][RHO_TTC];
  cw=pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][HEAT_CAP_TTC];
  rhowcw=rhow*cw;
  }
  else if (type==SOLUTE_TTC)
  {
  rhowcw=1;
  }
  ra=pspecies->pset->calc_process[ADVECTION_TTC]; // terme advectif (YES or NO)
  rd=pspecies->pset->calc_process[DIFFUSION_TTC]; // terme dispersif (YES or NO)
        for (icard=0;icard<NB_CARD_TTC;icard++)
        {
        tcl=plink->pspecies->p_boundary_ttc[i]->icl[icard];
        id_neigh=plink->pspecies->p_neigh_ttc[i]->ivois[icard];
        delta_L=plink->pspecies->p_neigh_ttc[i]->delta_L[icard]; // distance entre le centre de la maille et de son voisin
        dlip=p_param_calc_ttc[i]->disp_face[icard]; //
//Dispersif term
                if (rd==YES_TS)
                {
                        switch (tcl) {
                                case NO_BOUND_TTC: {
                                //fill the diagonal value
                                mat6[mat4[i+1]-1] += theta*dlip/(dist*delta_L);    //dist = dx ou dy; delta_L = distance centre maille
                                b[i] += -((1-theta)*dlip/(dist*delta_L))*var[i];
                                //fill the value for each neighbor
                                mat6[mat4[i+1]-1-nvois+nvois_ele_i] += -theta*dlip/(dist*delta_L);
                                b[i] += (1-theta)*dlip/(dist*delta_L)*var[id_neigh];
                                break; 			// fin face active
                                }
                                case DIRI_FACE_TTC: {
                                cl_val=plink->pspecies->p_boundary_ttc[i]->valcl[icard];
                                mat6[mat4[i+1]-1] += 2*theta*dlip/(dist*dist);
                        b[i] += -2*(1-theta)*var[i]*dlip/(dist*dist);
                        b[i] += 2*theta*cl_val*dlip/(dist*dist);
                        b[i] += 2*(1-theta)*cl_val*dlip/(dist*dist);
                                break; 			// fin face diri
                                }
                        }
                }

//Advectif term
                if (ra==YES_TS) // si on considere la partie advectif de lequation
                {
                u=pcarac_ttc->p_link[pspecies->id]->uface[i][icard]; // vitesse de Darcy
                        switch(icard){
                                case EAST_TTC:
                                        if(u >0.0) { //fill the diagonal value
                                        mat6[mat4[i+1]-1] += theta*u/dist;
                                                b[i] += -((1-theta)*u/dist)*var[i];
                                        }
                                   else
                                        {
                                                switch (tcl) {
                                                        case NO_BOUND_TTC:  //face active //fill the value for neighbor
                                                                if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
                                                                        mat6[mat4[i+1]-1-nvois+nvois_ele_i] += theta*u/dist;
                                                                        b[i] += -(1-theta)*u/dist*var[id_neigh];
                                                                }
                                                        break;
                                                        case DIRI_FACE_TTC: //face active //bound adv
                                                                cl_val=plink->pspecies->p_boundary_ttc[i]->valcl[icard];
                                                                b[i] += -theta*cl_val*u/dist;
                                                                b[i] += -((1-theta)*cl_val*u/dist);
                                                        break;
                                                }
                                        }
                                break;

                                case WEST_TTC:
                                        if(u < 0.0) { //fill the diagonal value
                                                mat6[mat4[i+1]-1] += theta*u/dist;
                                        b[i] += -((1-theta)*u/dist)*var[i];
                                        }
                                   else
                                        {
                                                switch (tcl) {
                                                        case NO_BOUND_TTC:  //face active
                                                                if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
                                                                        mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - theta*u/dist;
                                                                        b[i]+= (1-theta)*u/dist*var[id_neigh];
                                                                }
                                                        break;
                                                        case DIRI_FACE_TTC:
                                                                cl_val=plink->pspecies->p_boundary_ttc[i]->valcl[icard];
                                                                b[i] += theta*cl_val*u/dist;
                                                        b[i] += ((1-theta)*cl_val*u/dist);
                                                        break;
                                                }
                                        }
                                break;

                                case NORTH_TTC:
                                        if(u >0.0) { //fill the diagonal value
                                             mat6[mat4[i+1]-1] += theta*u/dist;
                                             b[i] += -((1-theta)*u/dist)*var[i];
                                        }
                                   else
                                        {
                                                switch (tcl) {
                                                        case NO_BOUND_TTC:  //face active //fill the value for neighbor
                                                                if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
                                                                mat6[mat4[i+1]-1-nvois+nvois_ele_i] += theta*u/dist;
                                                                b[i] += -(1-theta)*u/dist*var[id_neigh];
                                                                }
                                                        break;
                                                        case DIRI_FACE_TTC: //face active //bound adv
                                                                cl_val=plink->pspecies->p_boundary_ttc[i]->valcl[icard];
                                                                b[i] += - theta*cl_val*u/dist;
                                                        b[i] += - ((1-theta)*cl_val*u/dist);
                                                        break;
                                                }
                                        }
                                break;

                                case SOUTH_TTC:
                                        if(u < 0.0) { //fill the diagonal value
                                                mat6[mat4[i+1]-1] += -theta*u/dist;
                                                b[i] += ((1-theta)*u/dist)*var[i];
                                        }
                                   else
                                        {
                                                switch (tcl) {
                                                        case NO_BOUND_TTC:  //face active
                                                                if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
                                                                        mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - theta*u/dist;
                                                                        b[i]+= (1-theta)*u/dist*var[id_neigh];
                                                                }
                                                        break;
                                                        case DIRI_FACE_TTC:
                                                                cl_val=plink->pspecies->p_boundary_ttc[i]->valcl[icard];
                                                                b[i] += theta*cl_val*u/dist;
                                                        b[i] += ((1-theta)*cl_val*u/dist);
                                                        break;
                                                }
                                        }
                                break;
                        }
                }

                if(tcl==NO_BOUND_TTC)
                {
                        nvois_ele_i++;
                } // fin face active nb neig


                if(tcl==DIRI_CENTER_TTC) // variable impose au centre de la maille
                {
                cl_val=plink->pspecies->p_boundary_ttc[i]->valcl[1];
                b[i]=cl_val;
            mat6[mat4[i+1]-1]=1;//reste d'Arthur à verifier le bilan semble faux
                }
                if (tcl==NEU_FACE_TTC) //faceNEU_FACE_TTC) // flux impose a la face de la maille
                {
                cl_val=plink->pspecies->p_boundary_ttc[i]->valcl[icard];
            b[i] += -cl_val/(dist*rhowcw);
                }

        } // fin icard
}*/

// Fonction qui construit mat6 et b
void TTC_no_cap(s_carac_ttc *pcarac_ttc, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, int i, FILE *fpout) {
  s_gc *pgc;
  double *mat6, *b, dist, delta_L, *var, u;
  int tcl, type;
  int icard, id_neigh;
  int *mat4, *mat5, ra, rd, nvois;
  double theta, dlip, rhow, cw, rhowcw, cl_val;
  int nvois_ele_i = 0;
  int sub_icard;            // SW 28/05/2018
  int id_neigh1, id_neigh2; // SW 07/06/2018 pour conf_diff
  double u_east, u_west;
  double u_id_neigh1, u_id_neigh2;

  pgc = pspecies->pgc;
  type = pspecies->pset->type;
  var = pspecies->pval_ttc->val;                         // tableau contenant la variable etudiee (temperature ou solute)
  dist = pspecies->p_param_ttc[i]->param_syst[SIZE_TTC]; // taille de la maille
  mat6 = pgc->mat6;                                      // donne val
  mat5 = pgc->mat5;                                      // donne l'identifiant de chaque maille et de leur voisine
  mat4 = pgc->mat4;                                      // permet de connaitre le nombre de voisin pour chaque maille
  b = pgc->b;                                            // donne b
  nvois = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->nvois;
  theta = pcarac_ttc->theta; // parametre de semi implicite
  if (type == HEAT_TTC) {
    rhow = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][RHO_TTC];
    cw = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][HEAT_CAP_TTC];
    rhowcw = rhow * cw;
  } else if (type == SOLUTE_TTC) {
    rhowcw = 1;
  }
  ra = pspecies->pset->calc_process[ADVECTION_TTC]; // terme advectif (YES or NO)
  rd = pspecies->pset->calc_process[DIFFUSION_TTC]; // terme dispersif (YES or NO)
  for (icard = 0; icard < NB_CARD_TTC; icard++)
    for (sub_icard = 0; sub_icard < SUB_CARD_TTC; sub_icard++) { // SW 28/05/2018 pour confluence
      {
        tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];
        id_neigh = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->ivois[icard][sub_icard];
        delta_L = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // distance entre le centre de la maille et de son voisin
        dlip = p_param_calc_ttc[i]->disp_face[icard][sub_icard];                               //
                                                                                               // Dispersif term
        // if (rd==YES_TS)
        if (rd == YES_TS) {
          switch (tcl) {
          case NO_BOUND_TTC: {
            // fill the diagonal value
            if (id_neigh != NONE_TTC) {                                 // SW 28/05/2018 pour confluence
              mat6[mat4[i + 1] - 1] += theta * dlip / (dist * delta_L); // dist = dx ou dy; delta_L = distance centre maille
              b[i] += -((1 - theta) * dlip / (dist * delta_L)) * var[i];
              // fill the value for each neighbor
              mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * dlip / (dist * delta_L);
              b[i] += (1 - theta) * dlip / (dist * delta_L) * var[id_neigh];
            }
            break; // fin face active
          }
          case DIRI_FACE_TTC: {
            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
            mat6[mat4[i + 1] - 1] += 2 * theta * dlip / (dist * dist);
            b[i] += -2 * (1 - theta) * var[i] * dlip / (dist * dist);
            b[i] += 2 * theta * cl_val * dlip / (dist * dist);
            b[i] += 2 * (1 - theta) * cl_val * dlip / (dist * dist);
            // sub_icard = SUB_CARD_TTC; // SW 28/05/2018 pour calculer une seul fois
            break; // fin face diri
          }
          }
        }

        // Advectif term
        if (ra == YES_TS) // si on considere la partie advectif de lequation
        {
          if (i == 100)
            printf("yes\n");
          u = pcarac_ttc->uface[i][icard][sub_icard]; // SW 29/05/2018 pour confluence
          u_west = pcarac_ttc->uface[i][WEST_TTC][sub_icard];
          switch (icard) {
          case EAST_TTC:
            if (u > 0.0) { // fill the diagonal value
              // mat6[mat4[i+1]-1] += theta*u/dist;
              mat6[mat4[i + 1] - 1] += theta * u / dist + theta * (u - u_west) / dist;
              // b[i] += -((1-theta)*u/dist)*var[i];
              b[i] += -((1 - theta) * u / dist) * var[i] + (1 - theta) * var[i] * (u - u_west) / dist;
            } else {
              switch (tcl) {
              case NO_BOUND_TTC:                                                                                            // face active, fills the value for neighbor
                                                                                                                            // if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
                if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta * u / dist;
                  b[i] += -(1 - theta) * u / dist * var[id_neigh];
                }
                break;
              case DIRI_FACE_TTC: // face active, bound adv
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
                b[i] += -theta * cl_val * u / dist;
                b[i] += -((1 - theta) * cl_val * u / dist);
                sub_icard = SUB_CARD_TTC; // SW 28/05/2018 pour calculer une seul fois
                break;
              }
            }
            break;

          case WEST_TTC:
            if (u < 0.0) { // fill the diagonal value
              mat6[mat4[i + 1] - 1] += theta * u / dist;
              b[i] += -((1 - theta) * u / dist) * var[i];
            } else {
              switch (tcl) {
              case NO_BOUND_TTC:                                                                                            // face active
                                                                                                                            // if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
                if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
                  // mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - theta*u/dist;
                  u_east = pcarac_ttc->uface[i][EAST_TTC][sub_icard];
                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u_east / dist;
                  // b[i]+= (1-theta)*u/dist*var[id_neigh];
                  b[i] += (1 - theta) * u / dist * var[id_neigh];
                }
                break;
              case DIRI_FACE_TTC:
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
                b[i] += theta * cl_val * u / dist;
                b[i] += ((1 - theta) * cl_val * u / dist);
                sub_icard = SUB_CARD_TTC; // SW 28/05/2018 pour calculer une seul fois
                break;
              /* SW 07/06/2018 pour une confluence-diffluence*/
              // TODO hard values of the advectiv e term
              case CONF_DIFF_TTC:
                id_neigh1 = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->ivois[icard][0];
                id_neigh2 = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->ivois[icard][1];
                u_id_neigh1 = pcarac_ttc->uface[id_neigh1][EAST_TTC][0];
                u_id_neigh2 = pcarac_ttc->uface[id_neigh2][EAST_TTC][0];
                /*neighbor term*/
                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u * u_id_neigh1 / (dist * (u_id_neigh1 + u_id_neigh2)); // formule de melange
                nvois_ele_i++;
                b[i] += (1 - theta) * u / dist * (u_id_neigh1 * var[id_neigh1] / (u_id_neigh1 + u_id_neigh2));
                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u * u_id_neigh2 / (dist * (u_id_neigh1 + u_id_neigh2));
                b[i] += (1 - theta) * u / dist * (u_id_neigh2 * var[id_neigh2] / (u_id_neigh1 + u_id_neigh2));
                nvois_ele_i++;
                sub_icard = SUB_CARD_TTC;
                break;
              }
            }
            break;

          case NORTH_TTC:
            if (u > 0.0) { // fill the diagonal value
              mat6[mat4[i + 1] - 1] += theta * u / dist;
              b[i] += -((1 - theta) * u / dist) * var[i];
            } else {
              switch (tcl) {
              case NO_BOUND_TTC:
                // if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
                if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta * u / dist;
                  b[i] += -(1 - theta) * u / dist * var[id_neigh];
                }
                break;
              case DIRI_FACE_TTC: // face active //bound adv
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
                b[i] += -theta * cl_val * u / dist;
                b[i] += -((1 - theta) * cl_val * u / dist);
                sub_icard = SUB_CARD_TTC; // SW 28/05/2018 pour calculer une seul fois
                break;
              }
            }
            break;

          case SOUTH_TTC:
            if (u < 0.0) { // fill the diagonal value
              mat6[mat4[i + 1] - 1] += -theta * u / dist;
              b[i] += ((1 - theta) * u / dist) * var[i];
            } else {
              switch (tcl) {
              case NO_BOUND_TTC:
                // if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
                if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u / dist;
                  b[i] += (1 - theta) * u / dist * var[id_neigh];
                }
                break;
              case DIRI_FACE_TTC:
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
                b[i] += theta * cl_val * u / dist;
                b[i] += ((1 - theta) * cl_val * u / dist);
                sub_icard = SUB_CARD_TTC; // SW 28/05/2018 pour calculer une seul fois
                break;
              }
            }
            break;
          }
        }

        if (tcl == NO_BOUND_TTC && id_neigh != NONE_TTC) {
          nvois_ele_i++;
        } // fin face active nb neig

        if (tcl == DIRI_CENTER_TTC) // variable impose au centre de la maille
        {
          cl_val = pspecies->p_boundary_ttc[i]->valcl[1][1];
          b[i] = cl_val;
          mat6[mat4[i + 1] - 1] = 1; // reste d'Arthur à verifier le bilan semble faux
        }
        if (tcl == NEU_FACE_TTC) // faceNEU_FACE_TTC) // flux impose a la face de la maille
        {
          cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
          // b[i] += -cl_val/(dist*rhowcw);
          if (cl_val > 0) {                   // SW 08/06/2018 apport, si un prelevement, cela change pas la concentration
            b[i] += cl_val / (dist * rhowcw); // SW 17/05/2018 flux entrant ou sortie depend du signe de cl_val soit QC ou UM
            sub_icard = SUB_CARD_TTC;
          } else if (cl_val < 0) { // prelevement cl_val = Q/volume; /s pour solute
            mat6[mat4[i + 1] - 1] += (-cl_val * theta / dist);
            b[i] += ((1 - theta) * cl_val / dist) * var[i]; //(1-theta)*cl_val*var[i];//
            sub_icard = SUB_CARD_TTC;                       // is this correct
                                                            // if(i == 3392) //debug
            //  printf("id == 3392\n");
          }
          sub_icard = SUB_CARD_TTC;
        }
      } // fin sub_icard
    }   // fin icard
}

// Fonction qui construit mat6 et b pour prose, riviere
void TTC_no_cap_rive(s_carac_ttc *pcarac_ttc, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, int i, FILE *fpout) {
  s_gc *pgc;
  double *mat6, *b, dist, delta_L, *var, u;
  int tcl, type;
  int icard, id_neigh;
  int *mat4, *mat5, ra, rd, nvois;
  double theta, dlip, rhow, cw, rhowcw, cl_val;
  double surf_t, surf_titer;
  int nvois_ele_i = 0;
  int sub_icard;            // SW 28/05/2018
  int id_neigh1, id_neigh2; // SW 07/06/2018 pour conf_diff
  double u_id_neigh1, u_id_neigh2;
  double delta_L1, delta_L2;
  double coef_reoxy;

  pgc = pspecies->pgc;
  type = pspecies->pset->type;
  var = pspecies->pval_ttc->val;                         // tableau contenant la variable etudiee (temperature ou solute)
  dist = pspecies->p_param_ttc[i]->param_syst[SIZE_TTC]; // taille de la maille
  mat6 = pgc->mat6;                                      // donne val
  mat5 = pgc->mat5;                                      // donne l'identifiant de chaque maille et de leur voisine
  mat4 = pgc->mat4;                                      // permet de connaitre le nombre de voisin pour chaque maille
  b = pgc->b;                                            // donne b
  nvois = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->nvois;
  theta = pcarac_ttc->theta; // parametre de semi implicite
  // To account for dispersion in heat transport with flow rate (vol.) AB 30.09.19
  surf_t = p_param_calc_ttc[i]->coeff[SURF_T_TTC];
  surf_titer = p_param_calc_ttc[i]->coeff[SURF_TITER_TTC];

  if (type == HEAT_TTC) {
    rhow = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][RHO_TTC];
    cw = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][HEAT_CAP_TTC];
    rhowcw = rhow * cw;
  } else if (type == SOLUTE_TTC) {
    rhowcw = 1;
  }

  ra = pspecies->pset->calc_process[ADVECTION_TTC]; // terme advectif (YES or NO)
  rd = pspecies->pset->calc_process[DIFFUSION_TTC]; // terme dispersif (YES or NO)

  for (icard = 0; icard < NB_CARD_TTC; icard++)
    for (sub_icard = 0; sub_icard < SUB_CARD_TTC; sub_icard++) { // SW 28/05/2018 pour confluence
      {
        // printf("%d %d %d %lf\n",i,icard,sub_icard,mat6[mat4[i+1]-1]);
        tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];
        id_neigh = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->ivois[icard][sub_icard];
        delta_L = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // distance entre le centre de la maille et de son voisin
        dlip = p_param_calc_ttc[i]->disp_face[icard][sub_icard];                               //

        // Dispersif term
        if (rd == YES_TS) {
          switch (tcl) {
          case NO_BOUND_TTC: {
            // fill the diagonal value
            if (id_neigh != NONE_TTC) { // SW 28/05/2018 pour confluence
              // printf("vois %d ::::  %d %d %d\n",id_neigh,i,icard,sub_card);
              if (icard == EAST_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[WEST_TTC][1] == BARRAGE_TTC) // DK NOTSURE check
                break;                                                                                      // AB 11.09.2019 pour barrage => east face of the west neighbor corresponds to no diffusion (conduction does not go upward over a dam)
              mat6[mat4[i + 1] - 1] += surf_t * theta * dlip / (dist * delta_L);                            // dist = dx ou dy; delta_L = distance centre maille
              b[i] += -surf_titer * ((1 - theta) * dlip / (dist * delta_L)) * var[i];
              // fill the value for each neighbor
              mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -surf_t * theta * dlip / (dist * delta_L);
              b[i] += surf_titer * (1 - theta) * dlip / (dist * delta_L) * var[id_neigh];
            }
            break; // fin face active
          }
          case DIRI_FACE_TTC: {
            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
            mat6[mat4[i + 1] - 1] += 2 * surf_t * theta * dlip / (dist * dist);
            b[i] += -2 * surf_titer * (1 - theta) * var[i] * dlip / (dist * dist);
            b[i] += 2 * surf_t * theta * cl_val * dlip / (dist * dist);
            b[i] += 2 * surf_titer * (1 - theta) * cl_val * dlip / (dist * dist);
            if (ra == NO_TS)
              sub_icard = SUB_CARD_TTC; // SW 28/05/2018 pour calculer une seul fois
            break;                      // fin face diri
          }
          case NEU_FACE_TTC: { // Added le 29.08.2019 by AB (JB's son) to account for Neumann boundary conditions with the diffusivity
            if (icard == WEST_TTC || icard == EAST_TTC) {
              cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
              mat6[mat4[i + 1] - 1] += 2 * surf_t * theta * cl_val * dlip / dist;
              b[i] += 2 * surf_titer * (1 - theta) * dlip * cl_val / dist;
              if (ra == NO_TS)
                sub_icard = SUB_CARD_TTC;
              break;
            } else
              break;
          }
          case CONF_DIFF_TTC: {
            id_neigh1 = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->ivois[icard][0];
            id_neigh2 = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->ivois[icard][1];
            delta_L1 = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->delta_L[icard][0];
            delta_L2 = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->delta_L[icard][1];

            mat6[mat4[i + 1] - 1] += surf_t * theta * dlip / (dist * delta_L1); // dist = dx ou dy; delta_L = distance centre maille
            b[i] += -surf_titer * ((1 - theta) * dlip / (dist * delta_L1)) * var[i];
            // fill the value for neighbor 1
            mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -surf_t * theta * dlip / (dist * delta_L1);
            b[i] += surf_titer * (1 - theta) * dlip / (dist * delta_L1) * var[id_neigh1];
            nvois_ele_i++;

            mat6[mat4[i + 1] - 1] += surf_t * theta * dlip / (dist * delta_L2); // dist = dx ou dy; delta_L = distance centre maille
            b[i] += -surf_titer * ((1 - theta) * dlip / (dist * delta_L2)) * var[i];
            // fill the value for neighbor 2
            mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -surf_t * theta * dlip / (dist * delta_L2);
            b[i] += surf_titer * (1 - theta) * dlip / (dist * delta_L2) * var[id_neigh2];

            if (ra == NO_TS) {
              sub_icard = SUB_CARD_TTC;
              nvois_ele_i++;
            }

            break;
          }
          case BARRAGE_TTC:
            if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) {
              mat6[mat4[i + 1] - 1] += 2. * surf_t * theta * dlip / (dist * dist); // dist = dx ou dy; delta_L = distance centre maille
              b[i] += -2. * surf_titer * (1 - theta) * var[i] * dlip / (dist * dist);
              mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -2. * surf_t * theta * dlip / (dist * dist);
              b[i] += 2. * surf_titer * (1 - theta) * var[id_neigh] * dlip / (dist * dist);
            }
            if (ra == NO_TS) {
              sub_icard = SUB_CARD_TTC;
              nvois_ele_i++;
            }
            break;
          }
        }

        // Advectif term
        if (ra == YES_TS) // si on considere la partie advectif de lequation
        {
          u = pcarac_ttc->uface[i][icard][sub_icard];
          switch (icard) {
          case EAST_TTC:
            if (u > 0.0) { // fill the diagonal value
              mat6[mat4[i + 1] - 1] += theta * u / dist;
              b[i] += -((1 - theta) * u / dist) * var[i];
            } else {
              switch (tcl) {
              case NO_BOUND_TTC: // face active //fill the value for neighbor
                // if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
                if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta * u / dist;
                  b[i] += -(1 - theta) * u / dist * var[id_neigh];
                }
                break;
              case DIRI_FACE_TTC: // face active //bound adv
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
                b[i] += -theta * cl_val * u / dist;
                b[i] += -((1 - theta) * cl_val * u / dist);
                sub_icard = SUB_CARD_TTC; // SW 28/05/2018 pour calculer une seul fois
                break;
              }
            }
            break;

          case WEST_TTC:
            if (u < 0.0) { // fill the diagonal value
              mat6[mat4[i + 1] - 1] += theta * u / dist;
              b[i] += -((1 - theta) * u / dist) * var[i];
            } else {
              switch (tcl) {
              case NO_BOUND_TTC: // face active
                // if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
                if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u / dist;
                  b[i] += (1 - theta) * u / dist * var[id_neigh];
                }
                break;
              case DIRI_FACE_TTC:
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
                b[i] += theta * cl_val * u / dist;
                b[i] += ((1 - theta) * cl_val * u / dist);
                sub_icard = SUB_CARD_TTC; // SW 28/05/2018 pour calculer une seul fois
                break;
                /* SW 07/06/2018 pour une confluence-diffluence*/
              case CONF_DIFF_TTC:
                id_neigh1 = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->ivois[icard][0];
                id_neigh2 = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->ivois[icard][1];
                u_id_neigh1 = pcarac_ttc->uface[id_neigh1][EAST_TTC][0];
                u_id_neigh2 = pcarac_ttc->uface[id_neigh2][EAST_TTC][0];
                /*neighbor term*/
                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u * u_id_neigh1 / (dist * (u_id_neigh1 + u_id_neigh2)); // formule de melange
                nvois_ele_i++;
                b[i] += (1 - theta) * u / dist * (u_id_neigh1 * var[id_neigh1] / (u_id_neigh1 + u_id_neigh2));
                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u * u_id_neigh2 / (dist * (u_id_neigh1 + u_id_neigh2));
                b[i] += (1 - theta) * u / dist * (u_id_neigh2 * var[id_neigh2] / (u_id_neigh1 + u_id_neigh2));
                nvois_ele_i++;
                sub_icard = SUB_CARD_TTC;
                break;
                /*SW 24/10/2018 pour reoxygenation aux barrages*/
              case BARRAGE_TTC:
                if (pspecies->pset->oxygen == YES_TS) {
                  coef_reoxy = TTC_reoxygeneration_rhs(pcarac_ttc->p_link[pspecies->id]->preoxy_ttc->rd[i], pcarac_ttc->p_link[pspecies->id]->preoxy_ttc->rn[i], pcarac_ttc->p_link[pspecies->id]->preoxy_ttc->frac_debit_oxy[i], fpout);

                  // u -= plink->pspecies->preoxy_ttc->debit_surverse[i]*coef_reoxy; //implicite theta = 1.

                  // mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - u*(1 - coef_reoxy)/dist;
                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -(u - pcarac_ttc->p_link[pspecies->id]->preoxy_ttc->debit_surverse[i] * coef_reoxy) / dist;
                  b[i] += pcarac_ttc->p_link[pspecies->id]->preoxy_ttc->debit_surverse[i] * coef_reoxy * pcarac_ttc->p_link[pspecies->id]->preoxy_ttc->osat / dist;
                  // b[i]+= u*coef_reoxy*plink->pspecies->preoxy_ttc->osat/dist;
                  sub_icard = SUB_CARD_TTC;
                } else {
                  if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
                    mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u / dist;
                    b[i] += (1 - theta) * u / dist * var[id_neigh];
                  }
                }
                nvois_ele_i++; // Added by AB on 05.09.2019
                break;
              }
            }
            break;

          case NORTH_TTC:
            if (u > 0.0) { // fill the diagonal value
              mat6[mat4[i + 1] - 1] += theta * u / dist;
              b[i] += -((1 - theta) * u / dist) * var[i];
            } else {
              switch (tcl) {
              case NO_BOUND_TTC: // face active //fill the value for neighbor
                // if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
                if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta * u / dist;
                  b[i] += -(1 - theta) * u / dist * var[id_neigh];
                }
                break;
              case DIRI_FACE_TTC: // face active //bound adv
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
                b[i] += -theta * cl_val * u / dist;
                b[i] += -((1 - theta) * cl_val * u / dist);
                sub_icard = SUB_CARD_TTC; // SW 28/05/2018 pour calculer une seul fois
                break;
              }
            }
            break;

          case SOUTH_TTC:
            if (u < 0.0) { // fill the diagonal value
              mat6[mat4[i + 1] - 1] += -theta * u / dist;
              b[i] += ((1 - theta) * u / dist) * var[i];
            } else {
              switch (tcl) {
              case NO_BOUND_TTC: // face active
                // if(plink->pspecies->p_boundary_ttc[id_neigh]->icl[icard]!=DIRI_CENTER_TTC) {
                if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) { // SW 29/05/2018 pour confluence
                  mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u / dist;
                  b[i] += (1 - theta) * u / dist * var[id_neigh];
                }
                break;
              case DIRI_FACE_TTC:
                cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
                // if (type == HEAT_TTC)
                //   u = -u;
                b[i] += theta * cl_val * u / dist;
                b[i] += ((1 - theta) * cl_val * u / dist);
                sub_icard = SUB_CARD_TTC; // SW 28/05/2018 pour calculer une seul fois
                break;
              }
            }
            break;
          }
        }

        if (tcl == NO_BOUND_TTC && id_neigh != NONE_TTC) {
          nvois_ele_i++;
        } // fin face active nb neig

        if (tcl == DIRI_CENTER_TTC) // variable impose au centre de la maille
        {
          cl_val = pspecies->p_boundary_ttc[i]->valcl[1][1];
          b[i] = cl_val;
          mat6[mat4[i + 1] - 1] = 1; // reste d'Arthur à verifier le bilan semble faux
        }

        if (type == HEAT_TTC && (icard == NORTH_TTC || icard == SOUTH_TTC)) // AB 20.09.2019: allows to take into account an atmospheric heat flux
        {
          if (tcl == NEU_FACE_TTC) {
            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
            // printf("bb north: %lf %lf %lf\n",b[i],cl_val,theta);
            b[i] += theta * cl_val;
            b[i] += (1. - theta) * cl_val;
            // printf("ba north: %lf\n",b[i]);
            sub_icard = SUB_CARD_TTC;
          }
        }

        if (type == SOLUTE_TTC) {
          if (tcl == NEU_FACE_TTC) // faceNEU_FACE_TTC) // flux impose a la face de la maille
          {
            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
            // b[i] += -cl_val/(dist*rhowcw);
            if (cl_val > 0) {                   // SW 08/06/2018 apport, si un prelevement, cela change pas la concentration
              b[i] += cl_val / (dist * rhowcw); // SW 17/05/2018 flux entrant ou sortie depend du signe de cl_val soit u
              sub_icard = SUB_CARD_TTC;
            } else if (cl_val < 0) { // prelevement cl_val = u; /s pour solute
              mat6[mat4[i + 1] - 1] += (-cl_val * theta / dist);
              b[i] += ((1 - theta) * cl_val / dist) * var[i];
              sub_icard = SUB_CARD_TTC;
              // if(i == 3392) //debug
              //  printf("id == 3392\n");
            }
            sub_icard = SUB_CARD_TTC;
          }
        }

      } // fin sub_icard
    }   // fin icard
}

void TTC_fill_mat6_and_b(s_carac_ttc *pcarac_ttc, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, double dt, FILE *fpout) {
  s_gc *pgc;
  int nele;
  int i;
  double s, *mat6, *b, *var;
  int regime, type, *mat4;
  double surf_t, surf_titer; // SW 08/06/2018
  pgc = pspecies->pgc;
  regime = pcarac_ttc->regime; // regime permanent(=NO) ou transitoire(=YES)
                               //  printf("%s \n",TTC_name_regime(regime));
  type = pspecies->pset->type;
  nele = pcarac_ttc->count[NELE_TTC]; // récupère le nombre delement total
  mat6 = pgc->mat6;
  mat4 = pgc->mat4;
  b = pgc->b;

  var = pspecies->pval_ttc->val;

  // boucle sur le nombre delement
  for (i = 0; i < nele; i++) {
    // Calcul coefficient
    Calc_dispcond(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout);
    // Filling of b et mat6
    TTC_no_cap_rive(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout); // Remplit les parties de mat6 et b
    // TTC_no_cap(pcarac_ttc,p_param_calc_ttc,pspecies,i,fpout);//Remplit les parties de mat6 et b
    // TTC_schema_centre(pcarac_ttc,p_param_calc_ttc,pspecies,i,dt,fpout);
  }
  if (regime == TRANSIENT_TTC) {
    for (i = 0; i < nele; i++) {
      Calc_s(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout);
      s = p_param_calc_ttc[i]->coeff[S_TTC];
      // surf_t = p_param_calc_ttc->coeff[SURF_T_TTC][i];
      // surf_titer = p_param_calc_ttc->coeff[SURF_TITER_TTC][i];

      b[i] += s / dt * var[i];
      mat6[mat4[i + 1] - 1] += s / dt; // partie temporelle de A
    }
  }
  //     printf("////////////////////////b[%i] : %.18lf\n",i,b[i]);
  //   printf("////////////////////////mat6[%i] : %.18lf\n",mat4[i+1]-1,mat6[mat4[i+1]-1]);
}

// void TTC_fill_mat6_and_b_libaq(s_carac_ttc *pcarac_ttc,s_link_ttc *plink, s_param_calc_ttc **p_param_calc_ttc,s_species_ttc *pspecies,double dt,int* ts_count,FILE *fpout)
void TTC_fill_mat6_and_b_libaq(s_carac_ttc *pcarac_ttc, s_link_ttc *plink, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, double dt, FILE *fpout) {
  s_gc *pgc;
  int nele;
  int i;
  double s, *mat6, *b, *val, *val_old;
  int regime, type, *mat4;
  double surf_t, surf_titer; // SW 08/06/2018
  pgc = pspecies->pgc;
  regime = pcarac_ttc->regime; // regime permanent(=NO) ou transitoire(=YES)
                               //  printf("%s \n",TTC_name_regime(regime));
  type = pspecies->pset->type;
  nele = pcarac_ttc->count[NELE_TTC]; // récupère le nombre delement total
  mat6 = pgc->mat6;
  mat4 = pgc->mat4;
  b = pgc->b;

  val = pspecies->pval_ttc->val;
  val_old = pspecies->pval_ttc->val_old;

  //  for (i=0;i<nele;i++) LP_printf(fpout,"In %s : Val[%d] = %lf, b[i] = %lf \n", __func__, i, val[i], b[i]); // Check NG pour la première itération --> OK

  //  LP_printf(fpout,"In %s, nele is %d\n",__func__, nele);
  // boucle sur le nombre delement
  for (i = 0; i < nele; i++) {
    //        LP_printf(fpout,"entering loop \n");
    // Calcul coefficient
    Calc_dispcond(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout);
    // Filling of b et mat6

    TTC_no_cap_libaq(pcarac_ttc, plink, p_param_calc_ttc, pspecies, i, fpout); // Remplit les parties de mat6 et b
    // TTC_schema_centre(pcarac_ttc,p_param_calc_ttc,pspecies,i,dt,fpout);
  }
  if (regime == TRANSIENT_TTC) {
    for (i = 0; i < nele; i++) {
      if (pspecies->pset->type == SOLUTE_TTC) {
        Calc_s(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout);
        s = p_param_calc_ttc[i]->coeff[S_TTC];
        surf_t = p_param_calc_ttc[i]->coeff[SURF_T_TTC];
        surf_titer = p_param_calc_ttc[i]->coeff[SURF_TITER_TTC];

        b[i] += s / dt * val[i];
        mat6[mat4[i + 1] - 1] += s / dt; // partie temporelle de A
      } else if (pspecies->pset->type == HEAT_TTC) {
        Calc_s(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout);
        s = p_param_calc_ttc[i]->coeff[S_TTC];

        //                  LP_printf(fpout, "before temporal coeff b[i]: %lf, val_old[i] is %lf val[i] %lf \n",b[i], val_old[i], val[i]);
        b[i] += s / dt * val[i];
        //                  LP_printf(fpout, "b[i] is %lf, s is %e, dt is %lf, val_old[i] is %lf val[i] %lf  \n",  b[i], s, dt, val_old[i], val[i]);

        //                  LP_printf(fpout, " before temporal coeff mat6[i] is %lf \n", mat6[mat4[i+1]-1]);
        mat6[mat4[i + 1] - 1] += s / dt; // partie temporelle de A, addition of temporal coeff with t  * val[i]
                                         //                  LP_printf(fpout, " mat6[i] after partie temporalle is %lf \n", mat6[mat4[i+1]-1]);
      }
    }
    for (i = 0; i < nele; i++)
      LP_printf(fpout, "In %s : Val[%d] = %lf, b[i] = %lf \n", __func__, i, val[i], b[i]); // Check NG pour la première itération --> OK
  }
  //      printf("////////////////////////b[%i] : %.18lf\n",i,b[i]);
  //      printf("////////////////////////mat6[%i] : %.18lf\n",mat4[i+1]-1,mat6[mat4[i+1]-1]);
}

// void TTC_fill_mat6_and_b_libaq_test (s_carac_ttc *pcarac_ttc, s_link_ttc *plink, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, double dt, int *ts_count, FILE *fpout) {
void TTC_fill_mat6_and_b_libaq_test(s_carac_ttc *pcarac_ttc, s_link_ttc *plink, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, double dt, FILE *fpout) {
  s_gc *pgc = pspecies->pgc;
  int nele = pcarac_ttc->count[NELE_TTC];       // récupère le nombre delement total
  int nvois = pcarac_ttc->count[NVOIS_ELE_TTC]; // Number of neigbours of each element;
  int regime = pcarac_ttc->regime;              // regime permanent(=NO) ou transitoire(=YES)
  int type = pspecies->pset->type;              // Simulation type
  int i;                                        // iteration over nele

  double s;
  double surf_t, surf_titer;

  int *mat4 = pgc->mat4;                         // Number of terms in each row
  double *b = pgc->b;                            // RHS of the linear system
  double *mat6 = pgc->mat6;                      // Matrix of coefficients of in the LHS
  double *val = pspecies->pval_ttc->val;         // Temperature at the time step t
  double *val_old = pspecies->pval_ttc->val_old; // Temperature at time step t-1

  double *dVdt = pcarac_ttc->dVdt;
  double *qSink = pcarac_ttc->qSink;
  double *qSource = pcarac_ttc->qSource;

  double thickness;
  double dist;
  double volume;

  int nriverbed = pcarac_ttc->count[NRIVERBED_TTC]; // Number of the riverbed cells
  int naquitard = pcarac_ttc->count[NAQUITARD_TTC]; // Number of the aquitard cells

  //  for (i=0;i<nele;i++) LP_printf(fpout,"In %s : Val[%d] = %lf, b[i] = %lf \n", __func__, i, val[i], b[i]);
  //  for (i=0;i<nele+naquitard+nriverbed;i++) LP_printf(fpout,"mat4:%d\n", mat4[i]);
  //  for (i=0;i<nele+nvois+naquitard;i++) LP_printf(fpout,"mat5= %d \n",  pgc->mat5[i]);

  if (pspecies->pset->aquitard == AQUITARD_ON_TTC) {
    // Filling the aquitard cells
    for (int j = 0; j < naquitard; j++) {
      Calc_q(pcarac_ttc, pspecies, j, AQUITARD_TTC, fpout);
      Calc_aquitard_parameters(pspecies, j, AQUITARD_TTC, fpout);
      Calc_aquitard_peclet(pspecies, j, AQUITARD_TTC, fpout);
      Calc_coeff(pspecies, j, AQUITARD_TTC, fpout);
    }
  }
  if (pspecies->pset->riverbed == AQUITARD_ON_TTC) {
    // Filling the riverbed cells
    for (int j = 0; j < nriverbed; j++) {
      Calc_aquitard_parameters(pspecies, j, RIVERBED_TTC, fpout);
      Calc_coeff(pspecies, j, RIVERBED_TTC, fpout);
      Calc_aquitard_peclet(pspecies, j, RIVERBED_TTC, fpout);
    }
  }

  for (i = 0; i < nele; i++) {
    // In this function there is an error when river is solved. There is no lambda_solid to calculate effective lambda
    Calc_dispcond(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout);
  }

  for (i = 0; i < nele; i++) {
    Calc_dispcond_face(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout); // Calcul coefficient
    // TTC_no_cap_libaq_test(pcarac_ttc, plink, p_param_calc_ttc, pspecies, i, *ts_count, fpout);
    TTC_no_cap_libaq_test(pcarac_ttc, plink, p_param_calc_ttc, pspecies, i, fpout); // Filling of b et mat6
  }

  if (regime == TRANSIENT_TTC) {
    for (i = 0; i < nele; i++) {

      if (pspecies->pset->type == SOLUTE_TTC) {

        Calc_s(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout);
        s = p_param_calc_ttc[i]->coeff[S_TTC];
        surf_t = p_param_calc_ttc[i]->coeff[SURF_T_TTC];
        surf_titer = p_param_calc_ttc[i]->coeff[SURF_TITER_TTC];
        dist = pspecies->p_param_ttc[i]->param_syst[SIZE_TTC];
        thickness = pspecies->p_param_ttc[i]->thickness;
        volume = dist * dist * thickness;

        b[i] += s / dt * val[i];
        mat6[mat4[i + 1] - 1] += s / dt; // partie temporelle de A

        // If the advection is activated, Advective terms impact on the solute transport
        if (pspecies->pset->calc_process[ADVECTION_TTC] == YES_TS) {
          mat6[mat4[i + 1] - 1] += dVdt[i] / volume;
          mat6[mat4[i + 1] - 1] += -qSink[i] / volume;
          b[i] += -qSource[i] * val[i] / volume;
        }
      } else if (pspecies->pset->type == HEAT_TTC) {
        Calc_s(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout);
        s = p_param_calc_ttc[i]->coeff[S_TTC];
        dist = pspecies->p_param_ttc[i]->param_syst[SIZE_TTC];
        thickness = pspecies->p_param_ttc[i]->thickness;
        volume = dist * dist * thickness;

        b[i] += s / dt * val[i];
        mat6[mat4[i + 1] - 1] += s / dt; // partie temporelle de A, addition of temporal coeff with t  * val[i]

        // If the advection is activated, Advective terms impact on the heat transport
        if (pspecies->pset->calc_process[ADVECTION_TTC] == YES_TS) {
          mat6[mat4[i + 1] - 1] += dVdt[i] / volume;
          mat6[mat4[i + 1] - 1] += -qSink[i] / volume;
          b[i] += -qSource[i] * val[i] / volume;
        }
      }
    }
  }
}

void TTC_fill_mat6_and_b_rive(s_carac_ttc *pcarac_ttc, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, double dt, FILE *fpout) {
  s_gc *pgc;
  int nele;
  int i;
  double s, *mat6, *b, *var;
  int regime, type, *mat4;
  double surf_t, surf_titer; // SW 08/06/2018
  pgc = pspecies->pgc;
  regime = pcarac_ttc->regime; // regime permanent(=NO) ou transitoire(=YES)
  //  printf("%s \n",TTC_name_regime(regime));
  type = pspecies->pset->type;
  nele = pcarac_ttc->count[NELE_TTC]; // récupère le nombre delement total
  mat6 = pgc->mat6;
  mat4 = pgc->mat4;
  b = pgc->b;

  var = pspecies->pval_ttc->val;

  // boucle sur le nombre de element
  for (i = 0; i < nele; i++) {
    // Calcul coefficient
    Calc_dispcond(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout);
    // Filling of b et mat6
    TTC_no_cap_rive(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout); // Remplit les parties de mat6 et b
    // TTC_no_cap(pcarac_ttc,p_param_calc_ttc,pspecies,i,fpout);//Remplit les parties de mat6 et b
    // TTC_schema_centre(pcarac_ttc,p_param_calc_ttc,pspecies,i,dt,fpout);
  }
  if (regime == TRANSIENT_TTC) {
    for (i = 0; i < nele; i++) {
      Calc_s(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout);
      // s=p_param_calc_ttc->coeff[S_TTC][i];
      s = 1.;
      surf_t = p_param_calc_ttc[i]->coeff[SURF_T_TTC];
      //	LP_printf(fpout,"valeur de surf_t pour ele %d : %f\n",i,surf_t);
      surf_titer = p_param_calc_ttc[i]->coeff[SURF_TITER_TTC];
      // 		LP_printf(fpout,"valeur de surf_titer pour ele %d : %f\n",i,surf_titer);
      b[i] += s * surf_titer / dt * var[i];
      mat6[mat4[i + 1] - 1] += s * surf_t / dt; // partie temporelle de A
      // if(i == 40)
      // LP_printf(fpout,"var = %f\n",var[i]);
    }
  }
  //     printf("////////////////////////b[%i] : %.18lf\n",i,b[i]);
  //   printf("////////////////////////mat6[%i] : %.18lf\n",mat4[i+1]-1,mat6[mat4[i+1]-1]);
}

double TTC_reoxygeneration_rhs(double rd, double rn, double frac_debit_oxy, FILE *fp) {
  double a;
  a = 0.0;
  if (rd > EPS_TS)
    a = frac_debit_oxy * (rd - 1.) / rd;
  if (rn > EPS_TS)
    a += (1.0 - frac_debit_oxy) * (rn - 1.) / rn;
  return a;
}

void TTC_print_LHS(s_gc *pgc, FILE *fpout) {
  int j, k;
  LP_printf(fpout, "LHS : \n");
  k = 1;

  for (j = 0; j < pgc->lmat5; j++) {
    LP_printf(fpout, "id %d ", k - 1);
    LP_printf(fpout, "%e ", pgc->mat6[j]);
    if (j == (pgc->mat4[k] - 1)) {
      LP_printf(fpout, "| ");
      LP_printf(fpout, "\n");
      k++;
    }
    //  if(j%10==0 && j>0)
    // LP_printf(fpout,"\n");
  }
}

void TTC_print_RHS(s_gc *pgc, FILE *fpout) {
  int i;
  LP_printf(fpout, "RHS : \n");
  LP_printf(fpout, "ele b_i\n");
  for (i = 0; i < pgc->ndl; i++) {
    LP_printf(fpout, "%d %e\n ", i + 1, pgc->b[i]);
  }
}

// Fonction qui construit mat6 et b pour cawaqs (utilisables pour les réseaux HYD et HDERM)
void TTC_no_cap_cawaqs(s_carac_ttc *pcarac_ttc, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, int i, FILE *fpout) {
  s_gc *pgc;
  double *mat6, *b, dist, delta_L, *var, u;
  int tcl, type;
  int icard, id_neigh;
  int *mat4, *mat5, ra, rd, nvois;
  double theta, dlip, rhow, cw, rhowcw, cl_val;
  double surf_t, surf_titer;
  int nvois_ele_i = 0; // Initialisation du compteur de voisins
  int sub_icard;
  int id_neigh1, id_neigh2;
  double u_id_neigh1, u_id_neigh2;
  double delta_L1, delta_L2;
  double coef_reoxy;

  theta = pcarac_ttc->theta;
  pgc = pspecies->pgc;
  type = pspecies->pset->type;                           // type de la specie (HEAT, SOLUTE)
  var = pspecies->pval_ttc->val;                         // vecteur des valeurs de la variable approximée a t=t-dt
  dist = pspecies->p_param_ttc[i]->param_syst[SIZE_TTC]; // taille de la maille i
  mat6 = pgc->mat6;
  mat5 = pgc->mat5; // identifiants de chaque maille et de leurs voisines
  mat4 = pgc->mat4; // positions des termes diagonaux dans mat6
  b = pgc->b;

  nvois = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->nvois; // nombre total de voisins de la maille i
  surf_t = p_param_calc_ttc[i]->coeff[SURF_T_TTC];                 // surface mouillée à t
  surf_titer = p_param_calc_ttc[i]->coeff[SURF_TITER_TTC];         // surface mouillée à t-dt
                                                                   //  	LP_printf(fpout,"Id_ele %d neigh %d surf_t %f surf_titer %f \n",i,nvois,surf_t,surf_titer);

  if (type == HEAT_TTC) {
    rhow = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][RHO_TTC];
    cw = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][HEAT_CAP_TTC];
    rhowcw = rhow * cw;
  } else if (type == SOLUTE_TTC) {
    rhowcw = 1.0;
  }

  ra = pspecies->pset->calc_process[ADVECTION_TTC]; // Processus simulés (ra = composantes advectives, rd = composantes dispersives)
  rd = pspecies->pset->calc_process[DIFFUSION_TTC];

  for (icard = 0; icard < NB_CARD_RIV_TTC; icard++) {
    for (sub_icard = 0; sub_icard < SUB_CARD_TTC; sub_icard++) {

      tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard]; // type de condition limite à la face
      id_neigh = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->ivois[icard][sub_icard];
      delta_L = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // distance centre à centre entre la maille i et son voisin j
      dlip = p_param_calc_ttc[i]->disp_face[icard][sub_icard];                               // valeur du coefficient de dispersion à la face

      if (tcl == DIRI_CENTER_TTC && type == HEAT_TTC) {

        if (pspecies->p_boundary_ttc[i]->valcl[5][3] >= 230) {
          cl_val = pspecies->p_boundary_ttc[i]->valcl[5][3];
        } else {
          cl_val = 288.15; //; // This value will be the aquifer temperature assigned in CAW_fill_inflows_one_species_riv_aq
        }
      }

      // Composantes des coefficients matriciels dues à la dispersion
      if (rd == YES_TS) {
        switch (tcl) {
        case NO_BOUND_TTC: {
          // fill the diagonal value
          if (id_neigh != NONE_TTC) {
            // if (icard == EAST_TTC && plink->pspecies->p_boundary_ttc[id_neigh]->icl[WEST_TTC] == BARRAGE_TTC)
            // break; // AB 11.09.2019 pour barrage => east face of the west neighbor corresponds to no diffusion (conduction does not go upward over a dam)
            mat6[mat4[i + 1] - 1] += surf_t * theta * dlip / (dist * delta_L); // dist = dx ou dy; delta_L = distance centre maille
            b[i] += -surf_titer * ((1 - theta) * dlip / (dist * delta_L)) * var[i];
            // fill the value for each neighbor
            mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -surf_t * theta * dlip / (dist * delta_L);
            b[i] += surf_titer * (1 - theta) * dlip / (dist * delta_L) * var[id_neigh];
            //                            LP_printf(fpout, "COND_transport NOBOUND RIV in %s ,Face %s BC %s u: %lf , var[%d] = %lf dlip: %e dist %lf cl_val %lf surf_t %lf\n", __func__ ,
            //                                      TTC_param_card_river(icard), TTC_name_bound(tcl), u, i, var[i], dlip, dist, cl_val, surf_t);
          }
          break;
        } // fin face active

        case DIRI_FACE_TTC: {
          cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
          mat6[mat4[i + 1] - 1] += 2 * surf_t * theta * dlip / (dist * dist);
          b[i] += -2 * surf_titer * (1 - theta) * var[i] * dlip / (dist * dist);
          b[i] += 2 * surf_t * theta * cl_val * dlip / (dist * dist);
          b[i] += 2 * surf_titer * (1 - theta) * cl_val * dlip / (dist * dist);
          //                        LP_printf(fpout, "COND_transport DIRIFACE RIV in %s ,Face %s BC %s u: %lf , var[%d] = %lf dlip: %e dist %lf cl_val %lf\n", __func__ ,
          //                                  TTC_param_card_river(icard), TTC_name_bound(tcl), u, i, var[i], dlip, dist, cl_val);
          if (ra == NO_TS) {
            sub_icard = SUB_CARD_TTC;
          } // SW 28/05/2018 pour calculer une seul fois
          break;
        }

        case NEU_FACE_TTC: // A VERIFIER .... ajouté par aurélien
        {
          if (icard == WEST_TTC || icard == EAST_TTC) {
            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
            mat6[mat4[i + 1] - 1] += 2 * surf_t * theta * cl_val * dlip / dist;
            b[i] += 2 * surf_titer * (1 - theta) * dlip * cl_val / dist;

            //                            LP_printf(fpout, "COND_transport neuface RIV in %s ,Face %s BC %s u: %lf , var[%d] = %lf dlip: %e dist %lf cl_val %lf\n", __func__ ,
            //                                      TTC_param_card_river(icard), TTC_name_bound(tcl), u, i, var[i], dlip, dist, cl_val);
            if (ra == NO_TS) {
              sub_icard = SUB_CARD_TTC;
            }
            break;
          }
          break;
        }

        case CONF_DIFF_TTC: {
          id_neigh1 = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->ivois[icard][0];
          id_neigh2 = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->ivois[icard][1];
          delta_L1 = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->delta_L[icard][0];
          delta_L2 = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->delta_L[icard][1];

          mat6[mat4[i + 1] - 1] += surf_t * theta * dlip / (dist * delta_L1); // dist = dx ou dy; delta_L = distance centre maille
          b[i] += -surf_titer * ((1 - theta) * dlip / (dist * delta_L1)) * var[i];
          // fill the value for neighbor 1
          mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -surf_t * theta * dlip / (dist * delta_L1);
          b[i] += surf_titer * (1 - theta) * dlip / (dist * delta_L1) * var[id_neigh1];
          nvois_ele_i++;

          mat6[mat4[i + 1] - 1] += surf_t * theta * dlip / (dist * delta_L2); // dist = dx ou dy; delta_L = distance centre maille
          b[i] += -surf_titer * ((1 - theta) * dlip / (dist * delta_L2)) * var[i];
          // fill the value for neighbor 2
          mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -surf_t * theta * dlip / (dist * delta_L2);
          b[i] += surf_titer * (1 - theta) * dlip / (dist * delta_L2) * var[id_neigh2];

          //                        LP_printf(fpout, "COND_transport confdiff RIV in %s , face %s boundary type %s velocity: %lf , var[%d] = %lf dlip: %e dist %lf \n", __func__ ,
          //                                  TTC_param_card_river(icard), TTC_name_bound(tcl), u, i, var[i], dlip, dist);
          if (ra == NO_TS) {
            sub_icard = SUB_CARD_TTC; // 2
            nvois_ele_i++;
          }
          break;
        }

        } // fin du switch

      } // fin du bloc dispersif

      // Composantes des coefficients matriciels dues à la convection
      if (ra == YES_TS) {
        u = pcarac_ttc->uface[i][icard][sub_icard]; // équation résolue en débit ici u=Q

        switch (icard) {
        case DOWNSTREAM_TTC:
          if (u > 0.0) { // fill the diagonal value
            mat6[mat4[i + 1] - 1] += theta * u / dist;
            b[i] += -((1 - theta) * u / dist) * var[i];
            //                            LP_printf(fpout,"DOWNSTREAM active face u %e \n",u);

            /*if ( i == 7412 || i == 7143 || i == 7144  || i == 26161 || i == 26162  || i == 7571  || i == 7507 ) {
                LP_printf(fpout,"DOWNSTREAM active face u %e \n",u);
            }*/
          } else {
            switch (tcl) {
            case NO_BOUND_TTC: // face active //fill the value for neighbor

              if (id_neigh != NONE_TTC) {
                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += theta * u / dist;
                b[i] += -(1 - theta) * u / dist * var[id_neigh];
                //                                        LP_printf(fpout,"DOWNSTREAM active face u %e \n",u);

                /*if ( i == 7412 || i == 7143 || i == 7144  || i == 26161 || i == 26162 || i == 26222 || i == 26223 || i == 14475 || i == 15147 ) {
                    LP_printf(fpout, "ADV_transport RIV DOWNSTREAM in %s , face %s boundary type %s velocity: %e, var[%d] = %lf var_neigh[%d] = %lf dist %lf \n",
                    __func__ , TTC_param_card_river(icard),
                    TTC_name_bound(tcl), u,
                    i, var[i], id_neigh, var[id_neigh], dist
                              );
                }*/
              }
              break;

            case DIRI_FACE_TTC: // face active //bound adv
              cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
              b[i] += -theta * cl_val * u / dist;
              b[i] += -((1 - theta) * cl_val * u / dist);
              sub_icard = SUB_CARD_TTC; // SW 28/05/2018 pour calculer une seul fois
              break;
            }
          }
          break;

        case UPSTREAM_TTC: // Change direction names
          if (u < 0.0) {   // fill the diagonal value

            mat6[mat4[i + 1] - 1] += theta * u / dist;
            b[i] += -((1 - theta) * u / dist) * var[i];
            //                            LP_printf(fpout,"UPSTREAM active face u %e \n",u);
          } else {
            switch (tcl) {
            case NO_BOUND_TTC: // face active
              if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] != DIRI_CENTER_TTC) {
                mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u / dist;
                b[i] += (1 - theta) * u / dist * var[id_neigh];
                //                                        LP_printf(fpout,"UPSTREAM active face u %e \n",u);
                /*                                      if ( i == 7412 || i == 7143 || i == 7144 || i == 26161 || i == 26162  || i == 26222 || i == 26223 || i == 13851 || i == 13971 || i == 7571  || i == 7507  || i == 14475 || i == 15147) {
                                                            LP_printf(fpout, "ADV_transport RIV UPSTREAM in %s , face %s boundary type %s velocity: %e , var[%d] = %lf var_neigh[%d] = %lf dist %lf \n", __func__ ,
                                                                TTC_param_card_river(icard), TTC_name_bound(tcl), u, i, var[i], id_neigh, var[id_neigh], dist);
                                                            }*/
              } else if (id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard] == DIRI_CENTER_TTC) {

                cl_val = var[id_neigh];
                b[i] += theta * cl_val * u / dist;
                b[i] += ((1 - theta) * cl_val * u / dist);
                /*                                      LP_printf(fpout, "ADV_transport RIV UPSTREAM DIRI CEnter in %s , face %s boundary type %s velocity: %e , var[%d] = %lf var_neigh[%d] = %lf dist %lf \n", __func__ ,
                                                                TTC_param_card_river(icard), TTC_name_bound(tcl), u, i, var[i], id_neigh, var[id_neigh], dist);

                                                        if ( i == 7412 || i == 7143 || i == 7144 || i == 26161 || i == 26162  || i == 26222 || i == 26223 || i == 13851 || i == 13971 || i == 7571  || i == 7507  || i == 14475 || i == 15147) {
                                                            LP_printf(fpout, "ADV_transport RIV UPSTREAM in %s , face %s boundary type %s velocity: %e , var[%d] = %lf var_neigh[%d] = %lf dist %lf \n", __func__ ,
                                                            TTC_param_card_river(icard), TTC_name_bound(tcl), u, i, var[i], id_neigh, var[id_neigh], dist);
                                                        }*/

                //    sub_icard = SUB_CARD_TTC; // SW 28/05/2018 pour calculer une seul fois
              }
              break;

            case DIRI_FACE_TTC:
              cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
              b[i] += theta * cl_val * u / dist;
              b[i] += ((1 - theta) * cl_val * u / dist);
              sub_icard = SUB_CARD_TTC; // SW 28/05/2018 pour calculer une seule fois
              break;

            case CONF_DIFF_TTC:
              id_neigh1 = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->ivois[icard][0];
              id_neigh2 = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->ivois[icard][1];
              u_id_neigh1 = pcarac_ttc->uface[id_neigh1][EAST_TTC][0];
              u_id_neigh2 = pcarac_ttc->uface[id_neigh2][EAST_TTC][0];
              /*neighbor term*/
              mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u * u_id_neigh1 / (dist * (u_id_neigh1 + u_id_neigh2)); // formule de melange
              nvois_ele_i++;
              b[i] += (1 - theta) * u / dist * (u_id_neigh1 * var[id_neigh1] / (u_id_neigh1 + u_id_neigh2));
              mat6[mat4[i + 1] - 1 - nvois + nvois_ele_i] += -theta * u * u_id_neigh2 / (dist * (u_id_neigh1 + u_id_neigh2));
              b[i] += (1 - theta) * u / dist * (u_id_neigh2 * var[id_neigh2] / (u_id_neigh1 + u_id_neigh2));
              nvois_ele_i++;

              /*if ( i == 7412 || i == 7143 || i == 7144 || i == 7571 || i == 7507  || i == 14475 || i == 15147) {
                  LP_printf(fpout, "ADV_transport RIV UPSTREAM confdiff in %s , face %s boundary type %s velocity: %e , var[%d] = %lf var_neigh[%d] = %lf dist %lf \n", __func__ ,
                            TTC_param_card_river(icard), TTC_name_bound(tcl), u, i, var[i], id_neigh, var[id_neigh], dist
                            );
              }*/
              sub_icard = SUB_CARD_TTC;
              break;
            }
          }
          break; // fin du switch WEST_TTC

          /*	    			case BANKS_TTC:
                                                  if(u > 0.0)
                                                          { //fill the diagonal value
                                                                  mat6[mat4[i+1]-1] += theta*u/dist;
                                                                  b[i] += -((1-theta)*u/dist)*var[i];
                                                  }
                                                  else
                                                          {
                                                                  switch (tcl)
                                                                  {
                                                                          case NO_BOUND_TTC:  //face active //fill the value for neighbor

                                                                          if(id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard]!=DIRI_CENTER_TTC)
                                                                                  { // SW 29/05/2018 pour confluence
                                                                                          mat6[mat4[i+1]-1-nvois+nvois_ele_i] += theta*u/dist;
                                                                                          b[i] += -(1-theta)*u/dist*var[id_neigh];
                                                                                  }
                                                                                  break;

                                                                          case DIRI_FACE_TTC: //face active //bound adv
                                                                                  cl_val=pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
                                                                                  b[i] += - theta*cl_val*u/dist;
                                                                              b[i] += - ((1-theta)*cl_val*u/dist);
                                                                                  sub_icard = SUB_CARD_TTC; // SW 28/05/2018 pour calculer une seul fois
                                                                                   break;
                                                                  }
                                                          }
                                                  break;


                                                  case ATMOS_TTC:
                                                          if(u < 0.0)
                                                          { //fill the diagonal value
                                                                  mat6[mat4[i+1]-1] += -theta*u/dist;
                                                                  b[i] += ((1-theta)*u/dist)*var[i];
                                                          }
                                                          else
                                                          {
                                                                  switch (tcl)
                                                                  {
                                                                          case NO_BOUND_TTC:  //face active
                                                                                  if(id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard]!=DIRI_CENTER_TTC)
                                                                                  { // SW 29/05/2018 pour confluence
                                                                                          mat6[mat4[i+1]-1-nvois+nvois_ele_i] += - theta*u/dist;
                                                                                          b[i]+= (1-theta)*u/dist*var[id_neigh];
                                                                                  }
                                                                          break;

                                                                          case DIRI_FACE_TTC:
                                                                                  cl_val=pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
                                                                                  b[i] += theta*cl_val*u/dist;
                                                                                  b[i] += ((1-theta)*cl_val*u/dist);
                                                                                  sub_icard = SUB_CARD_TTC; // SW 28/05/2018 pour calculer une seul fois
                                                                                  break;
                                                                  }
                                                          }
                                                  break;
                              case RIVAQ_TTC:
                                  if(u > 0.0)
                                  { //fill the diagonal value
                                      mat6[mat4[i+1]-1] += theta*u/dist;
                                      b[i] += -((1-theta)*u/dist)*var[i];
                                  }
                                  else
                                  {
                                      switch (tcl)
                                      {
                                          case NO_BOUND_TTC:  //face active //fill the value for neighbor

                                              if(id_neigh != NONE_TTC && pspecies->p_boundary_ttc[id_neigh]->icl[icard][sub_icard]!=DIRI_CENTER_TTC)
                                              { // SW 29/05/2018 pour confluence
                                                  mat6[mat4[i+1]-1-nvois+nvois_ele_i] += theta*u/dist;
                                                  b[i] += -(1-theta)*u/dist*var[id_neigh];
                                              }
                                              break;

                                          case DIRI_FACE_TTC: //face active //bound adv
                                              cl_val=pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
                                              b[i] += - theta*cl_val*u/dist;
                                              b[i] += - ((1-theta)*cl_val*u/dist);
                                              sub_icard = SUB_CARD_TTC; // SW 28/05/2018 pour calculer une seul fois
                                              break;
                                      }
                                  }
                                  break;

          //                    LP_printf(fpout, "ADV_transport RIV in %s , face %s boundary type %s velocity: %lf , var[%d] = %lf var_neigh[%d] = %lf dist %lf \n", __func__ ,
          //                              TTC_param_card_river(icard), TTC_name_bound(tcl), u, i, var[i], id_neigh, var[id_neigh], dist
          //                              );*/

        } // fin du switch sur icard
      }   // fin de la composante advective

      if (tcl == NO_BOUND_TTC && id_neigh != NONE_TTC) {
        nvois_ele_i++;
      } // fin face active nb neig

      if (tcl == DIRI_CENTER_TTC) // variable impose au centre de la maille
      {
        //	    		cl_val=pspecies->p_boundary_ttc[i]->valcl[1][1];
        b[i] = cl_val;
        mat6[mat4[i + 1] - 1] = 1; // reste d'Arthur à verifier le bilan semble faux
        //                LP_printf(fpout, "BC is DIRI CENTER, ele %i cl_val: %lf\n", i, cl_val);
      }

      if (tcl == NEU_FACE_TTC) // Conditions de flux imposé à la face
      {

        /*  if (type == SOLUTE_TTC) {

            cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
            if (cl_val > 0) // apport
            {
              b[i] += cl_val / (dist * rhowcw);
              //sub_icard = SUB_CARD_TTC;
            }
            else if (cl_val < 0) // prélèvement   // cl_val = Q; /s pour solute
            {
              mat6[mat4[i + 1] - 1] += (-cl_val * theta / dist);
              b[i] += ((1 - theta) * cl_val / dist) * var[i];
              //sub_icard = SUB_CARD_TTC;
            }
            sub_icard = SUB_CARD_TTC;
          }*/
        // NG : 12/06/2023 : Getting rid of SOLUTE case. Making initial HEAT case working for both. Therefore, cl_val flux condition
        //  if (type == HEAT_TTC)                // is supposed to be pre-divided by the river element length.
        //  {
        if (icard == BANKS_TTC || icard == ATMOS_TTC || icard == RIVAQ_TTC) {
          cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
          //                        LP_printf(fpout,"NEUFACE at face %s id is %d, value is %lf, boundary type: %s dist %lf\n",
          //                                  TTC_param_card_river(icard), i,  cl_val, TTC_name_bound(tcl), dist);
          if (cl_val > 0) {
            b[i] += theta * cl_val;        // (dist *rhowcw)
            b[i] += (1. - theta) * cl_val; // (dist *rhowcw)

            // LP_printf(fpout,"NEUFACE  + at face %s id is %d, value is %e, boundary type: %s dist %lf\n", TTC_param_card_river(icard), i,  cl_val, TTC_name_bound(tcl), dist);

          } else if (cl_val < 0 && icard != ATMOS_TTC) {

            mat6[mat4[i + 1] - 1] += (-cl_val * theta); // /dist
            b[i] += ((1 - theta) * cl_val) * var[i];    // /dist
            /*if ( i == 7412 || i == 7143 || i == 7144 || i == 26161 || i == 26162  || i == 26222 || i == 26223 || i == 7571  || i == 7507 || i == 14475 || i == 15147) {
                    LP_printf(fpout,"NEUFACE -  at face %s id is %d, value is %e, boundary type: %s dist %lf\n",
                                          TTC_param_card_river(icard), i,  cl_val, TTC_name_bound(tcl), dist);
                }*/

          } else if (cl_val < 0 && icard == ATMOS_TTC) {
            b[i] += theta * cl_val;        // (dist *rhowcw)
            b[i] += (1. - theta) * cl_val; // (dist *rhowcw)
            /*if ( i == 7412 || i == 7143 || i == 7144 || i == 26161 || i == 26162  || i == 26222 || i == 26223 || i == 7571  || i == 7507 || i == 14475 || i == 15147) {
                    LP_printf(fpout,"NEUFACE -  at face %s id is %d, value is %e, boundary type: %s dist %lf\n",
                                          TTC_param_card_river(icard), i,  cl_val, TTC_name_bound(tcl), dist);
                }*/
          }
          sub_icard = SUB_CARD_TTC;
        }
        //    }
      }
    } // fin de boucle sur sub_icard
  }   // fin de boucle sur l'élément (icard)
}

void TTC_fill_mat6_and_b_cawaqs(s_carac_ttc *pcarac_ttc, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, double dt, FILE *fpout) {
  s_gc *pgc = pspecies->pgc;
  int i;
  int regime = pcarac_ttc->regime;
  int type = pspecies->pset->type;
  int nele = pcarac_ttc->count[NELE_TTC];
  s_link_ttc *plink = pcarac_ttc->p_link[pspecies->id];
  double s, *mat6, *b, *var;
  int *mat4;
  double surf_t, surf_titer;

  mat6 = pgc->mat6;
  mat4 = pgc->mat4;
  b = pgc->b;
  var = pspecies->pval_ttc->val;

  //  for (i=0;i<nele;i++) LP_printf(fpout,"In %s : Var[%d] = %f\n",__func__,i,var[i]);

  for (i = 0; i < nele; i++) {
    Calc_dispcond(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout);     // Calcul du coefficient de dispersion D
    TTC_no_cap_cawaqs(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout); // Remplissage des coefficients de mat6 et du RHS b
  }

  if (regime == TRANSIENT_TTC) {
    for (i = 0; i < nele; i++) {
      Calc_s(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout);
      // LP_printf(fpout,"valeur de s pour ele %d = %f\n",i,p_param_calc_ttc[i]->coeff[S_TTC]);  // NG check

      s = p_param_calc_ttc[i]->coeff[S_TTC];
      surf_t = p_param_calc_ttc[i]->coeff[SURF_T_TTC];         // surface mouillée à t
      surf_titer = p_param_calc_ttc[i]->coeff[SURF_TITER_TTC]; // surface moullée à t-dt

      if (pspecies->p_boundary_ttc[i]->icl[0][0] != DIRI_CENTER_TTC) {
        b[i] += s * surf_titer * var[i] / dt;
        mat6[mat4[i + 1] - 1] += s * surf_t / dt; // partie temporelle de A
      }

      //            if ( surf_t < 0.1 ) LP_warning(fpout, "River water level is below 0.1m in ele %d \n", i);
    }
  }
}

void TTC_print_mat4_mat5(int dim, int *array, FILE *flog) {
  int i;
  for (i = 0; i < dim; i++) {
    LP_printf(flog, "[%d] %d ", i, array[i]);
  }
  LP_printf(flog, "\n");
}
