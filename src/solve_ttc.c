/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: solve_ttc.c
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "CHR.h"
#include "GC.h"
#include "IO.h"

#include "TTC.h"

// solve_ttc
/////////////////////////////////////////////////////////////////////////////////////////

void TTC_steady_solve(s_carac_ttc *pcarac_ttc, s_link_ttc *plink, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, FILE *fpout) {
  s_gc *pgc;
  //    s_link_ttc *plink; // DK AR 26 08 2021 adding plink into the function
  //    plink=pcarac_ttc->plink; // DK AR 26 08 2021 adding plink into the function
  int i, regime, type, nele;
  double dt = 0;
  FILE *fpvar = NULL;
  FILE *fpmb = NULL;
  nele = pcarac_ttc->count[NELE_TTC];
  regime = pcarac_ttc->regime;
  type = pspecies->pset->type;
  pgc = pspecies->pgc;
  int ts_count = 0;

  // TTC_header_output_steady(fpvar,type);
  // TTC_header_output_MB_steady(fpmb,type);
  // TTC_init_tab_gc

  TTC_print_carac_properties(pcarac_ttc, fpout); // NG : 24/11/2020 // NG : 02/10/2021 : Updated version to work with DK branch ttc0.19

  TTC_init_mat5_mat4_libaq(plink, nele, fpout); // NG : 24/11/2020 Bug sur les subfaces corrigé. DK AR 26 08 2021 add plink instead of pspecies

  pgc->mat6 = GC_create_mat_double(pspecies->pgc->lmat5);
  TTC_init_RHS(pgc);
  pgc->x = GC_create_mat_double(pspecies->pgc->ndl);

  // On remplit mat6 qui sera utile pour le solveur libgc
  TTC_fill_mat6_and_b_libaq(pcarac_ttc, plink, p_param_calc_ttc, pspecies, dt, fpout);

  /* NG : 11/06/2021 : New GC_configure_gc() function used to update solver param, depending on the iteration number (steady) or time step we're at,
                       in order to save some calculation time. 'ts_count' (int*) is the counter of time steps or iteration. */
  TTC_print_mat(pspecies, fpout);
  // Je rends ça inerte (ts_count=0) pour ne pas avoir à modifier les arguments et faire bugger UTTC.
  GC_configure_gc(pgc, &ts_count, fpout);

  GC_solve_gc(pgc, TTC_GC, fpout);
  // --------------------------------------------

  // On met la solution de gc dans le tableau var à l'instant t qui servira d'initialisation pour l'iteration suivante
  for (i = 0; i < nele; i++) {
    pspecies->pval_ttc->val_old[i] = pspecies->pval_ttc->val[i];
    pspecies->pval_ttc->val[i] = pspecies->pgc->x[i];
    //        LP_printf(fpout,"%i \t %lf\n",i,pspecies->pval_ttc->val[i]);
    // NG 24/11/2020 : Attention, normalement ici il faudrait renvoyer le résultat pour chaque élément à libmesh avant de passer à l'itération suivante.

    // LP_printf(fpout,"Specie %s : T[%d] = %f\n",pspecies->name,i,plink->pspecies->pval_ttc->var[i]); // NG check
  }

  // for(i=0;i<nele;i++)
  //{
  // TTC_cal_condflux(pcarac_ttc,pparam_calc_ttc,pspecies,i);
  // TTC_cal_advflux(pcarac_ttc,pparam_calc_ttc,pspecies,i);
  // TTC_cal_totflux(pcarac_ttc,pparam_calc_ttc,pspecies,i);
  // }
  // TTC_print_output_var_steady(pcarac_ttc,plink->pspecies->pval_ttc,fpvar);
  // TTC_print_output_MB_steady(pcarac_ttc,pparam_calc_ttc,pspecies,fpmb);
}

// boucle temporelle
void TTC_time_loop(s_carac_ttc *pcarac_ttc, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, double dt, double t_end, int *ts_count, FILE *fpout) {
  // nt nombre d'iteration temporelle le temps final de simulation = dt*nt
  // dt discretisation temporelle (s)
  s_gc *pgc;
  s_link_ttc *plink;                        // DK AR 26 08 2021 adding plink into the function
  plink = pcarac_ttc->p_link[pspecies->id]; // DK AR 26 08 2021 adding plink into the function
  int nele;
  double t = 0;
  int i, regime, type, icard, tstep;

  nele = pcarac_ttc->count[NELE_TTC];
  regime = pcarac_ttc->regime;
  type = pspecies->pset->type;
  pgc = pspecies->pgc;
  FILE *fpvar = NULL;
  FILE *fpmb = NULL;
  TTC_header_output(fpvar, type);
  TTC_header_output_MB(fpmb, type);
  // initialisation //TTC_init_tab_gc
  TTC_init_mat5_mat4(plink, nele, fpout);

  // boucle sur le temps
  while (t < t_end) {

    t = t + dt;
    tstep = t / dt;
    LP_printf(fpout, "Inside %s, tstep is %d\n", __func__, tstep);

    // initialisation //TTC_init_tab_gc
    TTC_init_RHS(pgc);
    TTC_init_sol_vec(pgc);
    TTC_free_mat6(pgc);
    //  TTC_init_mat5_mat4(plink,nele,fpout); // DK mat5 mat4 invariant over time
    pgc->mat6 = GC_create_mat_double(pspecies->pgc->lmat5);
    // On remplit mat6 qui sera utile pour le solveur libgc
    //  TTC_fill_mat6_and_b(pcarac_ttc,p_param_calc_ttc,pspecies,dt,fpout);
    TTC_fill_mat6_and_b_libaq(pcarac_ttc, plink, p_param_calc_ttc, pspecies, dt, fpout);
    for (i = 0; i < nele; i++) {
      LP_printf(fpout, "%i \t %lf \t %lf \n", i, pspecies->pval_ttc->val[i], pgc->b[i]);
      // NG 24/11/2020 : Attention, normalement ici il faudrait renvoyer le résultat pour chaque élément à libmesh avant de passer à l'itération suivante.
      // LP_printf(fpout,"Specie %s : T[%d] = %f\n",pspecies->name,i,plink->pspecies->pval_ttc->var[i]); // NG check
    }
    /* NG : 11/06/2021 : New GC_configure_gc() function used to update solver param, depending on the iteration number or time step we're at,
                         in order to save some calculation time. 'ts_count' (int*) is the counter of time step iteration. */
    for (i = 0; i < nele; i++) {
      LP_printf(fpout, "%i \t val: %lf \t x : %lf \t b: %e \t mat6: %e\n", i, pspecies->pval_ttc->val[i], pspecies->pgc->x[i], pspecies->pgc->b[i], pspecies->pgc->mat6[i]);
    }
    GC_configure_gc(pgc, ts_count, fpout);
    GC_solve_gc(pgc, TTC_GC, fpout);
    // --------------------------------------------

    for (i = 0; i < nele; i++) {
      pspecies->pval_ttc->val_old[i] = pspecies->pval_ttc->val[i];
      pspecies->pval_ttc->val[i] = pspecies->pgc->x[i];
      LP_printf(fpout, "%i \t val: %lf \t x : %lf \t b: %e \t mat6: %e\n", i, pspecies->pval_ttc->val[i], pspecies->pgc->x[i], pspecies->pgc->b[i], pspecies->pgc->mat6[i]);
      // NG 24/11/2020 : Attention, normalement ici il faudrait renvoyer le résultat pour chaque élément à libmesh avant de passer à l'itération suivante.

      // LP_printf(fpout,"Specie %s : T[%d] = %f\n",pspecies->name,i,plink->pspecies->pval_ttc->var[i]); // NG check
    }
    //  plink->pspecies->pval_ttc->var=pgc->x;
    // On met la solution de gc dans le tableau var a linstant t qui servira d initialisation pour literation temporelle suivante
    //  for(i=0;i<nele;i++)
    //  {
    //  plink->pspecies->pval_ttc->val[i]=pgc->x[i];
    //  	}
    for (i = 0; i < nele; i++) {
      TTC_cal_condflux(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout);
      TTC_cal_advflux(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout);
      TTC_cal_totflux(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout);
    }
    printf(" %lf\n", t);
    TTC_print_output_var(pcarac_ttc, plink, t, fpvar);
    TTC_print_output_MB(pcarac_ttc, pspecies, t, fpmb);
  }
}

// boucle temporelle
void TTC_time_loop_libaq(s_carac_ttc *pcarac_ttc, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, double dt, double t_end, int *ts_count, FILE *fpout) {
  // nt nombre d'iteration temporelle le temps final de simulation = dt*nt
  // dt discretisation temporelle (s)
  s_gc *pgc;
  s_link_ttc *plink = pcarac_ttc->p_link[pspecies->id]; // DK AR 26 08 2021 adding plink into the function
  int nele;
  double t = 0;

  int i, regime, type, icard, tstep;
  int id;

  nele = pcarac_ttc->count[NELE_TTC];
  regime = pcarac_ttc->regime;
  type = pspecies->pset->type;
  pgc = pspecies->pgc;
  FILE *fpvar = NULL;
  FILE *fpmb = NULL;
  TTC_header_output(fpvar, type);
  TTC_header_output_MB(fpmb, type);
  // initialisation //TTC_init_tab_gc
  TTC_init_mat5_mat4(plink, nele, fpout);

  LP_printf(fpout, "Inside %s, tstep is %d\n", __func__, tstep);
  // boucle sur le temps
  while (t < t_end) {

    t = t + dt;
    tstep = t / dt;
    LP_printf(fpout, "Inside %s, tstep is %d, t is %lf, dt is %lf, t_end is %lf\n", __func__, tstep, t, dt, t_end);

    // initialisation //TTC_init_tab_gc
    TTC_init_RHS(pgc);
    TTC_init_sol_vec(pgc);
    TTC_free_mat6(pgc);
    //  TTC_init_mat5_mat4(plink,nele,fpout); // DK mat5 mat4 invariant over time
    pgc->mat6 = GC_create_mat_double(pspecies->pgc->lmat5);
    // On remplit mat6 qui sera utile pour le solveur libgc
    //  TTC_fill_mat6_and_b(pcarac_ttc,p_param_calc_ttc,pspecies,dt,fpout);
    TTC_fill_mat6_and_b_libaq(pcarac_ttc, plink, p_param_calc_ttc, pspecies, dt, fpout);
    TTC_print_mat(pspecies, fpout);

    //        TTC_print_RHS(pgc,fpout);
    //        TTC_print_LHS(pgc,fpout);
    /* NG : 11/06/2021 : New GC_configure_gc() function used to update solver param, depending on the iteration number or time step we're at,
                         in order to save some calculation time. 'ts_count' (int*) is the counter of time step iteration. */
    //        for(i=0;i<nele;i++)
    //        {
    //            LP_printf(fpout,"%i \t val: %lf \t x : %lf \t b: %e \t\n",i,pspecies->pval_ttc->val[i], pspecies->pgc->x[i], pspecies->pgc->b[i]);
    //        }
    GC_configure_gc(pgc, ts_count, fpout);
    GC_solve_gc(pgc, TTC_GC, fpout);
    // --------------------------------------------

    for (i = 0; i < nele; i++) {
      pspecies->pval_ttc->val_old[i] = pspecies->pval_ttc->val[i];
      pspecies->pval_ttc->val[i] = pspecies->pgc->x[i];
      //            LP_printf(fpout,"%i \t val: %lf \t x : %lf \t b: %e \t \n",i,pspecies->pval_ttc->val[i], pspecies->pgc->x[i], pspecies->pgc->b[i]);
      // NG 24/11/2020 : Attention, normalement ici il faudrait renvoyer le résultat pour chaque élément à libmesh avant de passer à l'itération suivante.

      // LP_printf(fpout,"Specie %s : T[%d] = %f\n",pspecies->name,i,plink->pspecies->pval_ttc->var[i]); // NG check
    }
    //  plink->pspecies->pval_ttc->var=pgc->x;
    // On met la solution de gc dans le tableau var a linstant t qui servira d initialisation pour literation temporelle suivante
    //  for(i=0;i<nele;i++)
    //  {
    //  plink->pspecies->pval_ttc->val[i]=pgc->x[i];
    //  	}
    for (i = 0; i < nele; i++) {
      TTC_cal_condflux(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout);
      TTC_cal_advflux(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout);
      TTC_cal_totflux(pcarac_ttc, p_param_calc_ttc, pspecies, i, fpout);
    }
    printf(" %lf\n", t);
    TTC_print_output_var(pcarac_ttc, plink, t, fpvar);
    TTC_print_output_MB(pcarac_ttc, pspecies, t, fpmb);
  }
}
