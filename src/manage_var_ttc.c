/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: manage_var_ttc.c
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "GC.h"
#include "CHR.h"
#include "IO.h"

#include "TTC.h"
// DK AR 26 08 2021 this file is obsolete, new one is manage_val_ttc.c
s_val_ttc *TTC_create_val() {
  s_val_ttc *pval_ttc;
  pval_ttc = new_val_ttc();
  bzero((char *)pval_ttc, sizeof(s_val_ttc));
  return pval_ttc;
}

// DK 20 08 2021 remove unused var variables
// DK AR 24 08 2021 remove var_nk > unused variable
s_val_ttc *TTC_init_val(int nele) {
  s_val_ttc *pval_ttc;

  pval_ttc = TTC_create_val();
  pval_ttc->val = TTC_create_tab_double(nele);
  pval_ttc->val_old = TTC_create_tab_double(nele);

  return pval_ttc;
}

s_coef_roxy_ttc *TTC_create_reoxy() {
  s_coef_roxy_ttc *preoxy_ttc;
  preoxy_ttc = new_reoxy_ttc();
  bzero((char *)preoxy_ttc, sizeof(s_coef_roxy_ttc));
  return preoxy_ttc;
}

s_coef_roxy_ttc *TTC_init_reoxy(int nele) {
  s_coef_roxy_ttc *preoxy_ttc;

  preoxy_ttc = TTC_create_reoxy();
  preoxy_ttc->debit_surverse = TTC_create_tab_double(nele);
  preoxy_ttc->rn = TTC_create_tab_double(nele);
  preoxy_ttc->rd = TTC_create_tab_double(nele);
  preoxy_ttc->frac_debit_oxy = TTC_create_tab_double(nele);
  return preoxy_ttc;
}
