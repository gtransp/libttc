/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: manage_species.c
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "GC.h"
#include "CHR.h"
#include "IO.h"

#include "TTC.h"

s_species_ttc *TTC_create_species() {
  s_species_ttc *pspecies;
  int i;

  pspecies = new_species_ttc();
  bzero((char *)pspecies, sizeof(s_species_ttc));
  pspecies->pset = TTC_init_settings(pspecies);
  return pspecies;
}

/* NG : 23/11/2020 : Fonction TTC_init_by_default() crée pour séparer
l'allocation mémoire pour pspecies et son initialisation dans TTC_init_species().
 DK 20 08 2021 Addition of setting structure, correcting references
 */
void TTC_init_species_by_default(s_species_ttc *pspecies, int nele, FILE *flog) {

  pspecies->p_param_ttc = TTC_init_param(nele);
  pspecies->pgc = GC_create_gc(pspecies->pset->iappli_gc);
  pspecies->pgc->b = TTC_create_tab_double(nele);
  pspecies->pval_ttc = TTC_init_val(nele);
  pspecies->p_flux_ttc = TTC_init_flux(nele, flog);
  // TODO Change the structure by reducing the tstep dimension and embedding it inside the structore to the variable with the time component (i.e. valcl)
  pspecies->p_boundary_ttc = TTC_init_boundary(nele);
  pspecies->p_source_ttc = TTC_init_boundary(nele);
}

s_species_ttc *TTC_init_species(int nele, int id_species, FILE *flog) {
  s_species_ttc *pspecies;
  pspecies = TTC_create_species();
  pspecies->id = id_species; // NG : 02/10/2021 : Filling Specie Intern ID

  TTC_init_species_by_default(pspecies, nele, flog);

  return pspecies;
}

/** \fn s_species_ttc *TTC_chain_fwd_ts(s_species_ttc *pd1,s_species_ttc *pd2)
/* \brief Links two elements with structure s_species_ttc */
s_species_ttc *TTC_chain_fwd_ts(s_species_ttc *pd1, s_species_ttc *pd2) {
  pd1->next = pd2;
  pd2->prev = pd1;
  return pd2;
}

/**\fn s_species_ttc *TS_secured_chain_fwd(s_species_ttc *pspecies_ttc1,s_species_ttc *pspecies_ttc2)
 *\brief chain species_ttc with the next one
 */
s_species_ttc *TTC_secured_chain_fwd(s_species_ttc *pspecies_ttc1, s_species_ttc *pspecies_ttc2) {
  if (pspecies_ttc1 != NULL)
    pspecies_ttc1 = TTC_chain_fwd_ts(pspecies_ttc1, pspecies_ttc2);
  else
    pspecies_ttc1 = pspecies_ttc2;
  return pspecies_ttc1;
}

int TTC_get_species_rank_by_name(s_carac_ttc *pcarac_ttc, char *name_verif, FILE *fpout) {
  s_species_ttc *pspecies;
  int i, length_name, length_verif, n;
  char *name;
  n = CODE_TS;
  for (i = 0; i < pcarac_ttc->count[NSPECIES_TTC]; i++) {
    pspecies = pcarac_ttc->p_species[i];
    name = pspecies->name;

    if (strcmp(name_verif, name) == 0) {
      n = i;
    }
  }
  if (n == CODE_TS) {
    LP_error(fpout, "NO species named %s in setup simul %s", name_verif, pspecies->name);
  } else {
    return n;
  }
}

s_settings_ttc *TTC_init_settings(s_species_ttc *pspecies) {

  s_settings_ttc *pset;
  int i;

  pset = new_settings_ttc();
  // NG : 06/11/2024 : Adding some more default initialization values;
  pset->iteration = ITERMAX_DEFAULT_TTC;
  pset->crconv = CRCONV_DEFAULT_TTC;
  for (i = 0; i < NPROCESSES_TTC; i++)
    pset->calc_process[i] = NO_TTC;

  pset->type = HEAT_TTC;
  pset->media_type = POROUS_TTC;
  pset->oxygen = NO_TS;
  pset->iappli_gc = TTC_GC;
  pset->aquitard = AQUITARD_OFF_TTC; // The initial value is 0; 1 enables aquitard calculations
  pset->riverbed = AQUITARD_OFF_TTC; // The initial value is 0; 1 enables aquitard calculations
  pset->source = NO_TTC;
  pset->boundaries = NO_TTC; // NG : 21/06/2023

  return pset;
}
