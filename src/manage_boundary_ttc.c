/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: manage_boundary_ttc.c
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "CHR.h"
#include "GC.h"
#include "IO.h"

#include "TTC.h"

/**
 * \fn
 * \brief
 * \return
 */
s_boundary_ttc *TTC_create_boundary() {

  s_boundary_ttc *pboundary_ttc;
  pboundary_ttc = new_boundary_ttc();
  bzero((char *)pboundary_ttc, sizeof(s_boundary_ttc));
  return pboundary_ttc;
}

/**
 * \fn s_boundary_ttc **TTC_init_boundary (int)
 * \brief
 * \return
 */
s_boundary_ttc **TTC_init_boundary(int nele) {
  s_boundary_ttc **p_boundary_ttc;

  p_boundary_ttc = (s_boundary_ttc **)malloc(nele * sizeof(s_boundary_ttc *));
  for (int i = 0; i < nele; i++) {
    p_boundary_ttc[i] = new_boundary_ttc();
  }
  return p_boundary_ttc;
}

/**
 * \fn s_count_ttc *TTC_create_counter()
 * \brief create a transport counter
 * \return pcount_ttc
 */
s_count_ttc *TTC_create_counter() {

  s_count_ttc *pcount_ttc;
  int i;

  pcount_ttc = new_count_ttc();
  bzero((char *)pcount_ttc, sizeof(s_count_ttc));

  pcount_ttc->elebound = (s_id_io **)malloc(N_BCNAME_TTC * sizeof(s_id_io *));
  for (i = 0; i < N_BCNAME_TTC; i++) {
    pcount_ttc->elebound[i] = NULL;
  }

  pcount_ttc->elesource = ((s_id_io **)malloc(NB_SOURCE_TTC * sizeof(s_id_io *)));
  for (i = 0; i < NB_SOURCE_TTC; i++) {
    pcount_ttc->elesource[i] = NULL;
  }

  pcount_ttc->eleactive = NULL;

  return pcount_ttc;
}

/**\fn s_source_ttc *TTC_define_source_val(int kindof,s_ft *pft,double stot,FILE *fpout)
 *\brief define sources values
 *\return *s_bound_aq
 */
s_boundary_ttc *TTC_define_bound_val(int kindof, s_ft *pft, double stot, FILE *fpout) {
  s_boundary_ttc *pbound;
  s_ft *ptmp;
  char *name_freq = NULL;
  int sign = 1;

  pbound = TTC_create_bboundary(kindof);

  stot = 1.;
  ptmp = TS_division_ft_double(pft, stot, fpout);

  pft = TS_free_ts(pft, fpout);
  pbound->pft = ptmp;
  if (pbound->pft->next != NULL) {
    pbound->freq = VARIABLE_TTC;
  }

  name_freq = TTC_name_freq(pbound->freq);
  // LP_printf(fpout,"Fonction %s : Assigning frequency %s.\n",__func__, name_freq);
  free(name_freq);

  return pbound;
}

/**\fn s_source_ttc *TTC_create_sboundary(int kindof)
 *\brief create BC structur
 *\return pbound
 */
s_boundary_ttc *TTC_create_bboundary(int kindof) {

  s_boundary_ttc *pboundary;
  pboundary = new_boundary_ttc();
  bzero((char *)pboundary, sizeof(s_boundary_ttc));
  pboundary->type = kindof;

  return pboundary;
}

/**\fn s_bound_aq *AQ_create_boundary(int kindof)
 *\brief create BC structure
 *\return pbound
 */
s_boundary_ttc *TTC_create_boundary_ts(int kindof) {

  s_boundary_ttc *pbound;
  pbound = new_boundary_ttc();
  bzero((char *)pbound, sizeof(s_boundary_ttc));
  pbound->type = kindof;

  return pbound;
}