/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: manage_param_thermic_default_ttc.c
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "GC.h"
#include "CHR.h"
#include "IO.h"

#include "TTC.h"

// DK AR 17 08 2021 refactor param_base_ttc is reduced into param ttc (dim = nele x nparam)
// allocation des valeurs defaults des parametres thermiques de l eau TODO check if the param is allocated correctly
// DK AR 20 07 2021 changes in the structure of the p_param_ttc are reflected here
void TTC_default_pthermic(s_param_ttc **p_param_ttc, int nele) {
  int i;
  for (i = 0; i < nele; i++) {
    p_param_ttc[i]->param_thermic[WATER_TTC][LAMBDA_TTC] = LAMBDAW_TTC;
    p_param_ttc[i]->param_thermic[WATER_TTC][RHO_TTC] = RHOW_TTC;
    p_param_ttc[i]->param_thermic[WATER_TTC][HEAT_CAP_TTC] = HEAT_CAPW_TTC;
  }
}
