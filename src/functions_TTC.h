/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: functions_TTC.h
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

// structures creees par les fonctions

////manage_carac_ttc
// les parametres de setup du modele (var, type de transport, dt...)
s_carac_ttc *TTC_create_carac();
// Initialisation des pointeurs de la structure carac
s_carac_ttc *TTC_init_carac();
// Afficage des propriétés des carac
void TTC_print_carac_properties(s_carac_ttc *, FILE *); // NG : 02/10/2021 : Updated version for libttc0.19 (DK branch)

////manage_species_ttc
// structure des parametres pour chaque especes
s_species_ttc *TTC_create_species();
// Allocation et Initialisation des pointeurs de la structure species
s_species_ttc *TTC_init_species(int, int, FILE *);
// NG : 23/11/2020 : Initialisation par défaut des pointeurs de la structure species
void TTC_init_species_by_default(s_species_ttc *, int, FILE *);

s_species_ttc *TTC_secured_chain_fwd(s_species_ttc *, s_species_ttc *);
int TTC_get_species_rank_by_name(s_carac_ttc *, char *, FILE *);
////manage_param_base_ttc
// les parametres caracteristiques
s_param_ttc *TTC_create_param(); // DK AR 27 08 2021 change param base into param_ttc initialization and allocation
// Initialisation des pointeurs de la structure param_base
s_param_ttc **TTC_init_param(int); // DK refactor no need for these structures & functions 17 08 2021

// DK AR initialize s_settings_ttc 27 08 2021
s_settings_ttc *TTC_init_settings(s_species_ttc *);

////manage_neigh.c
// Initialisation des pointeurs de la structure neigh
s_neigh_ttc **TTC_init_neigh(int);

////manage_param_aquitard_ttc // DK AR Aquitard 10 12 2021
// les parametres caracteristiques de aquitard
s_aquitard_ttc *TTC_create_aquitard(); // DK AR 27 08 2021 change param base into param_ttc initialization and allocation
// Initialisation des pointeurs de la structure aquitard
s_aquitard_ttc **TTC_init_aquitard(int);

// Calculation of coefficient upsilon of Kurylyk 2017;
void Calc_upsilon(s_species_ttc *, int, int, FILE *);
// Calculation of coefficient alpha of Kurylyk 2017;
void Calc_alpha(s_species_ttc *, int, int, FILE *);
// Pass the velocity calculated at the faces of two layers to aquitard structure;
void Calc_q(s_carac_ttc *, s_species_ttc *, int, int, FILE *);
// Calculate coefficients to solve the aquitard in each simulation;
void Calc_coeff(s_species_ttc *, int, int, FILE *);
// Aquitard parameterization
void Calc_aquitard_parameters(s_species_ttc *, int, int, FILE *);
// Calculate the Peclet number;
void Calc_aquitard_peclet(s_species_ttc *, int, int, FILE *);
// Calculate the heat fluxes;
void Calc_aquitard_fluxes(s_species_ttc *, int, int, FILE *);
// Init the riverbed
s_aquitard_ttc **TTC_init_riverbed(int);

////manage_param_thermic
// les parametres thermiques
// s_param_thermic_ttc *TTC_create_param_thermic(); DK refactor no need for these structures & functions 17 08 2021
// Initialisation des pointeurs de la structure param_thermic
// s_param_thermic_ttc *TTC_init_param_thermic(int); DK refactor no need for these structures & functions 17 08 2021

////manage_param_thermic_default_ttc.c
// Default water values
void TTC_default_pthermic(s_param_ttc **, int); // DK 18 08 2021 TODO Check allocation of the structure in this function s param ttc

////manage_param_solute
// les parametres solutes
// s_param_solute_ttc *TTC_create_param_solute(); DK refactor no need for these structures & functions 17 08 2021
// Initialisation des pointeurs de la structure param_solute
// s_param_solute_ttc *TTC_init_param_solute(int);DK refactor no need for these structures & functions 17 08 2021

////manage_flux_ttc
// Initialisation des pointeurs de la structure flux
// s_tot_ttc *TTC_init_totflux(int);
// s_cond_ttc *TTC_init_condflux(int);
// s_adv_ttc *TTC_init_advflux(int);
s_flux_ttc *TTC_create_flux();
s_flux_ttc **TTC_init_flux(int, FILE *);
void TTC_cal_condflux(s_carac_ttc *, s_param_calc_ttc **, s_species_ttc *, int, FILE *);
void TTC_cal_advflux(s_carac_ttc *, s_param_calc_ttc **, s_species_ttc *, int, FILE *);
void TTC_cal_advflux_riv(s_carac_ttc *, s_param_calc_ttc **, s_species_ttc *, int, FILE *);
void TTC_cal_totflux(s_carac_ttc *, s_param_calc_ttc **, s_species_ttc *, int, FILE *);
void TTC_calc_transport_mb(s_carac_ttc *, s_param_calc_ttc **, s_species_ttc *, int i, FILE *);
void TTC_cal_totflux_hyd(s_carac_ttc *, s_param_calc_ttc **, s_species_ttc *, int i, FILE *);

////manage_u_ttc
// Initialisation des pointeurs de la structure u
// s_u_ttc *TTC_init_u(int); DK 19 08 2021 changes in the structure, no need velocity structure currently

////manage_u_ttc
// Initialisation des pointeurs de la structure disp
// s_disp_ttc *TTC_init_disp(int); Refactor structure DK

////manage_var_ttc
// variables de sortie resultant de la simulation
s_val_ttc *TTC_create_val(); // DK refactor no need for these structures & functions 17 08 2021
// Initialisation des pointeurs de la structure var
s_val_ttc *TTC_init_val(int); // DK refactor no need for these structures & functions 17 08 2021

////manage_boundary_ttc
// conditions aux limites de dirichlet appliquees
s_boundary_ttc *TTC_create_boundary();
// Initialisation des pointeurs de la structure boundary
s_boundary_ttc **TTC_init_boundary(int);
// Initialisation des pointeurs de la structure type_bound
// s_type_bound_ttc *TTC_init_type_bound(int); Refactor structure DK
// Initialisation des pointeurs de la structure val_bc
// s_val_bc_ttc *TTC_init_val_bc(int); Refactor structure DK
// s_val_bc_t_ttc *TTC_init_val_t_bc(int, int); // DK AR init valbc_transient
s_coef_roxy_ttc *TTC_create_reoxy();  // SW 24/10/2018
s_coef_roxy_ttc *TTC_init_reoxy(int); // SW 24/10/2018
s_count_ttc *TTC_create_counter();
s_boundary_ttc *TTC_define_bound_val(int, s_ft *, double, FILE *);
s_boundary_ttc *TTC_create_bboundary(int);
s_boundary_ttc *TTC_create_boundary_ts(int);

////manage_link_ttc
// vecteurs de couplage entre une librairie et libttc
s_link_ttc *TTC_create_link();

// Initialisation des pointeurs de la structure link
s_link_ttc *TTC_init_link(s_carac_ttc *, s_species_ttc *, int); // NG : 30/09/2021 : s_link_ttc * argument removed.

char *TTC_link_get_name(s_link_ttc *);
// s_link_ttc *TTC_get_link_by_name(s_carac_ttc *,char *,FILE *); //DK AR 26 08 2021 Unused function
int **TTC_allocate_nsub(int);         // tableau avec le nombre de subface par direction pour l ensemble des elements
double ***TTC_allocate_uface(int);    // tableau allocation uface DK AR 27 08 2021
double *TTC_allocate_dvdt(int);       // Function to add the change in the specific discharge associated with water volume
double *TTC_allocate_SinkSource(int); // Function to allocate the sink and source terms

////allocation_tab.c
// Allocations et initialisations des tableaux
char **TTC_create_mat_char(int, int);     // matrice de characteres
int *TTC_create_tab_int(int);             // tableau de integers
double *TTC_create_tab_double(int);       // tableau de doubles
double **TTC_create_mat_double(int, int); // matrice de doubles
int **TTC_create_mat_int(int, int);       // matrice de integers
char *TTC_create_tab_char(int);           // tableau  de characteres

////manage_param_calc_ttc.c
// Definition des fonctions de calcul des coefficients en fonction des parametres d'entree et de transition avec LIBAQ
// coefficient calcules a partir des parametres d'entree de l utilisateur
s_param_calc_ttc *TTC_create_param_calc();
// Initialisation des pointeurs de la structure param_calc
s_param_calc_ttc **TTC_init_param_calc(int);
// Cacul du terme dispersion + conduction
void Calc_dispcond(s_carac_ttc *, s_param_calc_ttc **, s_species_ttc *, int, FILE *);
// Calcul du terme de stockage thermique ou solute
// Calcul coefficient dispersion de chaque face;
void Calc_dispcond_face(s_carac_ttc *, s_param_calc_ttc **, s_species_ttc *, int, FILE *);
// thermique rhoMCM/rhoWCW
// solute porosite
void Calc_s(s_carac_ttc *, s_param_calc_ttc **, s_species_ttc *, int, FILE *);

//// manage_mat_ttc.c
// Fonction calcul longueur de mat5
int TTC_calc_lmat5(s_link_ttc *, int, FILE *);
int TTC_calc_lmat5_test(s_link_ttc *, int, int, int, FILE *);
// function to initialize and fill the integer matrix and vector needed for pgc
void TTC_init_mat5_mat4(s_link_ttc *, int, FILE *);
void TTC_init_mat5_mat4_libaq(s_link_ttc *, int, FILE *);
void TTC_init_mat5_mat4_libaq_test(s_link_ttc *, int, int, int, FILE *);
void TTC_print_mat4_mat5(int, int *, FILE *);

// fonction creation x
void TTC_init_sol_vec(s_gc *);
// fonction creation b
void TTC_init_RHS(s_gc *);
// Fonction de remplissage de mat6 et b sans terme capacitif
void TTC_no_cap(s_carac_ttc *, s_param_calc_ttc **, s_species_ttc *, int, FILE *);
void TTC_no_cap_libaq(s_carac_ttc *, s_link_ttc *, s_param_calc_ttc **, s_species_ttc *, int, FILE *); // AR 19/11/2020
void TTC_no_cap_rive(s_carac_ttc *, s_param_calc_ttc **, s_species_ttc *, int, FILE *);                // SW 11/07/2018 pour prose riviere
void TTC_no_cap_libaq_test(s_carac_ttc *, s_link_ttc *, s_param_calc_ttc **, s_species_ttc *, int, FILE *);

// concatenation pour remplir mat6 avec terme capacitif
void TTC_fill_mat6_and_b(s_carac_ttc *, s_param_calc_ttc **, s_species_ttc *, double, FILE *);
void TTC_fill_mat6_and_b_libaq(s_carac_ttc *, s_link_ttc *, s_param_calc_ttc **, s_species_ttc *, double, FILE *);      // AR 19/11/2020
void TTC_fill_mat6_and_b_libaq_test(s_carac_ttc *, s_link_ttc *, s_param_calc_ttc **, s_species_ttc *, double, FILE *); // AR 19/11/2020

void TTC_fill_mat6_and_b_rive(s_carac_ttc *, s_param_calc_ttc **, s_species_ttc *, double, FILE *); // SW 18/10/2018
void TTC_free_mat6(s_gc *);
void TTC_free_mat4(s_gc *);
void TTC_schema_centre(s_carac_ttc *, s_param_calc_ttc **, s_species_ttc *, int, double, FILE *);     // SW 27/04/2018
double TTC_reoxygeneration_rhs(double, double, double, FILE *);                                       // SW 24/10/2018
void TTC_print_LHS(s_gc *, FILE *);                                                                   // NG : 19/08/2020
void TTC_print_RHS(s_gc *, FILE *);                                                                   // NG : 19/08/2020
void TTC_fill_mat6_and_b_cawaqs(s_carac_ttc *, s_param_calc_ttc **, s_species_ttc *, double, FILE *); // NG : 20/08/2020

////solve_ttc.c
// fonction qui concerne la boucle sur le temps
void TTC_steady_solve(s_carac_ttc *, s_link_ttc *, s_param_calc_ttc **, s_species_ttc *, FILE *);
void TTC_time_loop(s_carac_ttc *, s_param_calc_ttc **, s_species_ttc *, double, double, int *, FILE *);
void TTC_time_loop_libaq(s_carac_ttc *, s_param_calc_ttc **, s_species_ttc *, double, double, int *, FILE *);
////itos.c
// Conversion des input character user -> integer code
// Type de transport SOLUTE ou HEAT
char *TTC_name_equa_type(int);
// Type de probleme CONVECTION ou DIFFUSION
char *TTC_name_process(int);
// Regime TRANSIENT ou STEADY
char *TTC_name_regime(int);
// Type de moyenne calculee
char *TTC_mean_type(int);
// Frequence des sorties DAY MONTH YEAR
char *TTC_name_freq(int);
// Type de coeffcient D ou S
char *TTC_coeff(int);
// Type CDTL NEUMAN CENTRE ,....
char *TTC_name_bound(int);
// Compteur NELE,NVOIS_ELE,NBC,NSPECIES
char *TTC_count(int);
// Parametre generaux THETA et EPS_Q
char *TTC_general_param(int);
// Type de sortie VAR,FLUX
char *TTC_output_type(int);
// Parametres thermiques LAMBDA,HEAT_CAP,RHO
char *TTC_thermic_param(int);
// Type denvironnement WATER ou SOLID ou POROUS
char *TTC_environnement(int);
// Type de parametre systemique DISPERSIVITY ou POROSITY
char *TTC_param_syst(int);
// homogene ou heterogene
char *TTC_kindof_bound(int);
// direction
char *TTC_dir_bound(int);
// direction cardinal
char *TTC_param_card(int);
// direction cardinal of river
char *TTC_param_card_river(int);
// type of source input
char *TTC_name_source(int);
// type of aquitard parameter
char *TTC_param_aquitard(int);
// YES/NO flag
char *TTC_answer(int);

////math_function
double arithmetic_mean(double, double, double);
double geometric_mean(double, double, double);
double harmonic_mean(double, double, double);
double max(double, double);
// double quadratic_mean(double,double,double); //meme nom dans libts

// manage_print_mat.c

void TTC_print_mat(s_species_ttc *, FILE *);
void TTC_print_LHS_RHS(s_gc *, FILE *); // SW 18/10/2021

////manage_output_ttc.c
void TTC_header_output(FILE *, int);
void TTC_print_output_var(s_carac_ttc *, s_link_ttc *, double, FILE *); // DK change s_var_ttc to s_link ttc 20 08 2021
void TTC_header_output_steady(FILE *, int);
void TTC_print_output_var_steady(s_carac_ttc *, s_link_ttc *, FILE *); // DK change s_var_ttc to s_link ttc 20 08 2021
void TTC_header_output_MB(FILE *, int);
void TTC_header_output_MB_steady(FILE *, int);
void TTC_print_output_MB_steady(s_carac_ttc *, s_species_ttc *, FILE *);
void TTC_print_output_MB(s_carac_ttc *, s_species_ttc *, double, FILE *);

/// manage_source_ttc.c
s_source_ttc ***TTC_create_source(FILE *);
s_source_ttc *TTC_create_sboundary(int);
s_source_ttc *TTC_define_source_val(int, s_ft *, double, FILE *);
