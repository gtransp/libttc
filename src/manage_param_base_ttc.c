/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: manage_param_base_ttc.c
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "CHR.h"
#include "GC.h"
#include "IO.h"

#include "TTC.h"
// DK AR 23 08 2021 new structure to allocate should be inserted here
// allocation de la structure param_base_ttc
s_param_ttc *TTC_create_param() // DK AR 23 08 2021 turn param base into param_ttc
{
  s_param_ttc *p_param_ttc;
  p_param_ttc = new_param_ttc();
  bzero((char *)p_param_ttc, sizeof(s_param_ttc));
  return p_param_ttc;
}

// allocation des parametres numeriques de la structure param_base_ttc
s_param_ttc **TTC_init_param(int nele) {

  s_param_ttc **p_param_ttc;

  //  pparam_base_ttc=TTC_create_param_base(nele); // NG : 24/11/2020 : Incohérence
  //  pparam_ttc=TTC_create_param();
  p_param_ttc = ((s_param_ttc **)malloc(nele * sizeof(s_param_ttc *)));
  for (int i = 0; i < nele; i++) {
    p_param_ttc[i] = new_param_ttc();
  }
  //  pparam_ttc->param_syst[DISPERSIVITY_TTC]=TTC_create_tab_double(nele);
  //  pparam_ttc->param_syst[POROSITY_TTC]=TTC_create_tab_double(nele);
  //  pparam_ttc->param_syst[SIZE_TTC]=TTC_create_tab_double(nele);
  //  pparam_ttc->diff_mol_solute=TTC_init_param_solute(nele);
  //  pparam_ttc->param_thermic[WATER_TTC]=TTC_init_param_thermic(nele);
  //  pparam_ttc->param_thermic[SOLID_TTC]=TTC_init_param_thermic(nele);

  return p_param_ttc;
}
