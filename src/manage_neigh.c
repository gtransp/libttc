/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: manage_neigh.c
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "GC.h"
#include "CHR.h"
#include "IO.h"

#include "TTC.h"

s_neigh_ttc **TTC_init_neigh(int nele) {
  int i;
  s_neigh_ttc **p_neigh_ttc;
  // allocation d'un pointeur vers l'adresse de neigh_tcc
  p_neigh_ttc = (s_neigh_ttc **)malloc(nele * sizeof(s_neigh_ttc *));

  for (i = 0; i < nele; i++) {
    p_neigh_ttc[i] = new_neigh_ttc();
  }

  //  pneigh_ttc->pivois = (s_ivois_ttc *) malloc(nele * sizeof(s_ivois_ttc));
  //  pneigh_ttc->pdelta_L = (s_delta_L_ttc *) malloc(nele * sizeof(s_delta_L_ttc));
  //  pneigh_ttc->nvois=TTC_create_tab_int(nele);

  return p_neigh_ttc;
}
