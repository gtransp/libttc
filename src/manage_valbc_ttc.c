/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: manage_valbc_ttc.c
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "CHR.h"
#include "GC.h"
#include "IO.h"

#include "TTC.h"

// s_val_bc_ttc *TTC_init_val_bc(int nele)
//{
////  s_val_bc_ttc *pval_bc_ttc;
////
////// allocation d'un pointeur vers l'adresse de val_bc_tcc
////  pval_bc_ttc=new_val_bc_ttc();
////
////  pval_bc_ttc->pvalcl = (s_valcl_ttc *) malloc(nele * sizeof(s_valcl_ttc));
////
////  return pval_bc_ttc;
//}

// s_val_bc_t_ttc *TTC_init_val_t_bc(int nele, int tstep)
//{
//
//     s_val_bc_t_ttc **pval_bc_t_ttc;
//     // allocation d'un pointeur vers l'adresse de val_bc_tcc
//
//     pval_bc_t_ttc=(s_valcl_ttc *) malloc(tstep * sizeof(s_valcl_ttc));
//     for (int i = 0; i < tstep; i++) {
//         pval_bc_t_ttc[i]=pval_bc_t_ttc=(s_valcl_ttc *) malloc(nele * sizeof(s_valcl_ttc));;
//     }
//     pval_bc_t_ttc->pvalclt = TTC_create_mat_double(nele,tstep);
//     return pval_bc_t_ttc;
// }
