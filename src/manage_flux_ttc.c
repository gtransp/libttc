/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: manage_flux_ttc.c
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "CHR.h"
#include "GC.h"
#include "IO.h"

#include "TTC.h"
//
// s_tot_ttc **TTC_init_totflux(int nele)
//{
//  s_tot_ttc **ptot_ttc;
//  ptot_ttc=new_tot_ttc();
//  ptot_ttc->ptot_face_ttc = TTC_create_mat_double(nele,8);
//
//  return ptot_ttc;
//}
//
// s_cond_ttc *TTC_init_condflux(int nele)
//{
//   s_cond_ttc * pcond_ttc;
//  pcond_ttc=new_cond_ttc();
//  pcond_ttc->pcond_face_ttc = (s_cond_face_ttc *) malloc(nele * sizeof(s_cond_face_ttc));
//  return pcond_ttc;
//}
//
// s_adv_ttc *TTC_init_advflux(int nele)
//{
//   s_adv_ttc * padv_ttc;
//  padv_ttc=new_adv_ttc();
//  padv_ttc->padv_face_ttc = (s_adv_face_ttc *) malloc(nele * sizeof(s_adv_face_ttc));
//  return padv_ttc;
//}
//
//
// s_flux_ttc *TTC_create_flux()
//{
//  s_flux_ttc *pflux_ttc;
//  pflux_ttc=new_flux_ttc();
//  bzero((char *)pflux_ttc,sizeof(s_flux_ttc));
//  return pflux_ttc;
//}

s_flux_ttc **TTC_init_flux(int nele, FILE *flog) {

  s_flux_ttc **p_flux_ttc;
  int i;

  p_flux_ttc = (s_flux_ttc **)malloc(nele * sizeof(s_flux_ttc *));
  if (p_flux_ttc == NULL)
    LP_error(flog, "In libttc%4.2f, File %s in %s line %d : Bad memory allocation\n", VERSION_TTC, __FILE__, __func__, __LINE__);

  for (i = 0; i < nele; i++) {
    p_flux_ttc[i] = new_flux_ttc();
    if (p_flux_ttc[i] == NULL)
      LP_error(flog, "In libttc%4.2f, File %s in %s line %d : Bad memory allocation\n", VERSION_TTC, __FILE__, __func__, __LINE__);

    bzero((char *)p_flux_ttc[i], sizeof(s_flux_ttc)); // NG : 09/06/2023. Was not initialized.
  }

  return p_flux_ttc;
}

/**
 * \fn void TTC_cal_condflux(s_carac_ttc*, s_param_calc_ttc**, s_species_ttc*, int, FILE *)
 * \brief
 * \return
 */
void TTC_cal_condflux(s_carac_ttc *pcarac_ttc, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, int i, FILE *fpout) {

  s_link_ttc *plink = pcarac_ttc->p_link[pspecies->id];
  double dist, delta_L, *val;
  int tcl;
  int icard, id_neigh, sub_icard;
  double theta, dlip, cl_val, lambda;
  double qcondu;
  double thickness;
  int type = pspecies->pset->type;
  int nsub;

  val = pspecies->pval_ttc->val;
  dist = pspecies->p_param_ttc[i]->param_syst[SIZE_TTC];

  for (icard = 0; icard < NB_CARD_TTC; icard++) {
    nsub = plink->nsub[i][icard];
    switch (nsub) {
    case 0:
      sub_icard = 0;

      if (type == HEAT_TTC) {
        lambda = p_param_calc_ttc[i]->disp_face[icard][sub_icard] * RHOW_TTC * HEAT_CAPW_TTC;
      } else {
        lambda = p_param_calc_ttc[i]->disp_face[icard][sub_icard];
      }

      tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];
      id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];
      delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard];
      thickness = pspecies->p_param_ttc[i]->thickness;

      if (id_neigh != NONE_TTC) {
        id_neigh = id_neigh - 1;
      }

      if (id_neigh != NONE_TTC && tcl == NO_BOUND_TTC) // face active
      {
        // delta_L=pspecies->plink->p_neigh_ttc[i]->delta_L[icard]; // distance entre le centre de la maille et de son voisin
        /// WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
        if (icard == NORTH_TTC) {
          delta_L = -delta_L;
        } else if (icard == SOUTH_TTC) {
          delta_L = delta_L;
        } else if (icard == WEST_TTC) {
          delta_L = delta_L;
        } else if (icard == EAST_TTC) {
          delta_L = -delta_L;
        } else if (icard == BOTTOM_TTC) {
          delta_L = thickness;
        } else if (icard == TOP_TTC) {
          delta_L = -thickness;
        }
        // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=(double)dlip/(delta_L)*(val[i]-val[id_neigh])*dist;
        qcondu = (double)lambda * (val[i] - val[id_neigh]) / delta_L; // W/m/C * C / m = W/m2
        // LP_printf(fpout,"I %d dir %d lambd %f vali %f valneigh %f deltaL %f\n",i,icard,lambda,val[i],val[id_neigh],delta_L);
        pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = qcondu;

        /*LP_printf(fpout, "Inside %s NO_BOUND at face %s, lambda = %e, val[%d] = %lf val_neigh[%d] = %lf dist = %lf delta_L = %lf flux = %e \n",__func__,
                  TTC_param_card(icard), lambda, i, val[i],
                  id_neigh,val[id_neigh], dist, delta_L,qcondu);
                  */
      } else if (tcl == DIRI_FACE_TTC) // variable impose a la face de la maille
      {

        cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
        /* LP_printf(fpout, "Inside %s DIRI_FACE   at i %d  %s tcl: %s, transient regime  cl_val = %lf delta_L %lf thick %lf dist %lf\n",__func__, i,
                             TTC_param_card(icard),
                             TTC_name_bound(tcl),
                             pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard],
                             delta_L, thickness, dist); */

        if (icard == NORTH_TTC) {
          // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=dlip*((val[i]-cl_val)/-dist*2.)*dist;
          pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = lambda * 2 * ((val[i] - cl_val) / dist);
        } else if (icard == SOUTH_TTC) {
          // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=dlip*((val[i]-cl_val)/dist*2.)*dist;
          pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = lambda * 2 * ((val[i] - cl_val) / dist);
        } else if (icard == WEST_TTC) {
          // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=dlip*((val[i]-cl_val)/-dist*2.)*dist;
          pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = lambda * 2 * ((val[i] - cl_val) / dist);
        } else if (icard == EAST_TTC) {
          // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=dlip*((val[i]-cl_val)/dist*2.)*dist;
          pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = lambda * 2 * ((val[i] - cl_val) / dist);
        } else if (icard == BOTTOM_TTC) {
          // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=dlip*((val[i]-cl_val)/-dist*2.)*dist;
          pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = lambda * 2 * ((val[i] - cl_val) / thickness);
        } else if (icard == TOP_TTC) {
          // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=dlip*((val[i]-cl_val)/dist*2.)*dist;
          pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = lambda * 2 * ((val[i] - cl_val) / thickness);
        }

        // LP_printf(fpout, "Inside %s DIRI_FACE, val[%d] = %lf cl_val = %lf delta_L = %lf  flux = %e\n",__func__, i, val[i],cl_val, delta_L,  pspecies->p_flux_ttc[i]->condface[icard][sub_icard]);
      } else // variable impose a la face de la maille
      {
        pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = 0.;
      }
      //  }
      /*      else if (type == SOLUTE_TTC) {

               dlip = p_param_calc_ttc[i]->disp_face[icard][sub_icard]; // dispersion + conduction
               tcl = pspecies->p_boundary_ttc[i]->icl[icard][0];
               id_neigh = plink->p_neigh_ttc[i]->ivois[icard][0];

               if (id_neigh != NONE_TTC && tcl == NO_BOUND_TTC) //face active
               {
                 id_neigh -=1; // NG : 26/04/2023 : Bug fix
                 delta_L = plink->p_neigh_ttc[i]->delta_L[icard][0];
                 /// WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
                 if (icard == NORTH_TTC) {
                   delta_L = -delta_L;
                 }
                 else if (icard == SOUTH_TTC) {
                   delta_L = delta_L;
                 }
                 else if (icard == WEST_TTC) {
                   delta_L = -delta_L;
                 }
                 else if (icard == EAST_TTC) {
                   delta_L = delta_L;
                 }
                 // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=(double)dlip/(delta_L)*(val[i]-val[id_neigh])*dist;
                 pspecies->p_flux_ttc[i]->condface[icard][0] = (double) lambda / (delta_L) * (val[i] - val[id_neigh]);
                 // LP_printf(fpout, "Inside %s NO_BOUND, dlip = %e, val[%d] = %lf, val_neigh[%d] = %lf, dist = %lf, delta_L = %lf, flux = %e \n",__func__,dlip, i, val[i],id_neigh,val[id_neigh], dist, delta_L,pspecies->p_flux_ttc[i]->condface[icard][sub_icard]);
               }
               else if (tcl == DIRI_FACE_TTC) // variable impose a la face de la maille
               {
                 cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];


                 if (icard == NORTH_TTC) {
                   // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=dlip*((val[i]-cl_val)/-dist*2.)*dist;
                   pspecies->p_flux_ttc[i]->condface[icard][0] = lambda * ((val[i] - cl_val) / -dist * 2.);
                 }
                 else if (icard == SOUTH_TTC) {
                   // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=dlip*((val[i]-cl_val)/dist*2.)*dist;
                   pspecies->p_flux_ttc[i]->condface[icard][0] = lambda * ((val[i] - cl_val) / dist * 2.);
                 }
                 else if (icard == WEST_TTC) {
                   // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=dlip*((val[i]-cl_val)/-dist*2.)*dist;
                   pspecies->p_flux_ttc[i]->condface[icard][0] = lambda * ((val[i] - cl_val) / -dist * 2.);
                 }
                 else if (icard == EAST_TTC) {
                   // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=dlip*((val[i]-cl_val)/dist*2.)*dist;
                   pspecies->p_flux_ttc[i]->condface[icard][0] = lambda * ((val[i] - cl_val) / dist * 2.);
                 }

                 // LP_printf(fpout, "Inside %s DIRI_FACE, dlip = %e, val[%d] = %lf, cl_val = %lf, dist = %lf , flux = %e\n",__func__,dlip, i, val[i],cl_val, dist,  pspecies->p_flux_ttc[i]->condface[icard][sub_icard]);
                 //  sub_icard = SUB_CARD_TTC; // SW 29/05/2018 pour calculer une fois
                 // There is a shortcut here made by SW, it has to be corrected to include subfaces for the nested solution.
               }
               else // variable impose a la face de la maille
               {
                 pspecies->p_flux_ttc[i]->condface[icard][0] = 0;
                 // sub_icard = SUB_CARD_TTC; // SW 29/05/2018 pour calculer une fois
               }*/

      break;

    default:
      for (sub_icard = 0; sub_icard < nsub; sub_icard++) {

        if (type == HEAT_TTC) {
          lambda = p_param_calc_ttc[i]->disp_face[icard][sub_icard] * RHOW_TTC * HEAT_CAPW_TTC;
        } else {
          lambda = p_param_calc_ttc[i]->disp_face[icard][sub_icard];
        }

        tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];
        delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard];
        id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];

        if (id_neigh != NONE_TTC) {
          id_neigh = id_neigh - 1;
        }

        if (id_neigh != NONE_TTC && tcl == NO_BOUND_TTC) // face active
        {
          /// WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
          if (icard == NORTH_TTC) {
            delta_L = -plink->p_neigh_ttc[i]->delta_L[icard][sub_icard];
          } else if (icard == SOUTH_TTC) {
            delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard];
          } else if (icard == WEST_TTC) {
            delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard];
          } else if (icard == EAST_TTC) {
            delta_L = -plink->p_neigh_ttc[i]->delta_L[icard][sub_icard];
          } else if (icard == BOTTOM_TTC) {
            delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard];
          } else if (icard == TOP_TTC) {
            delta_L = -plink->p_neigh_ttc[i]->delta_L[icard][sub_icard];
          }
          // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=(double)dlip/(delta_L)*(val[i]-val[id_neigh])*dist;
          qcondu = (double)lambda * (val[i] - val[id_neigh]) / delta_L;
          //   LP_printf(fpout,"I %d dir %d lambd %f vali %f valneigh %f deltaL %f\n",i,icard,lambda,val[i],val[id_neigh],delta_L);

          pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = qcondu;
          /* LP_printf(fpout, "Inside %s NO_BOUND, lambda = %e, val[%d] = %lf, val_neigh[%d] = %lf, dist = %lf, delta_L = %lf, flux = %e \n",__func__,lambda, i, val[i],
                             id_neigh,val[id_neigh], dist, delta_L,qcondu); */
        } else if (tcl == DIRI_FACE_TTC) {
          cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

          if (icard == NORTH_TTC) {
            // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=dlip*((val[i]-cl_val)/-dist*2.)*dist;
            pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = lambda * 2 * ((val[i] - cl_val) / delta_L);
          } else if (icard == SOUTH_TTC) {
            // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=dlip*((val[i]-cl_val)/dist*2.)*dist;
            pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = lambda * 2 * ((val[i] - cl_val) / delta_L);
          } else if (icard == WEST_TTC) {
            // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=dlip*((val[i]-cl_val)/-dist*2.)*dist;
            pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = lambda * 2 * ((val[i] - cl_val) / delta_L);
          } else if (icard == EAST_TTC) {
            // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=dlip*((val[i]-cl_val)/dist*2.)*dist;
            pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = lambda * 2 * ((val[i] - cl_val) / delta_L);
          } else if (icard == BOTTOM_TTC) {
            // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=dlip*((val[i]-cl_val)/-dist*2.)*dist;
            pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = lambda * 2 * ((val[i] - cl_val) / delta_L);
          } else if (icard == TOP_TTC) {
            // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=dlip*((val[i]-cl_val)/dist*2.)*dist;
            pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = lambda * 2 * ((val[i] - cl_val) / delta_L);
          }

          /* LP_printf(fpout, "Inside %s DIRI_FACE, dlip = %e, val[%d] = %lf, cl_val = %lf, dist = %lf , flux = %e\n",__func__,dlip, i, val[i],cl_val, dist,  pspecies->p_flux_ttc[i]->condface[icard][sub_icard]);
                               sub_icard = SUB_CARD_TTC; */  //SW 29/05/2018 pour calculer une fois
          // There is a shortcut here made by SW, it has to be corrected to include subfaces for the nested solution.
        } else // variable impose a la face de la maille
        {
          pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = 0.;
          // sub_icard = SUB_CARD_TTC; // SW 29/05/2018 pour calculer une fois
        }
        // }
        /*       else if (type == SOLUTE_TTC) {

                 dlip = p_param_calc_ttc[i]->disp_face[icard][sub_icard]; // dispersion + conduction
                 tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];
                 id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];
                 if (id_neigh != NONE_TTC && tcl == NO_BOUND_TTC)
                 {
                   id_neigh-=1; // NG : 26/04/2023 : Bug fix
                   delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard];
                   /// WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
                   if (icard == NORTH_TTC) {
                     delta_L = -delta_L;
                   }
                   else if (icard == SOUTH_TTC) {
                     delta_L = delta_L;
                   }
                   else if (icard == WEST_TTC) {
                     delta_L = -delta_L;
                   }
                   else if (icard == EAST_TTC) {
                     delta_L = delta_L;
                   }
                   // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=(double)dlip/(delta_L)*(val[i]-val[id_neigh])*dist;
                   pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = (double) lambda / (delta_L) * (val[i] - val[id_neigh]);
                   // LP_printf(fpout, "Inside %s NO_BOUND, dlip = %e, val[%d] = %lf, val_neigh[%d] = %lf, dist = %lf, delta_L = %lf, flux = %e \n",__func__,dlip, i, val[i],id_neigh,val[id_neigh], dist, delta_L,pspecies->p_flux_ttc[i]->condface[icard][sub_icard]);
                 }
                 else if (tcl == DIRI_FACE_TTC)
                 {
                   cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

                   if (icard == NORTH_TTC) {
                     // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=dlip*((val[i]-cl_val)/-dist*2.)*dist;
                     pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = lambda * ((val[i] - cl_val) / -dist * 2.);
                   }
                   else if (icard == SOUTH_TTC) {
                     // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=dlip*((val[i]-cl_val)/dist*2.)*dist;
                     pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = lambda * ((val[i] - cl_val) / dist * 2.);
                   }
                   else if (icard == WEST_TTC) {
                     // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=dlip*((val[i]-cl_val)/-dist*2.)*dist;
                     pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = lambda * ((val[i] - cl_val) / -dist * 2.);
                   }
                   else if (icard == EAST_TTC) {
                     // pspecies->p_flux_ttc[i]->condface[icard][sub_icard]=dlip*((val[i]-cl_val)/dist*2.)*dist;
                     pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = lambda * ((val[i] - cl_val) / dist * 2.);
                   }

                   /* LP_printf(fpout, "Inside %s DIRI_FACE, dlip = %e, val[%d] = %lf, cl_val = %lf, dist = %lf , flux = %e\n",__func__,dlip, i, val[i],cl_val, dist,  pspecies->p_flux_ttc[i]->condface[icard][sub_icard]);
                                        sub_icard = SUB_CARD_TTC; */ // SW 29/05/2018 pour calculer une fois
        // There is a shortcut here made by SW, it has to be corrected to include subfaces for the nested solution.
        //  }
        // else // variable impose a la face de la maille
        // {
        //  pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = 0;
        // sub_icard = SUB_CARD_TTC; // SW 29/05/2018 pour calculer une fois
        // }
        // }
      }
      break;
      // }
    } // switch
  }   // card
} // fonction

/**
 * \fn void TTC_cal_advflux(s_carac_ttc*,s_param_calc_ttc**, s_species_ttc*, int, FILE*)
 * \brief
 * \return
 */
void TTC_cal_advflux(s_carac_ttc *pcarac_ttc, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, int i, FILE *fpout) {

  s_link_ttc *plink = pcarac_ttc->p_link[pspecies->id];
  double dist = pspecies->p_param_ttc[i]->param_syst[SIZE_TTC];
  double *val = pspecies->pval_ttc->val;
  double delta_L, u, qadv;
  double thickness = pspecies->p_param_ttc[i]->thickness;
  double theta, dlip, cl_val, rhow, cw, rhowcw;
  double area_vertical = dist * dist;
  double area_horizontal = dist * thickness;
  int tcl, nsub;
  int type = pspecies->pset->type;
  int icard, id_neigh, sub_icard;
  // int ttstep = *tstep;
  double t_kelvin;

  if (type == HEAT_TTC) {
    rhow = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][RHO_TTC];
    cw = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][HEAT_CAP_TTC];
    rhowcw = rhow * cw;
    t_kelvin = T_DATUM;
  } else if (type == SOLUTE_TTC) {
    rhowcw = 1;
    t_kelvin = 0.;
  }

  pspecies->p_flux_ttc[i]->adv_dVdt = pcarac_ttc->dVdt[i] * (val[i] - t_kelvin) * rhowcw / area_vertical;      // Advection due to rate of transient groundwater storage change
  pspecies->p_flux_ttc[i]->adv_Source = pcarac_ttc->qSource[i] * (val[i] - t_kelvin) * rhowcw / area_vertical; // Advection due to rate of flow rate of the fluid source
  pspecies->p_flux_ttc[i]->adv_Sink = pcarac_ttc->qSink[i] * (val[i] - t_kelvin) * rhowcw / area_vertical;     // Advection due to rate of flow rate of the fluid sink

  /* LP_printf(fpout, "Inside func %s rhowcw: %e val %e adv_dVdt %e adv_Source %e adv_Sink %e \n",
              __func__ , rhowcw, val[i], pcarac_ttc->dVdt[i], pcarac_ttc->qSource[i], pcarac_ttc->qSink[i]); */

  for (icard = 0; icard < NB_CARD_TTC; icard++) {

    nsub = plink->nsub[i][icard];
    pspecies->p_flux_ttc[i]->advface[icard][0] = 0.;

    switch (nsub) {
    case 0:
      sub_icard = 0;

      //  if (type == HEAT_TTC) {
      u = pcarac_ttc->uface[i][icard][sub_icard]; // DK AR 24 08 2021 change location of uface plink to pcarac
      // LP_printf(fpout, "Inside %s, ele [%d] face %s u = %e , \n",__func__, i, TTC_param_card(icard), u);
      dlip = p_param_calc_ttc[i]->disp_face[icard][sub_icard]; // dispersion + conduction
      tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];
      cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

      if (icard == EAST_TTC) {
        if (u >= 0.0) {
          pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (val[i] - t_kelvin) / area_horizontal; // Diagonal term
          // LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_i = %lf, Advflux: %e \n",__func__ , i, TTC_param_card(icard), rhowcw, u, val[i], pspecies->p_flux_ttc[i]->advface[icard][0]);
        } else if (tcl == NO_BOUND_TTC && u < 0) // face active
        {
          id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];

          if (id_neigh != NONE_TTC) {
            id_neigh = id_neigh - 1;

            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (val[id_neigh] - t_kelvin) / area_horizontal;
            // LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_i = %lf, Advflux: %e \n",__func__ , i, TTC_param_card(icard), rhowcw, u, val[id_neigh], pspecies->p_flux_ttc[i]->advface[icard][0]);
          }
        } else if (tcl == DIRI_FACE_TTC && u < 0) // face active
        {
          pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (cl_val - t_kelvin) / area_horizontal;
          // LP_printf(fpout, "In func: %s, ele: %d, face %s, id_neigh: %d, rhocw: %e , u: %e valneigh = %lf ,Advflux: %e \n",__func__ , i, TTC_param_card(icard), id_neigh,rhowcw, u, val[id_neigh], pspecies->p_flux_ttc[i]->advface[icard][0]);
        } else // face flux impose
        {
          pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
          // LP_printf(fpout, "In func DIRI: %s, ele: %d, face %s, FLUX_IMPOSED\n",__func__ , i, TTC_param_card(icard));
        }
      }
      if (icard == WEST_TTC) {
        if (u <= 0.0) {
          pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (val[i] - t_kelvin) / area_horizontal; // Diagonal term
          // LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_i = %lf, Advflux: %e \n",__func__ , i, TTC_param_card(icard), rhowcw, u, val[i], pspecies->p_flux_ttc[i]->advface[icard][0]);
        } else if (tcl == NO_BOUND_TTC && u > 0.0) // face active
        {
          id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];
          delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // distance entre le centre de la maille et de son voisin

          if (id_neigh != NONE_TTC) {
            id_neigh = id_neigh - 1;
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (val[id_neigh] - t_kelvin) / area_horizontal;
            // LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_i = %lf, Advflux: %e \n",__func__ , i, TTC_param_card(icard), rhowcw, u, val[i], pspecies->p_flux_ttc[i]->advface[icard][0]);
          }
        } else if (tcl == DIRI_FACE_TTC && u > 0.0) // face active
        {
          pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (cl_val - t_kelvin) / area_horizontal;
          // LP_printf(fpout, "In func DIRI: %s, ele: %d, face %s, rhocw: %e , u: %e clval = %lf, Advflux: %e \n",__func__ , i, TTC_param_card(icard), rhowcw, u, cl_val, pspecies->p_flux_ttc[i]->advface[icard][0]);
        } else // face flux impose
        {
          pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
          // LP_printf(fpout, "In func DIRI: %s, ele: %d, face %s, FLUX_IMPOSED\n",__func__ , i, TTC_param_card(icard));
        }
      }
      if (icard == NORTH_TTC) {
        if (u >= 0) {
          pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (val[i] - t_kelvin) / area_horizontal;
        } else if (tcl == NO_BOUND_TTC && u < 0) // face active
        {
          id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];

          if (id_neigh != NONE_TTC) {
            id_neigh = id_neigh - 1;

            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (val[id_neigh] - t_kelvin) / area_horizontal;
          }
        } else if (tcl == DIRI_FACE_TTC && u < 0) // face active
        {
          pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (cl_val - t_kelvin) / area_horizontal;
        } else // face flux impose
        {
          pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
        }
      }
      if (icard == SOUTH_TTC) {
        if (u <= 0) {
          pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (val[i] - t_kelvin) / area_horizontal;
        } else if (tcl == NO_BOUND_TTC && u > 0) // face active
        {

          id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];

          if (id_neigh != NONE_TTC) {
            id_neigh = id_neigh - 1;

            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (val[id_neigh] - t_kelvin) / area_horizontal;
          }

        } else if (tcl == DIRI_FACE_TTC && u > 0) // face active
        {
          pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (cl_val - t_kelvin) / area_horizontal;
        } else // face flux impose
        {
          pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
        }
      }                     // fin south
      if (icard == TOP_TTC) // DK AR 9 2 2022
      {
        if (u >= 0) {
          pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (val[i] - t_kelvin) / area_vertical; // DK AR 9 2 2022

          /* LP_printf(fpout, "In func: %s, ele: %d, face %s, id_neigh: %d Advflux: %e \n",__func__ , i,
                              TTC_param_card(icard), id_neigh, pspecies->p_flux_ttc[i]->advface[icard][0]);
                              LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_cell = %lf ,Advflux: %e \n",__func__ , i,
                              TTC_param_card(icard), rhowcw, u, val[i], pspecies->p_flux_ttc[i]->advface[icard][0]); */
        } else if (tcl == NO_BOUND_TTC && u < 0) // face active
        {

          id_neigh = plink->p_neigh_ttc[i]->ivois[icard][0];

          if (id_neigh != NONE_TTC) {
            id_neigh = id_neigh - 1;

            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (val[id_neigh] - t_kelvin) / area_vertical;
          }
          // LP_printf(fpout, "In func: %s, ele: %d, face %s, id_neigh: %d, rhocw: %e , u: %e valneigh = %lf ,Advflux: %e \n",__func__ , i, TTC_param_card(icard), id_neigh,rhowcw, u, val[id_neigh], pspecies->p_flux_ttc[i]->advface[icard][0]);
        } else if (tcl == DIRI_FACE_TTC && u < 0) // face active
        {
          pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (cl_val - t_kelvin) / area_vertical;
          // LP_printf(fpout, "In func: %s, ele: %d, face %s, DIRI cl_val= %lf u: %e Advflux: %e \n",__func__ , i, TTC_param_card(icard), cl_val,  u, pspecies->p_flux_ttc[i]->advface[icard][0]);
        } else // face flux impose
        {
          pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
        }
      }
      if (icard == BOTTOM_TTC) {
        if (u <= 0) {

          pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (val[i] - t_kelvin) / area_vertical;
          // LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_cell = %lf,Advflux: %e \n",__func__ , i, TTC_param_card(icard),rhowcw, u, val[i], pspecies->p_flux_ttc[i]->advface[icard][0]);
        } else if (tcl == NO_BOUND_TTC && u > 0) // face active
        {

          id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];

          if (id_neigh != NONE_TTC) {
            id_neigh = id_neigh - 1;
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (val[id_neigh] - t_kelvin) / area_vertical;
          }
          // LP_printf(fpout, "In func: %s, ele: %d, face %s, id_neigh: %d, rhocw: %e , u: %e valneigh = %lf ,Advflux: %e \n",__func__ , i, TTC_param_card(icard), id_neigh,rhowcw, u, val[id_neigh], pspecies->p_flux_ttc[i]->advface[icard][0]);
        } else if (tcl == DIRI_FACE_TTC && u > 0) // face active
        {
          pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (cl_val - t_kelvin) / area_vertical;
          // LP_printf(fpout, "In func: %s, ele: %d, face %s, DIRI cl_val= %lf u: %e cl_val: %lf Advflux: %e \n",__func__ , i, TTC_param_card(icard), cl_val,  u, pspecies->p_flux_ttc[i]->advface[icard][0]);
        } else // face flux impose
        {
          pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
        }
      } // fin BOTTOM_TTC
        // }

      /* else if (type == SOLUTE_TTC) {
         //u=*pspecies->plink->pu_ttc[i].uface[icard];
         u = pcarac_ttc->uface[i][icard][0]; // DK AR 24 08 2021 change location of uface plink to pcarac
         dlip = p_param_calc_ttc[i]->disp_face[icard][sub_icard]; // dispersion + conduction
         tcl = pspecies->p_boundary_ttc[i]->icl[icard][0];
         cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][0];

         if (icard == EAST_TTC) {
           if (u > 0) {
             // WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
             qadv = (rhowcw * u * val[i]) * dist;
           }
           else if (tcl == NO_BOUND_TTC && u < 0) // face active
           {
             id_neigh = plink->p_neigh_ttc[i]->ivois[icard][0];
             delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard];
             // WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
             if (id_neigh != NONE_TTC) {
               id_neigh-=1; // NG : 26/04/2023 : Bug fix
               pspecies->p_flux_ttc[i]->advface[icard][0] = (rhowcw * u * val[id_neigh]) * dist;
             }
           }
           else if (tcl == DIRI_FACE_TTC && u < 0) // face active
           {
             pspecies->p_flux_ttc[i]->advface[icard][0] = (rhowcw * u * cl_val) * dist * 2.;
           }
           else // face flux impose
           {
             pspecies->p_flux_ttc[i]->advface[icard][0] = 0;
           }
         }
         if (icard == WEST_TTC) {
           if (u < 0) {
             // WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
             qadv = (rhowcw * u * val[i]) * dist;
           }
           else if (tcl == NO_BOUND_TTC && u > 0) // face active
           {
             id_neigh = plink->p_neigh_ttc[i]->ivois[icard][0];
             delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard]; //SW 29/05/2018 pour confluence
             // WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
             if (id_neigh != NONE_TTC) {
               id_neigh-=1; // NG : 26/04/2023 : Bug fix
               pspecies->p_flux_ttc[i]->advface[icard][0] = (rhowcw * u * val[id_neigh]);
             }
           }
           else if (tcl == DIRI_FACE_TTC && u > 0) // face active
           {
             pspecies->p_flux_ttc[i]->advface[icard][0] = (rhowcw * u * cl_val);
           }
           else //face flux impose
           {
             pspecies->p_flux_ttc[i]->advface[icard][0] = 0;
           }
         }
         if (icard == NORTH_TTC) {
           if (u > 0) {
             // WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
             pspecies->p_flux_ttc[i]->advface[icard][0] = (rhowcw * u * val[i]);
           }
           else if (tcl == NO_BOUND_TTC && u < 0) //face active
           {
             id_neigh = plink->p_neigh_ttc[i]->ivois[icard][0];
             delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard];
             // WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
             if (id_neigh != NONE_TTC) {
               id_neigh-=1; // NG : 26/04/2023 : Bug fix
               pspecies->p_flux_ttc[i]->advface[icard][0] = (rhowcw * u * val[id_neigh]);
             }
           }
           else if (tcl == DIRI_FACE_TTC && u < 0) // face active
           {
             pspecies->p_flux_ttc[i]->advface[icard][0] = (rhowcw * u * cl_val);
             sub_icard = SUB_CARD_TTC; // SW 29/05/2018 no confluence pour DIRI_FACE_TTC   // SW shortcut ?
           }
           else // face flux impose
           {
             pspecies->p_flux_ttc[i]->advface[icard][0] = 0;
           }
         }

         if (icard == SOUTH_TTC) {
           if (u < 0) {
             // WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
             pspecies->p_flux_ttc[i]->advface[icard][0] = (rhowcw * u * val[i]);
           }
           else if (tcl == NO_BOUND_TTC && u > 0) // face active
           {
             id_neigh = plink->p_neigh_ttc[i]->ivois[icard][0];
             delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard];
             // WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
             if (id_neigh != NONE_TTC) {
               id_neigh-=1; // NG : 26/04/2023 : Bug fix
               pspecies->p_flux_ttc[i]->advface[icard][0] = (rhowcw * u * val[id_neigh]);
             }
           }
           else if (tcl == DIRI_FACE_TTC && u > 0) // face active
           {
             pspecies->p_flux_ttc[i]->advface[icard][0] = (rhowcw * u * cl_val);
           }
           else //face flux impose
           {
             pspecies->p_flux_ttc[i]->advface[icard][0] = 0;
           }
         }
       }*/
      break;

    default:
      for (sub_icard = 0; sub_icard < nsub; sub_icard++) {

        //  if (type == HEAT_TTC) {

        u = pcarac_ttc->uface[i][icard][sub_icard]; // DK AR 24 08 2021 change location of uface plink to pcarac
        // LP_printf(fpout, "Inside %s, ele [%d], u = %e,  nsub: %d\n",__func__, i, u, nsub);

        dlip = p_param_calc_ttc[i]->disp_face[icard][sub_icard]; // dispersion + conduction
        tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];
        cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

        if (icard == EAST_TTC) {
          if (u >= 0) {
            pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (val[i] - t_kelvin) / area_horizontal;
            // LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_i = %lf, Advflux: %e \n",__func__ , i, TTC_param_card(icard), rhowcw, u, val[i], pspecies->p_flux_ttc[i]->advface[icard][0]);
          } else if (tcl == NO_BOUND_TTC && u < 0) // face active
          {
            id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];

            if (id_neigh != NONE_TTC) {
              id_neigh = id_neigh - 1;

              pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (val[id_neigh] - t_kelvin) / area_horizontal;
              // LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_idneigh = %lf, Advflux: %e \n",__func__ , i, TTC_param_card(icard), rhowcw, u, val[id_neigh], pspecies->p_flux_ttc[i]->advface[icard][0]);
            }
          } else if (tcl == DIRI_FACE_TTC && u < 0) // face active
          {
            pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (cl_val - t_kelvin) / area_horizontal;
            // LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e clval = %lf, Advflux: %e \n",__func__ , i, TTC_param_card(icard), rhowcw, u, cl_val, pspecies->p_flux_ttc[i]->advface[icard][0]);
          } else // face flux impose
          {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
          }
        }
        if (icard == WEST_TTC) {
          if (u <= 0) {
            pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (val[i] - t_kelvin) / area_horizontal;
            // LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_i = %lf, Advflux: %e \n",__func__ , i, TTC_param_card(icard), rhowcw, u, val[i], pspecies->p_flux_ttc[i]->advface[icard][0]);
          } else if (tcl == NO_BOUND_TTC && u > 0) // face active
          {
            id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard]; // Id voisin

            if (id_neigh != NONE_TTC) {
              id_neigh = id_neigh - 1;

              pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (val[id_neigh] - t_kelvin) / area_horizontal;
              // LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_neigh = %lf, Advflux: %e \n",__func__ , i, TTC_param_card(icard), rhowcw, u, val[id_neigh], pspecies->p_flux_ttc[i]->advface[icard][0]);
            }
          } else if (tcl == DIRI_FACE_TTC && u > 0) // face active
          {
            pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (cl_val - t_kelvin) / area_horizontal;
            // LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e clval = %lf, Advflux: %e \n",__func__ , i, TTC_param_card(icard), rhowcw, u, cl_val, pspecies->p_flux_ttc[i]->advface[icard][0]);
          } else // face flux impose
          {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
          }
        }

        if (icard == NORTH_TTC) {
          if (u >= 0) {
            pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (val[i] - t_kelvin) / area_horizontal;
            // LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_neigh = %lf Advflux: %e \n",__func__ , i, TTC_param_card(icard), rhowcw, u, val[i], pspecies->p_flux_ttc[i]->advface[icard][0]);
          } else if (tcl == NO_BOUND_TTC && u < 0) // face active
          {

            id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];

            if (id_neigh != NONE_TTC) {
              id_neigh = id_neigh - 1;
              pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (val[id_neigh] - t_kelvin) / area_horizontal;
              // LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_neigh = %lf Advflux: %e \n",__func__ , i, TTC_param_card(icard), rhowcw, u, val[id_neigh], pspecies->p_flux_ttc[i]->advface[icard][0]);
            }
          } else if (tcl == DIRI_FACE_TTC && u < 0) // face active
          {
            pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (cl_val - t_kelvin) / area_horizontal;
          } else // face flux impose
          {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
          }
        }

        if (icard == SOUTH_TTC) {
          if (u <= 0) {
            pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (val[i] - t_kelvin) / area_horizontal;
            // LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_neigh = %lf Advflux: %e \n",__func__ , i, TTC_param_card(icard), rhowcw, u, val[i], pspecies->p_flux_ttc[i]->advface[icard][0]);
          } else if (tcl == NO_BOUND_TTC && u > 0) // face active
          {

            id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];

            if (id_neigh != NONE_TTC) {
              id_neigh = id_neigh - 1;

              pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (val[id_neigh] - t_kelvin) / area_horizontal;
              // LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_neigh = %lf Advflux: %e \n",__func__ , i, TTC_param_card(icard), rhowcw, u, val[id_neigh], pspecies->p_flux_ttc[i]->advface[icard][0]);
            }
          } else if (tcl == DIRI_FACE_TTC && u > 0) // face active
          {
            pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (cl_val - t_kelvin) / area_horizontal;
          } else // face flux impose
          {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
          }
        }

        if (icard == TOP_TTC) {
          if (u >= 0) {
            pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (val[i] - t_kelvin) / area_vertical;
          } else if (tcl == NO_BOUND_TTC && u < 0) // face active
          {
            id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];

            if (id_neigh != NONE_TTC) {
              id_neigh = id_neigh - 1;

              pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (val[id_neigh] - t_kelvin) / area_vertical;
              // LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_neigh = %lf Advflux: %e \n",__func__ , i, TTC_param_card(icard), rhowcw, u, val[id_neigh], pspecies->p_flux_ttc[i]->advface[icard][0]);
            }
          } else if (tcl == DIRI_FACE_TTC && u < 0) // face active
          {
            pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (cl_val - t_kelvin) / area_vertical / nsub;
          } else // face flux impose
          {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
          }
        }

        if (icard == BOTTOM_TTC) {
          if (u <= 0) {
            pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (val[i] - t_kelvin) / area_vertical;
          } else if (tcl == NO_BOUND_TTC && u > 0) // face active
          {

            id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];

            if (id_neigh != NONE_TTC) {
              id_neigh = id_neigh - 1;

              pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (val[id_neigh] - t_kelvin) / area_vertical;
            }

          } else if (tcl == DIRI_FACE_TTC && u > 0) // face active
          {
            pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (cl_val - t_kelvin) / area_vertical;
          } else // face flux impose
          {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
          }
        }

        //  }
        /*   else if (type == SOLUTE_TTC) {

             u = pcarac_ttc->uface[i][icard][sub_icard]; // DK AR 24 08 2021 change location of uface plink to pcarac
             dlip = p_param_calc_ttc[i]->disp_face[icard][sub_icard]; // dispersion + conduction
             tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];
             cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

             if (icard == EAST_TTC) {
               if (u > 0) {
                 // WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
                 qadv = (rhowcw * u * val[i]) * dist;
               }
               else if (tcl == NO_BOUND_TTC && u < 0) // face active
               {
                 id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];
                 delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard];

                 // WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
                 if (id_neigh != NONE_TTC) {
                   id_neigh-=1; // NG : 26/04/2023 : Bug fix
                   pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = (rhowcw * u * val[id_neigh]) * dist;
                 }
               }
               else if (tcl == DIRI_FACE_TTC && u < 0) // face active
               {
                 pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = (rhowcw * u * cl_val) * dist * 2.;
               }
               else //face flux impose
               {
                 pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
               }
             }
             if (icard == WEST_TTC) {
               if (u < 0) {
                 // WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
                 qadv = (rhowcw * u * val[i]) * dist;
               }
               else if (tcl == NO_BOUND_TTC && u > 0) //face active
               {
                 id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];
                 delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard];

                 // WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
                 if (id_neigh != NONE_TTC) {
                   id_neigh-=1; // NG : 26/04/2023 : Bug fix
                   pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = (rhowcw * u * val[id_neigh]) * dist;
                 }
               }
               else if (tcl == DIRI_FACE_TTC && u > 0) //face active
               {
                 pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = (rhowcw * u * cl_val) * dist * 2.;
               }
               else //face flux impose
               {
                 pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
               }
             }
             if (icard == NORTH_TTC) {
               if (u > 0) {
                 // WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
                 pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = (rhowcw * u * val[i]) * dist;
               }
               else if (tcl == NO_BOUND_TTC && u < 0) //face active
               {
                 id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];
                 delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard];

                 // WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
                 if (id_neigh != NONE_TTC) {
                   id_neigh-=1; // NG : 26/04/2023 : Bug fix
                   pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = (rhowcw * u * val[id_neigh]) * dist;
                 }
               }
               else if (tcl == DIRI_FACE_TTC && u < 0) //face active
               {
                 pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = (rhowcw * u * cl_val) * dist * 2.;
                 sub_icard = SUB_CARD_TTC; // SW 29/05/2018 no confluence pour DIRI_FACE_TTC
               }
               else //face flux impose
               {
                 pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
               }
             }

             if (icard == SOUTH_TTC) {
               if (u < 0) {
                 // WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
                 pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = (rhowcw * u * val[i]) * dist;
               }
               else if (tcl == NO_BOUND_TTC && u > 0) //face active
               {
                 id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];
                 delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard];
                 // WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
                 if (id_neigh != NONE_TTC) {
                   id_neigh-=1; // NG : 26/04/2023 : Bug fix
                   pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = (rhowcw * u * val[id_neigh]) * dist;
                 }
               }
               else if (tcl == DIRI_FACE_TTC && u > 0) //face active
               {
                 pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = (rhowcw * u * cl_val) * dist;
                 sub_icard = SUB_CARD_TTC; // SW 29/05/2018 no confluence pour DIRI_FACE_TTC       // SW shorcut ?
               }
               else //face flux impose
               {
                 pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
                 sub_icard = SUB_CARD_TTC; // SW 29/05/2018 no confluence pour DIRI_FACE_TTC       // SW shorcut ?
               }
             }
           }*/
      }
      break;
    }
  }
}

void TTC_cal_advflux_riv(s_carac_ttc *pcarac_ttc, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, int i, FILE *fpout) {
  s_link_ttc *plink = pcarac_ttc->p_link[pspecies->id];
  double delta_L, u;
  int tcl;
  int icard, id_neigh, sub_icard;
  double theta, dlip, cl_val, rhow, cw, rhowcw;
  double qadv;

  int type = pspecies->pset->type;
  double *val = pspecies->pval_ttc->val;                        // DK AR 24 08 2021 change val to val
  double dist = pspecies->p_param_ttc[i]->param_syst[SIZE_TTC]; // taille de la maille
  double width = pspecies->p_param_ttc[i]->width;
  int nsub;
  double wet_surface = 1.;
  if (pspecies->p_param_ttc[i]->surf != 0) {
    wet_surface = pspecies->p_param_ttc[i]->surf;
  }
  if (type == HEAT_TTC) {
    rhow = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][RHO_TTC];
    cw = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][HEAT_CAP_TTC];

    rhowcw = rhow * cw;

  } else if (type == SOLUTE_TTC) {
    rhowcw = 1;
  }
  //    LP_printf(fpout, "Inside func %s rhowcw: %e\n", __func__ , rhowcw);
  for (icard = 0; icard < NB_CARD_TTC; icard++) {
    nsub = plink->nsub[i][icard];

    switch (nsub) {
    case 0:
      sub_icard = 0;

      if (type == HEAT_TTC) {
        pcarac_ttc->uface[i][icard][sub_icard] = 0;
        if (icard == DOWNSTREAM_TTC || icard == UPSTREAM_TTC) {
          u = pcarac_ttc->uface[i][icard][sub_icard] / wet_surface;
        } else if (icard == RIVAQ_TTC) {
          u = pcarac_ttc->uface[i][icard][sub_icard] / dist / width;
        }

        //                    LP_printf(fpout, "Inside %s, ele [%d] face %s u = %e , wet surface: %lf\n",__func__, i, TTC_param_card(icard), u, wet_surface);

        dlip = p_param_calc_ttc[i]->disp_face[icard][sub_icard]; // dispersion + conduction
        tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];
        cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

        if (icard == EAST_TTC) {
          if (u >= 0.0) {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (val[i] - T_DATUM);
            //                            LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_i = %lf, Advflux: %e \n",__func__ , i,
            //                                      TTC_param_card(icard), rhowcw, u, val[i], pspecies->p_flux_ttc[i]->advface[icard][0]);
          } else if (tcl == NO_BOUND_TTC && u < 0) // face active
          {
            id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];

            if (id_neigh != NONE_TTC) {
              id_neigh = id_neigh - 1;

              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (val[id_neigh] - T_DATUM);
              //                                LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_i = %lf, Advflux: %e \n",__func__ , i,
              //                                          TTC_param_card(icard), rhowcw, u, val[id_neigh], pspecies->p_flux_ttc[i]->advface[icard][0]);
            }

          } else if (tcl == DIRI_FACE_TTC && u < 0) // face active
          {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (cl_val - T_DATUM);

            //                            LP_printf(fpout, "In func: %s, ele: %d, face %s, id_neigh: %d, rhocw: %e , u: %e valneigh = %lf ,Advflux: %e \n",__func__ , i,
            //                                      TTC_param_card(icard), id_neigh,rhowcw, u, val[id_neigh], pspecies->p_flux_ttc[i]->advface[icard][0]);

          } else // face flux impose
          {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
            //                            LP_printf(fpout, "In func DIRI: %s, ele: %d, face %s, FLUX_IMPOSED\n",__func__ , i,
            //                                      TTC_param_card(icard));
          }
        }
        if (icard == WEST_TTC) {
          if (u < 0.0) {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (val[i] - T_DATUM);
            //                            LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_i = %lf, Advflux: %e \n",__func__ , i,
            //                                      TTC_param_card(icard), rhowcw, u, val[i], pspecies->p_flux_ttc[i]->advface[icard][0]);
          } else if (tcl == NO_BOUND_TTC && u > 0.0) // face active
          {
            id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];
            delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // distance entre le centre de la maille et de son voisin

            if (id_neigh != NONE_TTC) {
              id_neigh = id_neigh - 1;
              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (val[id_neigh] - T_DATUM);
              //                                LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_i = %lf, Advflux: %e \n",__func__ , i,
              //                                          TTC_param_card(icard), rhowcw, u, val[i], pspecies->p_flux_ttc[i]->advface[icard][0]);
            }
          } else if (tcl == DIRI_FACE_TTC && u > 0.0) // face active
          {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * (cl_val - T_DATUM);
            //                            LP_printf(fpout, "In func DIRI: %s, ele: %d, face %s, rhocw: %e , u: %e clval = %lf, Advflux: %e \n",__func__ , i,
            //                                      TTC_param_card(icard), rhowcw, u, cl_val, pspecies->p_flux_ttc[i]->advface[icard][0]);
          } else // face flux impose
          {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
            //                            LP_printf(fpout, "In func DIRI: %s, ele: %d, face %s, FLUX_IMPOSED\n",__func__ , i,
            //                                      TTC_param_card(icard));
          }
        }
        if (icard == NORTH_TTC) {
          if (u > 0) {

            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * val[i];
          } else if (tcl == NO_BOUND_TTC && u < 0) // face active
          {

            id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];

            if (id_neigh != NONE_TTC) {
              id_neigh = id_neigh - 1;

              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * val[id_neigh];
            }
          } else if (tcl == DIRI_FACE_TTC && u < 0) // face active
          {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * cl_val;
          } else // face flux impose
          {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
          }
        }

        if (icard == SOUTH_TTC) {
          if (u < 0) {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * val[i];
          } else if (tcl == NO_BOUND_TTC && u > 0) // face active
          {

            id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];

            if (id_neigh != NONE_TTC) {
              id_neigh = id_neigh - 1;

              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * val[id_neigh];
            }

          } else if (tcl == DIRI_FACE_TTC && u > 0) // face active
          {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * cl_val;
          } else // face flux impose
          {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
          }
        }                     // fin south
        if (icard == TOP_TTC) // DK AR 9 2 2022
        {
          if (u > 0) {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * val[i]; // DK AR 9 2 2022

            //                            LP_printf(fpout, "In func: %s, ele: %d, face %s, id_neigh: %d Advflux: %e \n",__func__ , i,
            //                              TTC_param_card(icard), id_neigh, pspecies->p_flux_ttc[i]->advface[icard][0]);
            //                            LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_cell = %lf ,Advflux: %e \n",__func__ , i,
            //                                      TTC_param_card(icard), rhowcw, u, val[i], pspecies->p_flux_ttc[i]->advface[icard][0]);
          } else if (tcl == NO_BOUND_TTC && u < 0) // face active
          {

            id_neigh = plink->p_neigh_ttc[i]->ivois[icard][0];

            if (id_neigh != NONE_TTC) {
              id_neigh = id_neigh - 1;

              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * val[id_neigh];
            }

            //                            LP_printf(fpout, "In func: %s, ele: %d, face %s, id_neigh: %d, rhocw: %e , u: %e valneigh = %lf ,Advflux: %e \n",__func__ , i,
            //                                      TTC_param_card(icard), id_neigh,rhowcw, u, val[id_neigh], pspecies->p_flux_ttc[i]->advface[icard][0]);

          } else if (tcl == DIRI_FACE_TTC && u < 0) // face active
          {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * cl_val;
            //                            LP_printf(fpout, "In func: %s, ele: %d, face %s, DIRI cl_val= %lf u: %e Advflux: %e \n",__func__ , i,
            //                                      TTC_param_card(icard), cl_val,  u, pspecies->p_flux_ttc[i]->advface[icard][0]);

          } else // face flux impose
          {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
          }
        }

        if (icard == BOTTOM_TTC) {
          if (u < 0) {

            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * val[i];

            //                            LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_cell = %lf,Advflux: %e \n",__func__ , i,
            //                                      TTC_param_card(icard),rhowcw, u, val[i], pspecies->p_flux_ttc[i]->advface[icard][0]);
          } else if (tcl == NO_BOUND_TTC && u > 0) // face active
          {

            id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];

            if (id_neigh != NONE_TTC) {
              id_neigh = id_neigh - 1;

              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * val[id_neigh];
            }

            //                            LP_printf(fpout, "In func: %s, ele: %d, face %s, id_neigh: %d, rhocw: %e , u: %e valneigh = %lf ,Advflux: %e \n",__func__ , i,
            //                                      TTC_param_card(icard), id_neigh,rhowcw, u, val[id_neigh], pspecies->p_flux_ttc[i]->advface[icard][0]);
          } else if (tcl == DIRI_FACE_TTC && u > 0) // face active
          {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = rhowcw * u * cl_val;

            //                            LP_printf(fpout, "In func: %s, ele: %d, face %s, DIRI cl_val= %lf u: %e cl_val: %lf Advflux: %e \n",__func__ , i,
            //                                      TTC_param_card(icard), cl_val,  u, pspecies->p_flux_ttc[i]->advface[icard][0]);

          } else // face flux impose
          {
            pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
          }
        } // fin bottom_ttc

      } else if (type == SOLUTE_TTC) {
        // u=*pspecies->plink->pu_ttc[i].uface[icard];
        u = pcarac_ttc->uface[i][icard][0];                      // SW 29/05/2018 pour confluence DK AR 24 08 2021 change location of uface plink to pcarac
        dlip = p_param_calc_ttc[i]->disp_face[icard][sub_icard]; // dispersion + conduction
        tcl = pspecies->p_boundary_ttc[i]->icl[icard][0];
        cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][0];

        if (icard == EAST_TTC) {
          if (u > 0) {
            /// WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
            qadv = (rhowcw * u * val[i]) * dist;
          } else if (tcl == NO_BOUND_TTC && u < 0) // face active
          {
            // id_neigh=pspecies->plink->p_neigh_ttc[i]->ivois[icard];
            id_neigh = plink->p_neigh_ttc[i]->ivois[icard][0]; // SW 29/05/2018 pour confluence
            // delta_L=pspecies->plink->p_neigh_ttc[i]->delta_L[icard]; // distance entre le centre de la maille et de son voisin
            delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // SW 29/05/2018 pour confluence
                                                                        /// WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
            if (id_neigh != NONE_TTC)                                   // SW 29/05/2018 pour confluence
              pspecies->p_flux_ttc[i]->advface[icard][0] = (rhowcw * u * val[id_neigh]) * dist;
          } else if (tcl == DIRI_FACE_TTC && u < 0) // face active
          {
            pspecies->p_flux_ttc[i]->advface[icard][0] = (rhowcw * u * cl_val) * dist * 2.;
          } else // face flux impose
          {
            pspecies->p_flux_ttc[i]->advface[icard][0] = 0;
          }
        }
        if (icard == WEST_TTC) {
          if (u < 0) {
            /// WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
            qadv = (rhowcw * u * val[i]) * dist;
          } else if (tcl == NO_BOUND_TTC && u > 0) // face active
          {
            // id_neigh=pspecies->plink->p_neigh_ttc[i]->ivois[icard];
            id_neigh = plink->p_neigh_ttc[i]->ivois[icard][0]; // SW 29/05/2018 pour confluence
            // delta_L=pspecies->plink->p_neigh_ttc[i]->delta_L[icard]; // distance entre le centre de la maille et de son voisin
            delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // SW 29/05/2018 pour confluence
            /// WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
            if (id_neigh != NONE_TTC) // SW 29/05/2018 pour confluence
              pspecies->p_flux_ttc[i]->advface[icard][0] = (rhowcw * u * val[id_neigh]);
          } else if (tcl == DIRI_FACE_TTC && u > 0) // face active
          {
            pspecies->p_flux_ttc[i]->advface[icard][0] = (rhowcw * u * cl_val);
          } else // face flux impose
          {
            pspecies->p_flux_ttc[i]->advface[icard][0] = 0;
          }
        }
        if (icard == NORTH_TTC) {
          if (u > 0) {
            /// WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
            pspecies->p_flux_ttc[i]->advface[icard][0] = (rhowcw * u * val[i]);
          } else if (tcl == NO_BOUND_TTC && u < 0) // face active
          {
            // id_neigh=pspecies->plink->p_neigh_ttc[i]->ivois[icard];
            id_neigh = plink->p_neigh_ttc[i]->ivois[icard][0]; // SW 29/05/2018 pour confluence
            // delta_L=pspecies->plink->p_neigh_ttc[i]->delta_L[icard]; // distance entre le centre de la maille et de son voisin
            delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // SW 29/05/2018 pour confluence
            /// WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
            if (id_neigh != NONE_TTC) // SW 29/05/2018 pour confluence
              pspecies->p_flux_ttc[i]->advface[icard][0] = (rhowcw * u * val[id_neigh]);
          } else if (tcl == DIRI_FACE_TTC && u < 0) // face active
          {
            pspecies->p_flux_ttc[i]->advface[icard][0] = (rhowcw * u * cl_val);
            sub_icard = SUB_CARD_TTC; // SW 29/05/2018 no confluence pour DIRI_FACE_TTC
          } else                      // face flux impose
          {
            pspecies->p_flux_ttc[i]->advface[icard][0] = 0;
          }
        }

        if (icard == SOUTH_TTC) {
          if (u < 0) {
            /// WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
            pspecies->p_flux_ttc[i]->advface[icard][0] = (rhowcw * u * val[i]);
          } else if (tcl == NO_BOUND_TTC && u > 0) // face active
          {
            // id_neigh=pspecies->plink->p_neigh_ttc[i]->ivois[icard];
            id_neigh = plink->p_neigh_ttc[i]->ivois[icard][0]; // SW 29/05/2018 pour confluence
            // delta_L=pspecies->plink->p_neigh_ttc[i]->delta_L[icard]; // distance entre le centre de la maille et de son voisin
            delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // SW 29/05/2018 pour confluence
            /// WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
            if (id_neigh != NONE_TTC) // SW 29/05/2018 pour confluence
              pspecies->p_flux_ttc[i]->advface[icard][0] = (rhowcw * u * val[id_neigh]);
          } else if (tcl == DIRI_FACE_TTC && u > 0) // face active
          {
            pspecies->p_flux_ttc[i]->advface[icard][0] = (rhowcw * u * cl_val);
          } else // face flux impose
          {
            pspecies->p_flux_ttc[i]->advface[icard][0] = 0;
          }
        } // fin south
      }

      break;

    default:
      for (sub_icard = 0; sub_icard < nsub; sub_icard++) { // SW 29/05/2018 pour confluence

        if (type == HEAT_TTC) {

          if (icard == DOWNSTREAM_TTC || icard == UPSTREAM_TTC) {
            u = pcarac_ttc->uface[i][icard][sub_icard] / wet_surface;
          } else if (icard == RIVAQ_TTC) {
            u = pcarac_ttc->uface[i][icard][sub_icard] / dist / width;
          }
          //                        LP_printf(fpout, "Inside %s, ele [%d], u = %e, wetsurface: %lf , nsub: %d\n",__func__, i, u, wet_surface, nsub);

          dlip = p_param_calc_ttc[i]->disp_face[icard][sub_icard]; // dispersion + conduction
          tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];
          cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

          if (icard == EAST_TTC) {
            if (u > 0) {
              pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (val[i] - T_DATUM);
              //                                LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_i = %lf, Advflux: %e \n",__func__ , i,
              //                                          TTC_param_card(icard), rhowcw, u, val[i], pspecies->p_flux_ttc[i]->advface[icard][0]);
            } else if (tcl == NO_BOUND_TTC && u < 0) // face active
            {
              id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];

              if (id_neigh != NONE_TTC) {
                id_neigh = id_neigh - 1;

                pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (val[id_neigh] - T_DATUM);
                //                                    LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_idneigh = %lf, Advflux: %e \n",__func__ , i,
                //                                              TTC_param_card(icard), rhowcw, u, val[id_neigh], pspecies->p_flux_ttc[i]->advface[icard][0]);
              }

            } else if (tcl == DIRI_FACE_TTC && u < 0) // face active
            {
              pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (cl_val - T_DATUM);
              //                                LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e clval = %lf, Advflux: %e \n",__func__ , i,
              //                                          TTC_param_card(icard), rhowcw, u, cl_val, pspecies->p_flux_ttc[i]->advface[icard][0]);
            } else // face flux impose
            {
              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
            }
          }
          if (icard == WEST_TTC) {
            if (u < 0) {
              pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (val[i] - T_DATUM);
              //                                LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_i = %lf, Advflux: %e \n",__func__ , i,
              //                                          TTC_param_card(icard), rhowcw, u, val[i], pspecies->p_flux_ttc[i]->advface[icard][0]);
            } else if (tcl == NO_BOUND_TTC && u > 0) // face active
            {
              id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard]; // Id voisin

              if (id_neigh != NONE_TTC) {
                id_neigh = id_neigh - 1;

                pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (val[id_neigh] - T_DATUM);
                //                                    LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e val_neigh = %lf, Advflux: %e \n",__func__ , i,
                //                                              TTC_param_card(icard), rhowcw, u, val[id_neigh], pspecies->p_flux_ttc[i]->advface[icard][0]);
              }

            } else if (tcl == DIRI_FACE_TTC && u > 0) // face active
            {
              pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * (cl_val - T_DATUM);
              //                                LP_printf(fpout, "In func: %s, ele: %d, face %s, rhocw: %e , u: %e clval = %lf, Advflux: %e \n",__func__ , i,
              //                                          TTC_param_card(icard), rhowcw, u, cl_val, pspecies->p_flux_ttc[i]->advface[icard][0]);
            } else // face flux impose
            {
              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
            }
          }
          if (icard == NORTH_TTC) {
            if (u > 0) {
              pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * val[i];
            } else if (tcl == NO_BOUND_TTC && u < 0) // face active
            {

              id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];

              if (id_neigh != NONE_TTC) {
                id_neigh = id_neigh - 1;
                pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * val[id_neigh];
              }
            } else if (tcl == DIRI_FACE_TTC && u < 0) // face active
            {
              pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * cl_val;
            } else // face flux impose
            {
              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
            }
          }

          if (icard == SOUTH_TTC) {
            if (u < 0) {
              pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * val[i];
            } else if (tcl == NO_BOUND_TTC && u > 0) // face active
            {

              id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];

              if (id_neigh != NONE_TTC) {
                id_neigh = id_neigh - 1;

                pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * val[id_neigh];
              }

            } else if (tcl == DIRI_FACE_TTC && u > 0) // face active
            {
              pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * cl_val;
            } else // face flux impose
            {
              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
            }
          } // fin south
          if (icard == TOP_TTC) {
            if (u > 0) {
              pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * val[i];
            } else if (tcl == NO_BOUND_TTC && u < 0) // face active
            {

              id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];

              if (id_neigh != NONE_TTC) {
                id_neigh = id_neigh - 1;
                pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * val[id_neigh];
              }

            } else if (tcl == DIRI_FACE_TTC && u < 0) // face active
            {
              pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * cl_val;
            } else // face flux impose
            {
              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
            }
          }

          if (icard == BOTTOM_TTC) {
            if (u < 0) {
              pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * val[i];
            } else if (tcl == NO_BOUND_TTC && u > 0) // face active
            {

              id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard];

              if (id_neigh != NONE_TTC) {
                id_neigh = id_neigh - 1;

                pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * u * val[id_neigh];
              }

            } else if (tcl == DIRI_FACE_TTC && u > 0) // face active
            {
              pspecies->p_flux_ttc[i]->advface[icard][0] += rhowcw * (u * cl_val);
            } else // face flux impose
            {
              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
            }
          } // fin south

        } else if (type == SOLUTE_TTC) {
          // u=*pspecies->plink->pu_ttc[i].uface[icard];
          u = pcarac_ttc->uface[i][icard][sub_icard];              // SW 29/05/2018 pour confluence DK AR 24 08 2021 change location of uface plink to pcarac
          dlip = p_param_calc_ttc[i]->disp_face[icard][sub_icard]; // dispersion + conduction
          tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];
          cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

          if (icard == EAST_TTC) {
            if (u > 0) {
              /// WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
              qadv = (rhowcw * u * val[i]) * dist;
            } else if (tcl == NO_BOUND_TTC && u < 0) // face active
            {
              // id_neigh=pspecies->plink->p_neigh_ttc[i]->ivois[icard];
              id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard]; // SW 29/05/2018 pour confluence
              // delta_L=pspecies->plink->p_neigh_ttc[i]->delta_L[icard]; // distance entre le centre de la maille et de son voisin
              delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // SW 29/05/2018 pour confluence
                                                                          /// WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
              if (id_neigh != NONE_TTC)                                   // SW 29/05/2018 pour confluence
                pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = (rhowcw * u * val[id_neigh]) * dist;
            } else if (tcl == DIRI_FACE_TTC && u < 0) // face active
            {
              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = (rhowcw * u * cl_val) * dist * 2.;
            } else // face flux impose
            {
              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
            }
          }
          if (icard == WEST_TTC) {
            if (u < 0) {
              /// WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
              qadv = (rhowcw * u * val[i]) * dist;
            } else if (tcl == NO_BOUND_TTC && u > 0) // face active
            {
              // id_neigh=pspecies->plink->p_neigh_ttc[i]->ivois[icard];
              id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard]; // SW 29/05/2018 pour confluence
              // delta_L=pspecies->plink->p_neigh_ttc[i]->delta_L[icard]; // distance entre le centre de la maille et de son voisin
              delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // SW 29/05/2018 pour confluence
              /// WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
              if (id_neigh != NONE_TTC) // SW 29/05/2018 pour confluence
                pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = (rhowcw * u * val[id_neigh]) * dist;
            } else if (tcl == DIRI_FACE_TTC && u > 0) // face active
            {
              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = (rhowcw * u * cl_val) * dist * 2.;
            } else // face flux impose
            {
              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
            }
          }
          if (icard == NORTH_TTC) {
            if (u > 0) {
              /// WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = (rhowcw * u * val[i]) * dist;
            } else if (tcl == NO_BOUND_TTC && u < 0) // face active
            {
              // id_neigh=pspecies->plink->p_neigh_ttc[i]->ivois[icard];
              id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard]; // SW 29/05/2018 pour confluence
              // delta_L=pspecies->plink->p_neigh_ttc[i]->delta_L[icard]; // distance entre le centre de la maille et de son voisin
              delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // SW 29/05/2018 pour confluence
              /// WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
              if (id_neigh != NONE_TTC) // SW 29/05/2018 pour confluence
                pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = (rhowcw * u * val[id_neigh]) * dist;
            } else if (tcl == DIRI_FACE_TTC && u < 0) // face active
            {
              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = (rhowcw * u * cl_val) * dist * 2.;
              sub_icard = SUB_CARD_TTC; // SW 29/05/2018 no confluence pour DIRI_FACE_TTC
            } else                      // face flux impose
            {
              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
            }
          }

          if (icard == SOUTH_TTC) {
            if (u < 0) {
              /// WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = (rhowcw * u * val[i]) * dist;
            } else if (tcl == NO_BOUND_TTC && u > 0) // face active
            {
              // id_neigh=pspecies->plink->p_neigh_ttc[i]->ivois[icard];
              id_neigh = plink->p_neigh_ttc[i]->ivois[icard][sub_icard]; // SW 29/05/2018 pour confluence
              // delta_L=pspecies->plink->p_neigh_ttc[i]->delta_L[icard]; // distance entre le centre de la maille et de son voisin
              delta_L = plink->p_neigh_ttc[i]->delta_L[icard][sub_icard]; // SW 29/05/2018 pour confluence
              /// WARNING A CHANGER LORSQUE AVEC LA TAILLE DES SUBFACE dist doit etre egale à la taille de la subface.
              if (id_neigh != NONE_TTC) // SW 29/05/2018 pour confluence
                pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = (rhowcw * u * val[id_neigh]) * dist;
            } else if (tcl == DIRI_FACE_TTC && u > 0) // face active
            {
              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = (rhowcw * u * cl_val) * dist;
              sub_icard = SUB_CARD_TTC; // SW 29/05/2018 no confluence pour DIRI_FACE_TTC
            } else                      // face flux impose
            {
              pspecies->p_flux_ttc[i]->advface[icard][sub_icard] = 0;
              sub_icard = SUB_CARD_TTC; // SW 29/05/2018 no confluence pour DIRI_FACE_TTC
            }
          } // fin south
        }

      } // fin sub_icard

      break;
    }

  } // fin icard
}

void TTC_cal_totflux(s_carac_ttc *pcarac_ttc, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, int i, FILE *fpout) {
  s_link_ttc *plink = pcarac_ttc->p_link[pspecies->id];
  double dist, delta_L, *val, u;
  int tcl;
  int type;
  int icard, id_neigh, sub_icard;
  double theta, dlip, cl_val, rhow, cw, rhowcw;
  double qadv, qcondu, qtot;
  int nsub;

  for (icard = 0; icard < NB_CARD_TTC; icard++) {
    nsub = plink->nsub[i][icard];

    switch (nsub) {
    case 0:
      sub_icard = 0;
      tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];

      if (tcl == NEU_FACE_TTC) // faceNEU_FACE_TTC) // flux impose a la face de la maille
      {
        cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
        pspecies->p_flux_ttc[i]->totface[icard][sub_icard] = cl_val;
        //                        LP_printf(fpout,"In func %s cell i %d face %s NEU face %lf\n", __func__ , i, TTC_param_card_river(icard), cl_val);

      } else {
        if (pspecies->pset->calc_process[DIFFUSION_TTC] == NO_TTC) {
          qcondu = pspecies->p_flux_ttc[i]->condface[icard][sub_icard];
          //                            LP_printf(fpout, "Inside %s, putting conductive fluxes \n",__func__);
        } else {
          pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = 0.;
          qcondu = 0.;
        }
        if (pspecies->pset->calc_process[CONVECTION_TTC] == NO_TTC) {
          qadv = pspecies->p_flux_ttc[i]->advface[icard][sub_icard];
          //                            LP_printf(fpout, "Inside %s, putting advective fluxes \n",__func__);
        } else {
          qadv = 0.;
        }

        qtot = qadv + qcondu;
        pspecies->p_flux_ttc[i]->totface[icard][sub_icard] = qtot;

        //                LP_printf(fpout, "Inside %s, ele [%d], face %s, qadv = %lf, qcondu = %lf, qtot = %lf \n",__func__, i,
        //                          TTC_param_card(icard), qadv, qcondu, qtot);
      }
      break;
    default:
      for (sub_icard = 0; sub_icard < nsub; sub_icard++) {

        //                    tcl=pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];

        tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];

        if (tcl == NEU_FACE_TTC) // faceNEU_FACE_TTC) // flux impose a la face de la maille
        {
          cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];

          //                        cl_val=pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
          pspecies->p_flux_ttc[i]->totface[icard][sub_icard] = cl_val;
        } else {
          if (pspecies->pset->calc_process[DIFFUSION_TTC] == NO_TTC) {
            qcondu = pspecies->p_flux_ttc[i]->condface[icard][sub_icard];
          } else {
            qcondu = 0.;
          }
          if (pspecies->pset->calc_process[CONVECTION_TTC] == NO_TTC) {
            qadv = pspecies->p_flux_ttc[i]->advface[icard][sub_icard];
          } else {
            qadv = 0.;
          }

          qtot = qadv + qcondu;
          pspecies->p_flux_ttc[i]->totface[icard][sub_icard] = qtot;

          //                LP_printf(fpout, "Inside %s, ele [%d], qadv = %lf, qcondu = %lf, qtot = %lf \n",__func__, i, qadv, qcondu, qtot);
        }
      }
      break;
    }
  }
}

void TTC_calc_transport_mb(s_carac_ttc *pcarac_ttc, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, int i, FILE *fpout) {
  s_link_ttc *plink = pcarac_ttc->p_link[pspecies->id];
  double dist, delta_L, thickness, *val, u;
  int tcl;
  int type;
  int icard, id_neigh, sub_icard;
  double lambda, cl_val, rhow, cw, rhowcw, rhoc;
  double qtot = 0;
  double qcell = 0;
  double qface = 0;
  double qcond_tot = 0;

  double area_vertical, area_horizontal;    // Area of the faces in transport
  double temp_previous_ts, temp_current_ts; // Previous and current transport variables in °C or mg/l
  double volume;                            // Volume of cell in m3;
  double tstep_sec = 86400;                 // Time step in seconds;
  double mb = 0;                            //
  double mb_temp = 0;
  double dTdt = 0;       // Change of temperature in each time step
  double adv_dVdt = 0;   // Flux due to transient groundwater storage change
  double adv_Sink = 0;   // Flux due to flow rate of the sink term
  double adv_Source = 0; // Flux due to flow rate of the source term
  double flux_neumann = 0;

  int nsub;

  lambda = p_param_calc_ttc[i]->lambda;
  rhoc = p_param_calc_ttc[i]->rhoc;

  temp_previous_ts = pspecies->pval_ttc->val_old[i];
  temp_current_ts = pspecies->pval_ttc->val[i];

  thickness = pspecies->p_param_ttc[i]->thickness;
  dist = pspecies->p_param_ttc[i]->param_syst[SIZE_TTC];

  area_vertical = dist * dist;
  area_horizontal = thickness * dist;
  volume = dist * dist * thickness;

  dTdt = (temp_current_ts - temp_previous_ts) / tstep_sec; // Change of temperature per time step
  pspecies->p_flux_ttc[i]->dTdt = dTdt;

  qcell = dTdt * volume * rhoc; // K/s * m3 * J / m3 / K = J / s = Watt
                                //    LP_printf(fpout, "Inside %s, ele [%d]: temp previous: %lf temp_current: %lf , dist %lf thickness %lf volume %e rhoc %e \n",__func__, i, temp_previous_ts, temp_current_ts, dist, thickness, volume, rhoc);

  // Obtain fluxes at the faces and add them up depending on the directions
  for (icard = 0; icard < NB_CARD_TTC; icard++) {
    nsub = plink->nsub[i][icard];
    qface = 0;
    switch (nsub) {
    case 0:
      sub_icard = 0;
      if (icard == TOP_TTC || icard == BOTTOM_TTC) {
        qface += pspecies->p_flux_ttc[i]->advface[icard][sub_icard] * area_vertical;
        qcond_tot += pspecies->p_flux_ttc[i]->condface[icard][sub_icard] * area_vertical;

        tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard]; // Check BC types at each face
        if (tcl == NEU_FACE_TTC)                                  // faceNEU_FACE_TTC) // flux impose a la face de la maille
        {
          flux_neumann += pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard] * area_vertical;
        }
      } else {
        qface += pspecies->p_flux_ttc[i]->advface[icard][sub_icard] * area_horizontal;
        qcond_tot += pspecies->p_flux_ttc[i]->condface[icard][sub_icard] * area_horizontal;

        tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard]; // Check BC types at each face
        if (tcl == NEU_FACE_TTC)                                  // faceNEU_FACE_TTC) // flux impose a la face de la maille
        {
          flux_neumann += pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard] * area_horizontal;
        }
      }
      //                LP_printf(fpout, "Inside %s, ele [%d], face %s, qadv = %lf, qcondu = %lf, qtot = %lf \n",__func__, i,
      //                          TTC_param_card(icard), qadv, qcondu, qtot);
      break;
    default:
      for (sub_icard = 0; sub_icard < 1; sub_icard++) {

        if (icard == TOP_TTC || icard == BOTTOM_TTC) {
          qface += pspecies->p_flux_ttc[i]->advface[icard][sub_icard] * area_vertical;
          qcond_tot += pspecies->p_flux_ttc[i]->condface[icard][sub_icard] * area_vertical;
          //                        LP_printf(fpout, "Inside A %s, ele [%d], face %s qface %e qtot = %e qcond_tot: %e\n",__func__, i, TTC_param_card(icard) , qface, nsub, area_vertical);

          tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard]; // Check BC types at each face
          if (tcl == NEU_FACE_TTC)                                  // faceNEU_FACE_TTC) // flux impose a la face de la maille
          {
            flux_neumann += pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard] * area_vertical;
          }
        } else {
          qface += pspecies->p_flux_ttc[i]->advface[icard][sub_icard] * area_horizontal;
          qcond_tot += pspecies->p_flux_ttc[i]->condface[icard][sub_icard] * area_horizontal;

          tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard]; // Check BC types at each face
          if (tcl == NEU_FACE_TTC)                                  // faceNEU_FACE_TTC) // flux impose a la face de la maille
          {
            flux_neumann += pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard] * area_horizontal;
          }
        }
      }
      break;
    }

    /*
     * If Velocity at East is positive, flow is outwards Reversal
     *                East is negative, flow is inwards
     *                West is positive, flow is inwards
     *                West is negative, flow is outwards
     *                North is positive flow is outwards Reversal
     *                North is negative, flow is inwards
     *                South is positive flow is inwards
     *                South is negative flow is outwards
     *                Top is positive flow is outwards   Reversal
     *                Top is negative flow is inwards
     *                Bottom is positive flow is inwards
     *                Bottom is negative flow is outwards
     */
    if (icard == EAST_TTC) {
      qtot -= qface;
    } else if (icard == WEST_TTC) {
      qtot += qface;
    } else if (icard == NORTH_TTC) {
      qtot -= qface;
    } else if (icard == SOUTH_TTC) {
      qtot += qface;
    } else if (icard == TOP_TTC) {
      qtot -= qface;
    } else if (icard == BOTTOM_TTC) {
      qtot += qface;
    }
    //            LP_printf(fpout, "Inside %s, ele [%d], face %s qface %e qtot = %e qcond_tot: %e\n",__func__, i, TTC_param_card(icard) , qface, qtot, qcond_tot);
  }

  // If the advection is activated, Advective terms impact on the heat transport
  if (pspecies->pset->calc_process[ADVECTION_TTC] == YES_TS) {

    adv_dVdt = pspecies->p_flux_ttc[i]->adv_dVdt * area_vertical;
    adv_Sink = pspecies->p_flux_ttc[i]->adv_Sink * area_vertical;
    adv_Source = pspecies->p_flux_ttc[i]->adv_Source * area_vertical;
  }

  qtot -= qcond_tot; // Add conduction to advecitve fluxes
  qtot -= flux_neumann;
  qcell = qcell + adv_dVdt - adv_Source - adv_Sink;
  mb = fabs(qtot) - fabs(qcell);
  mb_temp = mb * tstep_sec / rhoc / volume;

  pspecies->p_flux_ttc[i]->mb_transport = mb_temp / dTdt * 100; // Percent error
  //    LP_printf(fpout, "Inside %s, ele [%d]: volume %e rhoc %e tstep_sec %e dT %e qtot %e qcell %e dVdt %e befDvDT %e Sink %e Source %e F_NEU %e MB(W) %e MB(-) = %e\n",
  //              __func__, i, volume, rhoc, tstep_sec,
  //              dTdt, qtot, qcell, adv_dVdt, qtot- qcell + adv_Sink  ,adv_Sink ,adv_Source , flux_neumann, mb, pspecies->p_flux_ttc[i]->mb_transport);
}

// Fonction non écrite pour le SOLUTE
void TTC_cal_totflux_hyd(s_carac_ttc *pcarac_ttc, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, int i, FILE *fpout) {

  s_link_ttc *plink = pcarac_ttc->p_link[pspecies->id];
  double theta, dlip, cl_val, rhow, cw, rhowcw, qadv, qcondu, qtot, dist, delta_L, *val, u;
  int tcl, icard, id_neigh, sub_icard, nsub;
  int type = pspecies->pset->type;
  double surf = pspecies->p_param_ttc[i]->surf;
  double width = pspecies->p_param_ttc[i]->width;

  if (type == HEAT_TTC) {
    rhow = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][RHO_TTC];
    cw = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][HEAT_CAP_TTC];
    rhowcw = rhow * cw;
  } else if (type == SOLUTE_TTC) {
    rhowcw = 1.0;
  }

  for (icard = 0; icard < NB_CARD_TTC; icard++) { // Logiquement, ca devrait etre NB_CARD_RIV_TTC ici (?)

    nsub = plink->nsub[i][icard];

    switch (nsub) {
    case 0:
      sub_icard = 0;
      tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];

      if (tcl == NEU_FACE_TTC) // flux impose a la face de la maille
      {
        cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard];
        pspecies->p_flux_ttc[i]->totface[icard][sub_icard] = cl_val * width * rhowcw;

      } else {
        if (pspecies->pset->calc_process[DIFFUSION_TTC] == NO_TTC) {
          qcondu = pspecies->p_flux_ttc[i]->condface[icard][sub_icard];
        } else {
          pspecies->p_flux_ttc[i]->condface[icard][sub_icard] = 0.;
          qcondu = 0.;
        }
        if (pspecies->pset->calc_process[CONVECTION_TTC] == NO_TTC) {
          qadv = pspecies->p_flux_ttc[i]->advface[icard][sub_icard];
        } else {
          qadv = 0.;
        }

        qtot = qadv + qcondu;
        pspecies->p_flux_ttc[i]->totface[icard][sub_icard] = qtot;
      }
      break;
    default:
      for (sub_icard = 0; sub_icard < nsub; sub_icard++) {

        tcl = pspecies->p_boundary_ttc[i]->icl[icard][sub_icard];

        if (tcl == NEU_FACE_TTC) // flux impose a la face de la maille
        {
          cl_val = pspecies->p_boundary_ttc[i]->valcl[icard][sub_icard]; // K*m2/s

          if (icard == ATMOS_TTC) {
            pspecies->p_flux_ttc[i]->totface[icard][sub_icard] = cl_val * width / surf * rhowcw;
          } else if (icard == RIVAQ_TTC || icard == BANKS_TTC) {
            pspecies->p_flux_ttc[i]->totface[icard][sub_icard] = cl_val * rhowcw / width;
          }

        } else {
          if (pspecies->pset->calc_process[DIFFUSION_TTC] == NO_TTC) {
            qcondu = pspecies->p_flux_ttc[i]->condface[icard][sub_icard];
          } else
            qcondu = 0.;

          if (pspecies->pset->calc_process[CONVECTION_TTC] == NO_TTC) {
            qadv = pspecies->p_flux_ttc[i]->advface[icard][sub_icard];
          } else
            qadv = 0.;

          qtot = qadv + qcondu;
          pspecies->p_flux_ttc[i]->totface[icard][sub_icard] = qtot;
        }
      }
      break;
    }
  }
}
