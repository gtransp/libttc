/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: param_TTC.h
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#define CODE_TTC CODE_TS
#define STRING_LENGTH STRING_LENGTH_LP
#define VERSION_TTC 0.33
#define NONE_TTC -99

#define ITERMAX_DEFAULT_TTC 5000
#define CRCONV_DEFAULT_TTC 1e-6

enum equa_type_ttc { HEAT_TTC, SOLUTE_TTC, N_EQUA_TYPE_TTC };
enum equa_processes_ttc { DIFFUSION_TTC, CONVECTION_TTC, NPROCESSES_TTC };
#define ADVECTION_TTC CONVECTION_TTC
enum regime_ttc { STEADY_TTC, TRANSIENT_TTC, N_REGI_TTC };
enum avering_method { ARITHMETIC_TTC, HARMONIC_TTC, GEOMETRIC_TTC, QUADRATIC_TTC, NMEAN_TTC };
enum freq_ttc { CONSTANT_TTC, VARIABLE_TTC, N_FREQ_TTC };
// enum coeff_ttc {DISP_TTC,S_TTC,NCOEFF_TTC};
// enum coeff_ttc {DISP_TTC,S_TTC,SURF_T_TTC,SURF_TITER_TTC,NCOEFF_TTC}; // SW 08/06/2018 ajoutd'une surface //AR merci de preciser ce commentaire!!!!
enum count_ttc { NELE_TTC, NVOIS_ELE_TTC, NBC_TTC, NSPECIES_TTC, NAQUITARD_TTC, NRIVERBED_TTC, N_COUNT_TTC };
enum general_param_ttc { THETA_TTC, EPS_Q_TTC, NPAR_TTC };
enum output_type_ttc { VAR_TTC, FLUX_TTC, NOUT_TTC };
enum activate_ttc { NO_TTC, YES_TTC, NACTIV_TTC };

#define THET_TTC 1.0  // Coefficient de semi-implicite  // NG : 02/10/2021 : Switching from 0.8 to 1.0. Used to forced to full implicit scheme TTC transport calculations.
#define DT_TTC 86400. // discretisation temporelle
#define DX_TTC 1.     // discretisation spatiale

#define SPECIES_TTC 1     // nombre despeces
#define ELE_TTC 1000      // nombre delements
#define VOIS_ELE_TTC 2998 // nombre de voisins+elements ->indique la taille des vecteurs necessaires au solveur
#define BC_TTC 2002       // nombre face avec CL

#define LAMBDAW_TTC 0.57          // W/m/K
#define RHOW_TTC 1000.            // kg/m³
#define HEAT_CAPW_TTC 4186.       // J/kg/K attention unite ici c'est C de leau et non pas rhoC
#define HEAT_DIFFW_TTC 0.00000014 // thermal diffusivity of water W.m2/s

#define T_DATUM 273.15

#define AQUITARD_OFF_TTC 0. // Aquitard calculations are closed DK AR 17 12 2021
#define AQUITARD_ON_TTC 1.  // Aquitard calculations are activated

#define VAR_INIT 0. // K initialisation de la temperature
