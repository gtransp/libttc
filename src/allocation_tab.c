/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: allocation_tab.c
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "CHR.h"
#include "GC.h"
#include "IO.h"

#include "TTC.h"

// manage_ttc

// création tableaux

char **TTC_create_mat_char(int sizex, int sizey) {
  char **mat_char;
  int i;

  mat_char = malloc(sizex * sizeof(char *));
  bzero((char *)mat_char, sizex * sizeof(char *));
  for (i = 0; i < sizex; i++) {
    mat_char[i] = malloc(sizey * sizeof(char));
    bzero((char *)mat_char[i], sizey * sizeof(char));
  }
  return mat_char;
}

double *TTC_create_tab_double(int size) {
  double *tab_double;
  tab_double = (double *)malloc(size * sizeof(double));
  bzero((char *)tab_double, size * sizeof(double));
  return tab_double;
}

int *TTC_create_tab_int(int size) {
  int *tab_int;
  tab_int = (int *)malloc(size * sizeof(int));
  bzero((char *)tab_int, size * sizeof(int));
  return tab_int;
}

char *TTC_create_tab_char(int size) {
  char *tab_char;
  tab_char = (char *)malloc(size * sizeof(char));
  bzero((char *)tab_char, size * sizeof(char));
  return tab_char;
}

double **TTC_create_mat_double(int sizex, int sizey) {
  double **mat_double;
  int i;
  mat_double = malloc(sizex * sizeof(double *));
  bzero((char *)mat_double, sizex * sizeof(double *));
  for (i = 0; i < sizex; i++) {
    mat_double[i] = malloc(sizey * sizeof(double));
    bzero((char *)mat_double[i], sizey * sizeof(double));
  }
  return mat_double;
}

int **TTC_create_mat_int(int sizex, int sizey) {
  int **mat_int;
  int i;
  mat_int = malloc(sizex * sizeof(int *));
  bzero((char *)mat_int, sizex * sizeof(int *));
  for (i = 0; i < sizex; i++) {
    mat_int[i] = malloc(sizey * sizeof(int));
    bzero((char *)mat_int[i], sizey * sizeof(int));
  }
  return mat_int;
}
