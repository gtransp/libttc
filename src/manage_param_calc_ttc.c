/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: manage_param_calc_ttc.c
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "GC.h"
#include "CHR.h"
#include "IO.h"

#include "TTC.h"

s_param_calc_ttc *TTC_create_param_calc() {
  s_param_calc_ttc *pparam_calc_ttc;
  pparam_calc_ttc = new_param_calc_ttc();
  bzero((char *)pparam_calc_ttc, sizeof(s_param_calc_ttc));
  return pparam_calc_ttc;
}

s_param_calc_ttc **TTC_init_param_calc(int nele) {
  s_param_calc_ttc **p_param_calc_ttc;

  p_param_calc_ttc = ((s_param_calc_ttc **)malloc(nele * sizeof(s_param_calc_ttc *))); // TTC_create_param_calc();
  for (int i = 0; i < nele; i++) {
    p_param_calc_ttc[i] = new_param_calc_ttc();
  }
  //  pparam_calc_ttc->pdisp_ttc=TTC_init_disp(nele);
  //  pparam_calc_ttc->coeff[DISP_TTC]=TTC_create_tab_double(nele);
  //  pparam_calc_ttc->coeff[S_TTC]=TTC_create_tab_double(nele);
  //  pparam_calc_ttc->coeff[SURF_T_TTC]=TTC_create_tab_double(nele); // SW 08/06/2018
  //  pparam_calc_ttc->coeff[SURF_TITER_TTC]=TTC_create_tab_double(nele); // SW 08/06/2018
  return p_param_calc_ttc;
}

// Function to calculate the coefficient of diffusion
//  DK AR 24 08 2021 Correction in the calls to the parameters stored in pparam ttc & param_thermic
void Calc_dispcond(s_carac_ttc *pcarac_ttc, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, int i, FILE *fpout) {
  double beta, rhow, cw, lambdas, lambdaw, omega, diff;
  double lambda, dlip, mean_u;
  int type, j, k, nb_sub;

  double **u = pcarac_ttc->uface[i];
  int *nsub = pcarac_ttc->p_link[pspecies->id]->nsub[i];

  type = pspecies->pset->type;
  beta = pspecies->p_param_ttc[i]->param_syst[DISPERSIVITY_TTC];
  omega = pspecies->p_param_ttc[i]->param_syst[POROSITY_TTC];

  if (type == HEAT_TTC) {
    lambdas = pspecies->p_param_ttc[i]->param_thermic[SOLID_TTC][LAMBDA_TTC];
    lambdaw = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][LAMBDA_TTC];
    rhow = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][RHO_TTC];
    cw = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][HEAT_CAP_TTC];
    lambda = geometric_mean(lambdaw, lambdas, omega);

    // CCC....TERME DISPERSIF
    //  TODO the velocity term should be corrected for the dispersivity term
    /* for (j=0;j<NB_CARD_TTC;j++)
    {
        nb_sub = pspecies->plink->nsub[i][j];
        for (int k = 0; k <= nb_sub; ++k) {
            u[j]=pspecies->plink->pu_ttc[i].uface[j][k]; // TODO there is trouble with the table of U, we need to change it with something appropriate
        }

        //for (k=0;k<NB_SUBCARD_TTC;k++)
        //printf("%d %lf\n",i,*u[j]);
    } */

    // mean_u=pow((*u[0]+*u[1]),2)/4+pow((*u[2]+*u[3]),2)/4; has to adapted for the subcards !!!!
    // printf("%d %lf %lf %lf\n",i,lambda,rhow[i],cw[i]);

    // Calculate the diffusion coefficient term
    p_param_calc_ttc[i]->lambda = lambda;
    dlip = lambda / (rhow * cw);

    //      printf( "Dlip, lambda, lambdaw, lambdas ,cw, rhow \n");
    //      printf("%e, %e, %e, %e, %e, %e \n",dlip, lambda, lambdaw, lambdas, cw, rhow);
    // Fill the p_param_calc_ttc with the thermal diffusion therm
    for (j = 0; j < NB_CARD_TTC; j++) {
      if (nsub[j] != 0) {
        for (k = 0; k < nsub[j]; k++) {
          p_param_calc_ttc[i]->disp_face[j][k] = dlip;
          //                    LP_printf(fpout,"Calc Dlip inside %s, ele %d nsub is %d, card %d sub card %d - Value %e\n",__func__ ,i ,nsub[j],j,k,p_param_calc_ttc[i]->disp_face[j][k]);
        }
      } else {
        p_param_calc_ttc[i]->disp_face[j][0] = dlip;
        //                LP_printf(fpout,"Calc Dlip inside %s, ele %d nsub is %d, card %d - Value %e\n",__func__ ,i ,nsub[j],j,p_param_calc_ttc[i]->disp_face[j][0]);
      }
    }
  } else if (type == SOLUTE_TTC) {
    // CCC....TERME DISPERSIF
    mean_u = 0.;

    // TODO the velocity term should be corrected for the dispersivity term
    for (j = 0; j < NB_CARD_TTC; j++) {
      // LP_printf(fpout,"Valeur de nsub pour ele %d card %d : %d\n",i,j,pcarac_ttc->p_link[pspecies->id]->nsub[i][j]);
      // nb_sub = pspecies->plink->nsub[i][j];
      // for (int k = 0; k <= nb_sub; ++k) {
      //                  u[j]=pcarac_ttc->uface[i][j][1]; // DK AR 24 08 2021 comment out velocityTODO there is trouble with the table of U, we need to change it with something appropriate
      //}

      // for (k=0;k<NB_SUBCARD_TTC;k++)
      // printf("%d %lf\n",i,*u[j]);
    }

    diff = pspecies->p_param_ttc[i]->diff_mol_solute;

    // NG : ********** Formula needs to be adapted for subfaces **************
    //    mean_u=pow((*u[0]+ *u[1]),2)/4+pow((*u[2]+*u[3]),2)/4; // Ne pas oublier de modifier le débit en vitesse pour tenir compte de l'ajout de la surface dans les coeff de diffusion

    dlip = beta * pow(mean_u, 0.5) + omega * diff;
    // LP_printf(fpout,"dlip : %f\n",dlip);
    for (j = 0; j < NB_CARD_TTC; j++) {
      if (nsub[j] != 0) {
        for (k = 0; k < nsub[j]; k++) {
          p_param_calc_ttc[i]->disp_face[j][k] = dlip;
          //                    LP_printf(fpout,"Calc Dlip inside %s, ele %d nsub is %d, card %d sub card %d - Value %e\n",__func__ ,i ,nsub[j],j,k,p_param_calc_ttc[i]->disp_face[j][k]);
        }
      } else {
        p_param_calc_ttc[i]->disp_face[j][0] = dlip;
        //                LP_printf(fpout,"Calc Dlip inside %s, ele %d nsub is %d, card %d - Value %e\n",__func__ ,i ,nsub[j],k,p_param_calc_ttc[i]->disp_face[j][0]);
      }
    }
  }
};

// Function to calculate the coefficient of diffusion
//  DK AR 24 08 2021 Correction in the calls to the parameters stored in pparam ttc & param_thermic
void Calc_dispcond_face(s_carac_ttc *pcarac_ttc, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, int i, FILE *fpout) {
  double rhow, cw, lambdas, lambdaw, diff;
  double lambda, dlip, mean_u;
  double lambda_neigh;        // Lambda_of the neighbouring cell at the face
  double lambda_aquitard = 0; // Lambda of the aquitard if exists
  double lambda_face;         // Lambda applied the face
  int j, k, nb_sub;
  int id_neigh;
  double **u = pcarac_ttc->uface[i];
  int *nsub = pcarac_ttc->p_link[pspecies->id]->nsub[i];
  // Aquitard lookup table to find if there is a neighbouring aquitard
  int iaq = 0;
  int naquitard = 0;
  int it_aquitard = i;

  double dist = pspecies->p_param_ttc[i]->param_syst[SIZE_TTC];
  double thickness = pspecies->p_param_ttc[i]->thickness;

  double area_vertical = dist * dist;
  double area_horizontal = dist * thickness;

  int type = pspecies->pset->type;
  double beta = pspecies->p_param_ttc[i]->param_syst[DISPERSIVITY_TTC];
  double omega = pspecies->p_param_ttc[i]->param_syst[POROSITY_TTC];

  if (type == HEAT_TTC) {

    rhow = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][RHO_TTC];
    cw = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][HEAT_CAP_TTC];

    for (j = 0; j < NB_CARD_TTC; j++) {
      /*            if ( pspecies->pset->aquitard == AQUITARD_ON_TTC ){
                      naquitard = pcarac_ttc->count[NAQUITARD_TTC];
                      if ( j == TOP_TTC || j == BOTTOM_TTC  ) {
      //                    LP_printf(fpout, "Aquitard is active at face %s  \n", TTC_param_card(j));
                          while ( iaq < naquitard) {
                              if ( it_aquitard == pspecies->p_aquitard_ttc[iaq]->id-1 ) {
                                  lambda_aquitard = pspecies->p_aquitard_ttc[iaq]->lambda[VAR_AQUITARD_TTC];
      //                            LP_printf(fpout, "The aquitard is active at the face %s, iaq: %d in this cell %d , %d, %d, %d, %lf thickness: %lf \n",
      //                                      TTC_param_card(j),
      //                                      it_aquitard, iaq,
      //                                      pspecies->p_aquitard_ttc[iaq]->id,
      //                                      pspecies->p_aquitard_ttc[iaq]->nlayer,
      //                                      pspecies->p_aquitard_ttc[iaq]->id_neigh,
      //                                      pspecies->p_aquitard_ttc[iaq]->lambda[VAR_AQUITARD_TTC]);
                              }
                              if ( it_aquitard ==  pspecies->p_aquitard_ttc[iaq]->id_neigh-1   ) {
                                  lambda_aquitard = pspecies->p_aquitard_ttc[iaq]->lambda[VAR_AQUITARD_TTC];

      //                            LP_printf(fpout, "The aquitard is active at the face %s, iaq: %d in this cell %d , %d, %d, %d, %lf thickness: %lf \n",
      //                                      TTC_param_card(j),
      //                                      it_aquitard, iaq,
      //                                      pspecies->p_aquitard_ttc[iaq]->id,
      //                                      pspecies->p_aquitard_ttc[iaq]->nlayer,
      //                                      pspecies->p_aquitard_ttc[iaq]->id_neigh,
      //                                      pspecies->p_aquitard_ttc[iaq]->lambda[VAR_AQUITARD_TTC]);

                              }
                              iaq++;
                          }
                      }
                  }*/

      lambda_aquitard = 1.42;

      if (nsub[j] != 0) {
        for (k = 0; k < nsub[j]; k++) {

          if ((j == TOP_TTC || j == BOTTOM_TTC) && pspecies->pset->aquitard == AQUITARD_ON_TTC) {

            id_neigh = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->ivois[j][k];

            lambda = p_param_calc_ttc[i]->lambda;

            if (id_neigh != NONE_TTC) {
              id_neigh = id_neigh - 1;
              lambda_neigh = p_param_calc_ttc[id_neigh]->lambda;
            } else {
              lambda_neigh = lambda;
            }

            if (lambda_aquitard != 0.) {
              lambda_face = arithmetic_mean(lambda, lambda_aquitard, 0.5);
            } else {
              lambda_face = arithmetic_mean(lambda, lambda_neigh, 0.5);
            }

            p_param_calc_ttc[i]->disp_face[j][k] = lambda_face / (rhow * cw);
          } else {
            id_neigh = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->ivois[j][k];

            lambda = p_param_calc_ttc[i]->lambda;

            if (id_neigh != NONE_TTC) {
              id_neigh = id_neigh - 1;
              lambda_neigh = p_param_calc_ttc[id_neigh]->lambda;
            } else {
              lambda_neigh = lambda;
            }

            lambda_face = arithmetic_mean(lambda, lambda_neigh, 0.5);

            p_param_calc_ttc[i]->disp_face[j][k] = beta * u[j][k] + lambda_face / (rhow * cw);
          }

          //                    LP_printf(fpout,"Calc Dlip inside %s, ele %d nsub is %d, card %d sub card %d - Value %e\n",__func__ ,i ,nsub[j],j,k,p_param_calc_ttc[i]->disp_face[j][k]);
        }
      } else {
        if ((j == TOP_TTC || j == BOTTOM_TTC) && pspecies->pset->aquitard == AQUITARD_ON_TTC) {
          id_neigh = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->ivois[j][0];

          lambda = p_param_calc_ttc[i]->lambda;
          if (id_neigh != NONE_TTC) {
            id_neigh = id_neigh - 1;
            lambda_neigh = p_param_calc_ttc[id_neigh]->lambda;
          } else {
            lambda_neigh = lambda;
          }
          if (lambda_aquitard != 0) {
            lambda_face = arithmetic_mean(lambda, lambda_aquitard, 0.5);
          } else {
            lambda_face = arithmetic_mean(lambda, lambda_neigh, 0.5);
          }

          p_param_calc_ttc[i]->disp_face[j][0] = lambda_face / (rhow * cw);
        } else {
          id_neigh = pcarac_ttc->p_link[pspecies->id]->p_neigh_ttc[i]->ivois[j][0];

          lambda = p_param_calc_ttc[i]->lambda;
          if (id_neigh != NONE_TTC) {
            id_neigh = id_neigh - 1;
            lambda_neigh = p_param_calc_ttc[id_neigh]->lambda;
          } else {
            lambda_neigh = lambda;
          }
          lambda_face = arithmetic_mean(lambda, lambda_neigh, 0.5);
          p_param_calc_ttc[i]->disp_face[j][0] = beta * u[j][0] + lambda_face / (rhow * cw);
        }
        //               LP_printf(fpout,"Inside %s, ele %d idneigh is %d, card %s lambda_neigh= %lf lambda cell = %lf, lambda_face = %lf Value %e\n",
        //                         __func__ ,i ,id_neigh,
        //                         TTC_param_card(j), lambda_neigh, lambda, lambda_face, p_param_calc_ttc[i]->disp_face[j][0]);
      }
    }
  } else if (type == SOLUTE_TTC) {
    // CCC....TERME DISPERSIF
    mean_u = 0.;

    // TODO the velocity term should be corrected for the dispersivity term
    for (j = 0; j < NB_CARD_TTC; j++) {
      // LP_printf(fpout,"Valeur de nsub pour ele %d card %d : %d\n",i,j,pcarac_ttc->p_link[pspecies->id]->nsub[i][j]);
      // nb_sub = pspecies->plink->nsub[i][j];
      // for (int k = 0; k <= nb_sub; ++k) {
      //                  u[j]=pcarac_ttc->uface[i][j][1]; // DK AR 24 08 2021 comment out velocityTODO there is trouble with the table of U, we need to change it with something appropriate
      //}

      // for (k=0;k<NB_SUBCARD_TTC;k++)
      // printf("%d %lf\n",i,*u[j]);
    }

    diff = pspecies->p_param_ttc[i]->diff_mol_solute;

    // NG : ********** Formula needs to be adapted for subfaces **************
    //    mean_u=pow((*u[0]+ *u[1]),2)/4+pow((*u[2]+*u[3]),2)/4; // Ne pas oublier de modifier le débit en vitesse pour tenir compte de l'ajout de la surface dans les coeff de diffusion

    dlip = beta * pow(mean_u, 0.5) + omega * diff;
    // LP_printf(fpout,"dlip : %f\n",dlip);
    for (j = 0; j < NB_CARD_TTC; j++) {
      if (nsub[j] != 0) {
        for (k = 0; k < nsub[j]; k++) {
          p_param_calc_ttc[i]->disp_face[j][k] = dlip;
          //                    LP_printf(fpout,"Calc Dlip inside %s, ele %d nsub is %d, card %d sub card %d - Value %e\n",__func__ ,i ,nsub[j],j,k,p_param_calc_ttc[i]->disp_face[j][k]);
        }
      } else {
        p_param_calc_ttc[i]->disp_face[j][0] = dlip;
        //                LP_printf(fpout,"Calc Dlip inside %s, ele %d nsub is %d, card %d - Value %e\n",__func__ ,i ,nsub[j],k,p_param_calc_ttc[i]->disp_face[j][0]);
      }
    }
  }
};

/////////////////////////////////////////////////////////////////////////////////////////

// Function to calculate the temporal coefficient
void Calc_s(s_carac_ttc *pcarac_ttc, s_param_calc_ttc **p_param_calc_ttc, s_species_ttc *pspecies, int i, FILE *fpout) {
  double rhow, cw, rhos, cs, omega;
  double rhocw, rhocs, rhoc;
  int type;

  omega = pspecies->p_param_ttc[i]->param_syst[POROSITY_TTC];

  type = pspecies->pset->type;

  if (type == HEAT_TTC) {
    rhow = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][RHO_TTC];
    cw = pspecies->p_param_ttc[i]->param_thermic[WATER_TTC][HEAT_CAP_TTC];
    rhos = pspecies->p_param_ttc[i]->param_thermic[SOLID_TTC][RHO_TTC];
    cs = pspecies->p_param_ttc[i]->param_thermic[SOLID_TTC][HEAT_CAP_TTC];
    rhocw = rhow * cw;
    rhocs = rhos * cs;
    rhoc = geometric_mean(rhocw, rhocs, omega); // Arithmetic mean of heat capacity of the porous medium (rhocw*omega+(1-omega)*rhocs)
    p_param_calc_ttc[i]->rhoc = rhoc;
    p_param_calc_ttc[i]->coeff[S_TTC] = rhoc / rhocw;
    //	printf("Temporal coefficient rhoc = %lf,heat cap water: rhocw = %lf,  s =  %lf \n",rhoc,rhocw,p_param_calc_ttc[i]->coeff[S_TTC]);
  } else if (type == SOLUTE_TTC) {
    p_param_calc_ttc[i]->coeff[S_TTC] = omega;
  }
};
