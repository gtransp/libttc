/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: param_TTC_shared.h
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

enum thermic_param_ttc { LAMBDA_TTC, HEAT_CAP_TTC, RHO_TTC, NPARAM_THERM_TTC };
enum environment_ttc { WATER_TTC, SOLID_TTC, POROUS_TTC, NENV_TTC };
enum param_syst { SIZE_TTC, DISPERSIVITY_TTC, POROSITY_TTC, NPARAM_SYST_TTC };
enum kindof_bound_ttc { HOMOGENOUS_TTC, HETEROGENOUS_TTC };
// enum name_bound_ttc {NO_BOUND_TTC,DIRI_CENTER_TTC,NEU_CENTER_TTC,DIRI_FACE_TTC,NEU_FACE_TTC,N_BCNAME_TTC};
//  DK added Geothermal, Uptake, Injection boundary types
enum name_bound_ttc { NO_BOUND_TTC, DIRI_CENTER_TTC, NEU_CENTER_TTC, DIRI_FACE_TTC, NEU_FACE_TTC, CONF_DIFF_TTC, BARRAGE_TTC, GEOTHERMAL_TTC, UPTAKE_TTC, INJECTION_TTC, STREAM_ATMOS_EX_TTC, STREAM_ADV_FLOW_TTC, STREAM_COND_FLOW_TTC, STREAM_AQ_ADV_TTC, STREAM_AQ_COND_TTC, N_BCNAME_TTC }; // SW 24/10/2018 add CONF_DIFF_TTC,BARRAGE_TTC
enum param_card_ttc { EAST_TTC, WEST_TTC, NORTH_TTC, SOUTH_TTC, BOTTOM_TTC, TOP_TTC, NB_CARD_TTC };                                                                                                                                                                                             // DK Increase the allocation of the param card to upper and lower
enum param_card_river_ttc { DOWNSTREAM_TTC, UPSTREAM_TTC, BANKS_TTC, ATMOS_TTC, RIVAQ_TTC, NB_CARD_RIV_TTC };                                                                                                                                                                                   // DK Increase the allocation of the param card to upper and lower

// #define SUB_CARD_TTC 2 // SW 28/05/2018 pour confluence
enum param_sub_ttc { ONE_TTC, TWO_TTC, THREE_TTC, FOUR_TTC, SUB_CARD_TTC }; // allocation nbsubface DK Increase the allocation of the subface for additional nsubface
// enum coeff_ttc {DISP_TTC,S_TTC,NCOEFF_TTC};
enum coeff_ttc { DISP_TTC, S_TTC, SURF_T_TTC, SURF_TITER_TTC, NCOEFF_TTC }; // SW 08/06/2018 ajoutd'une surface //AR merci de preciser ce commentaire!!!!//NF 27/7/2021 moved from param_TTC.h

enum aquitard_param_ttc { LAMBDA_AQ_TTC, THICKNESS_AQUITARD, NPARAM_AQUITARD_TTC }; // Variables of the aquitard layer at the bottom of mesh DK 25 08 2021
enum aquitard_var_ttc { VAR_ABOVE_TTC, VAR_AQUITARD_TTC, VAR_BELOW_TTC, NVAR_AQUITARD_TTC };
enum kindof_source_ttc { SOURCE_GEOTHERMAL_TTC, SOURCE_TEMPERATURE_TTC, SOURCE_HEAT_TTC, SOURCE_CONCENTRATION_TTC, SOURCE_INJECTION_TTC, NB_SOURCE_TTC };
enum type_analytical_sol_ttc { AQUITARD_TTC, RIVERBED_TTC, NSOLUTION_AQUITARD_TTC };
