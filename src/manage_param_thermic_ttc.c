/*-------------------------------------------------------------------------------
 *
 * LIBRARY NAME: libttc
 * FILE NAME: manage_param_thermic_ttc.c
 *
 * CONTRIBUTORS: Agnès RIVIERE, Deniz KILIC, Shuaitao WANG, Nicolas GALLOIS,
 *               Nicolas FLIPO
 *
 * LIBRARY BRIEF DESCRIPTION: Conservative transport (solute, heat) for
 * porous media (horizontal 2D) or free surface flow (longitudinal 1D).
 *
 * Library developed at the Geosciences Center, joint research center
 * of MINES Paris and ARMINES, PSL University, Fontainebleau, France.
 *
 * COPYRIGHT: (c) 2022 Contributors to the libttc Library.
 * CONTACT: Nicolas FLIPO <nicolas.flipo@minesparis.psl.eu>
 *          Nicolas GALLOIS <nicolas.gallois@minesparis.psl.eu>
 *
 * All rights reserved. This Library and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v20.html
 *
 *------------------------------------------------------------------------------*/

#include <stdlib.h>
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <time.h>
#include <math.h>
#include "libprint.h"
#include "time_series.h"
#include "CHR.h"
#include "GC.h"
#include "TTC.h"
// DK AR 23 08 2021 new structure to allocate should be inserted here
// s_param_thermic_ttc *TTC_create_param_thermic()
//{
//  s_param_thermic_ttc *pthermic_ttc;
//  pthermic_ttc=new_param_thermic_ttc();
//  bzero((char *)pthermic_ttc,sizeof(s_param_thermic_ttc));
//  return pthermic_ttc;
//}

// s_param_thermic_ttc *TTC_init_param_thermic(int nele)
//{
//   s_param_thermic_ttc *pthermic_ttc;
//
//   pthermic_ttc=TTC_create_param_thermic();
//   pthermic_ttc->param[LAMBDA_TTC]=TTC_create_tab_double(nele);
//   pthermic_ttc->param[RHO_TTC]=TTC_create_tab_double(nele);
//   pthermic_ttc->param[HEAT_CAP_TTC]=TTC_create_tab_double(nele);
//   return pthermic_ttc;
// }
